package com.citi.trading.strategy;

import static com.citi.trading.pricing.Pricing.SECONDS_PER_PERIOD;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.citi.trading.OrderPlacer;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;

/**
 * Implementation of a compromised price breakout trading strategy. We check if new high
 * price and low price are within previous open and close price If yes and
 * repeat for X times: flag enterTrend as true Then if breakout the trend:
 * Case1: new close price higher than new open: buy 
 * Case2: new close price lower than new open: purchase.
 *
 * @author Winston Liu
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PriceBreakoutTrader extends Trader<PriceBreakout> {

	private static final Logger LOGGER = Logger.getLogger(PriceBreakoutTrader.class.getName());

	private double upperBound; // upperbound of previous open & close
	private double lowerBound; // lowerbound of previous open & close
	
	private boolean buy = false; //indicating whether buy or sell
	

	public PriceBreakoutTrader(PricingSource pricing, OrderPlacer market, StrategyPersistence strategyPersistence) {
		super(pricing, market, strategyPersistence);
	}

	protected int periods(int msec) {
		return msec / 1000 / SECONDS_PER_PERIOD;
	}

	public int getNumberOfPeriodsToWatch() {
		return periods(strategy.getTimePeriod());
	}

	// Helper method to check whether entered breakout pattern
	private boolean reachTrend(PriceData data) {
		
		int timePeriod = periods(strategy.getTimePeriod());
		
		System.out.println(timePeriod);
		
		List<Double> openList = data.getData(timePeriod).
				mapToDouble(PricePoint::getOpen).boxed().collect(Collectors.toList());
		
		List<Double> closeList = data.getData(timePeriod).
				mapToDouble(PricePoint::getClose).boxed().collect(Collectors.toList());
		
		List<Double> highList = data.getData(timePeriod).
				mapToDouble(PricePoint::getHigh).boxed().collect(Collectors.toList());
		
		List<Double> lowList = data.getData(timePeriod).
				mapToDouble(PricePoint::getLow).boxed().collect(Collectors.toList());
		
		for (int i = 1; i < strategy.getRepeat(); i++) {
			Double newHigh = highList.get(i-1);
			Double newLow = lowList.get(i-1);
			Double oldOpen = openList.get(i);
			Double oldClose = closeList.get(i);
			
			if (i == 1 && newHigh - newLow > Math.abs(oldOpen - oldClose)) {
				
				System.out.println("Strategy ID " + strategy.getId() + " fail to reach breakout");
				return false;
			}
			
			if (i >1 && newHigh - newLow <= Math.abs(oldOpen - oldClose)) {
				
				System.out.println("Strategy ID " + strategy.getId() + " not formed repetion pattern yet");
				
				return false;
			}
			
		}
		
		System.out.println("Strategy ID " + strategy.getId() + "successfully reached breakout");
		
		// current close price
		double currentPrice = data.getData(1).findAny().get().getClose();

		// current open price
		double openPrice = data.getData(1).findAny().get().getClose();

		if (currentPrice > openPrice) {
			buy = true;
		} else {
			buy = false;
		}
				
		return true;

	}

	// function to judge whether a breakout pattern is identified
	protected void handleDataWhenOpen(PriceData data) {

		double currentPrice = data.getData(1).findAny().get().getClose();

		System.out.println("PriceBreakoutTrader handleDataWhenOpen strategy ID is " + strategy.getId()
				+ ", current price is " + currentPrice);
		double openingPrice = getStrategy().getOpenPosition().getOpeningTrade().getPrice();
		double profitOrLoss = currentPrice / openingPrice - 1.0;

		System.out.println("PriceBreakoutTrader handleDataWhenOpen strategy ID is " + strategy.getId()
				+ ", oepning price is " + openingPrice);

		if (Math.abs(profitOrLoss) > strategy.getExitThreshold()) {
			closer.placeOrder(currentPrice);
			if (!getStrategy().getOpenPosition().getOpeningTrade().isBuy()) {
				profitOrLoss = 0 - profitOrLoss;
			}
			LOGGER.info(
					String.format("Trader " + strategy.getId() + " closing position on a profit/loss of %5.3f percent.",
							profitOrLoss * 100));
		}

	}

	protected void handleDataWhenClosed(PriceData data) {
		if (tracking.get()) {

			if (reachTrend(data)) {

				// current close price
				double currentPrice = data.getData(1).findAny().get().getClose();

				System.out.println("PriceBreakoutTrader handleDataWhenClosed strategy ID is " + strategy.getId()
						+ ", current price is " + currentPrice);
				
				opener.placeOrder(buy, currentPrice); 

			} else {
				System.out.println("Strategy ID " + strategy.getId() + " not entered breakout yet");

			}
		} else if (data.getSize() >= getNumberOfPeriodsToWatch()) {

			tracking.set(true);
			LOGGER.info("Trader " + strategy.getId() + " got initial pricing data and baseline averages.");
		}

	}

}
