package com.citi.trading.strategy;

import static com.citi.trading.pricing.Pricing.SECONDS_PER_PERIOD;

import java.util.logging.Logger;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.citi.trading.OrderPlacer;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;


/**
 * Implementation of the bollinger band trading strategy.
 * We check the multiple of standard deviation from moving average.
 * If hit on low side: open a long position.
 * If hit on high side: open a short positon.
 *
 * @author Winston Liu
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BollingerBandTrader extends Trader<BollingerBand>{
	
	
	private static final Logger LOGGER =
			 Logger.getLogger(BollingerBandTrader.class.getName ());
	 
	private double upperBound;
	private double lowerBound;
	
	public BollingerBandTrader(PricingSource pricing, OrderPlacer market, StrategyPersistence strategyPersistence) {
		super(pricing, market, strategyPersistence);
	}
	
	
	protected int periods(int msec) {
		return msec / 1000 / SECONDS_PER_PERIOD;
	}
	    
	public int getNumberOfPeriodsToWatch() {
		return periods(strategy.getTimePeriod());
	}
	
	// Helper method to update pivot values
	private void checkBounds(PriceData data) {
		
		System.out.println("BollingerBandTrader strategy ID is " + strategy.getId() + " time period is " + periods(strategy.getTimePeriod()));
		System.out.println("BollingerBandTrader strategy ID is " + strategy.getId() + " multiple is " + strategy.getMultiple());
		System.out.println("BollingerBandTrader strategy ID is " + strategy.getId() + " exit is " + strategy.getExitThreshold());
		
		double average = data.getWindowAverage(periods(strategy.getTimePeriod()), PricePoint::getClose);
		double deviation = data.getWindowDeviation(periods(strategy.getTimePeriod()), PricePoint::getClose);
	
		
		upperBound = average + deviation*strategy.getMultiple();
		lowerBound = average - deviation*strategy.getMultiple();
		
		LOGGER.info(String.format("Trader " + strategy.getId() + " as of %s Bounds are %1.4f %1.4f",
    			data.getLatestTimestamp(), upperBound, lowerBound));
		
	}

	protected void handleDataWhenOpen(PriceData data) {
		    checkBounds(data); //Do we need this line?
    		double currentPrice = data.getData(1).findAny().get().getClose();
    		
    		System.out.println("BollingerBandTrader handleDataWhenOpen strategy ID is " + strategy.getId() + ", current price is " + currentPrice);
    		double openingPrice = getStrategy().getOpenPosition().getOpeningTrade().getPrice();
    		double profitOrLoss = currentPrice / openingPrice - 1.0;
    		
    		System.out.println("BollingerBandTrader handleDataWhenOpen strategy ID is " + strategy.getId() + ", oepning price is " + openingPrice);

    		if (Math.abs(profitOrLoss) > strategy.getExitThreshold()) {
    			closer.placeOrder(currentPrice);
    			if (!getStrategy().getOpenPosition().getOpeningTrade().isBuy()) {
    			profitOrLoss = 0 - profitOrLoss;
    			}
    		LOGGER.info(String.format("Trader " + strategy.getId() + " closing position on a profit/loss of %5.3f percent.", 
    				profitOrLoss * 100));
    		}
		
	}

	protected void handleDataWhenClosed(PriceData data) {
		if (tracking.get()) {
    			double currentPrice = data.getData(1).findAny().get().getClose();
    			checkBounds(data);
    			
        		System.out.println("BollingerBandTrader handleDataWhenClosed strategy ID is " + strategy.getId() + ", current price is " + currentPrice);

    		
    			if (currentPrice < lowerBound) {
    				//buy
    				opener.placeOrder(true, currentPrice);
    				LOGGER.info("Trader " + strategy.getId() + " opening long position as currentPrice lower than lower bound " + lowerBound);
    			} else if (currentPrice > upperBound) {
    				//sell 
    				opener.placeOrder(false, currentPrice);
    				LOGGER.info("Trader " + strategy.getId() + " opening short position as currentPrice higher than upper bound " + upperBound);
    			}
    		} else if (data.getSize() >= getNumberOfPeriodsToWatch()) {
    			checkBounds(data);
    			tracking.set(true);
    			LOGGER.info("Trader " + strategy.getId() + " got initial pricing data and baseline averages.");
	    }
		
	}		    
	 
}
