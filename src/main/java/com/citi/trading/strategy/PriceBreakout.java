package com.citi.trading.strategy;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Represents a compromised price-breakout trading strategy, adding the parameters
 * that are specific to that algorithm.
 * 
 * @author Winston Liu
 */
@Entity
@DiscriminatorValue("C")
public class PriceBreakout extends Strategy implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int timePeriod;
	private int repeat;
	private double exitThreshold;

	public PriceBreakout() {
	}
	
	public PriceBreakout(String stock, int size, int timePeriod, int repeat, double exitThreshold) {
		super(stock, size);
		this.timePeriod = timePeriod;
		this.repeat = repeat;
		this.exitThreshold = exitThreshold;
	}

	public double getExitThreshold() {
		return this.exitThreshold;
	}

	public void setExitThreshold(double exitThreshold) {
		this.exitThreshold = exitThreshold;
	}

	@Override
	public String toString() {
		return String.format("BollingerBand: [short=%d, long=%d, exit=%1.4f, %s]",
				timePeriod, repeat, exitThreshold, stringRepresentation());
	}

	public int getTimePeriod() {
		return timePeriod;
	}

	public void setTimePeriod(int timePeriod) {
		this.timePeriod = timePeriod;
	}

	public int getRepeat() {
		return repeat;
	}

	public void setRepeat(int repeat) {
		this.repeat = repeat;
	}
	
	

}
