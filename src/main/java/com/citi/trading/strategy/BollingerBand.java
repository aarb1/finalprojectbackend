package com.citi.trading.strategy;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Represents a bollinger-band trading strategy, adding the parameters
 * that are specific to that algorithm.
 * 
 * @author Winston Liu
 */
@Entity
@DiscriminatorValue("B")
public class BollingerBand extends Strategy implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int timePeriod;
	private int multiple;
	private double exitThreshold;

	public BollingerBand() {
	}
	
	public BollingerBand(String stock, int size, int timePeriod, int multiple, double exitThreshold) {
		super(stock, size);
		this.timePeriod = timePeriod;
		this.multiple = multiple;
		this.exitThreshold = exitThreshold;
	}

	public double getExitThreshold() {
		return this.exitThreshold;
	}

	public void setExitThreshold(double exitThreshold) {
		this.exitThreshold = exitThreshold;
	}

	@Override
	public String toString() {
		return String.format("BollingerBand: [short=%d, long=%d, exit=%1.4f, %s]",
				timePeriod, multiple, exitThreshold, stringRepresentation());
	}

	public int getTimePeriod() {
		return timePeriod;
	}

	public void setTimePeriod(int timePeriod) {
		this.timePeriod = timePeriod;
	}

	public int getMultiple() {
		return multiple;
	}

	public void setMultiple(int multiple) {
		this.multiple = multiple;
	}
	

}
