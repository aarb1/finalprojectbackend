/*! jQuery v3.4.1 | (c) JS Foundation and other contributors | jquery.org/license */
!function(e,t){"use strict";"object"==typeof module&&"object"==typeof module.exports?module.exports=e.document?t(e,!0):function(e){if(!e.document)throw new Error("jQuery requires a window with a document");return t(e)}:t(e)}("undefined"!=typeof window?window:this,function(C,e){"use strict";var t=[],E=C.document,r=Object.getPrototypeOf,s=t.slice,g=t.concat,u=t.push,i=t.indexOf,n={},o=n.toString,v=n.hasOwnProperty,a=v.toString,l=a.call(Object),y={},m=function(e){return"function"==typeof e&&"number"!=typeof e.nodeType},x=function(e){return null!=e&&e===e.window},c={type:!0,src:!0,nonce:!0,noModule:!0};function b(e,t,n){var r,i,o=(n=n||E).createElement("script");if(o.text=e,t)for(r in c)(i=t[r]||t.getAttribute&&t.getAttribute(r))&&o.setAttribute(r,i);n.head.appendChild(o).parentNode.removeChild(o)}function w(e){return null==e?e+"":"object"==typeof e||"function"==typeof e?n[o.call(e)]||"object":typeof e}var f="3.4.1",k=function(e,t){return new k.fn.init(e,t)},p=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;function d(e){var t=!!e&&"length"in e&&e.length,n=w(e);return!m(e)&&!x(e)&&("array"===n||0===t||"number"==typeof t&&0<t&&t-1 in e)}k.fn=k.prototype={jquery:f,constructor:k,length:0,toArray:function(){return s.call(this)},get:function(e){return null==e?s.call(this):e<0?this[e+this.length]:this[e]},pushStack:function(e){var t=k.merge(this.constructor(),e);return t.prevObject=this,t},each:function(e){return k.each(this,e)},map:function(n){return this.pushStack(k.map(this,function(e,t){return n.call(e,t,e)}))},slice:function(){return this.pushStack(s.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(e){var t=this.length,n=+e+(e<0?t:0);return this.pushStack(0<=n&&n<t?[this[n]]:[])},end:function(){return this.prevObject||this.constructor()},push:u,sort:t.sort,splice:t.splice},k.extend=k.fn.extend=function(){var e,t,n,r,i,o,a=arguments[0]||{},s=1,u=arguments.length,l=!1;for("boolean"==typeof a&&(l=a,a=arguments[s]||{},s++),"object"==typeof a||m(a)||(a={}),s===u&&(a=this,s--);s<u;s++)if(null!=(e=arguments[s]))for(t in e)r=e[t],"__proto__"!==t&&a!==r&&(l&&r&&(k.isPlainObject(r)||(i=Array.isArray(r)))?(n=a[t],o=i&&!Array.isArray(n)?[]:i||k.isPlainObject(n)?n:{},i=!1,a[t]=k.extend(l,o,r)):void 0!==r&&(a[t]=r));return a},k.extend({expando:"jQuery"+(f+Math.random()).replace(/\D/g,""),isReady:!0,error:function(e){throw new Error(e)},noop:function(){},isPlainObject:function(e){var t,n;return!(!e||"[object Object]"!==o.call(e))&&(!(t=r(e))||"function"==typeof(n=v.call(t,"constructor")&&t.constructor)&&a.call(n)===l)},isEmptyObject:function(e){var t;for(t in e)return!1;return!0},globalEval:function(e,t){b(e,{nonce:t&&t.nonce})},each:function(e,t){var n,r=0;if(d(e)){for(n=e.length;r<n;r++)if(!1===t.call(e[r],r,e[r]))break}else for(r in e)if(!1===t.call(e[r],r,e[r]))break;return e},trim:function(e){return null==e?"":(e+"").replace(p,"")},makeArray:function(e,t){var n=t||[];return null!=e&&(d(Object(e))?k.merge(n,"string"==typeof e?[e]:e):u.call(n,e)),n},inArray:function(e,t,n){return null==t?-1:i.call(t,e,n)},merge:function(e,t){for(var n=+t.length,r=0,i=e.length;r<n;r++)e[i++]=t[r];return e.length=i,e},grep:function(e,t,n){for(var r=[],i=0,o=e.length,a=!n;i<o;i++)!t(e[i],i)!==a&&r.push(e[i]);return r},map:function(e,t,n){var r,i,o=0,a=[];if(d(e))for(r=e.length;o<r;o++)null!=(i=t(e[o],o,n))&&a.push(i);else for(o in e)null!=(i=t(e[o],o,n))&&a.push(i);return g.apply([],a)},guid:1,support:y}),"function"==typeof Symbol&&(k.fn[Symbol.iterator]=t[Symbol.iterator]),k.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(e,t){n["[object "+t+"]"]=t.toLowerCase()});var h=function(n){var e,d,b,o,i,h,f,g,w,u,l,T,C,a,E,v,s,c,y,k="sizzle"+1*new Date,m=n.document,S=0,r=0,p=ue(),x=ue(),N=ue(),A=ue(),D=function(e,t){return e===t&&(l=!0),0},j={}.hasOwnProperty,t=[],q=t.pop,L=t.push,H=t.push,O=t.slice,P=function(e,t){for(var n=0,r=e.length;n<r;n++)if(e[n]===t)return n;return-1},R="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",M="[\\x20\\t\\r\\n\\f]",I="(?:\\\\.|[\\w-]|[^\0-\\xa0])+",W="\\["+M+"*("+I+")(?:"+M+"*([*^$|!~]?=)"+M+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+I+"))|)"+M+"*\\]",$=":("+I+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+W+")*)|.*)\\)|)",F=new RegExp(M+"+","g"),B=new RegExp("^"+M+"+|((?:^|[^\\\\])(?:\\\\.)*)"+M+"+$","g"),_=new RegExp("^"+M+"*,"+M+"*"),z=new RegExp("^"+M+"*([>+~]|"+M+")"+M+"*"),U=new RegExp(M+"|>"),X=new RegExp($),V=new RegExp("^"+I+"$"),G={ID:new RegExp("^#("+I+")"),CLASS:new RegExp("^\\.("+I+")"),TAG:new RegExp("^("+I+"|[*])"),ATTR:new RegExp("^"+W),PSEUDO:new RegExp("^"+$),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+M+"*(even|odd|(([+-]|)(\\d*)n|)"+M+"*(?:([+-]|)"+M+"*(\\d+)|))"+M+"*\\)|)","i"),bool:new RegExp("^(?:"+R+")$","i"),needsContext:new RegExp("^"+M+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+M+"*((?:-\\d)?\\d*)"+M+"*\\)|)(?=[^-]|$)","i")},Y=/HTML$/i,Q=/^(?:input|select|textarea|button)$/i,J=/^h\d$/i,K=/^[^{]+\{\s*\[native \w/,Z=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,ee=/[+~]/,te=new RegExp("\\\\([\\da-f]{1,6}"+M+"?|("+M+")|.)","ig"),ne=function(e,t,n){var r="0x"+t-65536;return r!=r||n?t:r<0?String.fromCharCode(r+65536):String.fromCharCode(r>>10|55296,1023&r|56320)},re=/([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,ie=function(e,t){return t?"\0"===e?"\ufffd":e.slice(0,-1)+"\\"+e.charCodeAt(e.length-1).toString(16)+" ":"\\"+e},oe=function(){T()},ae=be(function(e){return!0===e.disabled&&"fieldset"===e.nodeName.toLowerCase()},{dir:"parentNode",next:"legend"});try{H.apply(t=O.call(m.childNodes),m.childNodes),t[m.childNodes.length].nodeType}catch(e){H={apply:t.length?function(e,t){L.apply(e,O.call(t))}:function(e,t){var n=e.length,r=0;while(e[n++]=t[r++]);e.length=n-1}}}function se(t,e,n,r){var i,o,a,s,u,l,c,f=e&&e.ownerDocument,p=e?e.nodeType:9;if(n=n||[],"string"!=typeof t||!t||1!==p&&9!==p&&11!==p)return n;if(!r&&((e?e.ownerDocument||e:m)!==C&&T(e),e=e||C,E)){if(11!==p&&(u=Z.exec(t)))if(i=u[1]){if(9===p){if(!(a=e.getElementById(i)))return n;if(a.id===i)return n.push(a),n}else if(f&&(a=f.getElementById(i))&&y(e,a)&&a.id===i)return n.push(a),n}else{if(u[2])return H.apply(n,e.getElementsByTagName(t)),n;if((i=u[3])&&d.getElementsByClassName&&e.getElementsByClassName)return H.apply(n,e.getElementsByClassName(i)),n}if(d.qsa&&!A[t+" "]&&(!v||!v.test(t))&&(1!==p||"object"!==e.nodeName.toLowerCase())){if(c=t,f=e,1===p&&U.test(t)){(s=e.getAttribute("id"))?s=s.replace(re,ie):e.setAttribute("id",s=k),o=(l=h(t)).length;while(o--)l[o]="#"+s+" "+xe(l[o]);c=l.join(","),f=ee.test(t)&&ye(e.parentNode)||e}try{return H.apply(n,f.querySelectorAll(c)),n}catch(e){A(t,!0)}finally{s===k&&e.removeAttribute("id")}}}return g(t.replace(B,"$1"),e,n,r)}function ue(){var r=[];return function e(t,n){return r.push(t+" ")>b.cacheLength&&delete e[r.shift()],e[t+" "]=n}}function le(e){return e[k]=!0,e}function ce(e){var t=C.createElement("fieldset");try{return!!e(t)}catch(e){return!1}finally{t.parentNode&&t.parentNode.removeChild(t),t=null}}function fe(e,t){var n=e.split("|"),r=n.length;while(r--)b.attrHandle[n[r]]=t}function pe(e,t){var n=t&&e,r=n&&1===e.nodeType&&1===t.nodeType&&e.sourceIndex-t.sourceIndex;if(r)return r;if(n)while(n=n.nextSibling)if(n===t)return-1;return e?1:-1}function de(t){return function(e){return"input"===e.nodeName.toLowerCase()&&e.type===t}}function he(n){return function(e){var t=e.nodeName.toLowerCase();return("input"===t||"button"===t)&&e.type===n}}function ge(t){return function(e){return"form"in e?e.parentNode&&!1===e.disabled?"label"in e?"label"in e.parentNode?e.parentNode.disabled===t:e.disabled===t:e.isDisabled===t||e.isDisabled!==!t&&ae(e)===t:e.disabled===t:"label"in e&&e.disabled===t}}function ve(a){return le(function(o){return o=+o,le(function(e,t){var n,r=a([],e.length,o),i=r.length;while(i--)e[n=r[i]]&&(e[n]=!(t[n]=e[n]))})})}function ye(e){return e&&"undefined"!=typeof e.getElementsByTagName&&e}for(e in d=se.support={},i=se.isXML=function(e){var t=e.namespaceURI,n=(e.ownerDocument||e).documentElement;return!Y.test(t||n&&n.nodeName||"HTML")},T=se.setDocument=function(e){var t,n,r=e?e.ownerDocument||e:m;return r!==C&&9===r.nodeType&&r.documentElement&&(a=(C=r).documentElement,E=!i(C),m!==C&&(n=C.defaultView)&&n.top!==n&&(n.addEventListener?n.addEventListener("unload",oe,!1):n.attachEvent&&n.attachEvent("onunload",oe)),d.attributes=ce(function(e){return e.className="i",!e.getAttribute("className")}),d.getElementsByTagName=ce(function(e){return e.appendChild(C.createComment("")),!e.getElementsByTagName("*").length}),d.getElementsByClassName=K.test(C.getElementsByClassName),d.getById=ce(function(e){return a.appendChild(e).id=k,!C.getElementsByName||!C.getElementsByName(k).length}),d.getById?(b.filter.ID=function(e){var t=e.replace(te,ne);return function(e){return e.getAttribute("id")===t}},b.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&E){var n=t.getElementById(e);return n?[n]:[]}}):(b.filter.ID=function(e){var n=e.replace(te,ne);return function(e){var t="undefined"!=typeof e.getAttributeNode&&e.getAttributeNode("id");return t&&t.value===n}},b.find.ID=function(e,t){if("undefined"!=typeof t.getElementById&&E){var n,r,i,o=t.getElementById(e);if(o){if((n=o.getAttributeNode("id"))&&n.value===e)return[o];i=t.getElementsByName(e),r=0;while(o=i[r++])if((n=o.getAttributeNode("id"))&&n.value===e)return[o]}return[]}}),b.find.TAG=d.getElementsByTagName?function(e,t){return"undefined"!=typeof t.getElementsByTagName?t.getElementsByTagName(e):d.qsa?t.querySelectorAll(e):void 0}:function(e,t){var n,r=[],i=0,o=t.getElementsByTagName(e);if("*"===e){while(n=o[i++])1===n.nodeType&&r.push(n);return r}return o},b.find.CLASS=d.getElementsByClassName&&function(e,t){if("undefined"!=typeof t.getElementsByClassName&&E)return t.getElementsByClassName(e)},s=[],v=[],(d.qsa=K.test(C.querySelectorAll))&&(ce(function(e){a.appendChild(e).innerHTML="<a id='"+k+"'></a><select id='"+k+"-\r\\' msallowcapture=''><option selected=''></option></select>",e.querySelectorAll("[msallowcapture^='']").length&&v.push("[*^$]="+M+"*(?:''|\"\")"),e.querySelectorAll("[selected]").length||v.push("\\["+M+"*(?:value|"+R+")"),e.querySelectorAll("[id~="+k+"-]").length||v.push("~="),e.querySelectorAll(":checked").length||v.push(":checked"),e.querySelectorAll("a#"+k+"+*").length||v.push(".#.+[+~]")}),ce(function(e){e.innerHTML="<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";var t=C.createElement("input");t.setAttribute("type","hidden"),e.appendChild(t).setAttribute("name","D"),e.querySelectorAll("[name=d]").length&&v.push("name"+M+"*[*^$|!~]?="),2!==e.querySelectorAll(":enabled").length&&v.push(":enabled",":disabled"),a.appendChild(e).disabled=!0,2!==e.querySelectorAll(":disabled").length&&v.push(":enabled",":disabled"),e.querySelectorAll("*,:x"),v.push(",.*:")})),(d.matchesSelector=K.test(c=a.matches||a.webkitMatchesSelector||a.mozMatchesSelector||a.oMatchesSelector||a.msMatchesSelector))&&ce(function(e){d.disconnectedMatch=c.call(e,"*"),c.call(e,"[s!='']:x"),s.push("!=",$)}),v=v.length&&new RegExp(v.join("|")),s=s.length&&new RegExp(s.join("|")),t=K.test(a.compareDocumentPosition),y=t||K.test(a.contains)?function(e,t){var n=9===e.nodeType?e.documentElement:e,r=t&&t.parentNode;return e===r||!(!r||1!==r.nodeType||!(n.contains?n.contains(r):e.compareDocumentPosition&&16&e.compareDocumentPosition(r)))}:function(e,t){if(t)while(t=t.parentNode)if(t===e)return!0;return!1},D=t?function(e,t){if(e===t)return l=!0,0;var n=!e.compareDocumentPosition-!t.compareDocumentPosition;return n||(1&(n=(e.ownerDocument||e)===(t.ownerDocument||t)?e.compareDocumentPosition(t):1)||!d.sortDetached&&t.compareDocumentPosition(e)===n?e===C||e.ownerDocument===m&&y(m,e)?-1:t===C||t.ownerDocument===m&&y(m,t)?1:u?P(u,e)-P(u,t):0:4&n?-1:1)}:function(e,t){if(e===t)return l=!0,0;var n,r=0,i=e.parentNode,o=t.parentNode,a=[e],s=[t];if(!i||!o)return e===C?-1:t===C?1:i?-1:o?1:u?P(u,e)-P(u,t):0;if(i===o)return pe(e,t);n=e;while(n=n.parentNode)a.unshift(n);n=t;while(n=n.parentNode)s.unshift(n);while(a[r]===s[r])r++;return r?pe(a[r],s[r]):a[r]===m?-1:s[r]===m?1:0}),C},se.matches=function(e,t){return se(e,null,null,t)},se.matchesSelector=function(e,t){if((e.ownerDocument||e)!==C&&T(e),d.matchesSelector&&E&&!A[t+" "]&&(!s||!s.test(t))&&(!v||!v.test(t)))try{var n=c.call(e,t);if(n||d.disconnectedMatch||e.document&&11!==e.document.nodeType)return n}catch(e){A(t,!0)}return 0<se(t,C,null,[e]).length},se.contains=function(e,t){return(e.ownerDocument||e)!==C&&T(e),y(e,t)},se.attr=function(e,t){(e.ownerDocument||e)!==C&&T(e);var n=b.attrHandle[t.toLowerCase()],r=n&&j.call(b.attrHandle,t.toLowerCase())?n(e,t,!E):void 0;return void 0!==r?r:d.attributes||!E?e.getAttribute(t):(r=e.getAttributeNode(t))&&r.specified?r.value:null},se.escape=function(e){return(e+"").replace(re,ie)},se.error=function(e){throw new Error("Syntax error, unrecognized expression: "+e)},se.uniqueSort=function(e){var t,n=[],r=0,i=0;if(l=!d.detectDuplicates,u=!d.sortStable&&e.slice(0),e.sort(D),l){while(t=e[i++])t===e[i]&&(r=n.push(i));while(r--)e.splice(n[r],1)}return u=null,e},o=se.getText=function(e){var t,n="",r=0,i=e.nodeType;if(i){if(1===i||9===i||11===i){if("string"==typeof e.textContent)return e.textContent;for(e=e.firstChild;e;e=e.nextSibling)n+=o(e)}else if(3===i||4===i)return e.nodeValue}else while(t=e[r++])n+=o(t);return n},(b=se.selectors={cacheLength:50,createPseudo:le,match:G,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(e){return e[1]=e[1].replace(te,ne),e[3]=(e[3]||e[4]||e[5]||"").replace(te,ne),"~="===e[2]&&(e[3]=" "+e[3]+" "),e.slice(0,4)},CHILD:function(e){return e[1]=e[1].toLowerCase(),"nth"===e[1].slice(0,3)?(e[3]||se.error(e[0]),e[4]=+(e[4]?e[5]+(e[6]||1):2*("even"===e[3]||"odd"===e[3])),e[5]=+(e[7]+e[8]||"odd"===e[3])):e[3]&&se.error(e[0]),e},PSEUDO:function(e){var t,n=!e[6]&&e[2];return G.CHILD.test(e[0])?null:(e[3]?e[2]=e[4]||e[5]||"":n&&X.test(n)&&(t=h(n,!0))&&(t=n.indexOf(")",n.length-t)-n.length)&&(e[0]=e[0].slice(0,t),e[2]=n.slice(0,t)),e.slice(0,3))}},filter:{TAG:function(e){var t=e.replace(te,ne).toLowerCase();return"*"===e?function(){return!0}:function(e){return e.nodeName&&e.nodeName.toLowerCase()===t}},CLASS:function(e){var t=p[e+" "];return t||(t=new RegExp("(^|"+M+")"+e+"("+M+"|$)"))&&p(e,function(e){return t.test("string"==typeof e.className&&e.className||"undefined"!=typeof e.getAttribute&&e.getAttribute("class")||"")})},ATTR:function(n,r,i){return function(e){var t=se.attr(e,n);return null==t?"!="===r:!r||(t+="","="===r?t===i:"!="===r?t!==i:"^="===r?i&&0===t.indexOf(i):"*="===r?i&&-1<t.indexOf(i):"$="===r?i&&t.slice(-i.length)===i:"~="===r?-1<(" "+t.replace(F," ")+" ").indexOf(i):"|="===r&&(t===i||t.slice(0,i.length+1)===i+"-"))}},CHILD:function(h,e,t,g,v){var y="nth"!==h.slice(0,3),m="last"!==h.slice(-4),x="of-type"===e;return 1===g&&0===v?function(e){return!!e.parentNode}:function(e,t,n){var r,i,o,a,s,u,l=y!==m?"nextSibling":"previousSibling",c=e.parentNode,f=x&&e.nodeName.toLowerCase(),p=!n&&!x,d=!1;if(c){if(y){while(l){a=e;while(a=a[l])if(x?a.nodeName.toLowerCase()===f:1===a.nodeType)return!1;u=l="only"===h&&!u&&"nextSibling"}return!0}if(u=[m?c.firstChild:c.lastChild],m&&p){d=(s=(r=(i=(o=(a=c)[k]||(a[k]={}))[a.uniqueID]||(o[a.uniqueID]={}))[h]||[])[0]===S&&r[1])&&r[2],a=s&&c.childNodes[s];while(a=++s&&a&&a[l]||(d=s=0)||u.pop())if(1===a.nodeType&&++d&&a===e){i[h]=[S,s,d];break}}else if(p&&(d=s=(r=(i=(o=(a=e)[k]||(a[k]={}))[a.uniqueID]||(o[a.uniqueID]={}))[h]||[])[0]===S&&r[1]),!1===d)while(a=++s&&a&&a[l]||(d=s=0)||u.pop())if((x?a.nodeName.toLowerCase()===f:1===a.nodeType)&&++d&&(p&&((i=(o=a[k]||(a[k]={}))[a.uniqueID]||(o[a.uniqueID]={}))[h]=[S,d]),a===e))break;return(d-=v)===g||d%g==0&&0<=d/g}}},PSEUDO:function(e,o){var t,a=b.pseudos[e]||b.setFilters[e.toLowerCase()]||se.error("unsupported pseudo: "+e);return a[k]?a(o):1<a.length?(t=[e,e,"",o],b.setFilters.hasOwnProperty(e.toLowerCase())?le(function(e,t){var n,r=a(e,o),i=r.length;while(i--)e[n=P(e,r[i])]=!(t[n]=r[i])}):function(e){return a(e,0,t)}):a}},pseudos:{not:le(function(e){var r=[],i=[],s=f(e.replace(B,"$1"));return s[k]?le(function(e,t,n,r){var i,o=s(e,null,r,[]),a=e.length;while(a--)(i=o[a])&&(e[a]=!(t[a]=i))}):function(e,t,n){return r[0]=e,s(r,null,n,i),r[0]=null,!i.pop()}}),has:le(function(t){return function(e){return 0<se(t,e).length}}),contains:le(function(t){return t=t.replace(te,ne),function(e){return-1<(e.textContent||o(e)).indexOf(t)}}),lang:le(function(n){return V.test(n||"")||se.error("unsupported lang: "+n),n=n.replace(te,ne).toLowerCase(),function(e){var t;do{if(t=E?e.lang:e.getAttribute("xml:lang")||e.getAttribute("lang"))return(t=t.toLowerCase())===n||0===t.indexOf(n+"-")}while((e=e.parentNode)&&1===e.nodeType);return!1}}),target:function(e){var t=n.location&&n.location.hash;return t&&t.slice(1)===e.id},root:function(e){return e===a},focus:function(e){return e===C.activeElement&&(!C.hasFocus||C.hasFocus())&&!!(e.type||e.href||~e.tabIndex)},enabled:ge(!1),disabled:ge(!0),checked:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&!!e.checked||"option"===t&&!!e.selected},selected:function(e){return e.parentNode&&e.parentNode.selectedIndex,!0===e.selected},empty:function(e){for(e=e.firstChild;e;e=e.nextSibling)if(e.nodeType<6)return!1;return!0},parent:function(e){return!b.pseudos.empty(e)},header:function(e){return J.test(e.nodeName)},input:function(e){return Q.test(e.nodeName)},button:function(e){var t=e.nodeName.toLowerCase();return"input"===t&&"button"===e.type||"button"===t},text:function(e){var t;return"input"===e.nodeName.toLowerCase()&&"text"===e.type&&(null==(t=e.getAttribute("type"))||"text"===t.toLowerCase())},first:ve(function(){return[0]}),last:ve(function(e,t){return[t-1]}),eq:ve(function(e,t,n){return[n<0?n+t:n]}),even:ve(function(e,t){for(var n=0;n<t;n+=2)e.push(n);return e}),odd:ve(function(e,t){for(var n=1;n<t;n+=2)e.push(n);return e}),lt:ve(function(e,t,n){for(var r=n<0?n+t:t<n?t:n;0<=--r;)e.push(r);return e}),gt:ve(function(e,t,n){for(var r=n<0?n+t:n;++r<t;)e.push(r);return e})}}).pseudos.nth=b.pseudos.eq,{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})b.pseudos[e]=de(e);for(e in{submit:!0,reset:!0})b.pseudos[e]=he(e);function me(){}function xe(e){for(var t=0,n=e.length,r="";t<n;t++)r+=e[t].value;return r}function be(s,e,t){var u=e.dir,l=e.next,c=l||u,f=t&&"parentNode"===c,p=r++;return e.first?function(e,t,n){while(e=e[u])if(1===e.nodeType||f)return s(e,t,n);return!1}:function(e,t,n){var r,i,o,a=[S,p];if(n){while(e=e[u])if((1===e.nodeType||f)&&s(e,t,n))return!0}else while(e=e[u])if(1===e.nodeType||f)if(i=(o=e[k]||(e[k]={}))[e.uniqueID]||(o[e.uniqueID]={}),l&&l===e.nodeName.toLowerCase())e=e[u]||e;else{if((r=i[c])&&r[0]===S&&r[1]===p)return a[2]=r[2];if((i[c]=a)[2]=s(e,t,n))return!0}return!1}}function we(i){return 1<i.length?function(e,t,n){var r=i.length;while(r--)if(!i[r](e,t,n))return!1;return!0}:i[0]}function Te(e,t,n,r,i){for(var o,a=[],s=0,u=e.length,l=null!=t;s<u;s++)(o=e[s])&&(n&&!n(o,r,i)||(a.push(o),l&&t.push(s)));return a}function Ce(d,h,g,v,y,e){return v&&!v[k]&&(v=Ce(v)),y&&!y[k]&&(y=Ce(y,e)),le(function(e,t,n,r){var i,o,a,s=[],u=[],l=t.length,c=e||function(e,t,n){for(var r=0,i=t.length;r<i;r++)se(e,t[r],n);return n}(h||"*",n.nodeType?[n]:n,[]),f=!d||!e&&h?c:Te(c,s,d,n,r),p=g?y||(e?d:l||v)?[]:t:f;if(g&&g(f,p,n,r),v){i=Te(p,u),v(i,[],n,r),o=i.length;while(o--)(a=i[o])&&(p[u[o]]=!(f[u[o]]=a))}if(e){if(y||d){if(y){i=[],o=p.length;while(o--)(a=p[o])&&i.push(f[o]=a);y(null,p=[],i,r)}o=p.length;while(o--)(a=p[o])&&-1<(i=y?P(e,a):s[o])&&(e[i]=!(t[i]=a))}}else p=Te(p===t?p.splice(l,p.length):p),y?y(null,t,p,r):H.apply(t,p)})}function Ee(e){for(var i,t,n,r=e.length,o=b.relative[e[0].type],a=o||b.relative[" "],s=o?1:0,u=be(function(e){return e===i},a,!0),l=be(function(e){return-1<P(i,e)},a,!0),c=[function(e,t,n){var r=!o&&(n||t!==w)||((i=t).nodeType?u(e,t,n):l(e,t,n));return i=null,r}];s<r;s++)if(t=b.relative[e[s].type])c=[be(we(c),t)];else{if((t=b.filter[e[s].type].apply(null,e[s].matches))[k]){for(n=++s;n<r;n++)if(b.relative[e[n].type])break;return Ce(1<s&&we(c),1<s&&xe(e.slice(0,s-1).concat({value:" "===e[s-2].type?"*":""})).replace(B,"$1"),t,s<n&&Ee(e.slice(s,n)),n<r&&Ee(e=e.slice(n)),n<r&&xe(e))}c.push(t)}return we(c)}return me.prototype=b.filters=b.pseudos,b.setFilters=new me,h=se.tokenize=function(e,t){var n,r,i,o,a,s,u,l=x[e+" "];if(l)return t?0:l.slice(0);a=e,s=[],u=b.preFilter;while(a){for(o in n&&!(r=_.exec(a))||(r&&(a=a.slice(r[0].length)||a),s.push(i=[])),n=!1,(r=z.exec(a))&&(n=r.shift(),i.push({value:n,type:r[0].replace(B," ")}),a=a.slice(n.length)),b.filter)!(r=G[o].exec(a))||u[o]&&!(r=u[o](r))||(n=r.shift(),i.push({value:n,type:o,matches:r}),a=a.slice(n.length));if(!n)break}return t?a.length:a?se.error(e):x(e,s).slice(0)},f=se.compile=function(e,t){var n,v,y,m,x,r,i=[],o=[],a=N[e+" "];if(!a){t||(t=h(e)),n=t.length;while(n--)(a=Ee(t[n]))[k]?i.push(a):o.push(a);(a=N(e,(v=o,m=0<(y=i).length,x=0<v.length,r=function(e,t,n,r,i){var o,a,s,u=0,l="0",c=e&&[],f=[],p=w,d=e||x&&b.find.TAG("*",i),h=S+=null==p?1:Math.random()||.1,g=d.length;for(i&&(w=t===C||t||i);l!==g&&null!=(o=d[l]);l++){if(x&&o){a=0,t||o.ownerDocument===C||(T(o),n=!E);while(s=v[a++])if(s(o,t||C,n)){r.push(o);break}i&&(S=h)}m&&((o=!s&&o)&&u--,e&&c.push(o))}if(u+=l,m&&l!==u){a=0;while(s=y[a++])s(c,f,t,n);if(e){if(0<u)while(l--)c[l]||f[l]||(f[l]=q.call(r));f=Te(f)}H.apply(r,f),i&&!e&&0<f.length&&1<u+y.length&&se.uniqueSort(r)}return i&&(S=h,w=p),c},m?le(r):r))).selector=e}return a},g=se.select=function(e,t,n,r){var i,o,a,s,u,l="function"==typeof e&&e,c=!r&&h(e=l.selector||e);if(n=n||[],1===c.length){if(2<(o=c[0]=c[0].slice(0)).length&&"ID"===(a=o[0]).type&&9===t.nodeType&&E&&b.relative[o[1].type]){if(!(t=(b.find.ID(a.matches[0].replace(te,ne),t)||[])[0]))return n;l&&(t=t.parentNode),e=e.slice(o.shift().value.length)}i=G.needsContext.test(e)?0:o.length;while(i--){if(a=o[i],b.relative[s=a.type])break;if((u=b.find[s])&&(r=u(a.matches[0].replace(te,ne),ee.test(o[0].type)&&ye(t.parentNode)||t))){if(o.splice(i,1),!(e=r.length&&xe(o)))return H.apply(n,r),n;break}}}return(l||f(e,c))(r,t,!E,n,!t||ee.test(e)&&ye(t.parentNode)||t),n},d.sortStable=k.split("").sort(D).join("")===k,d.detectDuplicates=!!l,T(),d.sortDetached=ce(function(e){return 1&e.compareDocumentPosition(C.createElement("fieldset"))}),ce(function(e){return e.innerHTML="<a href='#'></a>","#"===e.firstChild.getAttribute("href")})||fe("type|href|height|width",function(e,t,n){if(!n)return e.getAttribute(t,"type"===t.toLowerCase()?1:2)}),d.attributes&&ce(function(e){return e.innerHTML="<input/>",e.firstChild.setAttribute("value",""),""===e.firstChild.getAttribute("value")})||fe("value",function(e,t,n){if(!n&&"input"===e.nodeName.toLowerCase())return e.defaultValue}),ce(function(e){return null==e.getAttribute("disabled")})||fe(R,function(e,t,n){var r;if(!n)return!0===e[t]?t.toLowerCase():(r=e.getAttributeNode(t))&&r.specified?r.value:null}),se}(C);k.find=h,k.expr=h.selectors,k.expr[":"]=k.expr.pseudos,k.uniqueSort=k.unique=h.uniqueSort,k.text=h.getText,k.isXMLDoc=h.isXML,k.contains=h.contains,k.escapeSelector=h.escape;var T=function(e,t,n){var r=[],i=void 0!==n;while((e=e[t])&&9!==e.nodeType)if(1===e.nodeType){if(i&&k(e).is(n))break;r.push(e)}return r},S=function(e,t){for(var n=[];e;e=e.nextSibling)1===e.nodeType&&e!==t&&n.push(e);return n},N=k.expr.match.needsContext;function A(e,t){return e.nodeName&&e.nodeName.toLowerCase()===t.toLowerCase()}var D=/^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;function j(e,n,r){return m(n)?k.grep(e,function(e,t){return!!n.call(e,t,e)!==r}):n.nodeType?k.grep(e,function(e){return e===n!==r}):"string"!=typeof n?k.grep(e,function(e){return-1<i.call(n,e)!==r}):k.filter(n,e,r)}k.filter=function(e,t,n){var r=t[0];return n&&(e=":not("+e+")"),1===t.length&&1===r.nodeType?k.find.matchesSelector(r,e)?[r]:[]:k.find.matches(e,k.grep(t,function(e){return 1===e.nodeType}))},k.fn.extend({find:function(e){var t,n,r=this.length,i=this;if("string"!=typeof e)return this.pushStack(k(e).filter(function(){for(t=0;t<r;t++)if(k.contains(i[t],this))return!0}));for(n=this.pushStack([]),t=0;t<r;t++)k.find(e,i[t],n);return 1<r?k.uniqueSort(n):n},filter:function(e){return this.pushStack(j(this,e||[],!1))},not:function(e){return this.pushStack(j(this,e||[],!0))},is:function(e){return!!j(this,"string"==typeof e&&N.test(e)?k(e):e||[],!1).length}});var q,L=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;(k.fn.init=function(e,t,n){var r,i;if(!e)return this;if(n=n||q,"string"==typeof e){if(!(r="<"===e[0]&&">"===e[e.length-1]&&3<=e.length?[null,e,null]:L.exec(e))||!r[1]&&t)return!t||t.jquery?(t||n).find(e):this.constructor(t).find(e);if(r[1]){if(t=t instanceof k?t[0]:t,k.merge(this,k.parseHTML(r[1],t&&t.nodeType?t.ownerDocument||t:E,!0)),D.test(r[1])&&k.isPlainObject(t))for(r in t)m(this[r])?this[r](t[r]):this.attr(r,t[r]);return this}return(i=E.getElementById(r[2]))&&(this[0]=i,this.length=1),this}return e.nodeType?(this[0]=e,this.length=1,this):m(e)?void 0!==n.ready?n.ready(e):e(k):k.makeArray(e,this)}).prototype=k.fn,q=k(E);var H=/^(?:parents|prev(?:Until|All))/,O={children:!0,contents:!0,next:!0,prev:!0};function P(e,t){while((e=e[t])&&1!==e.nodeType);return e}k.fn.extend({has:function(e){var t=k(e,this),n=t.length;return this.filter(function(){for(var e=0;e<n;e++)if(k.contains(this,t[e]))return!0})},closest:function(e,t){var n,r=0,i=this.length,o=[],a="string"!=typeof e&&k(e);if(!N.test(e))for(;r<i;r++)for(n=this[r];n&&n!==t;n=n.parentNode)if(n.nodeType<11&&(a?-1<a.index(n):1===n.nodeType&&k.find.matchesSelector(n,e))){o.push(n);break}return this.pushStack(1<o.length?k.uniqueSort(o):o)},index:function(e){return e?"string"==typeof e?i.call(k(e),this[0]):i.call(this,e.jquery?e[0]:e):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(e,t){return this.pushStack(k.uniqueSort(k.merge(this.get(),k(e,t))))},addBack:function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}}),k.each({parent:function(e){var t=e.parentNode;return t&&11!==t.nodeType?t:null},parents:function(e){return T(e,"parentNode")},parentsUntil:function(e,t,n){return T(e,"parentNode",n)},next:function(e){return P(e,"nextSibling")},prev:function(e){return P(e,"previousSibling")},nextAll:function(e){return T(e,"nextSibling")},prevAll:function(e){return T(e,"previousSibling")},nextUntil:function(e,t,n){return T(e,"nextSibling",n)},prevUntil:function(e,t,n){return T(e,"previousSibling",n)},siblings:function(e){return S((e.parentNode||{}).firstChild,e)},children:function(e){return S(e.firstChild)},contents:function(e){return"undefined"!=typeof e.contentDocument?e.contentDocument:(A(e,"template")&&(e=e.content||e),k.merge([],e.childNodes))}},function(r,i){k.fn[r]=function(e,t){var n=k.map(this,i,e);return"Until"!==r.slice(-5)&&(t=e),t&&"string"==typeof t&&(n=k.filter(t,n)),1<this.length&&(O[r]||k.uniqueSort(n),H.test(r)&&n.reverse()),this.pushStack(n)}});var R=/[^\x20\t\r\n\f]+/g;function M(e){return e}function I(e){throw e}function W(e,t,n,r){var i;try{e&&m(i=e.promise)?i.call(e).done(t).fail(n):e&&m(i=e.then)?i.call(e,t,n):t.apply(void 0,[e].slice(r))}catch(e){n.apply(void 0,[e])}}k.Callbacks=function(r){var e,n;r="string"==typeof r?(e=r,n={},k.each(e.match(R)||[],function(e,t){n[t]=!0}),n):k.extend({},r);var i,t,o,a,s=[],u=[],l=-1,c=function(){for(a=a||r.once,o=i=!0;u.length;l=-1){t=u.shift();while(++l<s.length)!1===s[l].apply(t[0],t[1])&&r.stopOnFalse&&(l=s.length,t=!1)}r.memory||(t=!1),i=!1,a&&(s=t?[]:"")},f={add:function(){return s&&(t&&!i&&(l=s.length-1,u.push(t)),function n(e){k.each(e,function(e,t){m(t)?r.unique&&f.has(t)||s.push(t):t&&t.length&&"string"!==w(t)&&n(t)})}(arguments),t&&!i&&c()),this},remove:function(){return k.each(arguments,function(e,t){var n;while(-1<(n=k.inArray(t,s,n)))s.splice(n,1),n<=l&&l--}),this},has:function(e){return e?-1<k.inArray(e,s):0<s.length},empty:function(){return s&&(s=[]),this},disable:function(){return a=u=[],s=t="",this},disabled:function(){return!s},lock:function(){return a=u=[],t||i||(s=t=""),this},locked:function(){return!!a},fireWith:function(e,t){return a||(t=[e,(t=t||[]).slice?t.slice():t],u.push(t),i||c()),this},fire:function(){return f.fireWith(this,arguments),this},fired:function(){return!!o}};return f},k.extend({Deferred:function(e){var o=[["notify","progress",k.Callbacks("memory"),k.Callbacks("memory"),2],["resolve","done",k.Callbacks("once memory"),k.Callbacks("once memory"),0,"resolved"],["reject","fail",k.Callbacks("once memory"),k.Callbacks("once memory"),1,"rejected"]],i="pending",a={state:function(){return i},always:function(){return s.done(arguments).fail(arguments),this},"catch":function(e){return a.then(null,e)},pipe:function(){var i=arguments;return k.Deferred(function(r){k.each(o,function(e,t){var n=m(i[t[4]])&&i[t[4]];s[t[1]](function(){var e=n&&n.apply(this,arguments);e&&m(e.promise)?e.promise().progress(r.notify).done(r.resolve).fail(r.reject):r[t[0]+"With"](this,n?[e]:arguments)})}),i=null}).promise()},then:function(t,n,r){var u=0;function l(i,o,a,s){return function(){var n=this,r=arguments,e=function(){var e,t;if(!(i<u)){if((e=a.apply(n,r))===o.promise())throw new TypeError("Thenable self-resolution");t=e&&("object"==typeof e||"function"==typeof e)&&e.then,m(t)?s?t.call(e,l(u,o,M,s),l(u,o,I,s)):(u++,t.call(e,l(u,o,M,s),l(u,o,I,s),l(u,o,M,o.notifyWith))):(a!==M&&(n=void 0,r=[e]),(s||o.resolveWith)(n,r))}},t=s?e:function(){try{e()}catch(e){k.Deferred.exceptionHook&&k.Deferred.exceptionHook(e,t.stackTrace),u<=i+1&&(a!==I&&(n=void 0,r=[e]),o.rejectWith(n,r))}};i?t():(k.Deferred.getStackHook&&(t.stackTrace=k.Deferred.getStackHook()),C.setTimeout(t))}}return k.Deferred(function(e){o[0][3].add(l(0,e,m(r)?r:M,e.notifyWith)),o[1][3].add(l(0,e,m(t)?t:M)),o[2][3].add(l(0,e,m(n)?n:I))}).promise()},promise:function(e){return null!=e?k.extend(e,a):a}},s={};return k.each(o,function(e,t){var n=t[2],r=t[5];a[t[1]]=n.add,r&&n.add(function(){i=r},o[3-e][2].disable,o[3-e][3].disable,o[0][2].lock,o[0][3].lock),n.add(t[3].fire),s[t[0]]=function(){return s[t[0]+"With"](this===s?void 0:this,arguments),this},s[t[0]+"With"]=n.fireWith}),a.promise(s),e&&e.call(s,s),s},when:function(e){var n=arguments.length,t=n,r=Array(t),i=s.call(arguments),o=k.Deferred(),a=function(t){return function(e){r[t]=this,i[t]=1<arguments.length?s.call(arguments):e,--n||o.resolveWith(r,i)}};if(n<=1&&(W(e,o.done(a(t)).resolve,o.reject,!n),"pending"===o.state()||m(i[t]&&i[t].then)))return o.then();while(t--)W(i[t],a(t),o.reject);return o.promise()}});var $=/^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;k.Deferred.exceptionHook=function(e,t){C.console&&C.console.warn&&e&&$.test(e.name)&&C.console.warn("jQuery.Deferred exception: "+e.message,e.stack,t)},k.readyException=function(e){C.setTimeout(function(){throw e})};var F=k.Deferred();function B(){E.removeEventListener("DOMContentLoaded",B),C.removeEventListener("load",B),k.ready()}k.fn.ready=function(e){return F.then(e)["catch"](function(e){k.readyException(e)}),this},k.extend({isReady:!1,readyWait:1,ready:function(e){(!0===e?--k.readyWait:k.isReady)||(k.isReady=!0)!==e&&0<--k.readyWait||F.resolveWith(E,[k])}}),k.ready.then=F.then,"complete"===E.readyState||"loading"!==E.readyState&&!E.documentElement.doScroll?C.setTimeout(k.ready):(E.addEventListener("DOMContentLoaded",B),C.addEventListener("load",B));var _=function(e,t,n,r,i,o,a){var s=0,u=e.length,l=null==n;if("object"===w(n))for(s in i=!0,n)_(e,t,s,n[s],!0,o,a);else if(void 0!==r&&(i=!0,m(r)||(a=!0),l&&(a?(t.call(e,r),t=null):(l=t,t=function(e,t,n){return l.call(k(e),n)})),t))for(;s<u;s++)t(e[s],n,a?r:r.call(e[s],s,t(e[s],n)));return i?e:l?t.call(e):u?t(e[0],n):o},z=/^-ms-/,U=/-([a-z])/g;function X(e,t){return t.toUpperCase()}function V(e){return e.replace(z,"ms-").replace(U,X)}var G=function(e){return 1===e.nodeType||9===e.nodeType||!+e.nodeType};function Y(){this.expando=k.expando+Y.uid++}Y.uid=1,Y.prototype={cache:function(e){var t=e[this.expando];return t||(t={},G(e)&&(e.nodeType?e[this.expando]=t:Object.defineProperty(e,this.expando,{value:t,configurable:!0}))),t},set:function(e,t,n){var r,i=this.cache(e);if("string"==typeof t)i[V(t)]=n;else for(r in t)i[V(r)]=t[r];return i},get:function(e,t){return void 0===t?this.cache(e):e[this.expando]&&e[this.expando][V(t)]},access:function(e,t,n){return void 0===t||t&&"string"==typeof t&&void 0===n?this.get(e,t):(this.set(e,t,n),void 0!==n?n:t)},remove:function(e,t){var n,r=e[this.expando];if(void 0!==r){if(void 0!==t){n=(t=Array.isArray(t)?t.map(V):(t=V(t))in r?[t]:t.match(R)||[]).length;while(n--)delete r[t[n]]}(void 0===t||k.isEmptyObject(r))&&(e.nodeType?e[this.expando]=void 0:delete e[this.expando])}},hasData:function(e){var t=e[this.expando];return void 0!==t&&!k.isEmptyObject(t)}};var Q=new Y,J=new Y,K=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,Z=/[A-Z]/g;function ee(e,t,n){var r,i;if(void 0===n&&1===e.nodeType)if(r="data-"+t.replace(Z,"-$&").toLowerCase(),"string"==typeof(n=e.getAttribute(r))){try{n="true"===(i=n)||"false"!==i&&("null"===i?null:i===+i+""?+i:K.test(i)?JSON.parse(i):i)}catch(e){}J.set(e,t,n)}else n=void 0;return n}k.extend({hasData:function(e){return J.hasData(e)||Q.hasData(e)},data:function(e,t,n){return J.access(e,t,n)},removeData:function(e,t){J.remove(e,t)},_data:function(e,t,n){return Q.access(e,t,n)},_removeData:function(e,t){Q.remove(e,t)}}),k.fn.extend({data:function(n,e){var t,r,i,o=this[0],a=o&&o.attributes;if(void 0===n){if(this.length&&(i=J.get(o),1===o.nodeType&&!Q.get(o,"hasDataAttrs"))){t=a.length;while(t--)a[t]&&0===(r=a[t].name).indexOf("data-")&&(r=V(r.slice(5)),ee(o,r,i[r]));Q.set(o,"hasDataAttrs",!0)}return i}return"object"==typeof n?this.each(function(){J.set(this,n)}):_(this,function(e){var t;if(o&&void 0===e)return void 0!==(t=J.get(o,n))?t:void 0!==(t=ee(o,n))?t:void 0;this.each(function(){J.set(this,n,e)})},null,e,1<arguments.length,null,!0)},removeData:function(e){return this.each(function(){J.remove(this,e)})}}),k.extend({queue:function(e,t,n){var r;if(e)return t=(t||"fx")+"queue",r=Q.get(e,t),n&&(!r||Array.isArray(n)?r=Q.access(e,t,k.makeArray(n)):r.push(n)),r||[]},dequeue:function(e,t){t=t||"fx";var n=k.queue(e,t),r=n.length,i=n.shift(),o=k._queueHooks(e,t);"inprogress"===i&&(i=n.shift(),r--),i&&("fx"===t&&n.unshift("inprogress"),delete o.stop,i.call(e,function(){k.dequeue(e,t)},o)),!r&&o&&o.empty.fire()},_queueHooks:function(e,t){var n=t+"queueHooks";return Q.get(e,n)||Q.access(e,n,{empty:k.Callbacks("once memory").add(function(){Q.remove(e,[t+"queue",n])})})}}),k.fn.extend({queue:function(t,n){var e=2;return"string"!=typeof t&&(n=t,t="fx",e--),arguments.length<e?k.queue(this[0],t):void 0===n?this:this.each(function(){var e=k.queue(this,t,n);k._queueHooks(this,t),"fx"===t&&"inprogress"!==e[0]&&k.dequeue(this,t)})},dequeue:function(e){return this.each(function(){k.dequeue(this,e)})},clearQueue:function(e){return this.queue(e||"fx",[])},promise:function(e,t){var n,r=1,i=k.Deferred(),o=this,a=this.length,s=function(){--r||i.resolveWith(o,[o])};"string"!=typeof e&&(t=e,e=void 0),e=e||"fx";while(a--)(n=Q.get(o[a],e+"queueHooks"))&&n.empty&&(r++,n.empty.add(s));return s(),i.promise(t)}});var te=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,ne=new RegExp("^(?:([+-])=|)("+te+")([a-z%]*)$","i"),re=["Top","Right","Bottom","Left"],ie=E.documentElement,oe=function(e){return k.contains(e.ownerDocument,e)},ae={composed:!0};ie.getRootNode&&(oe=function(e){return k.contains(e.ownerDocument,e)||e.getRootNode(ae)===e.ownerDocument});var se=function(e,t){return"none"===(e=t||e).style.display||""===e.style.display&&oe(e)&&"none"===k.css(e,"display")},ue=function(e,t,n,r){var i,o,a={};for(o in t)a[o]=e.style[o],e.style[o]=t[o];for(o in i=n.apply(e,r||[]),t)e.style[o]=a[o];return i};function le(e,t,n,r){var i,o,a=20,s=r?function(){return r.cur()}:function(){return k.css(e,t,"")},u=s(),l=n&&n[3]||(k.cssNumber[t]?"":"px"),c=e.nodeType&&(k.cssNumber[t]||"px"!==l&&+u)&&ne.exec(k.css(e,t));if(c&&c[3]!==l){u/=2,l=l||c[3],c=+u||1;while(a--)k.style(e,t,c+l),(1-o)*(1-(o=s()/u||.5))<=0&&(a=0),c/=o;c*=2,k.style(e,t,c+l),n=n||[]}return n&&(c=+c||+u||0,i=n[1]?c+(n[1]+1)*n[2]:+n[2],r&&(r.unit=l,r.start=c,r.end=i)),i}var ce={};function fe(e,t){for(var n,r,i,o,a,s,u,l=[],c=0,f=e.length;c<f;c++)(r=e[c]).style&&(n=r.style.display,t?("none"===n&&(l[c]=Q.get(r,"display")||null,l[c]||(r.style.display="")),""===r.style.display&&se(r)&&(l[c]=(u=a=o=void 0,a=(i=r).ownerDocument,s=i.nodeName,(u=ce[s])||(o=a.body.appendChild(a.createElement(s)),u=k.css(o,"display"),o.parentNode.removeChild(o),"none"===u&&(u="block"),ce[s]=u)))):"none"!==n&&(l[c]="none",Q.set(r,"display",n)));for(c=0;c<f;c++)null!=l[c]&&(e[c].style.display=l[c]);return e}k.fn.extend({show:function(){return fe(this,!0)},hide:function(){return fe(this)},toggle:function(e){return"boolean"==typeof e?e?this.show():this.hide():this.each(function(){se(this)?k(this).show():k(this).hide()})}});var pe=/^(?:checkbox|radio)$/i,de=/<([a-z][^\/\0>\x20\t\r\n\f]*)/i,he=/^$|^module$|\/(?:java|ecma)script/i,ge={option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};function ve(e,t){var n;return n="undefined"!=typeof e.getElementsByTagName?e.getElementsByTagName(t||"*"):"undefined"!=typeof e.querySelectorAll?e.querySelectorAll(t||"*"):[],void 0===t||t&&A(e,t)?k.merge([e],n):n}function ye(e,t){for(var n=0,r=e.length;n<r;n++)Q.set(e[n],"globalEval",!t||Q.get(t[n],"globalEval"))}ge.optgroup=ge.option,ge.tbody=ge.tfoot=ge.colgroup=ge.caption=ge.thead,ge.th=ge.td;var me,xe,be=/<|&#?\w+;/;function we(e,t,n,r,i){for(var o,a,s,u,l,c,f=t.createDocumentFragment(),p=[],d=0,h=e.length;d<h;d++)if((o=e[d])||0===o)if("object"===w(o))k.merge(p,o.nodeType?[o]:o);else if(be.test(o)){a=a||f.appendChild(t.createElement("div")),s=(de.exec(o)||["",""])[1].toLowerCase(),u=ge[s]||ge._default,a.innerHTML=u[1]+k.htmlPrefilter(o)+u[2],c=u[0];while(c--)a=a.lastChild;k.merge(p,a.childNodes),(a=f.firstChild).textContent=""}else p.push(t.createTextNode(o));f.textContent="",d=0;while(o=p[d++])if(r&&-1<k.inArray(o,r))i&&i.push(o);else if(l=oe(o),a=ve(f.appendChild(o),"script"),l&&ye(a),n){c=0;while(o=a[c++])he.test(o.type||"")&&n.push(o)}return f}me=E.createDocumentFragment().appendChild(E.createElement("div")),(xe=E.createElement("input")).setAttribute("type","radio"),xe.setAttribute("checked","checked"),xe.setAttribute("name","t"),me.appendChild(xe),y.checkClone=me.cloneNode(!0).cloneNode(!0).lastChild.checked,me.innerHTML="<textarea>x</textarea>",y.noCloneChecked=!!me.cloneNode(!0).lastChild.defaultValue;var Te=/^key/,Ce=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,Ee=/^([^.]*)(?:\.(.+)|)/;function ke(){return!0}function Se(){return!1}function Ne(e,t){return e===function(){try{return E.activeElement}catch(e){}}()==("focus"===t)}function Ae(e,t,n,r,i,o){var a,s;if("object"==typeof t){for(s in"string"!=typeof n&&(r=r||n,n=void 0),t)Ae(e,s,n,r,t[s],o);return e}if(null==r&&null==i?(i=n,r=n=void 0):null==i&&("string"==typeof n?(i=r,r=void 0):(i=r,r=n,n=void 0)),!1===i)i=Se;else if(!i)return e;return 1===o&&(a=i,(i=function(e){return k().off(e),a.apply(this,arguments)}).guid=a.guid||(a.guid=k.guid++)),e.each(function(){k.event.add(this,t,i,r,n)})}function De(e,i,o){o?(Q.set(e,i,!1),k.event.add(e,i,{namespace:!1,handler:function(e){var t,n,r=Q.get(this,i);if(1&e.isTrigger&&this[i]){if(r.length)(k.event.special[i]||{}).delegateType&&e.stopPropagation();else if(r=s.call(arguments),Q.set(this,i,r),t=o(this,i),this[i](),r!==(n=Q.get(this,i))||t?Q.set(this,i,!1):n={},r!==n)return e.stopImmediatePropagation(),e.preventDefault(),n.value}else r.length&&(Q.set(this,i,{value:k.event.trigger(k.extend(r[0],k.Event.prototype),r.slice(1),this)}),e.stopImmediatePropagation())}})):void 0===Q.get(e,i)&&k.event.add(e,i,ke)}k.event={global:{},add:function(t,e,n,r,i){var o,a,s,u,l,c,f,p,d,h,g,v=Q.get(t);if(v){n.handler&&(n=(o=n).handler,i=o.selector),i&&k.find.matchesSelector(ie,i),n.guid||(n.guid=k.guid++),(u=v.events)||(u=v.events={}),(a=v.handle)||(a=v.handle=function(e){return"undefined"!=typeof k&&k.event.triggered!==e.type?k.event.dispatch.apply(t,arguments):void 0}),l=(e=(e||"").match(R)||[""]).length;while(l--)d=g=(s=Ee.exec(e[l])||[])[1],h=(s[2]||"").split(".").sort(),d&&(f=k.event.special[d]||{},d=(i?f.delegateType:f.bindType)||d,f=k.event.special[d]||{},c=k.extend({type:d,origType:g,data:r,handler:n,guid:n.guid,selector:i,needsContext:i&&k.expr.match.needsContext.test(i),namespace:h.join(".")},o),(p=u[d])||((p=u[d]=[]).delegateCount=0,f.setup&&!1!==f.setup.call(t,r,h,a)||t.addEventListener&&t.addEventListener(d,a)),f.add&&(f.add.call(t,c),c.handler.guid||(c.handler.guid=n.guid)),i?p.splice(p.delegateCount++,0,c):p.push(c),k.event.global[d]=!0)}},remove:function(e,t,n,r,i){var o,a,s,u,l,c,f,p,d,h,g,v=Q.hasData(e)&&Q.get(e);if(v&&(u=v.events)){l=(t=(t||"").match(R)||[""]).length;while(l--)if(d=g=(s=Ee.exec(t[l])||[])[1],h=(s[2]||"").split(".").sort(),d){f=k.event.special[d]||{},p=u[d=(r?f.delegateType:f.bindType)||d]||[],s=s[2]&&new RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|$)"),a=o=p.length;while(o--)c=p[o],!i&&g!==c.origType||n&&n.guid!==c.guid||s&&!s.test(c.namespace)||r&&r!==c.selector&&("**"!==r||!c.selector)||(p.splice(o,1),c.selector&&p.delegateCount--,f.remove&&f.remove.call(e,c));a&&!p.length&&(f.teardown&&!1!==f.teardown.call(e,h,v.handle)||k.removeEvent(e,d,v.handle),delete u[d])}else for(d in u)k.event.remove(e,d+t[l],n,r,!0);k.isEmptyObject(u)&&Q.remove(e,"handle events")}},dispatch:function(e){var t,n,r,i,o,a,s=k.event.fix(e),u=new Array(arguments.length),l=(Q.get(this,"events")||{})[s.type]||[],c=k.event.special[s.type]||{};for(u[0]=s,t=1;t<arguments.length;t++)u[t]=arguments[t];if(s.delegateTarget=this,!c.preDispatch||!1!==c.preDispatch.call(this,s)){a=k.event.handlers.call(this,s,l),t=0;while((i=a[t++])&&!s.isPropagationStopped()){s.currentTarget=i.elem,n=0;while((o=i.handlers[n++])&&!s.isImmediatePropagationStopped())s.rnamespace&&!1!==o.namespace&&!s.rnamespace.test(o.namespace)||(s.handleObj=o,s.data=o.data,void 0!==(r=((k.event.special[o.origType]||{}).handle||o.handler).apply(i.elem,u))&&!1===(s.result=r)&&(s.preventDefault(),s.stopPropagation()))}return c.postDispatch&&c.postDispatch.call(this,s),s.result}},handlers:function(e,t){var n,r,i,o,a,s=[],u=t.delegateCount,l=e.target;if(u&&l.nodeType&&!("click"===e.type&&1<=e.button))for(;l!==this;l=l.parentNode||this)if(1===l.nodeType&&("click"!==e.type||!0!==l.disabled)){for(o=[],a={},n=0;n<u;n++)void 0===a[i=(r=t[n]).selector+" "]&&(a[i]=r.needsContext?-1<k(i,this).index(l):k.find(i,this,null,[l]).length),a[i]&&o.push(r);o.length&&s.push({elem:l,handlers:o})}return l=this,u<t.length&&s.push({elem:l,handlers:t.slice(u)}),s},addProp:function(t,e){Object.defineProperty(k.Event.prototype,t,{enumerable:!0,configurable:!0,get:m(e)?function(){if(this.originalEvent)return e(this.originalEvent)}:function(){if(this.originalEvent)return this.originalEvent[t]},set:function(e){Object.defineProperty(this,t,{enumerable:!0,configurable:!0,writable:!0,value:e})}})},fix:function(e){return e[k.expando]?e:new k.Event(e)},special:{load:{noBubble:!0},click:{setup:function(e){var t=this||e;return pe.test(t.type)&&t.click&&A(t,"input")&&De(t,"click",ke),!1},trigger:function(e){var t=this||e;return pe.test(t.type)&&t.click&&A(t,"input")&&De(t,"click"),!0},_default:function(e){var t=e.target;return pe.test(t.type)&&t.click&&A(t,"input")&&Q.get(t,"click")||A(t,"a")}},beforeunload:{postDispatch:function(e){void 0!==e.result&&e.originalEvent&&(e.originalEvent.returnValue=e.result)}}}},k.removeEvent=function(e,t,n){e.removeEventListener&&e.removeEventListener(t,n)},k.Event=function(e,t){if(!(this instanceof k.Event))return new k.Event(e,t);e&&e.type?(this.originalEvent=e,this.type=e.type,this.isDefaultPrevented=e.defaultPrevented||void 0===e.defaultPrevented&&!1===e.returnValue?ke:Se,this.target=e.target&&3===e.target.nodeType?e.target.parentNode:e.target,this.currentTarget=e.currentTarget,this.relatedTarget=e.relatedTarget):this.type=e,t&&k.extend(this,t),this.timeStamp=e&&e.timeStamp||Date.now(),this[k.expando]=!0},k.Event.prototype={constructor:k.Event,isDefaultPrevented:Se,isPropagationStopped:Se,isImmediatePropagationStopped:Se,isSimulated:!1,preventDefault:function(){var e=this.originalEvent;this.isDefaultPrevented=ke,e&&!this.isSimulated&&e.preventDefault()},stopPropagation:function(){var e=this.originalEvent;this.isPropagationStopped=ke,e&&!this.isSimulated&&e.stopPropagation()},stopImmediatePropagation:function(){var e=this.originalEvent;this.isImmediatePropagationStopped=ke,e&&!this.isSimulated&&e.stopImmediatePropagation(),this.stopPropagation()}},k.each({altKey:!0,bubbles:!0,cancelable:!0,changedTouches:!0,ctrlKey:!0,detail:!0,eventPhase:!0,metaKey:!0,pageX:!0,pageY:!0,shiftKey:!0,view:!0,"char":!0,code:!0,charCode:!0,key:!0,keyCode:!0,button:!0,buttons:!0,clientX:!0,clientY:!0,offsetX:!0,offsetY:!0,pointerId:!0,pointerType:!0,screenX:!0,screenY:!0,targetTouches:!0,toElement:!0,touches:!0,which:function(e){var t=e.button;return null==e.which&&Te.test(e.type)?null!=e.charCode?e.charCode:e.keyCode:!e.which&&void 0!==t&&Ce.test(e.type)?1&t?1:2&t?3:4&t?2:0:e.which}},k.event.addProp),k.each({focus:"focusin",blur:"focusout"},function(e,t){k.event.special[e]={setup:function(){return De(this,e,Ne),!1},trigger:function(){return De(this,e),!0},delegateType:t}}),k.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(e,i){k.event.special[e]={delegateType:i,bindType:i,handle:function(e){var t,n=e.relatedTarget,r=e.handleObj;return n&&(n===this||k.contains(this,n))||(e.type=r.origType,t=r.handler.apply(this,arguments),e.type=i),t}}}),k.fn.extend({on:function(e,t,n,r){return Ae(this,e,t,n,r)},one:function(e,t,n,r){return Ae(this,e,t,n,r,1)},off:function(e,t,n){var r,i;if(e&&e.preventDefault&&e.handleObj)return r=e.handleObj,k(e.delegateTarget).off(r.namespace?r.origType+"."+r.namespace:r.origType,r.selector,r.handler),this;if("object"==typeof e){for(i in e)this.off(i,t,e[i]);return this}return!1!==t&&"function"!=typeof t||(n=t,t=void 0),!1===n&&(n=Se),this.each(function(){k.event.remove(this,e,n,t)})}});var je=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,qe=/<script|<style|<link/i,Le=/checked\s*(?:[^=]|=\s*.checked.)/i,He=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;function Oe(e,t){return A(e,"table")&&A(11!==t.nodeType?t:t.firstChild,"tr")&&k(e).children("tbody")[0]||e}function Pe(e){return e.type=(null!==e.getAttribute("type"))+"/"+e.type,e}function Re(e){return"true/"===(e.type||"").slice(0,5)?e.type=e.type.slice(5):e.removeAttribute("type"),e}function Me(e,t){var n,r,i,o,a,s,u,l;if(1===t.nodeType){if(Q.hasData(e)&&(o=Q.access(e),a=Q.set(t,o),l=o.events))for(i in delete a.handle,a.events={},l)for(n=0,r=l[i].length;n<r;n++)k.event.add(t,i,l[i][n]);J.hasData(e)&&(s=J.access(e),u=k.extend({},s),J.set(t,u))}}function Ie(n,r,i,o){r=g.apply([],r);var e,t,a,s,u,l,c=0,f=n.length,p=f-1,d=r[0],h=m(d);if(h||1<f&&"string"==typeof d&&!y.checkClone&&Le.test(d))return n.each(function(e){var t=n.eq(e);h&&(r[0]=d.call(this,e,t.html())),Ie(t,r,i,o)});if(f&&(t=(e=we(r,n[0].ownerDocument,!1,n,o)).firstChild,1===e.childNodes.length&&(e=t),t||o)){for(s=(a=k.map(ve(e,"script"),Pe)).length;c<f;c++)u=e,c!==p&&(u=k.clone(u,!0,!0),s&&k.merge(a,ve(u,"script"))),i.call(n[c],u,c);if(s)for(l=a[a.length-1].ownerDocument,k.map(a,Re),c=0;c<s;c++)u=a[c],he.test(u.type||"")&&!Q.access(u,"globalEval")&&k.contains(l,u)&&(u.src&&"module"!==(u.type||"").toLowerCase()?k._evalUrl&&!u.noModule&&k._evalUrl(u.src,{nonce:u.nonce||u.getAttribute("nonce")}):b(u.textContent.replace(He,""),u,l))}return n}function We(e,t,n){for(var r,i=t?k.filter(t,e):e,o=0;null!=(r=i[o]);o++)n||1!==r.nodeType||k.cleanData(ve(r)),r.parentNode&&(n&&oe(r)&&ye(ve(r,"script")),r.parentNode.removeChild(r));return e}k.extend({htmlPrefilter:function(e){return e.replace(je,"<$1></$2>")},clone:function(e,t,n){var r,i,o,a,s,u,l,c=e.cloneNode(!0),f=oe(e);if(!(y.noCloneChecked||1!==e.nodeType&&11!==e.nodeType||k.isXMLDoc(e)))for(a=ve(c),r=0,i=(o=ve(e)).length;r<i;r++)s=o[r],u=a[r],void 0,"input"===(l=u.nodeName.toLowerCase())&&pe.test(s.type)?u.checked=s.checked:"input"!==l&&"textarea"!==l||(u.defaultValue=s.defaultValue);if(t)if(n)for(o=o||ve(e),a=a||ve(c),r=0,i=o.length;r<i;r++)Me(o[r],a[r]);else Me(e,c);return 0<(a=ve(c,"script")).length&&ye(a,!f&&ve(e,"script")),c},cleanData:function(e){for(var t,n,r,i=k.event.special,o=0;void 0!==(n=e[o]);o++)if(G(n)){if(t=n[Q.expando]){if(t.events)for(r in t.events)i[r]?k.event.remove(n,r):k.removeEvent(n,r,t.handle);n[Q.expando]=void 0}n[J.expando]&&(n[J.expando]=void 0)}}}),k.fn.extend({detach:function(e){return We(this,e,!0)},remove:function(e){return We(this,e)},text:function(e){return _(this,function(e){return void 0===e?k.text(this):this.empty().each(function(){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||(this.textContent=e)})},null,e,arguments.length)},append:function(){return Ie(this,arguments,function(e){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||Oe(this,e).appendChild(e)})},prepend:function(){return Ie(this,arguments,function(e){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var t=Oe(this,e);t.insertBefore(e,t.firstChild)}})},before:function(){return Ie(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this)})},after:function(){return Ie(this,arguments,function(e){this.parentNode&&this.parentNode.insertBefore(e,this.nextSibling)})},empty:function(){for(var e,t=0;null!=(e=this[t]);t++)1===e.nodeType&&(k.cleanData(ve(e,!1)),e.textContent="");return this},clone:function(e,t){return e=null!=e&&e,t=null==t?e:t,this.map(function(){return k.clone(this,e,t)})},html:function(e){return _(this,function(e){var t=this[0]||{},n=0,r=this.length;if(void 0===e&&1===t.nodeType)return t.innerHTML;if("string"==typeof e&&!qe.test(e)&&!ge[(de.exec(e)||["",""])[1].toLowerCase()]){e=k.htmlPrefilter(e);try{for(;n<r;n++)1===(t=this[n]||{}).nodeType&&(k.cleanData(ve(t,!1)),t.innerHTML=e);t=0}catch(e){}}t&&this.empty().append(e)},null,e,arguments.length)},replaceWith:function(){var n=[];return Ie(this,arguments,function(e){var t=this.parentNode;k.inArray(this,n)<0&&(k.cleanData(ve(this)),t&&t.replaceChild(e,this))},n)}}),k.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(e,a){k.fn[e]=function(e){for(var t,n=[],r=k(e),i=r.length-1,o=0;o<=i;o++)t=o===i?this:this.clone(!0),k(r[o])[a](t),u.apply(n,t.get());return this.pushStack(n)}});var $e=new RegExp("^("+te+")(?!px)[a-z%]+$","i"),Fe=function(e){var t=e.ownerDocument.defaultView;return t&&t.opener||(t=C),t.getComputedStyle(e)},Be=new RegExp(re.join("|"),"i");function _e(e,t,n){var r,i,o,a,s=e.style;return(n=n||Fe(e))&&(""!==(a=n.getPropertyValue(t)||n[t])||oe(e)||(a=k.style(e,t)),!y.pixelBoxStyles()&&$e.test(a)&&Be.test(t)&&(r=s.width,i=s.minWidth,o=s.maxWidth,s.minWidth=s.maxWidth=s.width=a,a=n.width,s.width=r,s.minWidth=i,s.maxWidth=o)),void 0!==a?a+"":a}function ze(e,t){return{get:function(){if(!e())return(this.get=t).apply(this,arguments);delete this.get}}}!function(){function e(){if(u){s.style.cssText="position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0",u.style.cssText="position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%",ie.appendChild(s).appendChild(u);var e=C.getComputedStyle(u);n="1%"!==e.top,a=12===t(e.marginLeft),u.style.right="60%",o=36===t(e.right),r=36===t(e.width),u.style.position="absolute",i=12===t(u.offsetWidth/3),ie.removeChild(s),u=null}}function t(e){return Math.round(parseFloat(e))}var n,r,i,o,a,s=E.createElement("div"),u=E.createElement("div");u.style&&(u.style.backgroundClip="content-box",u.cloneNode(!0).style.backgroundClip="",y.clearCloneStyle="content-box"===u.style.backgroundClip,k.extend(y,{boxSizingReliable:function(){return e(),r},pixelBoxStyles:function(){return e(),o},pixelPosition:function(){return e(),n},reliableMarginLeft:function(){return e(),a},scrollboxSize:function(){return e(),i}}))}();var Ue=["Webkit","Moz","ms"],Xe=E.createElement("div").style,Ve={};function Ge(e){var t=k.cssProps[e]||Ve[e];return t||(e in Xe?e:Ve[e]=function(e){var t=e[0].toUpperCase()+e.slice(1),n=Ue.length;while(n--)if((e=Ue[n]+t)in Xe)return e}(e)||e)}var Ye=/^(none|table(?!-c[ea]).+)/,Qe=/^--/,Je={position:"absolute",visibility:"hidden",display:"block"},Ke={letterSpacing:"0",fontWeight:"400"};function Ze(e,t,n){var r=ne.exec(t);return r?Math.max(0,r[2]-(n||0))+(r[3]||"px"):t}function et(e,t,n,r,i,o){var a="width"===t?1:0,s=0,u=0;if(n===(r?"border":"content"))return 0;for(;a<4;a+=2)"margin"===n&&(u+=k.css(e,n+re[a],!0,i)),r?("content"===n&&(u-=k.css(e,"padding"+re[a],!0,i)),"margin"!==n&&(u-=k.css(e,"border"+re[a]+"Width",!0,i))):(u+=k.css(e,"padding"+re[a],!0,i),"padding"!==n?u+=k.css(e,"border"+re[a]+"Width",!0,i):s+=k.css(e,"border"+re[a]+"Width",!0,i));return!r&&0<=o&&(u+=Math.max(0,Math.ceil(e["offset"+t[0].toUpperCase()+t.slice(1)]-o-u-s-.5))||0),u}function tt(e,t,n){var r=Fe(e),i=(!y.boxSizingReliable()||n)&&"border-box"===k.css(e,"boxSizing",!1,r),o=i,a=_e(e,t,r),s="offset"+t[0].toUpperCase()+t.slice(1);if($e.test(a)){if(!n)return a;a="auto"}return(!y.boxSizingReliable()&&i||"auto"===a||!parseFloat(a)&&"inline"===k.css(e,"display",!1,r))&&e.getClientRects().length&&(i="border-box"===k.css(e,"boxSizing",!1,r),(o=s in e)&&(a=e[s])),(a=parseFloat(a)||0)+et(e,t,n||(i?"border":"content"),o,r,a)+"px"}function nt(e,t,n,r,i){return new nt.prototype.init(e,t,n,r,i)}k.extend({cssHooks:{opacity:{get:function(e,t){if(t){var n=_e(e,"opacity");return""===n?"1":n}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,gridArea:!0,gridColumn:!0,gridColumnEnd:!0,gridColumnStart:!0,gridRow:!0,gridRowEnd:!0,gridRowStart:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{},style:function(e,t,n,r){if(e&&3!==e.nodeType&&8!==e.nodeType&&e.style){var i,o,a,s=V(t),u=Qe.test(t),l=e.style;if(u||(t=Ge(s)),a=k.cssHooks[t]||k.cssHooks[s],void 0===n)return a&&"get"in a&&void 0!==(i=a.get(e,!1,r))?i:l[t];"string"===(o=typeof n)&&(i=ne.exec(n))&&i[1]&&(n=le(e,t,i),o="number"),null!=n&&n==n&&("number"!==o||u||(n+=i&&i[3]||(k.cssNumber[s]?"":"px")),y.clearCloneStyle||""!==n||0!==t.indexOf("background")||(l[t]="inherit"),a&&"set"in a&&void 0===(n=a.set(e,n,r))||(u?l.setProperty(t,n):l[t]=n))}},css:function(e,t,n,r){var i,o,a,s=V(t);return Qe.test(t)||(t=Ge(s)),(a=k.cssHooks[t]||k.cssHooks[s])&&"get"in a&&(i=a.get(e,!0,n)),void 0===i&&(i=_e(e,t,r)),"normal"===i&&t in Ke&&(i=Ke[t]),""===n||n?(o=parseFloat(i),!0===n||isFinite(o)?o||0:i):i}}),k.each(["height","width"],function(e,u){k.cssHooks[u]={get:function(e,t,n){if(t)return!Ye.test(k.css(e,"display"))||e.getClientRects().length&&e.getBoundingClientRect().width?tt(e,u,n):ue(e,Je,function(){return tt(e,u,n)})},set:function(e,t,n){var r,i=Fe(e),o=!y.scrollboxSize()&&"absolute"===i.position,a=(o||n)&&"border-box"===k.css(e,"boxSizing",!1,i),s=n?et(e,u,n,a,i):0;return a&&o&&(s-=Math.ceil(e["offset"+u[0].toUpperCase()+u.slice(1)]-parseFloat(i[u])-et(e,u,"border",!1,i)-.5)),s&&(r=ne.exec(t))&&"px"!==(r[3]||"px")&&(e.style[u]=t,t=k.css(e,u)),Ze(0,t,s)}}}),k.cssHooks.marginLeft=ze(y.reliableMarginLeft,function(e,t){if(t)return(parseFloat(_e(e,"marginLeft"))||e.getBoundingClientRect().left-ue(e,{marginLeft:0},function(){return e.getBoundingClientRect().left}))+"px"}),k.each({margin:"",padding:"",border:"Width"},function(i,o){k.cssHooks[i+o]={expand:function(e){for(var t=0,n={},r="string"==typeof e?e.split(" "):[e];t<4;t++)n[i+re[t]+o]=r[t]||r[t-2]||r[0];return n}},"margin"!==i&&(k.cssHooks[i+o].set=Ze)}),k.fn.extend({css:function(e,t){return _(this,function(e,t,n){var r,i,o={},a=0;if(Array.isArray(t)){for(r=Fe(e),i=t.length;a<i;a++)o[t[a]]=k.css(e,t[a],!1,r);return o}return void 0!==n?k.style(e,t,n):k.css(e,t)},e,t,1<arguments.length)}}),((k.Tween=nt).prototype={constructor:nt,init:function(e,t,n,r,i,o){this.elem=e,this.prop=n,this.easing=i||k.easing._default,this.options=t,this.start=this.now=this.cur(),this.end=r,this.unit=o||(k.cssNumber[n]?"":"px")},cur:function(){var e=nt.propHooks[this.prop];return e&&e.get?e.get(this):nt.propHooks._default.get(this)},run:function(e){var t,n=nt.propHooks[this.prop];return this.options.duration?this.pos=t=k.easing[this.easing](e,this.options.duration*e,0,1,this.options.duration):this.pos=t=e,this.now=(this.end-this.start)*t+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),n&&n.set?n.set(this):nt.propHooks._default.set(this),this}}).init.prototype=nt.prototype,(nt.propHooks={_default:{get:function(e){var t;return 1!==e.elem.nodeType||null!=e.elem[e.prop]&&null==e.elem.style[e.prop]?e.elem[e.prop]:(t=k.css(e.elem,e.prop,""))&&"auto"!==t?t:0},set:function(e){k.fx.step[e.prop]?k.fx.step[e.prop](e):1!==e.elem.nodeType||!k.cssHooks[e.prop]&&null==e.elem.style[Ge(e.prop)]?e.elem[e.prop]=e.now:k.style(e.elem,e.prop,e.now+e.unit)}}}).scrollTop=nt.propHooks.scrollLeft={set:function(e){e.elem.nodeType&&e.elem.parentNode&&(e.elem[e.prop]=e.now)}},k.easing={linear:function(e){return e},swing:function(e){return.5-Math.cos(e*Math.PI)/2},_default:"swing"},k.fx=nt.prototype.init,k.fx.step={};var rt,it,ot,at,st=/^(?:toggle|show|hide)$/,ut=/queueHooks$/;function lt(){it&&(!1===E.hidden&&C.requestAnimationFrame?C.requestAnimationFrame(lt):C.setTimeout(lt,k.fx.interval),k.fx.tick())}function ct(){return C.setTimeout(function(){rt=void 0}),rt=Date.now()}function ft(e,t){var n,r=0,i={height:e};for(t=t?1:0;r<4;r+=2-t)i["margin"+(n=re[r])]=i["padding"+n]=e;return t&&(i.opacity=i.width=e),i}function pt(e,t,n){for(var r,i=(dt.tweeners[t]||[]).concat(dt.tweeners["*"]),o=0,a=i.length;o<a;o++)if(r=i[o].call(n,t,e))return r}function dt(o,e,t){var n,a,r=0,i=dt.prefilters.length,s=k.Deferred().always(function(){delete u.elem}),u=function(){if(a)return!1;for(var e=rt||ct(),t=Math.max(0,l.startTime+l.duration-e),n=1-(t/l.duration||0),r=0,i=l.tweens.length;r<i;r++)l.tweens[r].run(n);return s.notifyWith(o,[l,n,t]),n<1&&i?t:(i||s.notifyWith(o,[l,1,0]),s.resolveWith(o,[l]),!1)},l=s.promise({elem:o,props:k.extend({},e),opts:k.extend(!0,{specialEasing:{},easing:k.easing._default},t),originalProperties:e,originalOptions:t,startTime:rt||ct(),duration:t.duration,tweens:[],createTween:function(e,t){var n=k.Tween(o,l.opts,e,t,l.opts.specialEasing[e]||l.opts.easing);return l.tweens.push(n),n},stop:function(e){var t=0,n=e?l.tweens.length:0;if(a)return this;for(a=!0;t<n;t++)l.tweens[t].run(1);return e?(s.notifyWith(o,[l,1,0]),s.resolveWith(o,[l,e])):s.rejectWith(o,[l,e]),this}}),c=l.props;for(!function(e,t){var n,r,i,o,a;for(n in e)if(i=t[r=V(n)],o=e[n],Array.isArray(o)&&(i=o[1],o=e[n]=o[0]),n!==r&&(e[r]=o,delete e[n]),(a=k.cssHooks[r])&&"expand"in a)for(n in o=a.expand(o),delete e[r],o)n in e||(e[n]=o[n],t[n]=i);else t[r]=i}(c,l.opts.specialEasing);r<i;r++)if(n=dt.prefilters[r].call(l,o,c,l.opts))return m(n.stop)&&(k._queueHooks(l.elem,l.opts.queue).stop=n.stop.bind(n)),n;return k.map(c,pt,l),m(l.opts.start)&&l.opts.start.call(o,l),l.progress(l.opts.progress).done(l.opts.done,l.opts.complete).fail(l.opts.fail).always(l.opts.always),k.fx.timer(k.extend(u,{elem:o,anim:l,queue:l.opts.queue})),l}k.Animation=k.extend(dt,{tweeners:{"*":[function(e,t){var n=this.createTween(e,t);return le(n.elem,e,ne.exec(t),n),n}]},tweener:function(e,t){m(e)?(t=e,e=["*"]):e=e.match(R);for(var n,r=0,i=e.length;r<i;r++)n=e[r],dt.tweeners[n]=dt.tweeners[n]||[],dt.tweeners[n].unshift(t)},prefilters:[function(e,t,n){var r,i,o,a,s,u,l,c,f="width"in t||"height"in t,p=this,d={},h=e.style,g=e.nodeType&&se(e),v=Q.get(e,"fxshow");for(r in n.queue||(null==(a=k._queueHooks(e,"fx")).unqueued&&(a.unqueued=0,s=a.empty.fire,a.empty.fire=function(){a.unqueued||s()}),a.unqueued++,p.always(function(){p.always(function(){a.unqueued--,k.queue(e,"fx").length||a.empty.fire()})})),t)if(i=t[r],st.test(i)){if(delete t[r],o=o||"toggle"===i,i===(g?"hide":"show")){if("show"!==i||!v||void 0===v[r])continue;g=!0}d[r]=v&&v[r]||k.style(e,r)}if((u=!k.isEmptyObject(t))||!k.isEmptyObject(d))for(r in f&&1===e.nodeType&&(n.overflow=[h.overflow,h.overflowX,h.overflowY],null==(l=v&&v.display)&&(l=Q.get(e,"display")),"none"===(c=k.css(e,"display"))&&(l?c=l:(fe([e],!0),l=e.style.display||l,c=k.css(e,"display"),fe([e]))),("inline"===c||"inline-block"===c&&null!=l)&&"none"===k.css(e,"float")&&(u||(p.done(function(){h.display=l}),null==l&&(c=h.display,l="none"===c?"":c)),h.display="inline-block")),n.overflow&&(h.overflow="hidden",p.always(function(){h.overflow=n.overflow[0],h.overflowX=n.overflow[1],h.overflowY=n.overflow[2]})),u=!1,d)u||(v?"hidden"in v&&(g=v.hidden):v=Q.access(e,"fxshow",{display:l}),o&&(v.hidden=!g),g&&fe([e],!0),p.done(function(){for(r in g||fe([e]),Q.remove(e,"fxshow"),d)k.style(e,r,d[r])})),u=pt(g?v[r]:0,r,p),r in v||(v[r]=u.start,g&&(u.end=u.start,u.start=0))}],prefilter:function(e,t){t?dt.prefilters.unshift(e):dt.prefilters.push(e)}}),k.speed=function(e,t,n){var r=e&&"object"==typeof e?k.extend({},e):{complete:n||!n&&t||m(e)&&e,duration:e,easing:n&&t||t&&!m(t)&&t};return k.fx.off?r.duration=0:"number"!=typeof r.duration&&(r.duration in k.fx.speeds?r.duration=k.fx.speeds[r.duration]:r.duration=k.fx.speeds._default),null!=r.queue&&!0!==r.queue||(r.queue="fx"),r.old=r.complete,r.complete=function(){m(r.old)&&r.old.call(this),r.queue&&k.dequeue(this,r.queue)},r},k.fn.extend({fadeTo:function(e,t,n,r){return this.filter(se).css("opacity",0).show().end().animate({opacity:t},e,n,r)},animate:function(t,e,n,r){var i=k.isEmptyObject(t),o=k.speed(e,n,r),a=function(){var e=dt(this,k.extend({},t),o);(i||Q.get(this,"finish"))&&e.stop(!0)};return a.finish=a,i||!1===o.queue?this.each(a):this.queue(o.queue,a)},stop:function(i,e,o){var a=function(e){var t=e.stop;delete e.stop,t(o)};return"string"!=typeof i&&(o=e,e=i,i=void 0),e&&!1!==i&&this.queue(i||"fx",[]),this.each(function(){var e=!0,t=null!=i&&i+"queueHooks",n=k.timers,r=Q.get(this);if(t)r[t]&&r[t].stop&&a(r[t]);else for(t in r)r[t]&&r[t].stop&&ut.test(t)&&a(r[t]);for(t=n.length;t--;)n[t].elem!==this||null!=i&&n[t].queue!==i||(n[t].anim.stop(o),e=!1,n.splice(t,1));!e&&o||k.dequeue(this,i)})},finish:function(a){return!1!==a&&(a=a||"fx"),this.each(function(){var e,t=Q.get(this),n=t[a+"queue"],r=t[a+"queueHooks"],i=k.timers,o=n?n.length:0;for(t.finish=!0,k.queue(this,a,[]),r&&r.stop&&r.stop.call(this,!0),e=i.length;e--;)i[e].elem===this&&i[e].queue===a&&(i[e].anim.stop(!0),i.splice(e,1));for(e=0;e<o;e++)n[e]&&n[e].finish&&n[e].finish.call(this);delete t.finish})}}),k.each(["toggle","show","hide"],function(e,r){var i=k.fn[r];k.fn[r]=function(e,t,n){return null==e||"boolean"==typeof e?i.apply(this,arguments):this.animate(ft(r,!0),e,t,n)}}),k.each({slideDown:ft("show"),slideUp:ft("hide"),slideToggle:ft("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(e,r){k.fn[e]=function(e,t,n){return this.animate(r,e,t,n)}}),k.timers=[],k.fx.tick=function(){var e,t=0,n=k.timers;for(rt=Date.now();t<n.length;t++)(e=n[t])()||n[t]!==e||n.splice(t--,1);n.length||k.fx.stop(),rt=void 0},k.fx.timer=function(e){k.timers.push(e),k.fx.start()},k.fx.interval=13,k.fx.start=function(){it||(it=!0,lt())},k.fx.stop=function(){it=null},k.fx.speeds={slow:600,fast:200,_default:400},k.fn.delay=function(r,e){return r=k.fx&&k.fx.speeds[r]||r,e=e||"fx",this.queue(e,function(e,t){var n=C.setTimeout(e,r);t.stop=function(){C.clearTimeout(n)}})},ot=E.createElement("input"),at=E.createElement("select").appendChild(E.createElement("option")),ot.type="checkbox",y.checkOn=""!==ot.value,y.optSelected=at.selected,(ot=E.createElement("input")).value="t",ot.type="radio",y.radioValue="t"===ot.value;var ht,gt=k.expr.attrHandle;k.fn.extend({attr:function(e,t){return _(this,k.attr,e,t,1<arguments.length)},removeAttr:function(e){return this.each(function(){k.removeAttr(this,e)})}}),k.extend({attr:function(e,t,n){var r,i,o=e.nodeType;if(3!==o&&8!==o&&2!==o)return"undefined"==typeof e.getAttribute?k.prop(e,t,n):(1===o&&k.isXMLDoc(e)||(i=k.attrHooks[t.toLowerCase()]||(k.expr.match.bool.test(t)?ht:void 0)),void 0!==n?null===n?void k.removeAttr(e,t):i&&"set"in i&&void 0!==(r=i.set(e,n,t))?r:(e.setAttribute(t,n+""),n):i&&"get"in i&&null!==(r=i.get(e,t))?r:null==(r=k.find.attr(e,t))?void 0:r)},attrHooks:{type:{set:function(e,t){if(!y.radioValue&&"radio"===t&&A(e,"input")){var n=e.value;return e.setAttribute("type",t),n&&(e.value=n),t}}}},removeAttr:function(e,t){var n,r=0,i=t&&t.match(R);if(i&&1===e.nodeType)while(n=i[r++])e.removeAttribute(n)}}),ht={set:function(e,t,n){return!1===t?k.removeAttr(e,n):e.setAttribute(n,n),n}},k.each(k.expr.match.bool.source.match(/\w+/g),function(e,t){var a=gt[t]||k.find.attr;gt[t]=function(e,t,n){var r,i,o=t.toLowerCase();return n||(i=gt[o],gt[o]=r,r=null!=a(e,t,n)?o:null,gt[o]=i),r}});var vt=/^(?:input|select|textarea|button)$/i,yt=/^(?:a|area)$/i;function mt(e){return(e.match(R)||[]).join(" ")}function xt(e){return e.getAttribute&&e.getAttribute("class")||""}function bt(e){return Array.isArray(e)?e:"string"==typeof e&&e.match(R)||[]}k.fn.extend({prop:function(e,t){return _(this,k.prop,e,t,1<arguments.length)},removeProp:function(e){return this.each(function(){delete this[k.propFix[e]||e]})}}),k.extend({prop:function(e,t,n){var r,i,o=e.nodeType;if(3!==o&&8!==o&&2!==o)return 1===o&&k.isXMLDoc(e)||(t=k.propFix[t]||t,i=k.propHooks[t]),void 0!==n?i&&"set"in i&&void 0!==(r=i.set(e,n,t))?r:e[t]=n:i&&"get"in i&&null!==(r=i.get(e,t))?r:e[t]},propHooks:{tabIndex:{get:function(e){var t=k.find.attr(e,"tabindex");return t?parseInt(t,10):vt.test(e.nodeName)||yt.test(e.nodeName)&&e.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),y.optSelected||(k.propHooks.selected={get:function(e){var t=e.parentNode;return t&&t.parentNode&&t.parentNode.selectedIndex,null},set:function(e){var t=e.parentNode;t&&(t.selectedIndex,t.parentNode&&t.parentNode.selectedIndex)}}),k.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){k.propFix[this.toLowerCase()]=this}),k.fn.extend({addClass:function(t){var e,n,r,i,o,a,s,u=0;if(m(t))return this.each(function(e){k(this).addClass(t.call(this,e,xt(this)))});if((e=bt(t)).length)while(n=this[u++])if(i=xt(n),r=1===n.nodeType&&" "+mt(i)+" "){a=0;while(o=e[a++])r.indexOf(" "+o+" ")<0&&(r+=o+" ");i!==(s=mt(r))&&n.setAttribute("class",s)}return this},removeClass:function(t){var e,n,r,i,o,a,s,u=0;if(m(t))return this.each(function(e){k(this).removeClass(t.call(this,e,xt(this)))});if(!arguments.length)return this.attr("class","");if((e=bt(t)).length)while(n=this[u++])if(i=xt(n),r=1===n.nodeType&&" "+mt(i)+" "){a=0;while(o=e[a++])while(-1<r.indexOf(" "+o+" "))r=r.replace(" "+o+" "," ");i!==(s=mt(r))&&n.setAttribute("class",s)}return this},toggleClass:function(i,t){var o=typeof i,a="string"===o||Array.isArray(i);return"boolean"==typeof t&&a?t?this.addClass(i):this.removeClass(i):m(i)?this.each(function(e){k(this).toggleClass(i.call(this,e,xt(this),t),t)}):this.each(function(){var e,t,n,r;if(a){t=0,n=k(this),r=bt(i);while(e=r[t++])n.hasClass(e)?n.removeClass(e):n.addClass(e)}else void 0!==i&&"boolean"!==o||((e=xt(this))&&Q.set(this,"__className__",e),this.setAttribute&&this.setAttribute("class",e||!1===i?"":Q.get(this,"__className__")||""))})},hasClass:function(e){var t,n,r=0;t=" "+e+" ";while(n=this[r++])if(1===n.nodeType&&-1<(" "+mt(xt(n))+" ").indexOf(t))return!0;return!1}});var wt=/\r/g;k.fn.extend({val:function(n){var r,e,i,t=this[0];return arguments.length?(i=m(n),this.each(function(e){var t;1===this.nodeType&&(null==(t=i?n.call(this,e,k(this).val()):n)?t="":"number"==typeof t?t+="":Array.isArray(t)&&(t=k.map(t,function(e){return null==e?"":e+""})),(r=k.valHooks[this.type]||k.valHooks[this.nodeName.toLowerCase()])&&"set"in r&&void 0!==r.set(this,t,"value")||(this.value=t))})):t?(r=k.valHooks[t.type]||k.valHooks[t.nodeName.toLowerCase()])&&"get"in r&&void 0!==(e=r.get(t,"value"))?e:"string"==typeof(e=t.value)?e.replace(wt,""):null==e?"":e:void 0}}),k.extend({valHooks:{option:{get:function(e){var t=k.find.attr(e,"value");return null!=t?t:mt(k.text(e))}},select:{get:function(e){var t,n,r,i=e.options,o=e.selectedIndex,a="select-one"===e.type,s=a?null:[],u=a?o+1:i.length;for(r=o<0?u:a?o:0;r<u;r++)if(((n=i[r]).selected||r===o)&&!n.disabled&&(!n.parentNode.disabled||!A(n.parentNode,"optgroup"))){if(t=k(n).val(),a)return t;s.push(t)}return s},set:function(e,t){var n,r,i=e.options,o=k.makeArray(t),a=i.length;while(a--)((r=i[a]).selected=-1<k.inArray(k.valHooks.option.get(r),o))&&(n=!0);return n||(e.selectedIndex=-1),o}}}}),k.each(["radio","checkbox"],function(){k.valHooks[this]={set:function(e,t){if(Array.isArray(t))return e.checked=-1<k.inArray(k(e).val(),t)}},y.checkOn||(k.valHooks[this].get=function(e){return null===e.getAttribute("value")?"on":e.value})}),y.focusin="onfocusin"in C;var Tt=/^(?:focusinfocus|focusoutblur)$/,Ct=function(e){e.stopPropagation()};k.extend(k.event,{trigger:function(e,t,n,r){var i,o,a,s,u,l,c,f,p=[n||E],d=v.call(e,"type")?e.type:e,h=v.call(e,"namespace")?e.namespace.split("."):[];if(o=f=a=n=n||E,3!==n.nodeType&&8!==n.nodeType&&!Tt.test(d+k.event.triggered)&&(-1<d.indexOf(".")&&(d=(h=d.split(".")).shift(),h.sort()),u=d.indexOf(":")<0&&"on"+d,(e=e[k.expando]?e:new k.Event(d,"object"==typeof e&&e)).isTrigger=r?2:3,e.namespace=h.join("."),e.rnamespace=e.namespace?new RegExp("(^|\\.)"+h.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,e.result=void 0,e.target||(e.target=n),t=null==t?[e]:k.makeArray(t,[e]),c=k.event.special[d]||{},r||!c.trigger||!1!==c.trigger.apply(n,t))){if(!r&&!c.noBubble&&!x(n)){for(s=c.delegateType||d,Tt.test(s+d)||(o=o.parentNode);o;o=o.parentNode)p.push(o),a=o;a===(n.ownerDocument||E)&&p.push(a.defaultView||a.parentWindow||C)}i=0;while((o=p[i++])&&!e.isPropagationStopped())f=o,e.type=1<i?s:c.bindType||d,(l=(Q.get(o,"events")||{})[e.type]&&Q.get(o,"handle"))&&l.apply(o,t),(l=u&&o[u])&&l.apply&&G(o)&&(e.result=l.apply(o,t),!1===e.result&&e.preventDefault());return e.type=d,r||e.isDefaultPrevented()||c._default&&!1!==c._default.apply(p.pop(),t)||!G(n)||u&&m(n[d])&&!x(n)&&((a=n[u])&&(n[u]=null),k.event.triggered=d,e.isPropagationStopped()&&f.addEventListener(d,Ct),n[d](),e.isPropagationStopped()&&f.removeEventListener(d,Ct),k.event.triggered=void 0,a&&(n[u]=a)),e.result}},simulate:function(e,t,n){var r=k.extend(new k.Event,n,{type:e,isSimulated:!0});k.event.trigger(r,null,t)}}),k.fn.extend({trigger:function(e,t){return this.each(function(){k.event.trigger(e,t,this)})},triggerHandler:function(e,t){var n=this[0];if(n)return k.event.trigger(e,t,n,!0)}}),y.focusin||k.each({focus:"focusin",blur:"focusout"},function(n,r){var i=function(e){k.event.simulate(r,e.target,k.event.fix(e))};k.event.special[r]={setup:function(){var e=this.ownerDocument||this,t=Q.access(e,r);t||e.addEventListener(n,i,!0),Q.access(e,r,(t||0)+1)},teardown:function(){var e=this.ownerDocument||this,t=Q.access(e,r)-1;t?Q.access(e,r,t):(e.removeEventListener(n,i,!0),Q.remove(e,r))}}});var Et=C.location,kt=Date.now(),St=/\?/;k.parseXML=function(e){var t;if(!e||"string"!=typeof e)return null;try{t=(new C.DOMParser).parseFromString(e,"text/xml")}catch(e){t=void 0}return t&&!t.getElementsByTagName("parsererror").length||k.error("Invalid XML: "+e),t};var Nt=/\[\]$/,At=/\r?\n/g,Dt=/^(?:submit|button|image|reset|file)$/i,jt=/^(?:input|select|textarea|keygen)/i;function qt(n,e,r,i){var t;if(Array.isArray(e))k.each(e,function(e,t){r||Nt.test(n)?i(n,t):qt(n+"["+("object"==typeof t&&null!=t?e:"")+"]",t,r,i)});else if(r||"object"!==w(e))i(n,e);else for(t in e)qt(n+"["+t+"]",e[t],r,i)}k.param=function(e,t){var n,r=[],i=function(e,t){var n=m(t)?t():t;r[r.length]=encodeURIComponent(e)+"="+encodeURIComponent(null==n?"":n)};if(null==e)return"";if(Array.isArray(e)||e.jquery&&!k.isPlainObject(e))k.each(e,function(){i(this.name,this.value)});else for(n in e)qt(n,e[n],t,i);return r.join("&")},k.fn.extend({serialize:function(){return k.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var e=k.prop(this,"elements");return e?k.makeArray(e):this}).filter(function(){var e=this.type;return this.name&&!k(this).is(":disabled")&&jt.test(this.nodeName)&&!Dt.test(e)&&(this.checked||!pe.test(e))}).map(function(e,t){var n=k(this).val();return null==n?null:Array.isArray(n)?k.map(n,function(e){return{name:t.name,value:e.replace(At,"\r\n")}}):{name:t.name,value:n.replace(At,"\r\n")}}).get()}});var Lt=/%20/g,Ht=/#.*$/,Ot=/([?&])_=[^&]*/,Pt=/^(.*?):[ \t]*([^\r\n]*)$/gm,Rt=/^(?:GET|HEAD)$/,Mt=/^\/\//,It={},Wt={},$t="*/".concat("*"),Ft=E.createElement("a");function Bt(o){return function(e,t){"string"!=typeof e&&(t=e,e="*");var n,r=0,i=e.toLowerCase().match(R)||[];if(m(t))while(n=i[r++])"+"===n[0]?(n=n.slice(1)||"*",(o[n]=o[n]||[]).unshift(t)):(o[n]=o[n]||[]).push(t)}}function _t(t,i,o,a){var s={},u=t===Wt;function l(e){var r;return s[e]=!0,k.each(t[e]||[],function(e,t){var n=t(i,o,a);return"string"!=typeof n||u||s[n]?u?!(r=n):void 0:(i.dataTypes.unshift(n),l(n),!1)}),r}return l(i.dataTypes[0])||!s["*"]&&l("*")}function zt(e,t){var n,r,i=k.ajaxSettings.flatOptions||{};for(n in t)void 0!==t[n]&&((i[n]?e:r||(r={}))[n]=t[n]);return r&&k.extend(!0,e,r),e}Ft.href=Et.href,k.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:Et.href,type:"GET",isLocal:/^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Et.protocol),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":$t,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":JSON.parse,"text xml":k.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(e,t){return t?zt(zt(e,k.ajaxSettings),t):zt(k.ajaxSettings,e)},ajaxPrefilter:Bt(It),ajaxTransport:Bt(Wt),ajax:function(e,t){"object"==typeof e&&(t=e,e=void 0),t=t||{};var c,f,p,n,d,r,h,g,i,o,v=k.ajaxSetup({},t),y=v.context||v,m=v.context&&(y.nodeType||y.jquery)?k(y):k.event,x=k.Deferred(),b=k.Callbacks("once memory"),w=v.statusCode||{},a={},s={},u="canceled",T={readyState:0,getResponseHeader:function(e){var t;if(h){if(!n){n={};while(t=Pt.exec(p))n[t[1].toLowerCase()+" "]=(n[t[1].toLowerCase()+" "]||[]).concat(t[2])}t=n[e.toLowerCase()+" "]}return null==t?null:t.join(", ")},getAllResponseHeaders:function(){return h?p:null},setRequestHeader:function(e,t){return null==h&&(e=s[e.toLowerCase()]=s[e.toLowerCase()]||e,a[e]=t),this},overrideMimeType:function(e){return null==h&&(v.mimeType=e),this},statusCode:function(e){var t;if(e)if(h)T.always(e[T.status]);else for(t in e)w[t]=[w[t],e[t]];return this},abort:function(e){var t=e||u;return c&&c.abort(t),l(0,t),this}};if(x.promise(T),v.url=((e||v.url||Et.href)+"").replace(Mt,Et.protocol+"//"),v.type=t.method||t.type||v.method||v.type,v.dataTypes=(v.dataType||"*").toLowerCase().match(R)||[""],null==v.crossDomain){r=E.createElement("a");try{r.href=v.url,r.href=r.href,v.crossDomain=Ft.protocol+"//"+Ft.host!=r.protocol+"//"+r.host}catch(e){v.crossDomain=!0}}if(v.data&&v.processData&&"string"!=typeof v.data&&(v.data=k.param(v.data,v.traditional)),_t(It,v,t,T),h)return T;for(i in(g=k.event&&v.global)&&0==k.active++&&k.event.trigger("ajaxStart"),v.type=v.type.toUpperCase(),v.hasContent=!Rt.test(v.type),f=v.url.replace(Ht,""),v.hasContent?v.data&&v.processData&&0===(v.contentType||"").indexOf("application/x-www-form-urlencoded")&&(v.data=v.data.replace(Lt,"+")):(o=v.url.slice(f.length),v.data&&(v.processData||"string"==typeof v.data)&&(f+=(St.test(f)?"&":"?")+v.data,delete v.data),!1===v.cache&&(f=f.replace(Ot,"$1"),o=(St.test(f)?"&":"?")+"_="+kt+++o),v.url=f+o),v.ifModified&&(k.lastModified[f]&&T.setRequestHeader("If-Modified-Since",k.lastModified[f]),k.etag[f]&&T.setRequestHeader("If-None-Match",k.etag[f])),(v.data&&v.hasContent&&!1!==v.contentType||t.contentType)&&T.setRequestHeader("Content-Type",v.contentType),T.setRequestHeader("Accept",v.dataTypes[0]&&v.accepts[v.dataTypes[0]]?v.accepts[v.dataTypes[0]]+("*"!==v.dataTypes[0]?", "+$t+"; q=0.01":""):v.accepts["*"]),v.headers)T.setRequestHeader(i,v.headers[i]);if(v.beforeSend&&(!1===v.beforeSend.call(y,T,v)||h))return T.abort();if(u="abort",b.add(v.complete),T.done(v.success),T.fail(v.error),c=_t(Wt,v,t,T)){if(T.readyState=1,g&&m.trigger("ajaxSend",[T,v]),h)return T;v.async&&0<v.timeout&&(d=C.setTimeout(function(){T.abort("timeout")},v.timeout));try{h=!1,c.send(a,l)}catch(e){if(h)throw e;l(-1,e)}}else l(-1,"No Transport");function l(e,t,n,r){var i,o,a,s,u,l=t;h||(h=!0,d&&C.clearTimeout(d),c=void 0,p=r||"",T.readyState=0<e?4:0,i=200<=e&&e<300||304===e,n&&(s=function(e,t,n){var r,i,o,a,s=e.contents,u=e.dataTypes;while("*"===u[0])u.shift(),void 0===r&&(r=e.mimeType||t.getResponseHeader("Content-Type"));if(r)for(i in s)if(s[i]&&s[i].test(r)){u.unshift(i);break}if(u[0]in n)o=u[0];else{for(i in n){if(!u[0]||e.converters[i+" "+u[0]]){o=i;break}a||(a=i)}o=o||a}if(o)return o!==u[0]&&u.unshift(o),n[o]}(v,T,n)),s=function(e,t,n,r){var i,o,a,s,u,l={},c=e.dataTypes.slice();if(c[1])for(a in e.converters)l[a.toLowerCase()]=e.converters[a];o=c.shift();while(o)if(e.responseFields[o]&&(n[e.responseFields[o]]=t),!u&&r&&e.dataFilter&&(t=e.dataFilter(t,e.dataType)),u=o,o=c.shift())if("*"===o)o=u;else if("*"!==u&&u!==o){if(!(a=l[u+" "+o]||l["* "+o]))for(i in l)if((s=i.split(" "))[1]===o&&(a=l[u+" "+s[0]]||l["* "+s[0]])){!0===a?a=l[i]:!0!==l[i]&&(o=s[0],c.unshift(s[1]));break}if(!0!==a)if(a&&e["throws"])t=a(t);else try{t=a(t)}catch(e){return{state:"parsererror",error:a?e:"No conversion from "+u+" to "+o}}}return{state:"success",data:t}}(v,s,T,i),i?(v.ifModified&&((u=T.getResponseHeader("Last-Modified"))&&(k.lastModified[f]=u),(u=T.getResponseHeader("etag"))&&(k.etag[f]=u)),204===e||"HEAD"===v.type?l="nocontent":304===e?l="notmodified":(l=s.state,o=s.data,i=!(a=s.error))):(a=l,!e&&l||(l="error",e<0&&(e=0))),T.status=e,T.statusText=(t||l)+"",i?x.resolveWith(y,[o,l,T]):x.rejectWith(y,[T,l,a]),T.statusCode(w),w=void 0,g&&m.trigger(i?"ajaxSuccess":"ajaxError",[T,v,i?o:a]),b.fireWith(y,[T,l]),g&&(m.trigger("ajaxComplete",[T,v]),--k.active||k.event.trigger("ajaxStop")))}return T},getJSON:function(e,t,n){return k.get(e,t,n,"json")},getScript:function(e,t){return k.get(e,void 0,t,"script")}}),k.each(["get","post"],function(e,i){k[i]=function(e,t,n,r){return m(t)&&(r=r||n,n=t,t=void 0),k.ajax(k.extend({url:e,type:i,dataType:r,data:t,success:n},k.isPlainObject(e)&&e))}}),k._evalUrl=function(e,t){return k.ajax({url:e,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,converters:{"text script":function(){}},dataFilter:function(e){k.globalEval(e,t)}})},k.fn.extend({wrapAll:function(e){var t;return this[0]&&(m(e)&&(e=e.call(this[0])),t=k(e,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&t.insertBefore(this[0]),t.map(function(){var e=this;while(e.firstElementChild)e=e.firstElementChild;return e}).append(this)),this},wrapInner:function(n){return m(n)?this.each(function(e){k(this).wrapInner(n.call(this,e))}):this.each(function(){var e=k(this),t=e.contents();t.length?t.wrapAll(n):e.append(n)})},wrap:function(t){var n=m(t);return this.each(function(e){k(this).wrapAll(n?t.call(this,e):t)})},unwrap:function(e){return this.parent(e).not("body").each(function(){k(this).replaceWith(this.childNodes)}),this}}),k.expr.pseudos.hidden=function(e){return!k.expr.pseudos.visible(e)},k.expr.pseudos.visible=function(e){return!!(e.offsetWidth||e.offsetHeight||e.getClientRects().length)},k.ajaxSettings.xhr=function(){try{return new C.XMLHttpRequest}catch(e){}};var Ut={0:200,1223:204},Xt=k.ajaxSettings.xhr();y.cors=!!Xt&&"withCredentials"in Xt,y.ajax=Xt=!!Xt,k.ajaxTransport(function(i){var o,a;if(y.cors||Xt&&!i.crossDomain)return{send:function(e,t){var n,r=i.xhr();if(r.open(i.type,i.url,i.async,i.username,i.password),i.xhrFields)for(n in i.xhrFields)r[n]=i.xhrFields[n];for(n in i.mimeType&&r.overrideMimeType&&r.overrideMimeType(i.mimeType),i.crossDomain||e["X-Requested-With"]||(e["X-Requested-With"]="XMLHttpRequest"),e)r.setRequestHeader(n,e[n]);o=function(e){return function(){o&&(o=a=r.onload=r.onerror=r.onabort=r.ontimeout=r.onreadystatechange=null,"abort"===e?r.abort():"error"===e?"number"!=typeof r.status?t(0,"error"):t(r.status,r.statusText):t(Ut[r.status]||r.status,r.statusText,"text"!==(r.responseType||"text")||"string"!=typeof r.responseText?{binary:r.response}:{text:r.responseText},r.getAllResponseHeaders()))}},r.onload=o(),a=r.onerror=r.ontimeout=o("error"),void 0!==r.onabort?r.onabort=a:r.onreadystatechange=function(){4===r.readyState&&C.setTimeout(function(){o&&a()})},o=o("abort");try{r.send(i.hasContent&&i.data||null)}catch(e){if(o)throw e}},abort:function(){o&&o()}}}),k.ajaxPrefilter(function(e){e.crossDomain&&(e.contents.script=!1)}),k.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(e){return k.globalEval(e),e}}}),k.ajaxPrefilter("script",function(e){void 0===e.cache&&(e.cache=!1),e.crossDomain&&(e.type="GET")}),k.ajaxTransport("script",function(n){var r,i;if(n.crossDomain||n.scriptAttrs)return{send:function(e,t){r=k("<script>").attr(n.scriptAttrs||{}).prop({charset:n.scriptCharset,src:n.url}).on("load error",i=function(e){r.remove(),i=null,e&&t("error"===e.type?404:200,e.type)}),E.head.appendChild(r[0])},abort:function(){i&&i()}}});var Vt,Gt=[],Yt=/(=)\?(?=&|$)|\?\?/;k.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var e=Gt.pop()||k.expando+"_"+kt++;return this[e]=!0,e}}),k.ajaxPrefilter("json jsonp",function(e,t,n){var r,i,o,a=!1!==e.jsonp&&(Yt.test(e.url)?"url":"string"==typeof e.data&&0===(e.contentType||"").indexOf("application/x-www-form-urlencoded")&&Yt.test(e.data)&&"data");if(a||"jsonp"===e.dataTypes[0])return r=e.jsonpCallback=m(e.jsonpCallback)?e.jsonpCallback():e.jsonpCallback,a?e[a]=e[a].replace(Yt,"$1"+r):!1!==e.jsonp&&(e.url+=(St.test(e.url)?"&":"?")+e.jsonp+"="+r),e.converters["script json"]=function(){return o||k.error(r+" was not called"),o[0]},e.dataTypes[0]="json",i=C[r],C[r]=function(){o=arguments},n.always(function(){void 0===i?k(C).removeProp(r):C[r]=i,e[r]&&(e.jsonpCallback=t.jsonpCallback,Gt.push(r)),o&&m(i)&&i(o[0]),o=i=void 0}),"script"}),y.createHTMLDocument=((Vt=E.implementation.createHTMLDocument("").body).innerHTML="<form></form><form></form>",2===Vt.childNodes.length),k.parseHTML=function(e,t,n){return"string"!=typeof e?[]:("boolean"==typeof t&&(n=t,t=!1),t||(y.createHTMLDocument?((r=(t=E.implementation.createHTMLDocument("")).createElement("base")).href=E.location.href,t.head.appendChild(r)):t=E),o=!n&&[],(i=D.exec(e))?[t.createElement(i[1])]:(i=we([e],t,o),o&&o.length&&k(o).remove(),k.merge([],i.childNodes)));var r,i,o},k.fn.load=function(e,t,n){var r,i,o,a=this,s=e.indexOf(" ");return-1<s&&(r=mt(e.slice(s)),e=e.slice(0,s)),m(t)?(n=t,t=void 0):t&&"object"==typeof t&&(i="POST"),0<a.length&&k.ajax({url:e,type:i||"GET",dataType:"html",data:t}).done(function(e){o=arguments,a.html(r?k("<div>").append(k.parseHTML(e)).find(r):e)}).always(n&&function(e,t){a.each(function(){n.apply(this,o||[e.responseText,t,e])})}),this},k.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(e,t){k.fn[t]=function(e){return this.on(t,e)}}),k.expr.pseudos.animated=function(t){return k.grep(k.timers,function(e){return t===e.elem}).length},k.offset={setOffset:function(e,t,n){var r,i,o,a,s,u,l=k.css(e,"position"),c=k(e),f={};"static"===l&&(e.style.position="relative"),s=c.offset(),o=k.css(e,"top"),u=k.css(e,"left"),("absolute"===l||"fixed"===l)&&-1<(o+u).indexOf("auto")?(a=(r=c.position()).top,i=r.left):(a=parseFloat(o)||0,i=parseFloat(u)||0),m(t)&&(t=t.call(e,n,k.extend({},s))),null!=t.top&&(f.top=t.top-s.top+a),null!=t.left&&(f.left=t.left-s.left+i),"using"in t?t.using.call(e,f):c.css(f)}},k.fn.extend({offset:function(t){if(arguments.length)return void 0===t?this:this.each(function(e){k.offset.setOffset(this,t,e)});var e,n,r=this[0];return r?r.getClientRects().length?(e=r.getBoundingClientRect(),n=r.ownerDocument.defaultView,{top:e.top+n.pageYOffset,left:e.left+n.pageXOffset}):{top:0,left:0}:void 0},position:function(){if(this[0]){var e,t,n,r=this[0],i={top:0,left:0};if("fixed"===k.css(r,"position"))t=r.getBoundingClientRect();else{t=this.offset(),n=r.ownerDocument,e=r.offsetParent||n.documentElement;while(e&&(e===n.body||e===n.documentElement)&&"static"===k.css(e,"position"))e=e.parentNode;e&&e!==r&&1===e.nodeType&&((i=k(e).offset()).top+=k.css(e,"borderTopWidth",!0),i.left+=k.css(e,"borderLeftWidth",!0))}return{top:t.top-i.top-k.css(r,"marginTop",!0),left:t.left-i.left-k.css(r,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var e=this.offsetParent;while(e&&"static"===k.css(e,"position"))e=e.offsetParent;return e||ie})}}),k.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(t,i){var o="pageYOffset"===i;k.fn[t]=function(e){return _(this,function(e,t,n){var r;if(x(e)?r=e:9===e.nodeType&&(r=e.defaultView),void 0===n)return r?r[i]:e[t];r?r.scrollTo(o?r.pageXOffset:n,o?n:r.pageYOffset):e[t]=n},t,e,arguments.length)}}),k.each(["top","left"],function(e,n){k.cssHooks[n]=ze(y.pixelPosition,function(e,t){if(t)return t=_e(e,n),$e.test(t)?k(e).position()[n]+"px":t})}),k.each({Height:"height",Width:"width"},function(a,s){k.each({padding:"inner"+a,content:s,"":"outer"+a},function(r,o){k.fn[o]=function(e,t){var n=arguments.length&&(r||"boolean"!=typeof e),i=r||(!0===e||!0===t?"margin":"border");return _(this,function(e,t,n){var r;return x(e)?0===o.indexOf("outer")?e["inner"+a]:e.document.documentElement["client"+a]:9===e.nodeType?(r=e.documentElement,Math.max(e.body["scroll"+a],r["scroll"+a],e.body["offset"+a],r["offset"+a],r["client"+a])):void 0===n?k.css(e,t,i):k.style(e,t,n,i)},s,n?e:void 0,n)}})}),k.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "),function(e,n){k.fn[n]=function(e,t){return 0<arguments.length?this.on(n,null,e,t):this.trigger(n)}}),k.fn.extend({hover:function(e,t){return this.mouseenter(e).mouseleave(t||e)}}),k.fn.extend({bind:function(e,t,n){return this.on(e,null,t,n)},unbind:function(e,t){return this.off(e,null,t)},delegate:function(e,t,n,r){return this.on(t,e,n,r)},undelegate:function(e,t,n){return 1===arguments.length?this.off(e,"**"):this.off(t,e||"**",n)}}),k.proxy=function(e,t){var n,r,i;if("string"==typeof t&&(n=e[t],t=e,e=n),m(e))return r=s.call(arguments,2),(i=function(){return e.apply(t||this,r.concat(s.call(arguments)))}).guid=e.guid=e.guid||k.guid++,i},k.holdReady=function(e){e?k.readyWait++:k.ready(!0)},k.isArray=Array.isArray,k.parseJSON=JSON.parse,k.nodeName=A,k.isFunction=m,k.isWindow=x,k.camelCase=V,k.type=w,k.now=Date.now,k.isNumeric=function(e){var t=k.type(e);return("number"===t||"string"===t)&&!isNaN(e-parseFloat(e))},"function"==typeof define&&define.amd&&define("jquery",[],function(){return k});var Qt=C.jQuery,Jt=C.$;return k.noConflict=function(e){return C.$===k&&(C.$=Jt),e&&C.jQuery===k&&(C.jQuery=Qt),k},e||(C.jQuery=C.$=k),k});

;(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('@angular/forms'), require('rxjs'), require('rxjs/operators')) :
    typeof define === 'function' && define.amd ? define('ngb', ['exports', '@angular/core', '@angular/common', '@angular/forms', 'rxjs', 'rxjs/operators'], factory) :
    (global = global || self, factory(global.ngb = {}, global.ng.core, global.ng.common, global.ng.forms, global.rxjs, global.rxjs.operators));
}(this, function (exports, core, common, forms, rxjs, operators) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m) return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @param {?} value
     * @return {?}
     */
    function toInteger(value) {
        return parseInt("" + value, 10);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    function toString(value) {
        return (value !== undefined && value !== null) ? "" + value : '';
    }
    /**
     * @param {?} value
     * @param {?} max
     * @param {?=} min
     * @return {?}
     */
    function getValueInRange(value, max, min) {
        if (min === void 0) { min = 0; }
        return Math.max(Math.min(value, max), min);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    function isString(value) {
        return typeof value === 'string';
    }
    /**
     * @param {?} value
     * @return {?}
     */
    function isNumber(value) {
        return !isNaN(toInteger(value));
    }
    /**
     * @param {?} value
     * @return {?}
     */
    function isInteger(value) {
        return typeof value === 'number' && isFinite(value) && Math.floor(value) === value;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    function isDefined(value) {
        return value !== undefined && value !== null;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    function padNumber(value) {
        if (isNumber(value)) {
            return ("0" + value).slice(-2);
        }
        else {
            return '';
        }
    }
    /**
     * @param {?} text
     * @return {?}
     */
    function regExpEscape(text) {
        return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
    }
    /**
     * @param {?} element
     * @param {?} className
     * @return {?}
     */
    function hasClassName(element, className) {
        return element && element.className && element.className.split &&
            element.className.split(/\s+/).indexOf(className) >= 0;
    }
    if (typeof Element !== 'undefined' && !Element.prototype.closest) {
        // Polyfill for ie10+
        if (!Element.prototype.matches) {
            // IE uses the non-standard name: msMatchesSelector
            Element.prototype.matches = ((/** @type {?} */ (Element.prototype))).msMatchesSelector || Element.prototype.webkitMatchesSelector;
        }
        Element.prototype.closest = (/**
         * @param {?} s
         * @return {?}
         */
        function (s) {
            /** @type {?} */
            var el = this;
            if (!document.documentElement.contains(el)) {
                return null;
            }
            do {
                if (el.matches(s)) {
                    return el;
                }
                el = el.parentElement || el.parentNode;
            } while (el !== null && el.nodeType === 1);
            return null;
        });
    }
    /**
     * @param {?} element
     * @param {?} selector
     * @return {?}
     */
    function closest(element, selector) {
        if (!selector) {
            return null;
        }
        return element.closest(selector);
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A configuration service for the [NgbAccordion](#/components/accordion/api#NgbAccordion) component.
     *
     * You can inject this service, typically in your root component, and customize its properties
     * to provide default values for all accordions used in the application.
     */
    var NgbAccordionConfig = /** @class */ (function () {
        function NgbAccordionConfig() {
            this.closeOthers = false;
        }
        NgbAccordionConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbAccordionConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbAccordionConfig_Factory() { return new NgbAccordionConfig(); }, token: NgbAccordionConfig, providedIn: "root" });
        return NgbAccordionConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var nextId = 0;
    /**
     * A directive that wraps an accordion panel header with any HTML markup and a toggling button
     * marked with [`NgbPanelToggle`](#/components/accordion/api#NgbPanelToggle).
     * See the [header customization demo](#/components/accordion/examples#header) for more details.
     *
     * You can also use [`NgbPanelTitle`](#/components/accordion/api#NgbPanelTitle) to customize only the panel title.
     *
     * \@since 4.1.0
     */
    var NgbPanelHeader = /** @class */ (function () {
        function NgbPanelHeader(templateRef) {
            this.templateRef = templateRef;
        }
        NgbPanelHeader.decorators = [
            { type: core.Directive, args: [{ selector: 'ng-template[ngbPanelHeader]' },] }
        ];
        /** @nocollapse */
        NgbPanelHeader.ctorParameters = function () { return [
            { type: core.TemplateRef }
        ]; };
        return NgbPanelHeader;
    }());
    /**
     * A directive that wraps only the panel title with HTML markup inside.
     *
     * You can also use [`NgbPanelHeader`](#/components/accordion/api#NgbPanelHeader) to customize the full panel header.
     */
    var NgbPanelTitle = /** @class */ (function () {
        function NgbPanelTitle(templateRef) {
            this.templateRef = templateRef;
        }
        NgbPanelTitle.decorators = [
            { type: core.Directive, args: [{ selector: 'ng-template[ngbPanelTitle]' },] }
        ];
        /** @nocollapse */
        NgbPanelTitle.ctorParameters = function () { return [
            { type: core.TemplateRef }
        ]; };
        return NgbPanelTitle;
    }());
    /**
     * A directive that wraps the accordion panel content.
     */
    var NgbPanelContent = /** @class */ (function () {
        function NgbPanelContent(templateRef) {
            this.templateRef = templateRef;
        }
        NgbPanelContent.decorators = [
            { type: core.Directive, args: [{ selector: 'ng-template[ngbPanelContent]' },] }
        ];
        /** @nocollapse */
        NgbPanelContent.ctorParameters = function () { return [
            { type: core.TemplateRef }
        ]; };
        return NgbPanelContent;
    }());
    /**
     * A directive that wraps an individual accordion panel with title and collapsible content.
     */
    var NgbPanel = /** @class */ (function () {
        function NgbPanel() {
            /**
             *  If `true`, the panel is disabled an can't be toggled.
             */
            this.disabled = false;
            /**
             *  An optional id for the panel that must be unique on the page.
             *
             *  If not provided, it will be auto-generated in the `ngb-panel-xxx` format.
             */
            this.id = "ngb-panel-" + nextId++;
            this.isOpen = false;
        }
        /**
         * @return {?}
         */
        NgbPanel.prototype.ngAfterContentChecked = /**
         * @return {?}
         */
        function () {
            // We are using @ContentChildren instead of @ContentChild as in the Angular version being used
            // only @ContentChildren allows us to specify the {descendants: false} option.
            // Without {descendants: false} we are hitting bugs described in:
            // https://github.com/ng-bootstrap/ng-bootstrap/issues/2240
            this.titleTpl = this.titleTpls.first;
            this.headerTpl = this.headerTpls.first;
            this.contentTpl = this.contentTpls.first;
        };
        NgbPanel.decorators = [
            { type: core.Directive, args: [{ selector: 'ngb-panel' },] }
        ];
        NgbPanel.propDecorators = {
            disabled: [{ type: core.Input }],
            id: [{ type: core.Input }],
            title: [{ type: core.Input }],
            type: [{ type: core.Input }],
            titleTpls: [{ type: core.ContentChildren, args: [NgbPanelTitle, { descendants: false },] }],
            headerTpls: [{ type: core.ContentChildren, args: [NgbPanelHeader, { descendants: false },] }],
            contentTpls: [{ type: core.ContentChildren, args: [NgbPanelContent, { descendants: false },] }]
        };
        return NgbPanel;
    }());
    /**
     * Accordion is a collection of collapsible panels (bootstrap cards).
     *
     * It can ensure only one panel is opened at a time and allows to customize panel
     * headers.
     */
    var NgbAccordion = /** @class */ (function () {
        function NgbAccordion(config) {
            /**
             * An array or comma separated strings of panel ids that should be opened **initially**.
             *
             * For subsequent changes use methods like `expand()`, `collapse()`, etc. and
             * the `(panelChange)` event.
             */
            this.activeIds = [];
            /**
             * If `true`, panel content will be detached from DOM and not simply hidden when the panel is collapsed.
             */
            this.destroyOnHide = true;
            /**
             * Event emitted right before the panel toggle happens.
             *
             * See [NgbPanelChangeEvent](#/components/accordion/api#NgbPanelChangeEvent) for payload details.
             */
            this.panelChange = new core.EventEmitter();
            this.type = config.type;
            this.closeOtherPanels = config.closeOthers;
        }
        /**
         * Checks if a panel with a given id is expanded.
         */
        /**
         * Checks if a panel with a given id is expanded.
         * @param {?} panelId
         * @return {?}
         */
        NgbAccordion.prototype.isExpanded = /**
         * Checks if a panel with a given id is expanded.
         * @param {?} panelId
         * @return {?}
         */
        function (panelId) { return this.activeIds.indexOf(panelId) > -1; };
        /**
         * Expands a panel with a given id.
         *
         * Has no effect if the panel is already expanded or disabled.
         */
        /**
         * Expands a panel with a given id.
         *
         * Has no effect if the panel is already expanded or disabled.
         * @param {?} panelId
         * @return {?}
         */
        NgbAccordion.prototype.expand = /**
         * Expands a panel with a given id.
         *
         * Has no effect if the panel is already expanded or disabled.
         * @param {?} panelId
         * @return {?}
         */
        function (panelId) { this._changeOpenState(this._findPanelById(panelId), true); };
        /**
         * Expands all panels, if `[closeOthers]` is `false`.
         *
         * If `[closeOthers]` is `true`, it will expand the first panel, unless there is already a panel opened.
         */
        /**
         * Expands all panels, if `[closeOthers]` is `false`.
         *
         * If `[closeOthers]` is `true`, it will expand the first panel, unless there is already a panel opened.
         * @return {?}
         */
        NgbAccordion.prototype.expandAll = /**
         * Expands all panels, if `[closeOthers]` is `false`.
         *
         * If `[closeOthers]` is `true`, it will expand the first panel, unless there is already a panel opened.
         * @return {?}
         */
        function () {
            var _this = this;
            if (this.closeOtherPanels) {
                if (this.activeIds.length === 0 && this.panels.length) {
                    this._changeOpenState(this.panels.first, true);
                }
            }
            else {
                this.panels.forEach((/**
                 * @param {?} panel
                 * @return {?}
                 */
                function (panel) { return _this._changeOpenState(panel, true); }));
            }
        };
        /**
         * Collapses a panel with the given id.
         *
         * Has no effect if the panel is already collapsed or disabled.
         */
        /**
         * Collapses a panel with the given id.
         *
         * Has no effect if the panel is already collapsed or disabled.
         * @param {?} panelId
         * @return {?}
         */
        NgbAccordion.prototype.collapse = /**
         * Collapses a panel with the given id.
         *
         * Has no effect if the panel is already collapsed or disabled.
         * @param {?} panelId
         * @return {?}
         */
        function (panelId) { this._changeOpenState(this._findPanelById(panelId), false); };
        /**
         * Collapses all opened panels.
         */
        /**
         * Collapses all opened panels.
         * @return {?}
         */
        NgbAccordion.prototype.collapseAll = /**
         * Collapses all opened panels.
         * @return {?}
         */
        function () {
            var _this = this;
            this.panels.forEach((/**
             * @param {?} panel
             * @return {?}
             */
            function (panel) { _this._changeOpenState(panel, false); }));
        };
        /**
         * Toggles a panel with the given id.
         *
         * Has no effect if the panel is disabled.
         */
        /**
         * Toggles a panel with the given id.
         *
         * Has no effect if the panel is disabled.
         * @param {?} panelId
         * @return {?}
         */
        NgbAccordion.prototype.toggle = /**
         * Toggles a panel with the given id.
         *
         * Has no effect if the panel is disabled.
         * @param {?} panelId
         * @return {?}
         */
        function (panelId) {
            /** @type {?} */
            var panel = this._findPanelById(panelId);
            if (panel) {
                this._changeOpenState(panel, !panel.isOpen);
            }
        };
        /**
         * @return {?}
         */
        NgbAccordion.prototype.ngAfterContentChecked = /**
         * @return {?}
         */
        function () {
            var _this = this;
            // active id updates
            if (isString(this.activeIds)) {
                this.activeIds = this.activeIds.split(/\s*,\s*/);
            }
            // update panels open states
            this.panels.forEach((/**
             * @param {?} panel
             * @return {?}
             */
            function (panel) { return panel.isOpen = !panel.disabled && _this.activeIds.indexOf(panel.id) > -1; }));
            // closeOthers updates
            if (this.activeIds.length > 1 && this.closeOtherPanels) {
                this._closeOthers(this.activeIds[0]);
                this._updateActiveIds();
            }
        };
        /**
         * @private
         * @param {?} panel
         * @param {?} nextState
         * @return {?}
         */
        NgbAccordion.prototype._changeOpenState = /**
         * @private
         * @param {?} panel
         * @param {?} nextState
         * @return {?}
         */
        function (panel, nextState) {
            if (panel && !panel.disabled && panel.isOpen !== nextState) {
                /** @type {?} */
                var defaultPrevented_1 = false;
                this.panelChange.emit({ panelId: panel.id, nextState: nextState, preventDefault: (/**
                     * @return {?}
                     */
                    function () { defaultPrevented_1 = true; }) });
                if (!defaultPrevented_1) {
                    panel.isOpen = nextState;
                    if (nextState && this.closeOtherPanels) {
                        this._closeOthers(panel.id);
                    }
                    this._updateActiveIds();
                }
            }
        };
        /**
         * @private
         * @param {?} panelId
         * @return {?}
         */
        NgbAccordion.prototype._closeOthers = /**
         * @private
         * @param {?} panelId
         * @return {?}
         */
        function (panelId) {
            this.panels.forEach((/**
             * @param {?} panel
             * @return {?}
             */
            function (panel) {
                if (panel.id !== panelId) {
                    panel.isOpen = false;
                }
            }));
        };
        /**
         * @private
         * @param {?} panelId
         * @return {?}
         */
        NgbAccordion.prototype._findPanelById = /**
         * @private
         * @param {?} panelId
         * @return {?}
         */
        function (panelId) { return this.panels.find((/**
         * @param {?} p
         * @return {?}
         */
        function (p) { return p.id === panelId; })); };
        /**
         * @private
         * @return {?}
         */
        NgbAccordion.prototype._updateActiveIds = /**
         * @private
         * @return {?}
         */
        function () {
            this.activeIds = this.panels.filter((/**
             * @param {?} panel
             * @return {?}
             */
            function (panel) { return panel.isOpen && !panel.disabled; })).map((/**
             * @param {?} panel
             * @return {?}
             */
            function (panel) { return panel.id; }));
        };
        NgbAccordion.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-accordion',
                        exportAs: 'ngbAccordion',
                        host: { 'class': 'accordion', 'role': 'tablist', '[attr.aria-multiselectable]': '!closeOtherPanels' },
                        template: "\n    <ng-template #t ngbPanelHeader let-panel>\n      <button class=\"btn btn-link\" [ngbPanelToggle]=\"panel\">\n        {{panel.title}}<ng-template [ngTemplateOutlet]=\"panel.titleTpl?.templateRef\"></ng-template>\n      </button>\n    </ng-template>\n    <ng-template ngFor let-panel [ngForOf]=\"panels\">\n      <div class=\"card\">\n        <div role=\"tab\" id=\"{{panel.id}}-header\" [class]=\"'card-header ' + (panel.type ? 'bg-'+panel.type: type ? 'bg-'+type : '')\">\n          <ng-template [ngTemplateOutlet]=\"panel.headerTpl?.templateRef || t\"\n                       [ngTemplateOutletContext]=\"{$implicit: panel, opened: panel.isOpen}\"></ng-template>\n        </div>\n        <div id=\"{{panel.id}}\" role=\"tabpanel\" [attr.aria-labelledby]=\"panel.id + '-header'\"\n             class=\"collapse\" [class.show]=\"panel.isOpen\" *ngIf=\"!destroyOnHide || panel.isOpen\">\n          <div class=\"card-body\">\n               <ng-template [ngTemplateOutlet]=\"panel.contentTpl?.templateRef\"></ng-template>\n          </div>\n        </div>\n      </div>\n    </ng-template>\n  "
                    }] }
        ];
        /** @nocollapse */
        NgbAccordion.ctorParameters = function () { return [
            { type: NgbAccordionConfig }
        ]; };
        NgbAccordion.propDecorators = {
            panels: [{ type: core.ContentChildren, args: [NgbPanel,] }],
            activeIds: [{ type: core.Input }],
            closeOtherPanels: [{ type: core.Input, args: ['closeOthers',] }],
            destroyOnHide: [{ type: core.Input }],
            type: [{ type: core.Input }],
            panelChange: [{ type: core.Output }]
        };
        return NgbAccordion;
    }());
    /**
     * A directive to put on a button that toggles panel opening and closing.
     *
     * To be used inside the [`NgbPanelHeader`](#/components/accordion/api#NgbPanelHeader)
     *
     * \@since 4.1.0
     */
    var NgbPanelToggle = /** @class */ (function () {
        function NgbPanelToggle(accordion, panel) {
            this.accordion = accordion;
            this.panel = panel;
        }
        Object.defineProperty(NgbPanelToggle.prototype, "ngbPanelToggle", {
            set: /**
             * @param {?} panel
             * @return {?}
             */
            function (panel) {
                if (panel) {
                    this.panel = panel;
                }
            },
            enumerable: true,
            configurable: true
        });
        NgbPanelToggle.decorators = [
            { type: core.Directive, args: [{
                        selector: 'button[ngbPanelToggle]',
                        host: {
                            'type': 'button',
                            '[disabled]': 'panel.disabled',
                            '[class.collapsed]': '!panel.isOpen',
                            '[attr.aria-expanded]': 'panel.isOpen',
                            '[attr.aria-controls]': 'panel.id',
                            '(click)': 'accordion.toggle(panel.id)'
                        }
                    },] }
        ];
        /** @nocollapse */
        NgbPanelToggle.ctorParameters = function () { return [
            { type: NgbAccordion },
            { type: NgbPanel, decorators: [{ type: core.Optional }, { type: core.Host }] }
        ]; };
        NgbPanelToggle.propDecorators = {
            ngbPanelToggle: [{ type: core.Input }]
        };
        return NgbPanelToggle;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NGB_ACCORDION_DIRECTIVES = [NgbAccordion, NgbPanel, NgbPanelTitle, NgbPanelContent, NgbPanelHeader, NgbPanelToggle];
    var NgbAccordionModule = /** @class */ (function () {
        function NgbAccordionModule() {
        }
        NgbAccordionModule.decorators = [
            { type: core.NgModule, args: [{ declarations: NGB_ACCORDION_DIRECTIVES, exports: NGB_ACCORDION_DIRECTIVES, imports: [common.CommonModule] },] }
        ];
        return NgbAccordionModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A configuration service for the [NgbAlert](#/components/alert/api#NgbAlert) component.
     *
     * You can inject this service, typically in your root component, and customize its properties
     * to provide default values for all alerts used in the application.
     */
    var NgbAlertConfig = /** @class */ (function () {
        function NgbAlertConfig() {
            this.dismissible = true;
            this.type = 'warning';
        }
        NgbAlertConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbAlertConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbAlertConfig_Factory() { return new NgbAlertConfig(); }, token: NgbAlertConfig, providedIn: "root" });
        return NgbAlertConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * Alert is a component to provide contextual feedback messages for user.
     *
     * It supports several alert types and can be dismissed.
     */
    var NgbAlert = /** @class */ (function () {
        function NgbAlert(config, _renderer, _element) {
            this._renderer = _renderer;
            this._element = _element;
            /**
             * An event emitted when the close button is clicked. It has no payload and only relevant for dismissible alerts.
             */
            this.close = new core.EventEmitter();
            this.dismissible = config.dismissible;
            this.type = config.type;
        }
        /**
         * @return {?}
         */
        NgbAlert.prototype.closeHandler = /**
         * @return {?}
         */
        function () { this.close.emit(null); };
        /**
         * @param {?} changes
         * @return {?}
         */
        NgbAlert.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            /** @type {?} */
            var typeChange = changes['type'];
            if (typeChange && !typeChange.firstChange) {
                this._renderer.removeClass(this._element.nativeElement, "alert-" + typeChange.previousValue);
                this._renderer.addClass(this._element.nativeElement, "alert-" + typeChange.currentValue);
            }
        };
        /**
         * @return {?}
         */
        NgbAlert.prototype.ngOnInit = /**
         * @return {?}
         */
        function () { this._renderer.addClass(this._element.nativeElement, "alert-" + this.type); };
        NgbAlert.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-alert',
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        encapsulation: core.ViewEncapsulation.None,
                        host: { 'role': 'alert', 'class': 'alert', '[class.alert-dismissible]': 'dismissible' },
                        template: "\n    <ng-content></ng-content>\n    <button *ngIf=\"dismissible\" type=\"button\" class=\"close\" aria-label=\"Close\" i18n-aria-label=\"@@ngb.alert.close\"\n      (click)=\"closeHandler()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n    ",
                        styles: ["ngb-alert{display:block}"]
                    }] }
        ];
        /** @nocollapse */
        NgbAlert.ctorParameters = function () { return [
            { type: NgbAlertConfig },
            { type: core.Renderer2 },
            { type: core.ElementRef }
        ]; };
        NgbAlert.propDecorators = {
            dismissible: [{ type: core.Input }],
            type: [{ type: core.Input }],
            close: [{ type: core.Output }]
        };
        return NgbAlert;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbAlertModule = /** @class */ (function () {
        function NgbAlertModule() {
        }
        NgbAlertModule.decorators = [
            { type: core.NgModule, args: [{ declarations: [NgbAlert], exports: [NgbAlert], imports: [common.CommonModule], entryComponents: [NgbAlert] },] }
        ];
        return NgbAlertModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbButtonLabel = /** @class */ (function () {
        function NgbButtonLabel() {
        }
        NgbButtonLabel.decorators = [
            { type: core.Directive, args: [{
                        selector: '[ngbButtonLabel]',
                        host: { '[class.btn]': 'true', '[class.active]': 'active', '[class.disabled]': 'disabled', '[class.focus]': 'focused' }
                    },] }
        ];
        return NgbButtonLabel;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NGB_CHECKBOX_VALUE_ACCESSOR = {
        provide: forms.NG_VALUE_ACCESSOR,
        useExisting: core.forwardRef((/**
         * @return {?}
         */
        function () { return NgbCheckBox; })),
        multi: true
    };
    /**
     * Allows to easily create Bootstrap-style checkbox buttons.
     *
     * Integrates with forms, so the value of a checked button is bound to the underlying form control
     * either in a reactive or template-driven way.
     */
    var NgbCheckBox = /** @class */ (function () {
        function NgbCheckBox(_label, _cd) {
            this._label = _label;
            this._cd = _cd;
            /**
             * If `true`, the checkbox button will be disabled
             */
            this.disabled = false;
            /**
             * The form control value when the checkbox is checked.
             */
            this.valueChecked = true;
            /**
             * The form control value when the checkbox is unchecked.
             */
            this.valueUnChecked = false;
            this.onChange = (/**
             * @param {?} _
             * @return {?}
             */
            function (_) { });
            this.onTouched = (/**
             * @return {?}
             */
            function () { });
        }
        Object.defineProperty(NgbCheckBox.prototype, "focused", {
            set: /**
             * @param {?} isFocused
             * @return {?}
             */
            function (isFocused) {
                this._label.focused = isFocused;
                if (!isFocused) {
                    this.onTouched();
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} $event
         * @return {?}
         */
        NgbCheckBox.prototype.onInputChange = /**
         * @param {?} $event
         * @return {?}
         */
        function ($event) {
            /** @type {?} */
            var modelToPropagate = $event.target.checked ? this.valueChecked : this.valueUnChecked;
            this.onChange(modelToPropagate);
            this.onTouched();
            this.writeValue(modelToPropagate);
        };
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbCheckBox.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this.onChange = fn; };
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbCheckBox.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this.onTouched = fn; };
        /**
         * @param {?} isDisabled
         * @return {?}
         */
        NgbCheckBox.prototype.setDisabledState = /**
         * @param {?} isDisabled
         * @return {?}
         */
        function (isDisabled) {
            this.disabled = isDisabled;
            this._label.disabled = isDisabled;
        };
        /**
         * @param {?} value
         * @return {?}
         */
        NgbCheckBox.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.checked = value === this.valueChecked;
            this._label.active = this.checked;
            // label won't be updated, if it is inside the OnPush component when [ngModel] changes
            this._cd.markForCheck();
        };
        NgbCheckBox.decorators = [
            { type: core.Directive, args: [{
                        selector: '[ngbButton][type=checkbox]',
                        host: {
                            'autocomplete': 'off',
                            '[checked]': 'checked',
                            '[disabled]': 'disabled',
                            '(change)': 'onInputChange($event)',
                            '(focus)': 'focused = true',
                            '(blur)': 'focused = false'
                        },
                        providers: [NGB_CHECKBOX_VALUE_ACCESSOR]
                    },] }
        ];
        /** @nocollapse */
        NgbCheckBox.ctorParameters = function () { return [
            { type: NgbButtonLabel },
            { type: core.ChangeDetectorRef }
        ]; };
        NgbCheckBox.propDecorators = {
            disabled: [{ type: core.Input }],
            valueChecked: [{ type: core.Input }],
            valueUnChecked: [{ type: core.Input }]
        };
        return NgbCheckBox;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NGB_RADIO_VALUE_ACCESSOR = {
        provide: forms.NG_VALUE_ACCESSOR,
        useExisting: core.forwardRef((/**
         * @return {?}
         */
        function () { return NgbRadioGroup; })),
        multi: true
    };
    /** @type {?} */
    var nextId$1 = 0;
    /**
     * Allows to easily create Bootstrap-style radio buttons.
     *
     * Integrates with forms, so the value of a checked button is bound to the underlying form control
     * either in a reactive or template-driven way.
     */
    var NgbRadioGroup = /** @class */ (function () {
        function NgbRadioGroup() {
            this._radios = new Set();
            this._value = null;
            /**
             * Name of the radio group applied to radio input elements.
             *
             * Will be applied to all radio input elements inside the group,
             * unless [`NgbRadio`](#/components/buttons/api#NgbRadio)'s specify names themselves.
             *
             * If not provided, will be generated in the `ngb-radio-xx` format.
             */
            this.name = "ngb-radio-" + nextId$1++;
            this.onChange = (/**
             * @param {?} _
             * @return {?}
             */
            function (_) { });
            this.onTouched = (/**
             * @return {?}
             */
            function () { });
        }
        Object.defineProperty(NgbRadioGroup.prototype, "disabled", {
            get: /**
             * @return {?}
             */
            function () { return this._disabled; },
            set: /**
             * @param {?} isDisabled
             * @return {?}
             */
            function (isDisabled) { this.setDisabledState(isDisabled); },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} radio
         * @return {?}
         */
        NgbRadioGroup.prototype.onRadioChange = /**
         * @param {?} radio
         * @return {?}
         */
        function (radio) {
            this.writeValue(radio.value);
            this.onChange(radio.value);
        };
        /**
         * @return {?}
         */
        NgbRadioGroup.prototype.onRadioValueUpdate = /**
         * @return {?}
         */
        function () { this._updateRadiosValue(); };
        /**
         * @param {?} radio
         * @return {?}
         */
        NgbRadioGroup.prototype.register = /**
         * @param {?} radio
         * @return {?}
         */
        function (radio) { this._radios.add(radio); };
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbRadioGroup.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this.onChange = fn; };
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbRadioGroup.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this.onTouched = fn; };
        /**
         * @param {?} isDisabled
         * @return {?}
         */
        NgbRadioGroup.prototype.setDisabledState = /**
         * @param {?} isDisabled
         * @return {?}
         */
        function (isDisabled) {
            this._disabled = isDisabled;
            this._updateRadiosDisabled();
        };
        /**
         * @param {?} radio
         * @return {?}
         */
        NgbRadioGroup.prototype.unregister = /**
         * @param {?} radio
         * @return {?}
         */
        function (radio) { this._radios.delete(radio); };
        /**
         * @param {?} value
         * @return {?}
         */
        NgbRadioGroup.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._value = value;
            this._updateRadiosValue();
        };
        /**
         * @private
         * @return {?}
         */
        NgbRadioGroup.prototype._updateRadiosValue = /**
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            this._radios.forEach((/**
             * @param {?} radio
             * @return {?}
             */
            function (radio) { return radio.updateValue(_this._value); }));
        };
        /**
         * @private
         * @return {?}
         */
        NgbRadioGroup.prototype._updateRadiosDisabled = /**
         * @private
         * @return {?}
         */
        function () { this._radios.forEach((/**
         * @param {?} radio
         * @return {?}
         */
        function (radio) { return radio.updateDisabled(); })); };
        NgbRadioGroup.decorators = [
            { type: core.Directive, args: [{ selector: '[ngbRadioGroup]', host: { 'role': 'radiogroup' }, providers: [NGB_RADIO_VALUE_ACCESSOR] },] }
        ];
        NgbRadioGroup.propDecorators = {
            name: [{ type: core.Input }]
        };
        return NgbRadioGroup;
    }());
    /**
     * A directive that marks an input of type "radio" as a part of the
     * [`NgbRadioGroup`](#/components/buttons/api#NgbRadioGroup).
     */
    var NgbRadio = /** @class */ (function () {
        function NgbRadio(_group, _label, _renderer, _element, _cd) {
            this._group = _group;
            this._label = _label;
            this._renderer = _renderer;
            this._element = _element;
            this._cd = _cd;
            this._value = null;
            this._group.register(this);
            this.updateDisabled();
        }
        Object.defineProperty(NgbRadio.prototype, "value", {
            get: /**
             * @return {?}
             */
            function () { return this._value; },
            /**
             * The form control value when current radio button is checked.
             */
            set: /**
             * The form control value when current radio button is checked.
             * @param {?} value
             * @return {?}
             */
            function (value) {
                this._value = value;
                /** @type {?} */
                var stringValue = value ? value.toString() : '';
                this._renderer.setProperty(this._element.nativeElement, 'value', stringValue);
                this._group.onRadioValueUpdate();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbRadio.prototype, "disabled", {
            get: /**
             * @return {?}
             */
            function () { return this._group.disabled || this._disabled; },
            /**
             * If `true`, current radio button will be disabled.
             */
            set: /**
             * If `true`, current radio button will be disabled.
             * @param {?} isDisabled
             * @return {?}
             */
            function (isDisabled) {
                this._disabled = isDisabled !== false;
                this.updateDisabled();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbRadio.prototype, "focused", {
            set: /**
             * @param {?} isFocused
             * @return {?}
             */
            function (isFocused) {
                if (this._label) {
                    this._label.focused = isFocused;
                }
                if (!isFocused) {
                    this._group.onTouched();
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbRadio.prototype, "checked", {
            get: /**
             * @return {?}
             */
            function () { return this._checked; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbRadio.prototype, "nameAttr", {
            get: /**
             * @return {?}
             */
            function () { return this.name || this._group.name; },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        NgbRadio.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () { this._group.unregister(this); };
        /**
         * @return {?}
         */
        NgbRadio.prototype.onChange = /**
         * @return {?}
         */
        function () { this._group.onRadioChange(this); };
        /**
         * @param {?} value
         * @return {?}
         */
        NgbRadio.prototype.updateValue = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            // label won't be updated, if it is inside the OnPush component when [ngModel] changes
            if (this.value !== value) {
                this._cd.markForCheck();
            }
            this._checked = this.value === value;
            this._label.active = this._checked;
        };
        /**
         * @return {?}
         */
        NgbRadio.prototype.updateDisabled = /**
         * @return {?}
         */
        function () { this._label.disabled = this.disabled; };
        NgbRadio.decorators = [
            { type: core.Directive, args: [{
                        selector: '[ngbButton][type=radio]',
                        host: {
                            '[checked]': 'checked',
                            '[disabled]': 'disabled',
                            '[name]': 'nameAttr',
                            '(change)': 'onChange()',
                            '(focus)': 'focused = true',
                            '(blur)': 'focused = false'
                        }
                    },] }
        ];
        /** @nocollapse */
        NgbRadio.ctorParameters = function () { return [
            { type: NgbRadioGroup },
            { type: NgbButtonLabel },
            { type: core.Renderer2 },
            { type: core.ElementRef },
            { type: core.ChangeDetectorRef }
        ]; };
        NgbRadio.propDecorators = {
            name: [{ type: core.Input }],
            value: [{ type: core.Input, args: ['value',] }],
            disabled: [{ type: core.Input, args: ['disabled',] }]
        };
        return NgbRadio;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NGB_BUTTON_DIRECTIVES = [NgbButtonLabel, NgbCheckBox, NgbRadioGroup, NgbRadio];
    var NgbButtonsModule = /** @class */ (function () {
        function NgbButtonsModule() {
        }
        NgbButtonsModule.decorators = [
            { type: core.NgModule, args: [{ declarations: NGB_BUTTON_DIRECTIVES, exports: NGB_BUTTON_DIRECTIVES },] }
        ];
        return NgbButtonsModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A configuration service for the [NgbCarousel](#/components/carousel/api#NgbCarousel) component.
     *
     * You can inject this service, typically in your root component, and customize its properties
     * to provide default values for all carousels used in the application.
     */
    var NgbCarouselConfig = /** @class */ (function () {
        function NgbCarouselConfig() {
            this.interval = 5000;
            this.wrap = true;
            this.keyboard = true;
            this.pauseOnHover = true;
            this.showNavigationArrows = true;
            this.showNavigationIndicators = true;
        }
        NgbCarouselConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbCarouselConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbCarouselConfig_Factory() { return new NgbCarouselConfig(); }, token: NgbCarouselConfig, providedIn: "root" });
        return NgbCarouselConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var nextId$2 = 0;
    /**
     * A directive that wraps the individual carousel slide.
     */
    var NgbSlide = /** @class */ (function () {
        function NgbSlide(tplRef) {
            this.tplRef = tplRef;
            /**
             * Slide id that must be unique for the entire document.
             *
             * If not provided, will be generated in the `ngb-slide-xx` format.
             */
            this.id = "ngb-slide-" + nextId$2++;
        }
        NgbSlide.decorators = [
            { type: core.Directive, args: [{ selector: 'ng-template[ngbSlide]' },] }
        ];
        /** @nocollapse */
        NgbSlide.ctorParameters = function () { return [
            { type: core.TemplateRef }
        ]; };
        NgbSlide.propDecorators = {
            id: [{ type: core.Input }]
        };
        return NgbSlide;
    }());
    /**
     * Carousel is a component to easily create and control slideshows.
     *
     * Allows to set intervals, change the way user interacts with the slides and provides a programmatic API.
     */
    var NgbCarousel = /** @class */ (function () {
        function NgbCarousel(config, _platformId, _ngZone, _cd) {
            this._platformId = _platformId;
            this._ngZone = _ngZone;
            this._cd = _cd;
            this.NgbSlideEventSource = NgbSlideEventSource;
            this._destroy$ = new rxjs.Subject();
            this._interval$ = new rxjs.BehaviorSubject(0);
            this._mouseHover$ = new rxjs.BehaviorSubject(false);
            this._pauseOnHover$ = new rxjs.BehaviorSubject(false);
            this._pause$ = new rxjs.BehaviorSubject(false);
            this._wrap$ = new rxjs.BehaviorSubject(false);
            /**
             * An event emitted right after the slide transition is completed.
             *
             * See [`NgbSlideEvent`](#/components/carousel/api#NgbSlideEvent) for payload details.
             */
            this.slide = new core.EventEmitter();
            this.interval = config.interval;
            this.wrap = config.wrap;
            this.keyboard = config.keyboard;
            this.pauseOnHover = config.pauseOnHover;
            this.showNavigationArrows = config.showNavigationArrows;
            this.showNavigationIndicators = config.showNavigationIndicators;
        }
        Object.defineProperty(NgbCarousel.prototype, "interval", {
            get: /**
             * @return {?}
             */
            function () { return this._interval$.value; },
            /**
             * Time in milliseconds before the next slide is shown.
             */
            set: /**
             * Time in milliseconds before the next slide is shown.
             * @param {?} value
             * @return {?}
             */
            function (value) {
                this._interval$.next(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbCarousel.prototype, "wrap", {
            get: /**
             * @return {?}
             */
            function () { return this._wrap$.value; },
            /**
             * If `true`, will 'wrap' the carousel by switching from the last slide back to the first.
             */
            set: /**
             * If `true`, will 'wrap' the carousel by switching from the last slide back to the first.
             * @param {?} value
             * @return {?}
             */
            function (value) {
                this._wrap$.next(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbCarousel.prototype, "pauseOnHover", {
            get: /**
             * @return {?}
             */
            function () { return this._pauseOnHover$.value; },
            /**
             * If `true`, will pause slide switching when mouse cursor hovers the slide.
             *
             * @since 2.2.0
             */
            set: /**
             * If `true`, will pause slide switching when mouse cursor hovers the slide.
             *
             * \@since 2.2.0
             * @param {?} value
             * @return {?}
             */
            function (value) {
                this._pauseOnHover$.next(value);
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        NgbCarousel.prototype.mouseEnter = /**
         * @return {?}
         */
        function () {
            this._mouseHover$.next(true);
        };
        /**
         * @return {?}
         */
        NgbCarousel.prototype.mouseLeave = /**
         * @return {?}
         */
        function () {
            this._mouseHover$.next(false);
        };
        /**
         * @return {?}
         */
        NgbCarousel.prototype.ngAfterContentInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            // setInterval() doesn't play well with SSR and protractor,
            // so we should run it in the browser and outside Angular
            if (common.isPlatformBrowser(this._platformId)) {
                this._ngZone.runOutsideAngular((/**
                 * @return {?}
                 */
                function () {
                    /** @type {?} */
                    var hasNextSlide$ = rxjs.combineLatest(_this.slide.pipe(operators.map((/**
                     * @param {?} slideEvent
                     * @return {?}
                     */
                    function (slideEvent) { return slideEvent.current; })), operators.startWith(_this.activeId)), _this._wrap$, _this.slides.changes.pipe(operators.startWith(null)))
                        .pipe(operators.map((/**
                     * @param {?} __0
                     * @return {?}
                     */
                    function (_a) {
                        var _b = __read(_a, 2), currentSlideId = _b[0], wrap = _b[1];
                        /** @type {?} */
                        var slideArr = _this.slides.toArray();
                        /** @type {?} */
                        var currentSlideIdx = _this._getSlideIdxById(currentSlideId);
                        return wrap ? slideArr.length > 1 : currentSlideIdx < slideArr.length - 1;
                    })), operators.distinctUntilChanged());
                    rxjs.combineLatest(_this._pause$, _this._pauseOnHover$, _this._mouseHover$, _this._interval$, hasNextSlide$)
                        .pipe(operators.map((/**
                     * @param {?} __0
                     * @return {?}
                     */
                    function (_a) {
                        var _b = __read(_a, 5), pause = _b[0], pauseOnHover = _b[1], mouseHover = _b[2], interval = _b[3], hasNextSlide = _b[4];
                        return ((pause || (pauseOnHover && mouseHover) || !hasNextSlide) ? 0 : interval);
                    })), operators.distinctUntilChanged(), operators.switchMap((/**
                     * @param {?} interval
                     * @return {?}
                     */
                    function (interval) { return interval > 0 ? rxjs.timer(interval, interval) : rxjs.NEVER; })), operators.takeUntil(_this._destroy$))
                        .subscribe((/**
                     * @return {?}
                     */
                    function () { return _this._ngZone.run((/**
                     * @return {?}
                     */
                    function () { return _this.next(NgbSlideEventSource.TIMER); })); }));
                }));
            }
            this.slides.changes.pipe(operators.takeUntil(this._destroy$)).subscribe((/**
             * @return {?}
             */
            function () { return _this._cd.markForCheck(); }));
        };
        /**
         * @return {?}
         */
        NgbCarousel.prototype.ngAfterContentChecked = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var activeSlide = this._getSlideById(this.activeId);
            this.activeId = activeSlide ? activeSlide.id : (this.slides.length ? this.slides.first.id : null);
        };
        /**
         * @return {?}
         */
        NgbCarousel.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () { this._destroy$.next(); };
        /**
         * Navigates to a slide with the specified identifier.
         */
        /**
         * Navigates to a slide with the specified identifier.
         * @param {?} slideId
         * @param {?=} source
         * @return {?}
         */
        NgbCarousel.prototype.select = /**
         * Navigates to a slide with the specified identifier.
         * @param {?} slideId
         * @param {?=} source
         * @return {?}
         */
        function (slideId, source) {
            this._cycleToSelected(slideId, this._getSlideEventDirection(this.activeId, slideId), source);
        };
        /**
         * Navigates to the previous slide.
         */
        /**
         * Navigates to the previous slide.
         * @param {?=} source
         * @return {?}
         */
        NgbCarousel.prototype.prev = /**
         * Navigates to the previous slide.
         * @param {?=} source
         * @return {?}
         */
        function (source) {
            this._cycleToSelected(this._getPrevSlide(this.activeId), NgbSlideEventDirection.RIGHT, source);
        };
        /**
         * Navigates to the next slide.
         */
        /**
         * Navigates to the next slide.
         * @param {?=} source
         * @return {?}
         */
        NgbCarousel.prototype.next = /**
         * Navigates to the next slide.
         * @param {?=} source
         * @return {?}
         */
        function (source) {
            this._cycleToSelected(this._getNextSlide(this.activeId), NgbSlideEventDirection.LEFT, source);
        };
        /**
         * Pauses cycling through the slides.
         */
        /**
         * Pauses cycling through the slides.
         * @return {?}
         */
        NgbCarousel.prototype.pause = /**
         * Pauses cycling through the slides.
         * @return {?}
         */
        function () { this._pause$.next(true); };
        /**
         * Restarts cycling through the slides from left to right.
         */
        /**
         * Restarts cycling through the slides from left to right.
         * @return {?}
         */
        NgbCarousel.prototype.cycle = /**
         * Restarts cycling through the slides from left to right.
         * @return {?}
         */
        function () { this._pause$.next(false); };
        /**
         * @private
         * @param {?} slideIdx
         * @param {?} direction
         * @param {?=} source
         * @return {?}
         */
        NgbCarousel.prototype._cycleToSelected = /**
         * @private
         * @param {?} slideIdx
         * @param {?} direction
         * @param {?=} source
         * @return {?}
         */
        function (slideIdx, direction, source) {
            /** @type {?} */
            var selectedSlide = this._getSlideById(slideIdx);
            if (selectedSlide && selectedSlide.id !== this.activeId) {
                this.slide.emit({ prev: this.activeId, current: selectedSlide.id, direction: direction, paused: this._pause$.value, source: source });
                this.activeId = selectedSlide.id;
            }
            // we get here after the interval fires or any external API call like next(), prev() or select()
            this._cd.markForCheck();
        };
        /**
         * @private
         * @param {?} currentActiveSlideId
         * @param {?} nextActiveSlideId
         * @return {?}
         */
        NgbCarousel.prototype._getSlideEventDirection = /**
         * @private
         * @param {?} currentActiveSlideId
         * @param {?} nextActiveSlideId
         * @return {?}
         */
        function (currentActiveSlideId, nextActiveSlideId) {
            /** @type {?} */
            var currentActiveSlideIdx = this._getSlideIdxById(currentActiveSlideId);
            /** @type {?} */
            var nextActiveSlideIdx = this._getSlideIdxById(nextActiveSlideId);
            return currentActiveSlideIdx > nextActiveSlideIdx ? NgbSlideEventDirection.RIGHT : NgbSlideEventDirection.LEFT;
        };
        /**
         * @private
         * @param {?} slideId
         * @return {?}
         */
        NgbCarousel.prototype._getSlideById = /**
         * @private
         * @param {?} slideId
         * @return {?}
         */
        function (slideId) { return this.slides.find((/**
         * @param {?} slide
         * @return {?}
         */
        function (slide) { return slide.id === slideId; })); };
        /**
         * @private
         * @param {?} slideId
         * @return {?}
         */
        NgbCarousel.prototype._getSlideIdxById = /**
         * @private
         * @param {?} slideId
         * @return {?}
         */
        function (slideId) {
            return this.slides.toArray().indexOf(this._getSlideById(slideId));
        };
        /**
         * @private
         * @param {?} currentSlideId
         * @return {?}
         */
        NgbCarousel.prototype._getNextSlide = /**
         * @private
         * @param {?} currentSlideId
         * @return {?}
         */
        function (currentSlideId) {
            /** @type {?} */
            var slideArr = this.slides.toArray();
            /** @type {?} */
            var currentSlideIdx = this._getSlideIdxById(currentSlideId);
            /** @type {?} */
            var isLastSlide = currentSlideIdx === slideArr.length - 1;
            return isLastSlide ? (this.wrap ? slideArr[0].id : slideArr[slideArr.length - 1].id) :
                slideArr[currentSlideIdx + 1].id;
        };
        /**
         * @private
         * @param {?} currentSlideId
         * @return {?}
         */
        NgbCarousel.prototype._getPrevSlide = /**
         * @private
         * @param {?} currentSlideId
         * @return {?}
         */
        function (currentSlideId) {
            /** @type {?} */
            var slideArr = this.slides.toArray();
            /** @type {?} */
            var currentSlideIdx = this._getSlideIdxById(currentSlideId);
            /** @type {?} */
            var isFirstSlide = currentSlideIdx === 0;
            return isFirstSlide ? (this.wrap ? slideArr[slideArr.length - 1].id : slideArr[0].id) :
                slideArr[currentSlideIdx - 1].id;
        };
        NgbCarousel.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-carousel',
                        exportAs: 'ngbCarousel',
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        host: {
                            'class': 'carousel slide',
                            '[style.display]': '"block"',
                            'tabIndex': '0',
                            '(keydown.arrowLeft)': 'keyboard && prev(NgbSlideEventSource.ARROW_LEFT)',
                            '(keydown.arrowRight)': 'keyboard && next(NgbSlideEventSource.ARROW_RIGHT)'
                        },
                        template: "\n    <ol class=\"carousel-indicators\" *ngIf=\"showNavigationIndicators\">\n      <li *ngFor=\"let slide of slides\" [id]=\"slide.id\" [class.active]=\"slide.id === activeId\"\n          (click)=\"select(slide.id, NgbSlideEventSource.INDICATOR)\"></li>\n    </ol>\n    <div class=\"carousel-inner\">\n      <div *ngFor=\"let slide of slides\" class=\"carousel-item\" [class.active]=\"slide.id === activeId\">\n        <ng-template [ngTemplateOutlet]=\"slide.tplRef\"></ng-template>\n      </div>\n    </div>\n    <a class=\"carousel-control-prev\" role=\"button\" (click)=\"prev(NgbSlideEventSource.ARROW_LEFT)\" *ngIf=\"showNavigationArrows\">\n      <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>\n      <span class=\"sr-only\" i18n=\"@@ngb.carousel.previous\">Previous</span>\n    </a>\n    <a class=\"carousel-control-next\" role=\"button\" (click)=\"next(NgbSlideEventSource.ARROW_RIGHT)\" *ngIf=\"showNavigationArrows\">\n      <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>\n      <span class=\"sr-only\" i18n=\"@@ngb.carousel.next\">Next</span>\n    </a>\n  "
                    }] }
        ];
        /** @nocollapse */
        NgbCarousel.ctorParameters = function () { return [
            { type: NgbCarouselConfig },
            { type: undefined, decorators: [{ type: core.Inject, args: [core.PLATFORM_ID,] }] },
            { type: core.NgZone },
            { type: core.ChangeDetectorRef }
        ]; };
        NgbCarousel.propDecorators = {
            slides: [{ type: core.ContentChildren, args: [NgbSlide,] }],
            activeId: [{ type: core.Input }],
            interval: [{ type: core.Input }],
            wrap: [{ type: core.Input }],
            keyboard: [{ type: core.Input }],
            pauseOnHover: [{ type: core.Input }],
            showNavigationArrows: [{ type: core.Input }],
            showNavigationIndicators: [{ type: core.Input }],
            slide: [{ type: core.Output }],
            mouseEnter: [{ type: core.HostListener, args: ['mouseenter',] }],
            mouseLeave: [{ type: core.HostListener, args: ['mouseleave',] }]
        };
        return NgbCarousel;
    }());
    /** @enum {string} */
    var NgbSlideEventDirection = {
        LEFT: (/** @type {?} */ ('left')),
        RIGHT: (/** @type {?} */ ('right')),
    };
    /** @enum {string} */
    var NgbSlideEventSource = {
        TIMER: 'timer',
        ARROW_LEFT: 'arrowLeft',
        ARROW_RIGHT: 'arrowRight',
        INDICATOR: 'indicator',
    };
    /** @type {?} */
    var NGB_CAROUSEL_DIRECTIVES = [NgbCarousel, NgbSlide];

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbCarouselModule = /** @class */ (function () {
        function NgbCarouselModule() {
        }
        NgbCarouselModule.decorators = [
            { type: core.NgModule, args: [{ declarations: NGB_CAROUSEL_DIRECTIVES, exports: NGB_CAROUSEL_DIRECTIVES, imports: [common.CommonModule] },] }
        ];
        return NgbCarouselModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A directive to provide a simple way of hiding and showing elements on the page.
     */
    var NgbCollapse = /** @class */ (function () {
        function NgbCollapse() {
            /**
             * If `true`, will collapse the element or show it otherwise.
             */
            this.collapsed = false;
        }
        NgbCollapse.decorators = [
            { type: core.Directive, args: [{
                        selector: '[ngbCollapse]',
                        exportAs: 'ngbCollapse',
                        host: { '[class.collapse]': 'true', '[class.show]': '!collapsed' }
                    },] }
        ];
        NgbCollapse.propDecorators = {
            collapsed: [{ type: core.Input, args: ['ngbCollapse',] }]
        };
        return NgbCollapse;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbCollapseModule = /** @class */ (function () {
        function NgbCollapseModule() {
        }
        NgbCollapseModule.decorators = [
            { type: core.NgModule, args: [{ declarations: [NgbCollapse], exports: [NgbCollapse] },] }
        ];
        return NgbCollapseModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A simple class that represents a date that datepicker also uses internally.
     *
     * It is the implementation of the `NgbDateStruct` interface that adds some convenience methods,
     * like `.equals()`, `.before()`, etc.
     *
     * All datepicker APIs consume `NgbDateStruct`, but return `NgbDate`.
     *
     * In many cases it is simpler to manipulate these objects together with
     * [`NgbCalendar`](#/components/datepicker/api#NgbCalendar) than native JS Dates.
     *
     * See the [date format overview](#/components/datepicker/overview#date-model) for more details.
     *
     * \@since 3.0.0
     */
    var   /**
     * A simple class that represents a date that datepicker also uses internally.
     *
     * It is the implementation of the `NgbDateStruct` interface that adds some convenience methods,
     * like `.equals()`, `.before()`, etc.
     *
     * All datepicker APIs consume `NgbDateStruct`, but return `NgbDate`.
     *
     * In many cases it is simpler to manipulate these objects together with
     * [`NgbCalendar`](#/components/datepicker/api#NgbCalendar) than native JS Dates.
     *
     * See the [date format overview](#/components/datepicker/overview#date-model) for more details.
     *
     * \@since 3.0.0
     */
    NgbDate = /** @class */ (function () {
        function NgbDate(year, month, day) {
            this.year = isInteger(year) ? year : null;
            this.month = isInteger(month) ? month : null;
            this.day = isInteger(day) ? day : null;
        }
        /**
         * A **static method** that creates a new date object from the `NgbDateStruct`,
         *
         * ex. `NgbDate.from({year: 2000, month: 5, day: 1})`.
         *
         * If the `date` is already of `NgbDate` type, the method will return the same object.
         */
        /**
         * A **static method** that creates a new date object from the `NgbDateStruct`,
         *
         * ex. `NgbDate.from({year: 2000, month: 5, day: 1})`.
         *
         * If the `date` is already of `NgbDate` type, the method will return the same object.
         * @param {?} date
         * @return {?}
         */
        NgbDate.from = /**
         * A **static method** that creates a new date object from the `NgbDateStruct`,
         *
         * ex. `NgbDate.from({year: 2000, month: 5, day: 1})`.
         *
         * If the `date` is already of `NgbDate` type, the method will return the same object.
         * @param {?} date
         * @return {?}
         */
        function (date) {
            if (date instanceof NgbDate) {
                return date;
            }
            return date ? new NgbDate(date.year, date.month, date.day) : null;
        };
        /**
         * Checks if the current date is equal to another date.
         */
        /**
         * Checks if the current date is equal to another date.
         * @param {?} other
         * @return {?}
         */
        NgbDate.prototype.equals = /**
         * Checks if the current date is equal to another date.
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return other && this.year === other.year && this.month === other.month && this.day === other.day;
        };
        /**
         * Checks if the current date is before another date.
         */
        /**
         * Checks if the current date is before another date.
         * @param {?} other
         * @return {?}
         */
        NgbDate.prototype.before = /**
         * Checks if the current date is before another date.
         * @param {?} other
         * @return {?}
         */
        function (other) {
            if (!other) {
                return false;
            }
            if (this.year === other.year) {
                if (this.month === other.month) {
                    return this.day === other.day ? false : this.day < other.day;
                }
                else {
                    return this.month < other.month;
                }
            }
            else {
                return this.year < other.year;
            }
        };
        /**
         * Checks if the current date is after another date.
         */
        /**
         * Checks if the current date is after another date.
         * @param {?} other
         * @return {?}
         */
        NgbDate.prototype.after = /**
         * Checks if the current date is after another date.
         * @param {?} other
         * @return {?}
         */
        function (other) {
            if (!other) {
                return false;
            }
            if (this.year === other.year) {
                if (this.month === other.month) {
                    return this.day === other.day ? false : this.day > other.day;
                }
                else {
                    return this.month > other.month;
                }
            }
            else {
                return this.year > other.year;
            }
        };
        return NgbDate;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @param {?} jsDate
     * @return {?}
     */
    function fromJSDate(jsDate) {
        return new NgbDate(jsDate.getFullYear(), jsDate.getMonth() + 1, jsDate.getDate());
    }
    /**
     * @param {?} date
     * @return {?}
     */
    function toJSDate(date) {
        /** @type {?} */
        var jsDate = new Date(date.year, date.month - 1, date.day, 12);
        // this is done avoid 30 -> 1930 conversion
        if (!isNaN(jsDate.getTime())) {
            jsDate.setFullYear(date.year);
        }
        return jsDate;
    }
    /**
     * @return {?}
     */
    function NGB_DATEPICKER_CALENDAR_FACTORY() {
        return new NgbCalendarGregorian();
    }
    /**
     * A service that represents the calendar used by the datepicker.
     *
     * The default implementation uses the Gregorian calendar. You can inject it in your own
     * implementations if necessary to simplify `NgbDate` calculations.
     * @abstract
     */
    var NgbCalendar = /** @class */ (function () {
        function NgbCalendar() {
        }
        NgbCalendar.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root', useFactory: NGB_DATEPICKER_CALENDAR_FACTORY },] }
        ];
        /** @nocollapse */ NgbCalendar.ngInjectableDef = core.ɵɵdefineInjectable({ factory: NGB_DATEPICKER_CALENDAR_FACTORY, token: NgbCalendar, providedIn: "root" });
        return NgbCalendar;
    }());
    var NgbCalendarGregorian = /** @class */ (function (_super) {
        __extends(NgbCalendarGregorian, _super);
        function NgbCalendarGregorian() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * @return {?}
         */
        NgbCalendarGregorian.prototype.getDaysPerWeek = /**
         * @return {?}
         */
        function () { return 7; };
        /**
         * @return {?}
         */
        NgbCalendarGregorian.prototype.getMonths = /**
         * @return {?}
         */
        function () { return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]; };
        /**
         * @return {?}
         */
        NgbCalendarGregorian.prototype.getWeeksPerMonth = /**
         * @return {?}
         */
        function () { return 6; };
        /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        NgbCalendarGregorian.prototype.getNext = /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        function (date, period, number) {
            if (period === void 0) { period = 'd'; }
            if (number === void 0) { number = 1; }
            /** @type {?} */
            var jsDate = toJSDate(date);
            switch (period) {
                case 'y':
                    return new NgbDate(date.year + number, 1, 1);
                case 'm':
                    jsDate = new Date(date.year, date.month + number - 1, 1, 12);
                    break;
                case 'd':
                    jsDate.setDate(jsDate.getDate() + number);
                    break;
                default:
                    return date;
            }
            return fromJSDate(jsDate);
        };
        /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        NgbCalendarGregorian.prototype.getPrev = /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        function (date, period, number) {
            if (period === void 0) { period = 'd'; }
            if (number === void 0) { number = 1; }
            return this.getNext(date, period, -number);
        };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbCalendarGregorian.prototype.getWeekday = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            /** @type {?} */
            var jsDate = toJSDate(date);
            /** @type {?} */
            var day = jsDate.getDay();
            // in JS Date Sun=0, in ISO 8601 Sun=7
            return day === 0 ? 7 : day;
        };
        /**
         * @param {?} week
         * @param {?} firstDayOfWeek
         * @return {?}
         */
        NgbCalendarGregorian.prototype.getWeekNumber = /**
         * @param {?} week
         * @param {?} firstDayOfWeek
         * @return {?}
         */
        function (week, firstDayOfWeek) {
            // in JS Date Sun=0, in ISO 8601 Sun=7
            if (firstDayOfWeek === 7) {
                firstDayOfWeek = 0;
            }
            /** @type {?} */
            var thursdayIndex = (4 + 7 - firstDayOfWeek) % 7;
            /** @type {?} */
            var date = week[thursdayIndex];
            /** @type {?} */
            var jsDate = toJSDate(date);
            jsDate.setDate(jsDate.getDate() + 4 - (jsDate.getDay() || 7)); // Thursday
            // Thursday
            /** @type {?} */
            var time = jsDate.getTime();
            jsDate.setMonth(0); // Compare with Jan 1
            jsDate.setDate(1);
            return Math.floor(Math.round((time - jsDate.getTime()) / 86400000) / 7) + 1;
        };
        /**
         * @return {?}
         */
        NgbCalendarGregorian.prototype.getToday = /**
         * @return {?}
         */
        function () { return fromJSDate(new Date()); };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbCalendarGregorian.prototype.isValid = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            if (!date || !isInteger(date.year) || !isInteger(date.month) || !isInteger(date.day)) {
                return false;
            }
            // year 0 doesn't exist in Gregorian calendar
            if (date.year === 0) {
                return false;
            }
            /** @type {?} */
            var jsDate = toJSDate(date);
            return !isNaN(jsDate.getTime()) && jsDate.getFullYear() === date.year && jsDate.getMonth() + 1 === date.month &&
                jsDate.getDate() === date.day;
        };
        NgbCalendarGregorian.decorators = [
            { type: core.Injectable }
        ];
        return NgbCalendarGregorian;
    }(NgbCalendar));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @param {?} prev
     * @param {?} next
     * @return {?}
     */
    function isChangedDate(prev, next) {
        return !dateComparator(prev, next);
    }
    /**
     * @param {?} prev
     * @param {?} next
     * @return {?}
     */
    function isChangedMonth(prev, next) {
        return !prev && !next ? false : !prev || !next ? true : prev.year !== next.year || prev.month !== next.month;
    }
    /**
     * @param {?} prev
     * @param {?} next
     * @return {?}
     */
    function dateComparator(prev, next) {
        return (!prev && !next) || (!!prev && !!next && prev.equals(next));
    }
    /**
     * @param {?} minDate
     * @param {?} maxDate
     * @return {?}
     */
    function checkMinBeforeMax(minDate, maxDate) {
        if (maxDate && minDate && maxDate.before(minDate)) {
            throw new Error("'maxDate' " + maxDate + " should be greater than 'minDate' " + minDate);
        }
    }
    /**
     * @param {?} date
     * @param {?} minDate
     * @param {?} maxDate
     * @return {?}
     */
    function checkDateInRange(date, minDate, maxDate) {
        if (date && minDate && date.before(minDate)) {
            return minDate;
        }
        if (date && maxDate && date.after(maxDate)) {
            return maxDate;
        }
        return date;
    }
    /**
     * @param {?} date
     * @param {?} state
     * @return {?}
     */
    function isDateSelectable(date, state) {
        var minDate = state.minDate, maxDate = state.maxDate, disabled = state.disabled, markDisabled = state.markDisabled;
        // clang-format off
        return !(!isDefined(date) ||
            disabled ||
            (markDisabled && markDisabled(date, { year: date.year, month: date.month })) ||
            (minDate && date.before(minDate)) ||
            (maxDate && date.after(maxDate)));
        // clang-format on
    }
    /**
     * @param {?} calendar
     * @param {?} date
     * @param {?} minDate
     * @param {?} maxDate
     * @return {?}
     */
    function generateSelectBoxMonths(calendar, date, minDate, maxDate) {
        if (!date) {
            return [];
        }
        /** @type {?} */
        var months = calendar.getMonths(date.year);
        if (minDate && date.year === minDate.year) {
            /** @type {?} */
            var index = months.findIndex((/**
             * @param {?} month
             * @return {?}
             */
            function (month) { return month === minDate.month; }));
            months = months.slice(index);
        }
        if (maxDate && date.year === maxDate.year) {
            /** @type {?} */
            var index = months.findIndex((/**
             * @param {?} month
             * @return {?}
             */
            function (month) { return month === maxDate.month; }));
            months = months.slice(0, index + 1);
        }
        return months;
    }
    /**
     * @param {?} date
     * @param {?} minDate
     * @param {?} maxDate
     * @return {?}
     */
    function generateSelectBoxYears(date, minDate, maxDate) {
        if (!date) {
            return [];
        }
        /** @type {?} */
        var start = minDate && minDate.year || date.year - 10;
        /** @type {?} */
        var end = maxDate && maxDate.year || date.year + 10;
        return Array.from({ length: end - start + 1 }, (/**
         * @param {?} e
         * @param {?} i
         * @return {?}
         */
        function (e, i) { return start + i; }));
    }
    /**
     * @param {?} calendar
     * @param {?} date
     * @param {?} maxDate
     * @return {?}
     */
    function nextMonthDisabled(calendar, date, maxDate) {
        return maxDate && calendar.getNext(date, 'm').after(maxDate);
    }
    /**
     * @param {?} calendar
     * @param {?} date
     * @param {?} minDate
     * @return {?}
     */
    function prevMonthDisabled(calendar, date, minDate) {
        /** @type {?} */
        var prevDate = calendar.getPrev(date, 'm');
        return minDate && (prevDate.year === minDate.year && prevDate.month < minDate.month ||
            prevDate.year < minDate.year && minDate.month === 1);
    }
    /**
     * @param {?} calendar
     * @param {?} date
     * @param {?} state
     * @param {?} i18n
     * @param {?} force
     * @return {?}
     */
    function buildMonths(calendar, date, state, i18n, force) {
        var displayMonths = state.displayMonths, months = state.months;
        // move old months to a temporary array
        /** @type {?} */
        var monthsToReuse = months.splice(0, months.length);
        // generate new first dates, nullify or reuse months
        /** @type {?} */
        var firstDates = Array.from({ length: displayMonths }, (/**
         * @param {?} _
         * @param {?} i
         * @return {?}
         */
        function (_, i) {
            /** @type {?} */
            var firstDate = calendar.getNext(date, 'm', i);
            months[i] = null;
            if (!force) {
                /** @type {?} */
                var reusedIndex = monthsToReuse.findIndex((/**
                 * @param {?} month
                 * @return {?}
                 */
                function (month) { return month.firstDate.equals(firstDate); }));
                // move reused month back to months
                if (reusedIndex !== -1) {
                    months[i] = monthsToReuse.splice(reusedIndex, 1)[0];
                }
            }
            return firstDate;
        }));
        // rebuild nullified months
        firstDates.forEach((/**
         * @param {?} firstDate
         * @param {?} i
         * @return {?}
         */
        function (firstDate, i) {
            if (months[i] === null) {
                months[i] = buildMonth(calendar, firstDate, state, i18n, monthsToReuse.shift() || (/** @type {?} */ ({})));
            }
        }));
        return months;
    }
    /**
     * @param {?} calendar
     * @param {?} date
     * @param {?} state
     * @param {?} i18n
     * @param {?=} month
     * @return {?}
     */
    function buildMonth(calendar, date, state, i18n, month) {
        if (month === void 0) { month = (/** @type {?} */ ({})); }
        var dayTemplateData = state.dayTemplateData, minDate = state.minDate, maxDate = state.maxDate, firstDayOfWeek = state.firstDayOfWeek, markDisabled = state.markDisabled, outsideDays = state.outsideDays;
        /** @type {?} */
        var calendarToday = calendar.getToday();
        month.firstDate = null;
        month.lastDate = null;
        month.number = date.month;
        month.year = date.year;
        month.weeks = month.weeks || [];
        month.weekdays = month.weekdays || [];
        date = getFirstViewDate(calendar, date, firstDayOfWeek);
        // month has weeks
        for (var week = 0; week < calendar.getWeeksPerMonth(); week++) {
            /** @type {?} */
            var weekObject = month.weeks[week];
            if (!weekObject) {
                weekObject = month.weeks[week] = { number: 0, days: [], collapsed: true };
            }
            /** @type {?} */
            var days = weekObject.days;
            // week has days
            for (var day = 0; day < calendar.getDaysPerWeek(); day++) {
                if (week === 0) {
                    month.weekdays[day] = calendar.getWeekday(date);
                }
                /** @type {?} */
                var newDate = new NgbDate(date.year, date.month, date.day);
                /** @type {?} */
                var nextDate = calendar.getNext(newDate);
                /** @type {?} */
                var ariaLabel = i18n.getDayAriaLabel(newDate);
                // marking date as disabled
                /** @type {?} */
                var disabled = !!((minDate && newDate.before(minDate)) || (maxDate && newDate.after(maxDate)));
                if (!disabled && markDisabled) {
                    disabled = markDisabled(newDate, { month: month.number, year: month.year });
                }
                // today
                /** @type {?} */
                var today = newDate.equals(calendarToday);
                // adding user-provided data to the context
                /** @type {?} */
                var contextUserData = dayTemplateData ? dayTemplateData(newDate, { month: month.number, year: month.year }) : undefined;
                // saving first date of the month
                if (month.firstDate === null && newDate.month === month.number) {
                    month.firstDate = newDate;
                }
                // saving last date of the month
                if (newDate.month === month.number && nextDate.month !== month.number) {
                    month.lastDate = newDate;
                }
                /** @type {?} */
                var dayObject = days[day];
                if (!dayObject) {
                    dayObject = days[day] = (/** @type {?} */ ({}));
                }
                dayObject.date = newDate;
                dayObject.context = Object.assign(dayObject.context || {}, {
                    $implicit: newDate,
                    date: newDate,
                    data: contextUserData,
                    currentMonth: month.number, disabled: disabled,
                    focused: false,
                    selected: false, today: today
                });
                dayObject.tabindex = -1;
                dayObject.ariaLabel = ariaLabel;
                dayObject.hidden = false;
                date = nextDate;
            }
            weekObject.number = calendar.getWeekNumber(days.map((/**
             * @param {?} day
             * @return {?}
             */
            function (day) { return day.date; })), firstDayOfWeek);
            // marking week as collapsed
            weekObject.collapsed = outsideDays === 'collapsed' && days[0].date.month !== month.number &&
                days[days.length - 1].date.month !== month.number;
        }
        return month;
    }
    /**
     * @param {?} calendar
     * @param {?} date
     * @param {?} firstDayOfWeek
     * @return {?}
     */
    function getFirstViewDate(calendar, date, firstDayOfWeek) {
        /** @type {?} */
        var daysPerWeek = calendar.getDaysPerWeek();
        /** @type {?} */
        var firstMonthDate = new NgbDate(date.year, date.month, 1);
        /** @type {?} */
        var dayOfWeek = calendar.getWeekday(firstMonthDate) % daysPerWeek;
        return calendar.getPrev(firstMonthDate, 'd', (daysPerWeek + dayOfWeek - firstDayOfWeek) % daysPerWeek);
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @param {?} locale
     * @return {?}
     */
    function NGB_DATEPICKER_18N_FACTORY(locale) {
        return new NgbDatepickerI18nDefault(locale);
    }
    /**
     * A service supplying i18n data to the datepicker component.
     *
     * The default implementation of this service uses the Angular locale and registered locale data for
     * weekdays and month names (as explained in the Angular i18n guide).
     *
     * It also provides a way to i18n data that depends on calendar calculations, like aria labels, day, week and year
     * numerals. For other static labels the datepicker uses the default Angular i18n.
     *
     * See the [i18n demo](#/components/datepicker/examples#i18n) and
     * [Hebrew calendar demo](#/components/datepicker/calendars#hebrew) on how to extend this class and define
     * a custom provider for i18n.
     * @abstract
     */
    var NgbDatepickerI18n = /** @class */ (function () {
        function NgbDatepickerI18n() {
        }
        /**
         * Returns the textual representation of a day that is rendered in a day cell.
         *
         * @since 3.0.0
         */
        /**
         * Returns the textual representation of a day that is rendered in a day cell.
         *
         * \@since 3.0.0
         * @param {?} date
         * @return {?}
         */
        NgbDatepickerI18n.prototype.getDayNumerals = /**
         * Returns the textual representation of a day that is rendered in a day cell.
         *
         * \@since 3.0.0
         * @param {?} date
         * @return {?}
         */
        function (date) { return "" + date.day; };
        /**
         * Returns the textual representation of a week number rendered by datepicker.
         *
         * @since 3.0.0
         */
        /**
         * Returns the textual representation of a week number rendered by datepicker.
         *
         * \@since 3.0.0
         * @param {?} weekNumber
         * @return {?}
         */
        NgbDatepickerI18n.prototype.getWeekNumerals = /**
         * Returns the textual representation of a week number rendered by datepicker.
         *
         * \@since 3.0.0
         * @param {?} weekNumber
         * @return {?}
         */
        function (weekNumber) { return "" + weekNumber; };
        /**
         * Returns the textual representation of a year that is rendered in the datepicker year select box.
         *
         * @since 3.0.0
         */
        /**
         * Returns the textual representation of a year that is rendered in the datepicker year select box.
         *
         * \@since 3.0.0
         * @param {?} year
         * @return {?}
         */
        NgbDatepickerI18n.prototype.getYearNumerals = /**
         * Returns the textual representation of a year that is rendered in the datepicker year select box.
         *
         * \@since 3.0.0
         * @param {?} year
         * @return {?}
         */
        function (year) { return "" + year; };
        NgbDatepickerI18n.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root', useFactory: NGB_DATEPICKER_18N_FACTORY, deps: [core.LOCALE_ID] },] }
        ];
        /** @nocollapse */ NgbDatepickerI18n.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbDatepickerI18n_Factory() { return NGB_DATEPICKER_18N_FACTORY(core.ɵɵinject(core.LOCALE_ID)); }, token: NgbDatepickerI18n, providedIn: "root" });
        return NgbDatepickerI18n;
    }());
    var NgbDatepickerI18nDefault = /** @class */ (function (_super) {
        __extends(NgbDatepickerI18nDefault, _super);
        function NgbDatepickerI18nDefault(_locale) {
            var _this = _super.call(this) || this;
            _this._locale = _locale;
            /** @type {?} */
            var weekdaysStartingOnSunday = common.getLocaleDayNames(_locale, common.FormStyle.Standalone, common.TranslationWidth.Short);
            _this._weekdaysShort = weekdaysStartingOnSunday.map((/**
             * @param {?} day
             * @param {?} index
             * @return {?}
             */
            function (day, index) { return weekdaysStartingOnSunday[(index + 1) % 7]; }));
            _this._monthsShort = common.getLocaleMonthNames(_locale, common.FormStyle.Standalone, common.TranslationWidth.Abbreviated);
            _this._monthsFull = common.getLocaleMonthNames(_locale, common.FormStyle.Standalone, common.TranslationWidth.Wide);
            return _this;
        }
        /**
         * @param {?} weekday
         * @return {?}
         */
        NgbDatepickerI18nDefault.prototype.getWeekdayShortName = /**
         * @param {?} weekday
         * @return {?}
         */
        function (weekday) { return this._weekdaysShort[weekday - 1]; };
        /**
         * @param {?} month
         * @return {?}
         */
        NgbDatepickerI18nDefault.prototype.getMonthShortName = /**
         * @param {?} month
         * @return {?}
         */
        function (month) { return this._monthsShort[month - 1]; };
        /**
         * @param {?} month
         * @return {?}
         */
        NgbDatepickerI18nDefault.prototype.getMonthFullName = /**
         * @param {?} month
         * @return {?}
         */
        function (month) { return this._monthsFull[month - 1]; };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbDatepickerI18nDefault.prototype.getDayAriaLabel = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            /** @type {?} */
            var jsDate = new Date(date.year, date.month - 1, date.day);
            return common.formatDate(jsDate, 'fullDate', this._locale);
        };
        NgbDatepickerI18nDefault.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        NgbDatepickerI18nDefault.ctorParameters = function () { return [
            { type: String, decorators: [{ type: core.Inject, args: [core.LOCALE_ID,] }] }
        ]; };
        return NgbDatepickerI18nDefault;
    }(NgbDatepickerI18n));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbDatepickerService = /** @class */ (function () {
        function NgbDatepickerService(_calendar, _i18n) {
            this._calendar = _calendar;
            this._i18n = _i18n;
            this._model$ = new rxjs.Subject();
            this._select$ = new rxjs.Subject();
            this._state = {
                disabled: false,
                displayMonths: 1,
                firstDayOfWeek: 1,
                focusVisible: false,
                months: [],
                navigation: 'select',
                outsideDays: 'visible',
                prevDisabled: false,
                nextDisabled: false,
                selectBoxes: { years: [], months: [] },
                selectedDate: null
            };
        }
        Object.defineProperty(NgbDatepickerService.prototype, "model$", {
            get: /**
             * @return {?}
             */
            function () { return this._model$.pipe(operators.filter((/**
             * @param {?} model
             * @return {?}
             */
            function (model) { return model.months.length > 0; }))); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbDatepickerService.prototype, "select$", {
            get: /**
             * @return {?}
             */
            function () { return this._select$.pipe(operators.filter((/**
             * @param {?} date
             * @return {?}
             */
            function (date) { return date !== null; }))); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbDatepickerService.prototype, "dayTemplateData", {
            set: /**
             * @param {?} dayTemplateData
             * @return {?}
             */
            function (dayTemplateData) {
                if (this._state.dayTemplateData !== dayTemplateData) {
                    this._nextState({ dayTemplateData: dayTemplateData });
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbDatepickerService.prototype, "disabled", {
            set: /**
             * @param {?} disabled
             * @return {?}
             */
            function (disabled) {
                if (this._state.disabled !== disabled) {
                    this._nextState({ disabled: disabled });
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbDatepickerService.prototype, "displayMonths", {
            set: /**
             * @param {?} displayMonths
             * @return {?}
             */
            function (displayMonths) {
                displayMonths = toInteger(displayMonths);
                if (isInteger(displayMonths) && displayMonths > 0 && this._state.displayMonths !== displayMonths) {
                    this._nextState({ displayMonths: displayMonths });
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbDatepickerService.prototype, "firstDayOfWeek", {
            set: /**
             * @param {?} firstDayOfWeek
             * @return {?}
             */
            function (firstDayOfWeek) {
                firstDayOfWeek = toInteger(firstDayOfWeek);
                if (isInteger(firstDayOfWeek) && firstDayOfWeek >= 0 && this._state.firstDayOfWeek !== firstDayOfWeek) {
                    this._nextState({ firstDayOfWeek: firstDayOfWeek });
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbDatepickerService.prototype, "focusVisible", {
            set: /**
             * @param {?} focusVisible
             * @return {?}
             */
            function (focusVisible) {
                if (this._state.focusVisible !== focusVisible && !this._state.disabled) {
                    this._nextState({ focusVisible: focusVisible });
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbDatepickerService.prototype, "maxDate", {
            set: /**
             * @param {?} date
             * @return {?}
             */
            function (date) {
                /** @type {?} */
                var maxDate = this.toValidDate(date, null);
                if (isChangedDate(this._state.maxDate, maxDate)) {
                    this._nextState({ maxDate: maxDate });
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbDatepickerService.prototype, "markDisabled", {
            set: /**
             * @param {?} markDisabled
             * @return {?}
             */
            function (markDisabled) {
                if (this._state.markDisabled !== markDisabled) {
                    this._nextState({ markDisabled: markDisabled });
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbDatepickerService.prototype, "minDate", {
            set: /**
             * @param {?} date
             * @return {?}
             */
            function (date) {
                /** @type {?} */
                var minDate = this.toValidDate(date, null);
                if (isChangedDate(this._state.minDate, minDate)) {
                    this._nextState({ minDate: minDate });
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbDatepickerService.prototype, "navigation", {
            set: /**
             * @param {?} navigation
             * @return {?}
             */
            function (navigation) {
                if (this._state.navigation !== navigation) {
                    this._nextState({ navigation: navigation });
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbDatepickerService.prototype, "outsideDays", {
            set: /**
             * @param {?} outsideDays
             * @return {?}
             */
            function (outsideDays) {
                if (this._state.outsideDays !== outsideDays) {
                    this._nextState({ outsideDays: outsideDays });
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} date
         * @return {?}
         */
        NgbDatepickerService.prototype.focus = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            if (!this._state.disabled && this._calendar.isValid(date) && isChangedDate(this._state.focusDate, date)) {
                this._nextState({ focusDate: date });
            }
        };
        /**
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        NgbDatepickerService.prototype.focusMove = /**
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        function (period, number) {
            this.focus(this._calendar.getNext(this._state.focusDate, period, number));
        };
        /**
         * @return {?}
         */
        NgbDatepickerService.prototype.focusSelect = /**
         * @return {?}
         */
        function () {
            if (isDateSelectable(this._state.focusDate, this._state)) {
                this.select(this._state.focusDate, { emitEvent: true });
            }
        };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbDatepickerService.prototype.open = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            /** @type {?} */
            var firstDate = this.toValidDate(date, this._calendar.getToday());
            if (!this._state.disabled && (!this._state.firstDate || isChangedMonth(this._state.firstDate, date))) {
                this._nextState({ firstDate: firstDate });
            }
        };
        /**
         * @param {?} date
         * @param {?=} options
         * @return {?}
         */
        NgbDatepickerService.prototype.select = /**
         * @param {?} date
         * @param {?=} options
         * @return {?}
         */
        function (date, options) {
            if (options === void 0) { options = {}; }
            /** @type {?} */
            var selectedDate = this.toValidDate(date, null);
            if (!this._state.disabled) {
                if (isChangedDate(this._state.selectedDate, selectedDate)) {
                    this._nextState({ selectedDate: selectedDate });
                }
                if (options.emitEvent && isDateSelectable(selectedDate, this._state)) {
                    this._select$.next(selectedDate);
                }
            }
        };
        /**
         * @param {?} date
         * @param {?=} defaultValue
         * @return {?}
         */
        NgbDatepickerService.prototype.toValidDate = /**
         * @param {?} date
         * @param {?=} defaultValue
         * @return {?}
         */
        function (date, defaultValue) {
            /** @type {?} */
            var ngbDate = NgbDate.from(date);
            if (defaultValue === undefined) {
                defaultValue = this._calendar.getToday();
            }
            return this._calendar.isValid(ngbDate) ? ngbDate : defaultValue;
        };
        /**
         * @private
         * @param {?} patch
         * @return {?}
         */
        NgbDatepickerService.prototype._nextState = /**
         * @private
         * @param {?} patch
         * @return {?}
         */
        function (patch) {
            /** @type {?} */
            var newState = this._updateState(patch);
            this._patchContexts(newState);
            this._state = newState;
            this._model$.next(this._state);
        };
        /**
         * @private
         * @param {?} state
         * @return {?}
         */
        NgbDatepickerService.prototype._patchContexts = /**
         * @private
         * @param {?} state
         * @return {?}
         */
        function (state) {
            var months = state.months, displayMonths = state.displayMonths, selectedDate = state.selectedDate, focusDate = state.focusDate, focusVisible = state.focusVisible, disabled = state.disabled, outsideDays = state.outsideDays;
            state.months.forEach((/**
             * @param {?} month
             * @return {?}
             */
            function (month) {
                month.weeks.forEach((/**
                 * @param {?} week
                 * @return {?}
                 */
                function (week) {
                    week.days.forEach((/**
                     * @param {?} day
                     * @return {?}
                     */
                    function (day) {
                        // patch focus flag
                        if (focusDate) {
                            day.context.focused = focusDate.equals(day.date) && focusVisible;
                        }
                        // calculating tabindex
                        day.tabindex = !disabled && day.date.equals(focusDate) && focusDate.month === month.number ? 0 : -1;
                        // override context disabled
                        if (disabled === true) {
                            day.context.disabled = true;
                        }
                        // patch selection flag
                        if (selectedDate !== undefined) {
                            day.context.selected = selectedDate !== null && selectedDate.equals(day.date);
                        }
                        // visibility
                        if (month.number !== day.date.month) {
                            day.hidden = outsideDays === 'hidden' || outsideDays === 'collapsed' ||
                                (displayMonths > 1 && day.date.after(months[0].firstDate) &&
                                    day.date.before(months[displayMonths - 1].lastDate));
                        }
                    }));
                }));
            }));
        };
        /**
         * @private
         * @param {?} patch
         * @return {?}
         */
        NgbDatepickerService.prototype._updateState = /**
         * @private
         * @param {?} patch
         * @return {?}
         */
        function (patch) {
            // patching fields
            /** @type {?} */
            var state = Object.assign({}, this._state, patch);
            /** @type {?} */
            var startDate = state.firstDate;
            // min/max dates changed
            if ('minDate' in patch || 'maxDate' in patch) {
                checkMinBeforeMax(state.minDate, state.maxDate);
                state.focusDate = checkDateInRange(state.focusDate, state.minDate, state.maxDate);
                state.firstDate = checkDateInRange(state.firstDate, state.minDate, state.maxDate);
                startDate = state.focusDate;
            }
            // disabled
            if ('disabled' in patch) {
                state.focusVisible = false;
            }
            // initial rebuild via 'select()'
            if ('selectedDate' in patch && this._state.months.length === 0) {
                startDate = state.selectedDate;
            }
            // terminate early if only focus visibility was changed
            if ('focusVisible' in patch) {
                return state;
            }
            // focus date changed
            if ('focusDate' in patch) {
                state.focusDate = checkDateInRange(state.focusDate, state.minDate, state.maxDate);
                startDate = state.focusDate;
                // nothing to rebuild if only focus changed and it is still visible
                if (state.months.length !== 0 && !state.focusDate.before(state.firstDate) &&
                    !state.focusDate.after(state.lastDate)) {
                    return state;
                }
            }
            // first date changed
            if ('firstDate' in patch) {
                state.firstDate = checkDateInRange(state.firstDate, state.minDate, state.maxDate);
                startDate = state.firstDate;
            }
            // rebuilding months
            if (startDate) {
                /** @type {?} */
                var forceRebuild = 'dayTemplateData' in patch || 'firstDayOfWeek' in patch || 'markDisabled' in patch ||
                    'minDate' in patch || 'maxDate' in patch || 'disabled' in patch || 'outsideDays' in patch;
                /** @type {?} */
                var months = buildMonths(this._calendar, startDate, state, this._i18n, forceRebuild);
                // updating months and boundary dates
                state.months = months;
                state.firstDate = months.length > 0 ? months[0].firstDate : undefined;
                state.lastDate = months.length > 0 ? months[months.length - 1].lastDate : undefined;
                // reset selected date if 'markDisabled' returns true
                if ('selectedDate' in patch && !isDateSelectable(state.selectedDate, state)) {
                    state.selectedDate = null;
                }
                // adjusting focus after months were built
                if ('firstDate' in patch) {
                    if (state.focusDate === undefined || state.focusDate.before(state.firstDate) ||
                        state.focusDate.after(state.lastDate)) {
                        state.focusDate = startDate;
                    }
                }
                // adjusting months/years for the select box navigation
                /** @type {?} */
                var yearChanged = !this._state.firstDate || this._state.firstDate.year !== state.firstDate.year;
                /** @type {?} */
                var monthChanged = !this._state.firstDate || this._state.firstDate.month !== state.firstDate.month;
                if (state.navigation === 'select') {
                    // years ->  boundaries (min/max were changed)
                    if ('minDate' in patch || 'maxDate' in patch || state.selectBoxes.years.length === 0 || yearChanged) {
                        state.selectBoxes.years = generateSelectBoxYears(state.firstDate, state.minDate, state.maxDate);
                    }
                    // months -> when current year or boundaries change
                    if ('minDate' in patch || 'maxDate' in patch || state.selectBoxes.months.length === 0 || yearChanged) {
                        state.selectBoxes.months =
                            generateSelectBoxMonths(this._calendar, state.firstDate, state.minDate, state.maxDate);
                    }
                }
                else {
                    state.selectBoxes = { years: [], months: [] };
                }
                // updating navigation arrows -> boundaries change (min/max) or month/year changes
                if ((state.navigation === 'arrows' || state.navigation === 'select') &&
                    (monthChanged || yearChanged || 'minDate' in patch || 'maxDate' in patch || 'disabled' in patch)) {
                    state.prevDisabled = state.disabled || prevMonthDisabled(this._calendar, state.firstDate, state.minDate);
                    state.nextDisabled = state.disabled || nextMonthDisabled(this._calendar, state.lastDate, state.maxDate);
                }
            }
            return state;
        };
        NgbDatepickerService.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        NgbDatepickerService.ctorParameters = function () { return [
            { type: NgbCalendar },
            { type: NgbDatepickerI18n }
        ]; };
        return NgbDatepickerService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {number} */
    var Key = {
        Tab: 9,
        Enter: 13,
        Escape: 27,
        Space: 32,
        PageUp: 33,
        PageDown: 34,
        End: 35,
        Home: 36,
        ArrowLeft: 37,
        ArrowUp: 38,
        ArrowRight: 39,
        ArrowDown: 40,
    };
    Key[Key.Tab] = 'Tab';
    Key[Key.Enter] = 'Enter';
    Key[Key.Escape] = 'Escape';
    Key[Key.Space] = 'Space';
    Key[Key.PageUp] = 'PageUp';
    Key[Key.PageDown] = 'PageDown';
    Key[Key.End] = 'End';
    Key[Key.Home] = 'Home';
    Key[Key.ArrowLeft] = 'ArrowLeft';
    Key[Key.ArrowUp] = 'ArrowUp';
    Key[Key.ArrowRight] = 'ArrowRight';
    Key[Key.ArrowDown] = 'ArrowDown';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbDatepickerKeyMapService = /** @class */ (function () {
        function NgbDatepickerKeyMapService(_service, _calendar) {
            var _this = this;
            this._service = _service;
            this._calendar = _calendar;
            _service.model$.subscribe((/**
             * @param {?} model
             * @return {?}
             */
            function (model) {
                _this._minDate = model.minDate;
                _this._maxDate = model.maxDate;
                _this._firstViewDate = model.firstDate;
                _this._lastViewDate = model.lastDate;
            }));
        }
        /**
         * @param {?} event
         * @return {?}
         */
        NgbDatepickerKeyMapService.prototype.processKey = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            // tslint:disable-next-line:deprecation
            switch (event.which) {
                case Key.PageUp:
                    this._service.focusMove(event.shiftKey ? 'y' : 'm', -1);
                    break;
                case Key.PageDown:
                    this._service.focusMove(event.shiftKey ? 'y' : 'm', 1);
                    break;
                case Key.End:
                    this._service.focus(event.shiftKey ? this._maxDate : this._lastViewDate);
                    break;
                case Key.Home:
                    this._service.focus(event.shiftKey ? this._minDate : this._firstViewDate);
                    break;
                case Key.ArrowLeft:
                    this._service.focusMove('d', -1);
                    break;
                case Key.ArrowUp:
                    this._service.focusMove('d', -this._calendar.getDaysPerWeek());
                    break;
                case Key.ArrowRight:
                    this._service.focusMove('d', 1);
                    break;
                case Key.ArrowDown:
                    this._service.focusMove('d', this._calendar.getDaysPerWeek());
                    break;
                case Key.Enter:
                case Key.Space:
                    this._service.focusSelect();
                    break;
                default:
                    return;
            }
            // note 'return' in default case
            event.preventDefault();
            event.stopPropagation();
        };
        NgbDatepickerKeyMapService.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        NgbDatepickerKeyMapService.ctorParameters = function () { return [
            { type: NgbDatepickerService },
            { type: NgbCalendar }
        ]; };
        return NgbDatepickerKeyMapService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {number} */
    var NavigationEvent = {
        PREV: 0,
        NEXT: 1,
    };
    NavigationEvent[NavigationEvent.PREV] = 'PREV';
    NavigationEvent[NavigationEvent.NEXT] = 'NEXT';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A configuration service for the [`NgbDatepicker`](#/components/datepicker/api#NgbDatepicker) component.
     *
     * You can inject this service, typically in your root component, and customize the values of its properties in
     * order to provide default values for all the datepickers used in the application.
     */
    var NgbDatepickerConfig = /** @class */ (function () {
        function NgbDatepickerConfig() {
            this.displayMonths = 1;
            this.firstDayOfWeek = 1;
            this.navigation = 'select';
            this.outsideDays = 'visible';
            this.showWeekdays = true;
            this.showWeekNumbers = false;
        }
        NgbDatepickerConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbDatepickerConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbDatepickerConfig_Factory() { return new NgbDatepickerConfig(); }, token: NgbDatepickerConfig, providedIn: "root" });
        return NgbDatepickerConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @return {?}
     */
    function NGB_DATEPICKER_DATE_ADAPTER_FACTORY() {
        return new NgbDateStructAdapter();
    }
    /**
     * An abstract service that does the conversion between the internal datepicker `NgbDateStruct` model and
     * any provided user date model `D`, ex. a string, a native date, etc.
     *
     * The adapter is used **only** for conversion when binding datepicker to a form control,
     * ex. `[(ngModel)]="userDateModel"`. Here `userDateModel` can be of any type.
     *
     * The default datepicker implementation assumes we use `NgbDateStruct` as a user model.
     *
     * See the [date format overview](#/components/datepicker/overview#date-model) for more details
     * and the [custom adapter demo](#/components/datepicker/examples#adapter) for an example.
     * @abstract
     * @template D
     */
    var NgbDateAdapter = /** @class */ (function () {
        function NgbDateAdapter() {
        }
        NgbDateAdapter.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root', useFactory: NGB_DATEPICKER_DATE_ADAPTER_FACTORY },] }
        ];
        /** @nocollapse */ NgbDateAdapter.ngInjectableDef = core.ɵɵdefineInjectable({ factory: NGB_DATEPICKER_DATE_ADAPTER_FACTORY, token: NgbDateAdapter, providedIn: "root" });
        return NgbDateAdapter;
    }());
    var NgbDateStructAdapter = /** @class */ (function (_super) {
        __extends(NgbDateStructAdapter, _super);
        function NgbDateStructAdapter() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * Converts a NgbDateStruct value into NgbDateStruct value
         */
        /**
         * Converts a NgbDateStruct value into NgbDateStruct value
         * @param {?} date
         * @return {?}
         */
        NgbDateStructAdapter.prototype.fromModel = /**
         * Converts a NgbDateStruct value into NgbDateStruct value
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return (date && isInteger(date.year) && isInteger(date.month) && isInteger(date.day)) ?
                { year: date.year, month: date.month, day: date.day } :
                null;
        };
        /**
         * Converts a NgbDateStruct value into NgbDateStruct value
         */
        /**
         * Converts a NgbDateStruct value into NgbDateStruct value
         * @param {?} date
         * @return {?}
         */
        NgbDateStructAdapter.prototype.toModel = /**
         * Converts a NgbDateStruct value into NgbDateStruct value
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return (date && isInteger(date.year) && isInteger(date.month) && isInteger(date.day)) ?
                { year: date.year, month: date.month, day: date.day } :
                null;
        };
        NgbDateStructAdapter.decorators = [
            { type: core.Injectable }
        ];
        return NgbDateStructAdapter;
    }(NgbDateAdapter));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NGB_DATEPICKER_VALUE_ACCESSOR = {
        provide: forms.NG_VALUE_ACCESSOR,
        useExisting: core.forwardRef((/**
         * @return {?}
         */
        function () { return NgbDatepicker; })),
        multi: true
    };
    /**
     * A highly configurable component that helps you with selecting calendar dates.
     *
     * `NgbDatepicker` is meant to be displayed inline on a page or put inside a popup.
     */
    var NgbDatepicker = /** @class */ (function () {
        function NgbDatepicker(_keyMapService, _service, _calendar, i18n, config, _cd, _elementRef, _ngbDateAdapter, _ngZone) {
            var _this = this;
            this._keyMapService = _keyMapService;
            this._service = _service;
            this._calendar = _calendar;
            this.i18n = i18n;
            this._cd = _cd;
            this._elementRef = _elementRef;
            this._ngbDateAdapter = _ngbDateAdapter;
            this._ngZone = _ngZone;
            this._destroyed$ = new rxjs.Subject();
            /**
             * An event emitted right before the navigation happens and displayed month changes.
             *
             * See [`NgbDatepickerNavigateEvent`](#/components/datepicker/api#NgbDatepickerNavigateEvent) for the payload info.
             */
            this.navigate = new core.EventEmitter();
            /**
             * An event emitted when user selects a date using keyboard or mouse.
             *
             * The payload of the event is currently selected `NgbDate`.
             */
            this.select = new core.EventEmitter();
            this.onChange = (/**
             * @param {?} _
             * @return {?}
             */
            function (_) { });
            this.onTouched = (/**
             * @return {?}
             */
            function () { });
            ['dayTemplate', 'dayTemplateData', 'displayMonths', 'firstDayOfWeek', 'footerTemplate', 'markDisabled', 'minDate',
                'maxDate', 'navigation', 'outsideDays', 'showWeekdays', 'showWeekNumbers', 'startDate']
                .forEach((/**
             * @param {?} input
             * @return {?}
             */
            function (input) { return _this[input] = config[input]; }));
            _service.select$.pipe(operators.takeUntil(this._destroyed$)).subscribe((/**
             * @param {?} date
             * @return {?}
             */
            function (date) { _this.select.emit(date); }));
            _service.model$.pipe(operators.takeUntil(this._destroyed$)).subscribe((/**
             * @param {?} model
             * @return {?}
             */
            function (model) {
                /** @type {?} */
                var newDate = model.firstDate;
                /** @type {?} */
                var oldDate = _this.model ? _this.model.firstDate : null;
                /** @type {?} */
                var navigationPrevented = false;
                // emitting navigation event if the first month changes
                if (!newDate.equals(oldDate)) {
                    _this.navigate.emit({
                        current: oldDate ? { year: oldDate.year, month: oldDate.month } : null,
                        next: { year: newDate.year, month: newDate.month },
                        preventDefault: (/**
                         * @return {?}
                         */
                        function () { return navigationPrevented = true; })
                    });
                    // can't prevent the very first navigation
                    if (navigationPrevented && oldDate !== null) {
                        _this._service.open(oldDate);
                        return;
                    }
                }
                /** @type {?} */
                var newSelectedDate = model.selectedDate;
                /** @type {?} */
                var newFocusedDate = model.focusDate;
                /** @type {?} */
                var oldFocusedDate = _this.model ? _this.model.focusDate : null;
                _this.model = model;
                // handling selection change
                if (isChangedDate(newSelectedDate, _this._controlValue)) {
                    _this._controlValue = newSelectedDate;
                    _this.onTouched();
                    _this.onChange(_this._ngbDateAdapter.toModel(newSelectedDate));
                }
                // handling focus change
                if (isChangedDate(newFocusedDate, oldFocusedDate) && oldFocusedDate && model.focusVisible) {
                    _this.focus();
                }
                _cd.markForCheck();
            }));
        }
        /**
         * @return {?}
         */
        NgbDatepicker.prototype.focus = /**
         * @return {?}
         */
        function () {
            var _this = this;
            this._ngZone.onStable.asObservable().pipe(operators.take(1)).subscribe((/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var elementToFocus = _this._elementRef.nativeElement.querySelector('div.ngb-dp-day[tabindex="0"]');
                if (elementToFocus) {
                    elementToFocus.focus();
                }
            }));
        };
        /**
         * Navigates to the provided date.
         *
         * With the default calendar we use ISO 8601: 'month' is 1=Jan ... 12=Dec.
         * If nothing or invalid date provided calendar will open current month.
         *
         * Use the `[startDate]` input as an alternative.
         */
        /**
         * Navigates to the provided date.
         *
         * With the default calendar we use ISO 8601: 'month' is 1=Jan ... 12=Dec.
         * If nothing or invalid date provided calendar will open current month.
         *
         * Use the `[startDate]` input as an alternative.
         * @param {?=} date
         * @return {?}
         */
        NgbDatepicker.prototype.navigateTo = /**
         * Navigates to the provided date.
         *
         * With the default calendar we use ISO 8601: 'month' is 1=Jan ... 12=Dec.
         * If nothing or invalid date provided calendar will open current month.
         *
         * Use the `[startDate]` input as an alternative.
         * @param {?=} date
         * @return {?}
         */
        function (date) {
            this._service.open(NgbDate.from(date ? date.day ? (/** @type {?} */ (date)) : __assign({}, date, { day: 1 }) : null));
        };
        /**
         * @return {?}
         */
        NgbDatepicker.prototype.ngAfterViewInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            this._ngZone.runOutsideAngular((/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var focusIns$ = rxjs.fromEvent(_this._monthsEl.nativeElement, 'focusin');
                /** @type {?} */
                var focusOuts$ = rxjs.fromEvent(_this._monthsEl.nativeElement, 'focusout');
                // we're changing 'focusVisible' only when entering or leaving months view
                // and ignoring all focus events where both 'target' and 'related' target are day cells
                rxjs.merge(focusIns$, focusOuts$)
                    .pipe(operators.filter((/**
                 * @param {?} __0
                 * @return {?}
                 */
                function (_a) {
                    var target = _a.target, relatedTarget = _a.relatedTarget;
                    return !(hasClassName(target, 'ngb-dp-day') && hasClassName(relatedTarget, 'ngb-dp-day'));
                })), operators.takeUntil(_this._destroyed$))
                    .subscribe((/**
                 * @param {?} __0
                 * @return {?}
                 */
                function (_a) {
                    var type = _a.type;
                    return _this._ngZone.run((/**
                     * @return {?}
                     */
                    function () { return _this._service.focusVisible = type === 'focusin'; }));
                }));
            }));
        };
        /**
         * @return {?}
         */
        NgbDatepicker.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () { this._destroyed$.next(); };
        /**
         * @return {?}
         */
        NgbDatepicker.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            if (this.model === undefined) {
                ['dayTemplateData', 'displayMonths', 'markDisabled', 'firstDayOfWeek', 'navigation', 'minDate', 'maxDate',
                    'outsideDays']
                    .forEach((/**
                 * @param {?} input
                 * @return {?}
                 */
                function (input) { return _this._service[input] = _this[input]; }));
                this.navigateTo(this.startDate);
            }
        };
        /**
         * @param {?} changes
         * @return {?}
         */
        NgbDatepicker.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            var _this = this;
            ['dayTemplateData', 'displayMonths', 'markDisabled', 'firstDayOfWeek', 'navigation', 'minDate', 'maxDate',
                'outsideDays']
                .filter((/**
             * @param {?} input
             * @return {?}
             */
            function (input) { return input in changes; }))
                .forEach((/**
             * @param {?} input
             * @return {?}
             */
            function (input) { return _this._service[input] = _this[input]; }));
            if ('startDate' in changes) {
                var _a = changes.startDate, currentValue = _a.currentValue, previousValue = _a.previousValue;
                if (isChangedMonth(previousValue, currentValue)) {
                    this.navigateTo(this.startDate);
                }
            }
        };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbDatepicker.prototype.onDateSelect = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            this._service.focus(date);
            this._service.select(date, { emitEvent: true });
        };
        /**
         * @param {?} event
         * @return {?}
         */
        NgbDatepicker.prototype.onKeyDown = /**
         * @param {?} event
         * @return {?}
         */
        function (event) { this._keyMapService.processKey(event); };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbDatepicker.prototype.onNavigateDateSelect = /**
         * @param {?} date
         * @return {?}
         */
        function (date) { this._service.open(date); };
        /**
         * @param {?} event
         * @return {?}
         */
        NgbDatepicker.prototype.onNavigateEvent = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            switch (event) {
                case NavigationEvent.PREV:
                    this._service.open(this._calendar.getPrev(this.model.firstDate, 'm', 1));
                    break;
                case NavigationEvent.NEXT:
                    this._service.open(this._calendar.getNext(this.model.firstDate, 'm', 1));
                    break;
            }
        };
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbDatepicker.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this.onChange = fn; };
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbDatepicker.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this.onTouched = fn; };
        /**
         * @param {?} isDisabled
         * @return {?}
         */
        NgbDatepicker.prototype.setDisabledState = /**
         * @param {?} isDisabled
         * @return {?}
         */
        function (isDisabled) { this._service.disabled = isDisabled; };
        /**
         * @param {?} value
         * @return {?}
         */
        NgbDatepicker.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._controlValue = NgbDate.from(this._ngbDateAdapter.fromModel(value));
            this._service.select(this._controlValue);
        };
        NgbDatepicker.decorators = [
            { type: core.Component, args: [{
                        exportAs: 'ngbDatepicker',
                        selector: 'ngb-datepicker',
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        encapsulation: core.ViewEncapsulation.None,
                        template: "\n    <ng-template #dt let-date=\"date\" let-currentMonth=\"currentMonth\" let-selected=\"selected\" let-disabled=\"disabled\" let-focused=\"focused\">\n      <div ngbDatepickerDayView\n        [date]=\"date\"\n        [currentMonth]=\"currentMonth\"\n        [selected]=\"selected\"\n        [disabled]=\"disabled\"\n        [focused]=\"focused\">\n      </div>\n    </ng-template>\n\n    <div class=\"ngb-dp-header\">\n      <ngb-datepicker-navigation *ngIf=\"navigation !== 'none'\"\n        [date]=\"model.firstDate\"\n        [months]=\"model.months\"\n        [disabled]=\"model.disabled\"\n        [showSelect]=\"model.navigation === 'select'\"\n        [prevDisabled]=\"model.prevDisabled\"\n        [nextDisabled]=\"model.nextDisabled\"\n        [selectBoxes]=\"model.selectBoxes\"\n        (navigate)=\"onNavigateEvent($event)\"\n        (select)=\"onNavigateDateSelect($event)\">\n      </ngb-datepicker-navigation>\n    </div>\n\n    <div #months class=\"ngb-dp-months\" (keydown)=\"onKeyDown($event)\">\n      <ng-template ngFor let-month [ngForOf]=\"model.months\" let-i=\"index\">\n        <div class=\"ngb-dp-month\">\n          <div *ngIf=\"navigation === 'none' || (displayMonths > 1 && navigation === 'select')\"\n                class=\"ngb-dp-month-name\">\n            {{ i18n.getMonthFullName(month.number, month.year) }} {{ i18n.getYearNumerals(month.year) }}\n          </div>\n          <ngb-datepicker-month-view\n            [month]=\"month\"\n            [dayTemplate]=\"dayTemplate || dt\"\n            [showWeekdays]=\"showWeekdays\"\n            [showWeekNumbers]=\"showWeekNumbers\"\n            (select)=\"onDateSelect($event)\">\n          </ngb-datepicker-month-view>\n        </div>\n      </ng-template>\n    </div>\n\n    <ng-template [ngTemplateOutlet]=\"footerTemplate\"></ng-template>\n  ",
                        providers: [NGB_DATEPICKER_VALUE_ACCESSOR, NgbDatepickerService, NgbDatepickerKeyMapService],
                        styles: ["ngb-datepicker{border:1px solid #dfdfdf;border-radius:.25rem;display:inline-block}ngb-datepicker-month-view{pointer-events:auto}ngb-datepicker.dropdown-menu{padding:0}.ngb-dp-body{z-index:1050}.ngb-dp-header{border-bottom:0;border-radius:.25rem .25rem 0 0;padding-top:.25rem;background-color:#f8f9fa}.ngb-dp-months{display:-ms-flexbox;display:flex}.ngb-dp-month{pointer-events:none}.ngb-dp-month-name{font-size:larger;height:2rem;line-height:2rem;text-align:center;background-color:#f8f9fa}.ngb-dp-month+.ngb-dp-month .ngb-dp-month-name,.ngb-dp-month+.ngb-dp-month .ngb-dp-week{padding-left:1rem}.ngb-dp-month:last-child .ngb-dp-week{padding-right:.25rem}.ngb-dp-month:first-child .ngb-dp-week{padding-left:.25rem}.ngb-dp-month .ngb-dp-week:last-child{padding-bottom:.25rem}"]
                    }] }
        ];
        /** @nocollapse */
        NgbDatepicker.ctorParameters = function () { return [
            { type: NgbDatepickerKeyMapService },
            { type: NgbDatepickerService },
            { type: NgbCalendar },
            { type: NgbDatepickerI18n },
            { type: NgbDatepickerConfig },
            { type: core.ChangeDetectorRef },
            { type: core.ElementRef },
            { type: NgbDateAdapter },
            { type: core.NgZone }
        ]; };
        NgbDatepicker.propDecorators = {
            _monthsEl: [{ type: core.ViewChild, args: ['months', { static: true },] }],
            dayTemplate: [{ type: core.Input }],
            dayTemplateData: [{ type: core.Input }],
            displayMonths: [{ type: core.Input }],
            firstDayOfWeek: [{ type: core.Input }],
            footerTemplate: [{ type: core.Input }],
            markDisabled: [{ type: core.Input }],
            maxDate: [{ type: core.Input }],
            minDate: [{ type: core.Input }],
            navigation: [{ type: core.Input }],
            outsideDays: [{ type: core.Input }],
            showWeekdays: [{ type: core.Input }],
            showWeekNumbers: [{ type: core.Input }],
            startDate: [{ type: core.Input }],
            navigate: [{ type: core.Output }],
            select: [{ type: core.Output }]
        };
        return NgbDatepicker;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbDatepickerMonthView = /** @class */ (function () {
        function NgbDatepickerMonthView(i18n) {
            this.i18n = i18n;
            this.select = new core.EventEmitter();
        }
        /**
         * @param {?} day
         * @return {?}
         */
        NgbDatepickerMonthView.prototype.doSelect = /**
         * @param {?} day
         * @return {?}
         */
        function (day) {
            if (!day.context.disabled && !day.hidden) {
                this.select.emit(day.date);
            }
        };
        NgbDatepickerMonthView.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-datepicker-month-view',
                        host: { 'role': 'grid' },
                        encapsulation: core.ViewEncapsulation.None,
                        template: "\n    <div *ngIf=\"showWeekdays\" class=\"ngb-dp-week ngb-dp-weekdays bg-light\">\n      <div *ngIf=\"showWeekNumbers\" class=\"ngb-dp-weekday ngb-dp-showweek\"></div>\n      <div *ngFor=\"let w of month.weekdays\" class=\"ngb-dp-weekday small\">\n        {{ i18n.getWeekdayShortName(w) }}\n      </div>\n    </div>\n    <ng-template ngFor let-week [ngForOf]=\"month.weeks\">\n      <div *ngIf=\"!week.collapsed\" class=\"ngb-dp-week\" role=\"row\">\n        <div *ngIf=\"showWeekNumbers\" class=\"ngb-dp-week-number small text-muted\">{{ i18n.getWeekNumerals(week.number) }}</div>\n        <div *ngFor=\"let day of week.days\" (click)=\"doSelect(day)\" class=\"ngb-dp-day\" role=\"gridcell\"\n          [class.disabled]=\"day.context.disabled\"\n          [tabindex]=\"day.tabindex\"\n          [class.hidden]=\"day.hidden\"\n          [class.ngb-dp-today]=\"day.context.today\"\n          [attr.aria-label]=\"day.ariaLabel\">\n          <ng-template [ngIf]=\"!day.hidden\">\n            <ng-template [ngTemplateOutlet]=\"dayTemplate\" [ngTemplateOutletContext]=\"day.context\"></ng-template>\n          </ng-template>\n        </div>\n      </div>\n    </ng-template>\n  ",
                        styles: ["ngb-datepicker-month-view{display:block}.ngb-dp-week-number,.ngb-dp-weekday{line-height:2rem;text-align:center;font-style:italic}.ngb-dp-weekday{color:#5bc0de;color:var(--info)}.ngb-dp-week{border-radius:.25rem;display:-ms-flexbox;display:flex}.ngb-dp-weekdays{border-bottom:1px solid rgba(0,0,0,.125);border-radius:0}.ngb-dp-day,.ngb-dp-week-number,.ngb-dp-weekday{width:2rem;height:2rem}.ngb-dp-day{cursor:pointer}.ngb-dp-day.disabled,.ngb-dp-day.hidden{cursor:default}"]
                    }] }
        ];
        /** @nocollapse */
        NgbDatepickerMonthView.ctorParameters = function () { return [
            { type: NgbDatepickerI18n }
        ]; };
        NgbDatepickerMonthView.propDecorators = {
            dayTemplate: [{ type: core.Input }],
            month: [{ type: core.Input }],
            showWeekdays: [{ type: core.Input }],
            showWeekNumbers: [{ type: core.Input }],
            select: [{ type: core.Output }]
        };
        return NgbDatepickerMonthView;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbDatepickerNavigation = /** @class */ (function () {
        function NgbDatepickerNavigation(i18n) {
            this.i18n = i18n;
            this.navigation = NavigationEvent;
            this.months = [];
            this.navigate = new core.EventEmitter();
            this.select = new core.EventEmitter();
        }
        NgbDatepickerNavigation.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-datepicker-navigation',
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        encapsulation: core.ViewEncapsulation.None,
                        template: "\n    <div class=\"ngb-dp-arrow\">\n      <button type=\"button\" class=\"btn btn-link ngb-dp-arrow-btn\" (click)=\"navigate.emit(navigation.PREV)\" [disabled]=\"prevDisabled\"\n              i18n-aria-label=\"@@ngb.datepicker.previous-month\" aria-label=\"Previous month\"\n              i18n-title=\"@@ngb.datepicker.previous-month\" title=\"Previous month\">\n        <span class=\"ngb-dp-navigation-chevron\"></span>\n      </button>\n    </div>\n    <ngb-datepicker-navigation-select *ngIf=\"showSelect\" class=\"ngb-dp-navigation-select\"\n      [date]=\"date\"\n      [disabled] = \"disabled\"\n      [months]=\"selectBoxes.months\"\n      [years]=\"selectBoxes.years\"\n      (select)=\"select.emit($event)\">\n    </ngb-datepicker-navigation-select>\n\n    <ng-template *ngIf=\"!showSelect\" ngFor let-month [ngForOf]=\"months\" let-i=\"index\">\n      <div class=\"ngb-dp-arrow\" *ngIf=\"i > 0\"></div>\n      <div class=\"ngb-dp-month-name\">\n        {{ i18n.getMonthFullName(month.number, month.year) }} {{ i18n.getYearNumerals(month.year) }}\n      </div>\n      <div class=\"ngb-dp-arrow\" *ngIf=\"i !== months.length - 1\"></div>\n    </ng-template>\n    <div class=\"ngb-dp-arrow right\">\n      <button type=\"button\" class=\"btn btn-link ngb-dp-arrow-btn\" (click)=\"navigate.emit(navigation.NEXT)\" [disabled]=\"nextDisabled\"\n              i18n-aria-label=\"@@ngb.datepicker.next-month\" aria-label=\"Next month\"\n              i18n-title=\"@@ngb.datepicker.next-month\" title=\"Next month\">\n        <span class=\"ngb-dp-navigation-chevron\"></span>\n      </button>\n    </div>\n    ",
                        styles: ["ngb-datepicker-navigation{display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center}.ngb-dp-navigation-chevron{border-style:solid;border-width:.2em .2em 0 0;display:inline-block;width:.75em;height:.75em;margin-left:.25em;margin-right:.15em;-webkit-transform:rotate(-135deg);transform:rotate(-135deg)}.right .ngb-dp-navigation-chevron{-webkit-transform:rotate(45deg);transform:rotate(45deg);margin-left:.15em;margin-right:.25em}.ngb-dp-arrow{display:-ms-flexbox;display:flex;-ms-flex:1 1 auto;flex:1 1 auto;padding-right:0;padding-left:0;margin:0;width:2rem;height:2rem}.ngb-dp-arrow.right{-ms-flex-pack:end;justify-content:flex-end}.ngb-dp-arrow-btn{padding:0 .25rem;margin:0 .5rem;border:none;background-color:transparent;z-index:1}.ngb-dp-arrow-btn:focus{outline-width:1px;outline-style:auto}@media all and (-ms-high-contrast:none),(-ms-high-contrast:active){.ngb-dp-arrow-btn:focus{outline-style:solid}}.ngb-dp-month-name{font-size:larger;height:2rem;line-height:2rem;text-align:center}.ngb-dp-navigation-select{display:-ms-flexbox;display:flex;-ms-flex:1 1 9rem;flex:1 1 9rem}"]
                    }] }
        ];
        /** @nocollapse */
        NgbDatepickerNavigation.ctorParameters = function () { return [
            { type: NgbDatepickerI18n }
        ]; };
        NgbDatepickerNavigation.propDecorators = {
            date: [{ type: core.Input }],
            disabled: [{ type: core.Input }],
            months: [{ type: core.Input }],
            showSelect: [{ type: core.Input }],
            prevDisabled: [{ type: core.Input }],
            nextDisabled: [{ type: core.Input }],
            selectBoxes: [{ type: core.Input }],
            navigate: [{ type: core.Output }],
            select: [{ type: core.Output }]
        };
        return NgbDatepickerNavigation;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var isContainedIn = (/**
     * @param {?} element
     * @param {?=} array
     * @return {?}
     */
    function (element, array) {
        return array ? array.some((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.contains(element); })) : false;
    });
    /** @type {?} */
    var matchesSelectorIfAny = (/**
     * @param {?} element
     * @param {?=} selector
     * @return {?}
     */
    function (element, selector) {
        return !selector || closest(element, selector) != null;
    });
    // we'll have to use 'touch' events instead of 'mouse' events on iOS and add a more significant delay
    // to avoid re-opening when handling (click) on a toggling element
    // TODO: use proper Angular platform detection when NgbAutoClose becomes a service and we can inject PLATFORM_ID
    /** @type {?} */
    var iOS = false;
    if (typeof navigator !== 'undefined') {
        iOS = !!navigator.userAgent && /iPad|iPhone|iPod/.test(navigator.userAgent);
    }
    /**
     * @param {?} zone
     * @param {?} document
     * @param {?} type
     * @param {?} close
     * @param {?} closed$
     * @param {?} insideElements
     * @param {?=} ignoreElements
     * @param {?=} insideSelector
     * @return {?}
     */
    function ngbAutoClose(zone, document, type, close, closed$, insideElements, ignoreElements, insideSelector) {
        // closing on ESC and outside clicks
        if (type) {
            zone.runOutsideAngular((/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var shouldCloseOnClick = (/**
                 * @param {?} event
                 * @return {?}
                 */
                function (event) {
                    /** @type {?} */
                    var element = (/** @type {?} */ (event.target));
                    if ((event instanceof MouseEvent && event.button === 2) || isContainedIn(element, ignoreElements)) {
                        return false;
                    }
                    if (type === 'inside') {
                        return isContainedIn(element, insideElements) && matchesSelectorIfAny(element, insideSelector);
                    }
                    else if (type === 'outside') {
                        return !isContainedIn(element, insideElements);
                    }
                    else /* if (type === true) */ {
                        return matchesSelectorIfAny(element, insideSelector) || !isContainedIn(element, insideElements);
                    }
                });
                /** @type {?} */
                var escapes$ = rxjs.fromEvent(document, 'keydown')
                    .pipe(operators.takeUntil(closed$), 
                // tslint:disable-next-line:deprecation
                operators.filter((/**
                 * @param {?} e
                 * @return {?}
                 */
                function (e) { return e.which === Key.Escape; })));
                // we have to pre-calculate 'shouldCloseOnClick' on 'mousedown/touchstart',
                // because on 'mouseup/touchend' DOM nodes might be detached
                /** @type {?} */
                var mouseDowns$ = rxjs.fromEvent(document, iOS ? 'touchstart' : 'mousedown')
                    .pipe(operators.map(shouldCloseOnClick), operators.takeUntil(closed$));
                /** @type {?} */
                var closeableClicks$ = (/** @type {?} */ (rxjs.fromEvent(document, iOS ? 'touchend' : 'mouseup')
                    .pipe(operators.withLatestFrom(mouseDowns$), operators.filter((/**
                 * @param {?} __0
                 * @return {?}
                 */
                function (_a) {
                    var _b = __read(_a, 2), _ = _b[0], shouldClose = _b[1];
                    return shouldClose;
                })), operators.delay(iOS ? 16 : 0), operators.takeUntil(closed$))));
                rxjs.race([escapes$, closeableClicks$]).subscribe((/**
                 * @return {?}
                 */
                function () { return zone.run(close); }));
            }));
        }
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var FOCUSABLE_ELEMENTS_SELECTOR = [
        'a[href]', 'button:not([disabled])', 'input:not([disabled]):not([type="hidden"])', 'select:not([disabled])',
        'textarea:not([disabled])', '[contenteditable]', '[tabindex]:not([tabindex="-1"])'
    ].join(', ');
    /**
     * Returns first and last focusable elements inside of a given element based on specific CSS selector
     * @param {?} element
     * @return {?}
     */
    function getFocusableBoundaryElements(element) {
        /** @type {?} */
        var list = Array.from((/** @type {?} */ (element.querySelectorAll(FOCUSABLE_ELEMENTS_SELECTOR))))
            .filter((/**
         * @param {?} el
         * @return {?}
         */
        function (el) { return el.tabIndex !== -1; }));
        return [list[0], list[list.length - 1]];
    }
    /**
     * Function that enforces browser focus to be trapped inside a DOM element.
     *
     * Works only for clicks inside the element and navigation with 'Tab', ignoring clicks outside of the element
     *
     * \@param element The element around which focus will be trapped inside
     * \@param stopFocusTrap$ The observable stream. When completed the focus trap will clean up listeners
     * and free internal resources
     * \@param refocusOnClick Put the focus back to the last focused element whenever a click occurs on element (default to
     * false)
     * @type {?}
     */
    var ngbFocusTrap = (/**
     * @param {?} element
     * @param {?} stopFocusTrap$
     * @param {?=} refocusOnClick
     * @return {?}
     */
    function (element, stopFocusTrap$, refocusOnClick) {
        if (refocusOnClick === void 0) { refocusOnClick = false; }
        // last focused element
        /** @type {?} */
        var lastFocusedElement$ = rxjs.fromEvent(element, 'focusin').pipe(operators.takeUntil(stopFocusTrap$), operators.map((/**
         * @param {?} e
         * @return {?}
         */
        function (e) { return e.target; })));
        // 'tab' / 'shift+tab' stream
        rxjs.fromEvent(element, 'keydown')
            .pipe(operators.takeUntil(stopFocusTrap$), 
        // tslint:disable:deprecation
        operators.filter((/**
         * @param {?} e
         * @return {?}
         */
        function (e) { return e.which === Key.Tab; })), 
        // tslint:enable:deprecation
        operators.withLatestFrom(lastFocusedElement$))
            .subscribe((/**
         * @param {?} __0
         * @return {?}
         */
        function (_a) {
            var _b = __read(_a, 2), tabEvent = _b[0], focusedElement = _b[1];
            var _c = __read(getFocusableBoundaryElements(element), 2), first = _c[0], last = _c[1];
            if ((focusedElement === first || focusedElement === element) && tabEvent.shiftKey) {
                last.focus();
                tabEvent.preventDefault();
            }
            if (focusedElement === last && !tabEvent.shiftKey) {
                first.focus();
                tabEvent.preventDefault();
            }
        }));
        // inside click
        if (refocusOnClick) {
            rxjs.fromEvent(element, 'click')
                .pipe(operators.takeUntil(stopFocusTrap$), operators.withLatestFrom(lastFocusedElement$), operators.map((/**
             * @param {?} arr
             * @return {?}
             */
            function (arr) { return (/** @type {?} */ (arr[1])); })))
                .subscribe((/**
             * @param {?} lastFocusedElement
             * @return {?}
             */
            function (lastFocusedElement) { return lastFocusedElement.focus(); }));
        }
    });

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    // previous version:
    // https://github.com/angular-ui/bootstrap/blob/07c31d0731f7cb068a1932b8e01d2312b796b4ec/src/position/position.js
    var 
    // previous version:
    // https://github.com/angular-ui/bootstrap/blob/07c31d0731f7cb068a1932b8e01d2312b796b4ec/src/position/position.js
    Positioning = /** @class */ (function () {
        function Positioning() {
        }
        /**
         * @private
         * @param {?} element
         * @return {?}
         */
        Positioning.prototype.getAllStyles = /**
         * @private
         * @param {?} element
         * @return {?}
         */
        function (element) { return window.getComputedStyle(element); };
        /**
         * @private
         * @param {?} element
         * @param {?} prop
         * @return {?}
         */
        Positioning.prototype.getStyle = /**
         * @private
         * @param {?} element
         * @param {?} prop
         * @return {?}
         */
        function (element, prop) { return this.getAllStyles(element)[prop]; };
        /**
         * @private
         * @param {?} element
         * @return {?}
         */
        Positioning.prototype.isStaticPositioned = /**
         * @private
         * @param {?} element
         * @return {?}
         */
        function (element) {
            return (this.getStyle(element, 'position') || 'static') === 'static';
        };
        /**
         * @private
         * @param {?} element
         * @return {?}
         */
        Positioning.prototype.offsetParent = /**
         * @private
         * @param {?} element
         * @return {?}
         */
        function (element) {
            /** @type {?} */
            var offsetParentEl = (/** @type {?} */ (element.offsetParent)) || document.documentElement;
            while (offsetParentEl && offsetParentEl !== document.documentElement && this.isStaticPositioned(offsetParentEl)) {
                offsetParentEl = (/** @type {?} */ (offsetParentEl.offsetParent));
            }
            return offsetParentEl || document.documentElement;
        };
        /**
         * @param {?} element
         * @param {?=} round
         * @return {?}
         */
        Positioning.prototype.position = /**
         * @param {?} element
         * @param {?=} round
         * @return {?}
         */
        function (element, round) {
            if (round === void 0) { round = true; }
            /** @type {?} */
            var elPosition;
            /** @type {?} */
            var parentOffset = { width: 0, height: 0, top: 0, bottom: 0, left: 0, right: 0 };
            if (this.getStyle(element, 'position') === 'fixed') {
                elPosition = element.getBoundingClientRect();
                elPosition = {
                    top: elPosition.top,
                    bottom: elPosition.bottom,
                    left: elPosition.left,
                    right: elPosition.right,
                    height: elPosition.height,
                    width: elPosition.width
                };
            }
            else {
                /** @type {?} */
                var offsetParentEl = this.offsetParent(element);
                elPosition = this.offset(element, false);
                if (offsetParentEl !== document.documentElement) {
                    parentOffset = this.offset(offsetParentEl, false);
                }
                parentOffset.top += offsetParentEl.clientTop;
                parentOffset.left += offsetParentEl.clientLeft;
            }
            elPosition.top -= parentOffset.top;
            elPosition.bottom -= parentOffset.top;
            elPosition.left -= parentOffset.left;
            elPosition.right -= parentOffset.left;
            if (round) {
                elPosition.top = Math.round(elPosition.top);
                elPosition.bottom = Math.round(elPosition.bottom);
                elPosition.left = Math.round(elPosition.left);
                elPosition.right = Math.round(elPosition.right);
            }
            return elPosition;
        };
        /**
         * @param {?} element
         * @param {?=} round
         * @return {?}
         */
        Positioning.prototype.offset = /**
         * @param {?} element
         * @param {?=} round
         * @return {?}
         */
        function (element, round) {
            if (round === void 0) { round = true; }
            /** @type {?} */
            var elBcr = element.getBoundingClientRect();
            /** @type {?} */
            var viewportOffset = {
                top: window.pageYOffset - document.documentElement.clientTop,
                left: window.pageXOffset - document.documentElement.clientLeft
            };
            /** @type {?} */
            var elOffset = {
                height: elBcr.height || element.offsetHeight,
                width: elBcr.width || element.offsetWidth,
                top: elBcr.top + viewportOffset.top,
                bottom: elBcr.bottom + viewportOffset.top,
                left: elBcr.left + viewportOffset.left,
                right: elBcr.right + viewportOffset.left
            };
            if (round) {
                elOffset.height = Math.round(elOffset.height);
                elOffset.width = Math.round(elOffset.width);
                elOffset.top = Math.round(elOffset.top);
                elOffset.bottom = Math.round(elOffset.bottom);
                elOffset.left = Math.round(elOffset.left);
                elOffset.right = Math.round(elOffset.right);
            }
            return elOffset;
        };
        /*
          Return false if the element to position is outside the viewport
        */
        /*
            Return false if the element to position is outside the viewport
          */
        /**
         * @param {?} hostElement
         * @param {?} targetElement
         * @param {?} placement
         * @param {?=} appendToBody
         * @return {?}
         */
        Positioning.prototype.positionElements = /*
            Return false if the element to position is outside the viewport
          */
        /**
         * @param {?} hostElement
         * @param {?} targetElement
         * @param {?} placement
         * @param {?=} appendToBody
         * @return {?}
         */
        function (hostElement, targetElement, placement, appendToBody) {
            var _a = __read(placement.split('-'), 2), _b = _a[0], placementPrimary = _b === void 0 ? 'top' : _b, _c = _a[1], placementSecondary = _c === void 0 ? 'center' : _c;
            /** @type {?} */
            var hostElPosition = appendToBody ? this.offset(hostElement, false) : this.position(hostElement, false);
            /** @type {?} */
            var targetElStyles = this.getAllStyles(targetElement);
            /** @type {?} */
            var marginTop = parseFloat(targetElStyles.marginTop);
            /** @type {?} */
            var marginBottom = parseFloat(targetElStyles.marginBottom);
            /** @type {?} */
            var marginLeft = parseFloat(targetElStyles.marginLeft);
            /** @type {?} */
            var marginRight = parseFloat(targetElStyles.marginRight);
            /** @type {?} */
            var topPosition = 0;
            /** @type {?} */
            var leftPosition = 0;
            switch (placementPrimary) {
                case 'top':
                    topPosition = (hostElPosition.top - (targetElement.offsetHeight + marginTop + marginBottom));
                    break;
                case 'bottom':
                    topPosition = (hostElPosition.top + hostElPosition.height);
                    break;
                case 'left':
                    leftPosition = (hostElPosition.left - (targetElement.offsetWidth + marginLeft + marginRight));
                    break;
                case 'right':
                    leftPosition = (hostElPosition.left + hostElPosition.width);
                    break;
            }
            switch (placementSecondary) {
                case 'top':
                    topPosition = hostElPosition.top;
                    break;
                case 'bottom':
                    topPosition = hostElPosition.top + hostElPosition.height - targetElement.offsetHeight;
                    break;
                case 'left':
                    leftPosition = hostElPosition.left;
                    break;
                case 'right':
                    leftPosition = hostElPosition.left + hostElPosition.width - targetElement.offsetWidth;
                    break;
                case 'center':
                    if (placementPrimary === 'top' || placementPrimary === 'bottom') {
                        leftPosition = (hostElPosition.left + hostElPosition.width / 2 - targetElement.offsetWidth / 2);
                    }
                    else {
                        topPosition = (hostElPosition.top + hostElPosition.height / 2 - targetElement.offsetHeight / 2);
                    }
                    break;
            }
            /// The translate3d/gpu acceleration render a blurry text on chrome, the next line is commented until a browser fix
            // targetElement.style.transform = `translate3d(${Math.round(leftPosition)}px, ${Math.floor(topPosition)}px, 0px)`;
            targetElement.style.transform = "translate(" + Math.round(leftPosition) + "px, " + Math.round(topPosition) + "px)";
            // Check if the targetElement is inside the viewport
            /** @type {?} */
            var targetElBCR = targetElement.getBoundingClientRect();
            /** @type {?} */
            var html = document.documentElement;
            /** @type {?} */
            var windowHeight = window.innerHeight || html.clientHeight;
            /** @type {?} */
            var windowWidth = window.innerWidth || html.clientWidth;
            return targetElBCR.left >= 0 && targetElBCR.top >= 0 && targetElBCR.right <= windowWidth &&
                targetElBCR.bottom <= windowHeight;
        };
        return Positioning;
    }());
    /** @type {?} */
    var placementSeparator = /\s+/;
    /** @type {?} */
    var positionService = new Positioning();
    /*
     * Accept the placement array and applies the appropriate placement dependent on the viewport.
     * Returns the applied placement.
     * In case of auto placement, placements are selected in order
     *   'top', 'bottom', 'left', 'right',
     *   'top-left', 'top-right',
     *   'bottom-left', 'bottom-right',
     *   'left-top', 'left-bottom',
     *   'right-top', 'right-bottom'.
     * */
    /**
     * @param {?} hostElement
     * @param {?} targetElement
     * @param {?} placement
     * @param {?=} appendToBody
     * @param {?=} baseClass
     * @return {?}
     */
    function positionElements(hostElement, targetElement, placement, appendToBody, baseClass) {
        var e_1, _a;
        /** @type {?} */
        var placementVals = Array.isArray(placement) ? placement : (/** @type {?} */ (placement.split(placementSeparator)));
        /** @type {?} */
        var allowedPlacements = [
            'top', 'bottom', 'left', 'right', 'top-left', 'top-right', 'bottom-left', 'bottom-right', 'left-top', 'left-bottom',
            'right-top', 'right-bottom'
        ];
        /** @type {?} */
        var classList = targetElement.classList;
        /** @type {?} */
        var addClassesToTarget = (/**
         * @param {?} targetPlacement
         * @return {?}
         */
        function (targetPlacement) {
            var _a = __read(targetPlacement.split('-'), 2), primary = _a[0], secondary = _a[1];
            /** @type {?} */
            var classes = [];
            if (baseClass) {
                classes.push(baseClass + "-" + primary);
                if (secondary) {
                    classes.push(baseClass + "-" + primary + "-" + secondary);
                }
                classes.forEach((/**
                 * @param {?} classname
                 * @return {?}
                 */
                function (classname) { classList.add(classname); }));
            }
            return classes;
        });
        // Remove old placement classes to avoid issues
        if (baseClass) {
            allowedPlacements.forEach((/**
             * @param {?} placementToRemove
             * @return {?}
             */
            function (placementToRemove) { classList.remove(baseClass + "-" + placementToRemove); }));
        }
        // replace auto placement with other placements
        /** @type {?} */
        var hasAuto = placementVals.findIndex((/**
         * @param {?} val
         * @return {?}
         */
        function (val) { return val === 'auto'; }));
        if (hasAuto >= 0) {
            allowedPlacements.forEach((/**
             * @param {?} obj
             * @return {?}
             */
            function (obj) {
                if (placementVals.find((/**
                 * @param {?} val
                 * @return {?}
                 */
                function (val) { return val.search('^' + obj) !== -1; })) == null) {
                    placementVals.splice(hasAuto++, 1, (/** @type {?} */ (obj)));
                }
            }));
        }
        // coordinates where to position
        // Required for transform:
        /** @type {?} */
        var style = targetElement.style;
        style.position = 'absolute';
        style.top = '0';
        style.left = '0';
        style['will-change'] = 'transform';
        /** @type {?} */
        var testPlacement;
        /** @type {?} */
        var isInViewport = false;
        try {
            for (var placementVals_1 = __values(placementVals), placementVals_1_1 = placementVals_1.next(); !placementVals_1_1.done; placementVals_1_1 = placementVals_1.next()) {
                testPlacement = placementVals_1_1.value;
                /** @type {?} */
                var addedClasses = addClassesToTarget(testPlacement);
                if (positionService.positionElements(hostElement, targetElement, testPlacement, appendToBody)) {
                    isInViewport = true;
                    break;
                }
                // Remove the baseClasses for further calculation
                if (baseClass) {
                    addedClasses.forEach((/**
                     * @param {?} classname
                     * @return {?}
                     */
                    function (classname) { classList.remove(classname); }));
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (placementVals_1_1 && !placementVals_1_1.done && (_a = placementVals_1.return)) _a.call(placementVals_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        if (!isInViewport) {
            // If nothing match, the first placement is the default one
            testPlacement = placementVals[0];
            addClassesToTarget(testPlacement);
            positionService.positionElements(hostElement, targetElement, testPlacement, appendToBody);
        }
        return testPlacement;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @return {?}
     */
    function NGB_DATEPICKER_PARSER_FORMATTER_FACTORY() {
        return new NgbDateISOParserFormatter();
    }
    /**
     * An abstract service for parsing and formatting dates for the
     * [`NgbInputDatepicker`](#/components/datepicker/api#NgbInputDatepicker) directive.
     * Converts between the internal `NgbDateStruct` model presentation and a `string` that is displayed in the
     * input element.
     *
     * When user types something in the input this service attempts to parse it into a `NgbDateStruct` object.
     * And vice versa, when users selects a date in the calendar with the mouse, it must be displayed as a `string`
     * in the input.
     *
     * Default implementation uses the ISO 8601 format, but you can provide another implementation via DI
     * to use an alternative string format or a custom parsing logic.
     *
     * See the [date format overview](#/components/datepicker/overview#date-model) for more details.
     * @abstract
     */
    var NgbDateParserFormatter = /** @class */ (function () {
        function NgbDateParserFormatter() {
        }
        NgbDateParserFormatter.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root', useFactory: NGB_DATEPICKER_PARSER_FORMATTER_FACTORY },] }
        ];
        /** @nocollapse */ NgbDateParserFormatter.ngInjectableDef = core.ɵɵdefineInjectable({ factory: NGB_DATEPICKER_PARSER_FORMATTER_FACTORY, token: NgbDateParserFormatter, providedIn: "root" });
        return NgbDateParserFormatter;
    }());
    var NgbDateISOParserFormatter = /** @class */ (function (_super) {
        __extends(NgbDateISOParserFormatter, _super);
        function NgbDateISOParserFormatter() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * @param {?} value
         * @return {?}
         */
        NgbDateISOParserFormatter.prototype.parse = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            if (value) {
                /** @type {?} */
                var dateParts = value.trim().split('-');
                if (dateParts.length === 1 && isNumber(dateParts[0])) {
                    return { year: toInteger(dateParts[0]), month: null, day: null };
                }
                else if (dateParts.length === 2 && isNumber(dateParts[0]) && isNumber(dateParts[1])) {
                    return { year: toInteger(dateParts[0]), month: toInteger(dateParts[1]), day: null };
                }
                else if (dateParts.length === 3 && isNumber(dateParts[0]) && isNumber(dateParts[1]) && isNumber(dateParts[2])) {
                    return { year: toInteger(dateParts[0]), month: toInteger(dateParts[1]), day: toInteger(dateParts[2]) };
                }
            }
            return null;
        };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbDateISOParserFormatter.prototype.format = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return date ?
                date.year + "-" + (isNumber(date.month) ? padNumber(date.month) : '') + "-" + (isNumber(date.day) ? padNumber(date.day) : '') :
                '';
        };
        NgbDateISOParserFormatter.decorators = [
            { type: core.Injectable }
        ];
        return NgbDateISOParserFormatter;
    }(NgbDateParserFormatter));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NGB_DATEPICKER_VALUE_ACCESSOR$1 = {
        provide: forms.NG_VALUE_ACCESSOR,
        useExisting: core.forwardRef((/**
         * @return {?}
         */
        function () { return NgbInputDatepicker; })),
        multi: true
    };
    /** @type {?} */
    var NGB_DATEPICKER_VALIDATOR = {
        provide: forms.NG_VALIDATORS,
        useExisting: core.forwardRef((/**
         * @return {?}
         */
        function () { return NgbInputDatepicker; })),
        multi: true
    };
    /**
     * A directive that allows to stick a datepicker popup to an input field.
     *
     * Manages interaction with the input field itself, does value formatting and provides forms integration.
     */
    var NgbInputDatepicker = /** @class */ (function () {
        function NgbInputDatepicker(_parserFormatter, _elRef, _vcRef, _renderer, _cfr, _ngZone, _service, _calendar, _dateAdapter, _document, _changeDetector) {
            var _this = this;
            this._parserFormatter = _parserFormatter;
            this._elRef = _elRef;
            this._vcRef = _vcRef;
            this._renderer = _renderer;
            this._cfr = _cfr;
            this._ngZone = _ngZone;
            this._service = _service;
            this._calendar = _calendar;
            this._dateAdapter = _dateAdapter;
            this._document = _document;
            this._changeDetector = _changeDetector;
            this._cRef = null;
            this._disabled = false;
            /**
             * Indicates whether the datepicker popup should be closed automatically after date selection / outside click or not.
             *
             * * `true` - the popup will close on both date selection and outside click.
             * * `false` - the popup can only be closed manually via `close()` or `toggle()` methods.
             * * `"inside"` - the popup will close on date selection, but not outside clicks.
             * * `"outside"` - the popup will close only on the outside click and not on date selection/inside clicks.
             *
             * \@since 3.0.0
             */
            this.autoClose = true;
            /**
             * The preferred placement of the datepicker popup.
             *
             * Possible values are `"top"`, `"top-left"`, `"top-right"`, `"bottom"`, `"bottom-left"`,
             * `"bottom-right"`, `"left"`, `"left-top"`, `"left-bottom"`, `"right"`, `"right-top"`,
             * `"right-bottom"`
             *
             * Accepts an array of strings or a string with space separated possible values.
             *
             * The default order of preference is `"bottom-left bottom-right top-left top-right"`
             *
             * Please see the [positioning overview](#/positioning) for more details.
             */
            this.placement = ['bottom-left', 'bottom-right', 'top-left', 'top-right'];
            /**
             * An event emitted when user selects a date using keyboard or mouse.
             *
             * The payload of the event is currently selected `NgbDate`.
             *
             * \@since 1.1.1
             */
            this.dateSelect = new core.EventEmitter();
            /**
             * Event emitted right after the navigation happens and displayed month changes.
             *
             * See [`NgbDatepickerNavigateEvent`](#/components/datepicker/api#NgbDatepickerNavigateEvent) for the payload info.
             */
            this.navigate = new core.EventEmitter();
            /**
             * An event fired after closing datepicker window.
             *
             * \@since 4.2.0
             */
            this.closed = new core.EventEmitter();
            this._onChange = (/**
             * @param {?} _
             * @return {?}
             */
            function (_) { });
            this._onTouched = (/**
             * @return {?}
             */
            function () { });
            this._validatorChange = (/**
             * @return {?}
             */
            function () { });
            this._zoneSubscription = _ngZone.onStable.subscribe((/**
             * @return {?}
             */
            function () { return _this._updatePopupPosition(); }));
        }
        Object.defineProperty(NgbInputDatepicker.prototype, "disabled", {
            get: /**
             * @return {?}
             */
            function () {
                return this._disabled;
            },
            set: /**
             * @param {?} value
             * @return {?}
             */
            function (value) {
                this._disabled = value === '' || (value && value !== 'false');
                if (this.isOpen()) {
                    this._cRef.instance.setDisabledState(this._disabled);
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbInputDatepicker.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this._onChange = fn; };
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbInputDatepicker.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this._onTouched = fn; };
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbInputDatepicker.prototype.registerOnValidatorChange = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this._validatorChange = fn; };
        /**
         * @param {?} isDisabled
         * @return {?}
         */
        NgbInputDatepicker.prototype.setDisabledState = /**
         * @param {?} isDisabled
         * @return {?}
         */
        function (isDisabled) { this.disabled = isDisabled; };
        /**
         * @param {?} c
         * @return {?}
         */
        NgbInputDatepicker.prototype.validate = /**
         * @param {?} c
         * @return {?}
         */
        function (c) {
            /** @type {?} */
            var value = c.value;
            if (value === null || value === undefined) {
                return null;
            }
            /** @type {?} */
            var ngbDate = this._fromDateStruct(this._dateAdapter.fromModel(value));
            if (!this._calendar.isValid(ngbDate)) {
                return { 'ngbDate': { invalid: c.value } };
            }
            if (this.minDate && ngbDate.before(NgbDate.from(this.minDate))) {
                return { 'ngbDate': { requiredBefore: this.minDate } };
            }
            if (this.maxDate && ngbDate.after(NgbDate.from(this.maxDate))) {
                return { 'ngbDate': { requiredAfter: this.maxDate } };
            }
        };
        /**
         * @param {?} value
         * @return {?}
         */
        NgbInputDatepicker.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._model = this._fromDateStruct(this._dateAdapter.fromModel(value));
            this._writeModelValue(this._model);
        };
        /**
         * @param {?} value
         * @param {?=} updateView
         * @return {?}
         */
        NgbInputDatepicker.prototype.manualDateChange = /**
         * @param {?} value
         * @param {?=} updateView
         * @return {?}
         */
        function (value, updateView) {
            if (updateView === void 0) { updateView = false; }
            /** @type {?} */
            var inputValueChanged = value !== this._inputValue;
            if (inputValueChanged) {
                this._inputValue = value;
                this._model = this._fromDateStruct(this._parserFormatter.parse(value));
            }
            if (inputValueChanged || !updateView) {
                this._onChange(this._model ? this._dateAdapter.toModel(this._model) : (value === '' ? null : value));
            }
            if (updateView && this._model) {
                this._writeModelValue(this._model);
            }
        };
        /**
         * @return {?}
         */
        NgbInputDatepicker.prototype.isOpen = /**
         * @return {?}
         */
        function () { return !!this._cRef; };
        /**
         * Opens the datepicker popup.
         *
         * If the related form control contains a valid date, the corresponding month will be opened.
         */
        /**
         * Opens the datepicker popup.
         *
         * If the related form control contains a valid date, the corresponding month will be opened.
         * @return {?}
         */
        NgbInputDatepicker.prototype.open = /**
         * Opens the datepicker popup.
         *
         * If the related form control contains a valid date, the corresponding month will be opened.
         * @return {?}
         */
        function () {
            var _this = this;
            if (!this.isOpen()) {
                /** @type {?} */
                var cf = this._cfr.resolveComponentFactory(NgbDatepicker);
                this._cRef = this._vcRef.createComponent(cf);
                this._applyPopupStyling(this._cRef.location.nativeElement);
                this._applyDatepickerInputs(this._cRef.instance);
                this._subscribeForDatepickerOutputs(this._cRef.instance);
                this._cRef.instance.ngOnInit();
                this._cRef.instance.writeValue(this._dateAdapter.toModel(this._model));
                // date selection event handling
                this._cRef.instance.registerOnChange((/**
                 * @param {?} selectedDate
                 * @return {?}
                 */
                function (selectedDate) {
                    _this.writeValue(selectedDate);
                    _this._onChange(selectedDate);
                    _this._onTouched();
                }));
                this._cRef.changeDetectorRef.detectChanges();
                this._cRef.instance.setDisabledState(this.disabled);
                if (this.container === 'body') {
                    window.document.querySelector(this.container).appendChild(this._cRef.location.nativeElement);
                }
                // focus handling
                ngbFocusTrap(this._cRef.location.nativeElement, this.closed, true);
                this._cRef.instance.focus();
                ngbAutoClose(this._ngZone, this._document, this.autoClose, (/**
                 * @return {?}
                 */
                function () { return _this.close(); }), this.closed, [], [this._elRef.nativeElement, this._cRef.location.nativeElement]);
            }
        };
        /**
         * Closes the datepicker popup.
         */
        /**
         * Closes the datepicker popup.
         * @return {?}
         */
        NgbInputDatepicker.prototype.close = /**
         * Closes the datepicker popup.
         * @return {?}
         */
        function () {
            if (this.isOpen()) {
                this._vcRef.remove(this._vcRef.indexOf(this._cRef.hostView));
                this._cRef = null;
                this.closed.emit();
                this._changeDetector.markForCheck();
            }
        };
        /**
         * Toggles the datepicker popup.
         */
        /**
         * Toggles the datepicker popup.
         * @return {?}
         */
        NgbInputDatepicker.prototype.toggle = /**
         * Toggles the datepicker popup.
         * @return {?}
         */
        function () {
            if (this.isOpen()) {
                this.close();
            }
            else {
                this.open();
            }
        };
        /**
         * Navigates to the provided date.
         *
         * With the default calendar we use ISO 8601: 'month' is 1=Jan ... 12=Dec.
         * If nothing or invalid date provided calendar will open current month.
         *
         * Use the `[startDate]` input as an alternative.
         */
        /**
         * Navigates to the provided date.
         *
         * With the default calendar we use ISO 8601: 'month' is 1=Jan ... 12=Dec.
         * If nothing or invalid date provided calendar will open current month.
         *
         * Use the `[startDate]` input as an alternative.
         * @param {?=} date
         * @return {?}
         */
        NgbInputDatepicker.prototype.navigateTo = /**
         * Navigates to the provided date.
         *
         * With the default calendar we use ISO 8601: 'month' is 1=Jan ... 12=Dec.
         * If nothing or invalid date provided calendar will open current month.
         *
         * Use the `[startDate]` input as an alternative.
         * @param {?=} date
         * @return {?}
         */
        function (date) {
            if (this.isOpen()) {
                this._cRef.instance.navigateTo(date);
            }
        };
        /**
         * @return {?}
         */
        NgbInputDatepicker.prototype.onBlur = /**
         * @return {?}
         */
        function () { this._onTouched(); };
        /**
         * @param {?} changes
         * @return {?}
         */
        NgbInputDatepicker.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            if (changes['minDate'] || changes['maxDate']) {
                this._validatorChange();
            }
        };
        /**
         * @return {?}
         */
        NgbInputDatepicker.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            this.close();
            this._zoneSubscription.unsubscribe();
        };
        /**
         * @private
         * @param {?} datepickerInstance
         * @return {?}
         */
        NgbInputDatepicker.prototype._applyDatepickerInputs = /**
         * @private
         * @param {?} datepickerInstance
         * @return {?}
         */
        function (datepickerInstance) {
            var _this = this;
            ['dayTemplate', 'dayTemplateData', 'displayMonths', 'firstDayOfWeek', 'footerTemplate', 'markDisabled', 'minDate',
                'maxDate', 'navigation', 'outsideDays', 'showNavigation', 'showWeekdays', 'showWeekNumbers']
                .forEach((/**
             * @param {?} optionName
             * @return {?}
             */
            function (optionName) {
                if (_this[optionName] !== undefined) {
                    datepickerInstance[optionName] = _this[optionName];
                }
            }));
            datepickerInstance.startDate = this.startDate || this._model;
        };
        /**
         * @private
         * @param {?} nativeElement
         * @return {?}
         */
        NgbInputDatepicker.prototype._applyPopupStyling = /**
         * @private
         * @param {?} nativeElement
         * @return {?}
         */
        function (nativeElement) {
            this._renderer.addClass(nativeElement, 'dropdown-menu');
            this._renderer.addClass(nativeElement, 'show');
            if (this.container === 'body') {
                this._renderer.addClass(nativeElement, 'ngb-dp-body');
            }
        };
        /**
         * @private
         * @param {?} datepickerInstance
         * @return {?}
         */
        NgbInputDatepicker.prototype._subscribeForDatepickerOutputs = /**
         * @private
         * @param {?} datepickerInstance
         * @return {?}
         */
        function (datepickerInstance) {
            var _this = this;
            datepickerInstance.navigate.subscribe((/**
             * @param {?} navigateEvent
             * @return {?}
             */
            function (navigateEvent) { return _this.navigate.emit(navigateEvent); }));
            datepickerInstance.select.subscribe((/**
             * @param {?} date
             * @return {?}
             */
            function (date) {
                _this.dateSelect.emit(date);
                if (_this.autoClose === true || _this.autoClose === 'inside') {
                    _this.close();
                }
            }));
        };
        /**
         * @private
         * @param {?} model
         * @return {?}
         */
        NgbInputDatepicker.prototype._writeModelValue = /**
         * @private
         * @param {?} model
         * @return {?}
         */
        function (model) {
            /** @type {?} */
            var value = this._parserFormatter.format(model);
            this._inputValue = value;
            this._renderer.setProperty(this._elRef.nativeElement, 'value', value);
            if (this.isOpen()) {
                this._cRef.instance.writeValue(this._dateAdapter.toModel(model));
                this._onTouched();
            }
        };
        /**
         * @private
         * @param {?} date
         * @return {?}
         */
        NgbInputDatepicker.prototype._fromDateStruct = /**
         * @private
         * @param {?} date
         * @return {?}
         */
        function (date) {
            /** @type {?} */
            var ngbDate = date ? new NgbDate(date.year, date.month, date.day) : null;
            return this._calendar.isValid(ngbDate) ? ngbDate : null;
        };
        /**
         * @private
         * @return {?}
         */
        NgbInputDatepicker.prototype._updatePopupPosition = /**
         * @private
         * @return {?}
         */
        function () {
            if (!this._cRef) {
                return;
            }
            /** @type {?} */
            var hostElement;
            if (typeof this.positionTarget === 'string') {
                hostElement = window.document.querySelector(this.positionTarget);
            }
            else if (this.positionTarget instanceof HTMLElement) {
                hostElement = this.positionTarget;
            }
            else {
                hostElement = this._elRef.nativeElement;
            }
            if (this.positionTarget && !hostElement) {
                throw new Error('ngbDatepicker could not find element declared in [positionTarget] to position against.');
            }
            positionElements(hostElement, this._cRef.location.nativeElement, this.placement, this.container === 'body');
        };
        NgbInputDatepicker.decorators = [
            { type: core.Directive, args: [{
                        selector: 'input[ngbDatepicker]',
                        exportAs: 'ngbDatepicker',
                        host: {
                            '(input)': 'manualDateChange($event.target.value)',
                            '(change)': 'manualDateChange($event.target.value, true)',
                            '(blur)': 'onBlur()',
                            '[disabled]': 'disabled'
                        },
                        providers: [NGB_DATEPICKER_VALUE_ACCESSOR$1, NGB_DATEPICKER_VALIDATOR, NgbDatepickerService]
                    },] }
        ];
        /** @nocollapse */
        NgbInputDatepicker.ctorParameters = function () { return [
            { type: NgbDateParserFormatter },
            { type: core.ElementRef },
            { type: core.ViewContainerRef },
            { type: core.Renderer2 },
            { type: core.ComponentFactoryResolver },
            { type: core.NgZone },
            { type: NgbDatepickerService },
            { type: NgbCalendar },
            { type: NgbDateAdapter },
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
            { type: core.ChangeDetectorRef }
        ]; };
        NgbInputDatepicker.propDecorators = {
            autoClose: [{ type: core.Input }],
            dayTemplate: [{ type: core.Input }],
            dayTemplateData: [{ type: core.Input }],
            displayMonths: [{ type: core.Input }],
            firstDayOfWeek: [{ type: core.Input }],
            footerTemplate: [{ type: core.Input }],
            markDisabled: [{ type: core.Input }],
            minDate: [{ type: core.Input }],
            maxDate: [{ type: core.Input }],
            navigation: [{ type: core.Input }],
            outsideDays: [{ type: core.Input }],
            placement: [{ type: core.Input }],
            showWeekdays: [{ type: core.Input }],
            showWeekNumbers: [{ type: core.Input }],
            startDate: [{ type: core.Input }],
            container: [{ type: core.Input }],
            positionTarget: [{ type: core.Input }],
            dateSelect: [{ type: core.Output }],
            navigate: [{ type: core.Output }],
            closed: [{ type: core.Output }],
            disabled: [{ type: core.Input }]
        };
        return NgbInputDatepicker;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbDatepickerDayView = /** @class */ (function () {
        function NgbDatepickerDayView(i18n) {
            this.i18n = i18n;
        }
        /**
         * @return {?}
         */
        NgbDatepickerDayView.prototype.isMuted = /**
         * @return {?}
         */
        function () { return !this.selected && (this.date.month !== this.currentMonth || this.disabled); };
        NgbDatepickerDayView.decorators = [
            { type: core.Component, args: [{
                        selector: '[ngbDatepickerDayView]',
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        encapsulation: core.ViewEncapsulation.None,
                        host: {
                            'class': 'btn-light',
                            '[class.bg-primary]': 'selected',
                            '[class.text-white]': 'selected',
                            '[class.text-muted]': 'isMuted()',
                            '[class.outside]': 'isMuted()',
                            '[class.active]': 'focused'
                        },
                        template: "{{ i18n.getDayNumerals(date) }}",
                        styles: ["[ngbDatepickerDayView]{text-align:center;width:2rem;height:2rem;line-height:2rem;border-radius:.25rem;background:0 0}[ngbDatepickerDayView].outside{opacity:.5}"]
                    }] }
        ];
        /** @nocollapse */
        NgbDatepickerDayView.ctorParameters = function () { return [
            { type: NgbDatepickerI18n }
        ]; };
        NgbDatepickerDayView.propDecorators = {
            currentMonth: [{ type: core.Input }],
            date: [{ type: core.Input }],
            disabled: [{ type: core.Input }],
            focused: [{ type: core.Input }],
            selected: [{ type: core.Input }]
        };
        return NgbDatepickerDayView;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbDatepickerNavigationSelect = /** @class */ (function () {
        function NgbDatepickerNavigationSelect(i18n) {
            this.i18n = i18n;
            this.select = new core.EventEmitter();
        }
        /**
         * @param {?} month
         * @return {?}
         */
        NgbDatepickerNavigationSelect.prototype.changeMonth = /**
         * @param {?} month
         * @return {?}
         */
        function (month) { this.select.emit(new NgbDate(this.date.year, toInteger(month), 1)); };
        /**
         * @param {?} year
         * @return {?}
         */
        NgbDatepickerNavigationSelect.prototype.changeYear = /**
         * @param {?} year
         * @return {?}
         */
        function (year) { this.select.emit(new NgbDate(toInteger(year), this.date.month, 1)); };
        NgbDatepickerNavigationSelect.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-datepicker-navigation-select',
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        encapsulation: core.ViewEncapsulation.None,
                        template: "\n    <select\n      [disabled]=\"disabled\"\n      class=\"custom-select\"\n      [value]=\"date?.month\"\n      i18n-aria-label=\"@@ngb.datepicker.select-month\" aria-label=\"Select month\"\n      i18n-title=\"@@ngb.datepicker.select-month\" title=\"Select month\"\n      (change)=\"changeMonth($event.target.value)\">\n        <option *ngFor=\"let m of months\" [attr.aria-label]=\"i18n.getMonthFullName(m, date?.year)\"\n                [value]=\"m\">{{ i18n.getMonthShortName(m, date?.year) }}</option>\n    </select><select\n      [disabled]=\"disabled\"\n      class=\"custom-select\"\n      [value]=\"date?.year\"\n      i18n-aria-label=\"@@ngb.datepicker.select-year\" aria-label=\"Select year\"\n      i18n-title=\"@@ngb.datepicker.select-year\" title=\"Select year\"\n      (change)=\"changeYear($event.target.value)\">\n        <option *ngFor=\"let y of years\" [value]=\"y\">{{ i18n.getYearNumerals(y) }}</option>\n    </select>\n  ",
                        styles: ["ngb-datepicker-navigation-select>.custom-select{-ms-flex:1 1 auto;flex:1 1 auto;padding:0 .5rem;font-size:.875rem;height:1.85rem}"]
                    }] }
        ];
        /** @nocollapse */
        NgbDatepickerNavigationSelect.ctorParameters = function () { return [
            { type: NgbDatepickerI18n }
        ]; };
        NgbDatepickerNavigationSelect.propDecorators = {
            date: [{ type: core.Input }],
            disabled: [{ type: core.Input }],
            months: [{ type: core.Input }],
            years: [{ type: core.Input }],
            select: [{ type: core.Output }]
        };
        return NgbDatepickerNavigationSelect;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @abstract
     */
    var NgbCalendarHijri = /** @class */ (function (_super) {
        __extends(NgbCalendarHijri, _super);
        function NgbCalendarHijri() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * @return {?}
         */
        NgbCalendarHijri.prototype.getDaysPerWeek = /**
         * @return {?}
         */
        function () { return 7; };
        /**
         * @return {?}
         */
        NgbCalendarHijri.prototype.getMonths = /**
         * @return {?}
         */
        function () { return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]; };
        /**
         * @return {?}
         */
        NgbCalendarHijri.prototype.getWeeksPerMonth = /**
         * @return {?}
         */
        function () { return 6; };
        /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        NgbCalendarHijri.prototype.getNext = /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        function (date, period, number) {
            if (period === void 0) { period = 'd'; }
            if (number === void 0) { number = 1; }
            date = new NgbDate(date.year, date.month, date.day);
            switch (period) {
                case 'y':
                    date = this._setYear(date, date.year + number);
                    date.month = 1;
                    date.day = 1;
                    return date;
                case 'm':
                    date = this._setMonth(date, date.month + number);
                    date.day = 1;
                    return date;
                case 'd':
                    return this._setDay(date, date.day + number);
                default:
                    return date;
            }
        };
        /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        NgbCalendarHijri.prototype.getPrev = /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        function (date, period, number) {
            if (period === void 0) { period = 'd'; }
            if (number === void 0) { number = 1; }
            return this.getNext(date, period, -number);
        };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbCalendarHijri.prototype.getWeekday = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            /** @type {?} */
            var day = this.toGregorian(date).getDay();
            // in JS Date Sun=0, in ISO 8601 Sun=7
            return day === 0 ? 7 : day;
        };
        /**
         * @param {?} week
         * @param {?} firstDayOfWeek
         * @return {?}
         */
        NgbCalendarHijri.prototype.getWeekNumber = /**
         * @param {?} week
         * @param {?} firstDayOfWeek
         * @return {?}
         */
        function (week, firstDayOfWeek) {
            // in JS Date Sun=0, in ISO 8601 Sun=7
            if (firstDayOfWeek === 7) {
                firstDayOfWeek = 0;
            }
            /** @type {?} */
            var thursdayIndex = (4 + 7 - firstDayOfWeek) % 7;
            /** @type {?} */
            var date = week[thursdayIndex];
            /** @type {?} */
            var jsDate = this.toGregorian(date);
            jsDate.setDate(jsDate.getDate() + 4 - (jsDate.getDay() || 7)); // Thursday
            // Thursday
            /** @type {?} */
            var time = jsDate.getTime();
            /** @type {?} */
            var MuhDate = this.toGregorian(new NgbDate(date.year, 1, 1));
            return Math.floor(Math.round((time - MuhDate.getTime()) / 86400000) / 7) + 1;
        };
        /**
         * @return {?}
         */
        NgbCalendarHijri.prototype.getToday = /**
         * @return {?}
         */
        function () { return this.fromGregorian(new Date()); };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbCalendarHijri.prototype.isValid = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return date && isNumber(date.year) && isNumber(date.month) && isNumber(date.day) &&
                !isNaN(this.toGregorian(date).getTime());
        };
        /**
         * @private
         * @param {?} date
         * @param {?} day
         * @return {?}
         */
        NgbCalendarHijri.prototype._setDay = /**
         * @private
         * @param {?} date
         * @param {?} day
         * @return {?}
         */
        function (date, day) {
            day = +day;
            /** @type {?} */
            var mDays = this.getDaysPerMonth(date.month, date.year);
            if (day <= 0) {
                while (day <= 0) {
                    date = this._setMonth(date, date.month - 1);
                    mDays = this.getDaysPerMonth(date.month, date.year);
                    day += mDays;
                }
            }
            else if (day > mDays) {
                while (day > mDays) {
                    day -= mDays;
                    date = this._setMonth(date, date.month + 1);
                    mDays = this.getDaysPerMonth(date.month, date.year);
                }
            }
            date.day = day;
            return date;
        };
        /**
         * @private
         * @param {?} date
         * @param {?} month
         * @return {?}
         */
        NgbCalendarHijri.prototype._setMonth = /**
         * @private
         * @param {?} date
         * @param {?} month
         * @return {?}
         */
        function (date, month) {
            month = +month;
            date.year = date.year + Math.floor((month - 1) / 12);
            date.month = Math.floor(((month - 1) % 12 + 12) % 12) + 1;
            return date;
        };
        /**
         * @private
         * @param {?} date
         * @param {?} year
         * @return {?}
         */
        NgbCalendarHijri.prototype._setYear = /**
         * @private
         * @param {?} date
         * @param {?} year
         * @return {?}
         */
        function (date, year) {
            date.year = +year;
            return date;
        };
        NgbCalendarHijri.decorators = [
            { type: core.Injectable }
        ];
        return NgbCalendarHijri;
    }(NgbCalendar));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * Checks if islamic year is a leap year
     * @param {?} hYear
     * @return {?}
     */
    function isIslamicLeapYear(hYear) {
        return (14 + 11 * hYear) % 30 < 11;
    }
    /**
     * Checks if gregorian years is a leap year
     * @param {?} gDate
     * @return {?}
     */
    function isGregorianLeapYear(gDate) {
        /** @type {?} */
        var year = gDate.getFullYear();
        return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0;
    }
    /**
     * Returns the start of Hijri Month.
     * `hMonth` is 0 for Muharram, 1 for Safar, etc.
     * `hYear` is any Hijri hYear.
     * @param {?} hYear
     * @param {?} hMonth
     * @return {?}
     */
    function getIslamicMonthStart(hYear, hMonth) {
        return Math.ceil(29.5 * hMonth) + (hYear - 1) * 354 + Math.floor((3 + 11 * hYear) / 30.0);
    }
    /**
     * Returns the start of Hijri year.
     * `year` is any Hijri year.
     * @param {?} year
     * @return {?}
     */
    function getIslamicYearStart(year) {
        return (year - 1) * 354 + Math.floor((3 + 11 * year) / 30.0);
    }
    /**
     * @param {?} a
     * @param {?} b
     * @return {?}
     */
    function mod(a, b) {
        return a - b * Math.floor(a / b);
    }
    /**
     * The civil calendar is one type of Hijri calendars used in islamic countries.
     * Uses a fixed cycle of alternating 29- and 30-day months,
     * with a leap day added to the last month of 11 out of every 30 years.
     * http://cldr.unicode.org/development/development-process/design-proposals/islamic-calendar-types
     * All the calculations here are based on the equations from "Calendrical Calculations" By Edward M. Reingold, Nachum
     * Dershowitz.
     * @type {?}
     */
    var GREGORIAN_EPOCH = 1721425.5;
    /** @type {?} */
    var ISLAMIC_EPOCH = 1948439.5;
    var NgbCalendarIslamicCivil = /** @class */ (function (_super) {
        __extends(NgbCalendarIslamicCivil, _super);
        function NgbCalendarIslamicCivil() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * Returns the equivalent islamic(civil) date value for a give input Gregorian date.
         * `gDate` is a JS Date to be converted to Hijri.
         */
        /**
         * Returns the equivalent islamic(civil) date value for a give input Gregorian date.
         * `gDate` is a JS Date to be converted to Hijri.
         * @param {?} gDate
         * @return {?}
         */
        NgbCalendarIslamicCivil.prototype.fromGregorian = /**
         * Returns the equivalent islamic(civil) date value for a give input Gregorian date.
         * `gDate` is a JS Date to be converted to Hijri.
         * @param {?} gDate
         * @return {?}
         */
        function (gDate) {
            /** @type {?} */
            var gYear = gDate.getFullYear();
            /** @type {?} */
            var gMonth = gDate.getMonth();
            /** @type {?} */
            var gDay = gDate.getDate();
            /** @type {?} */
            var julianDay = GREGORIAN_EPOCH - 1 + 365 * (gYear - 1) + Math.floor((gYear - 1) / 4) +
                -Math.floor((gYear - 1) / 100) + Math.floor((gYear - 1) / 400) +
                Math.floor((367 * (gMonth + 1) - 362) / 12 + (gMonth + 1 <= 2 ? 0 : isGregorianLeapYear(gDate) ? -1 : -2) + gDay);
            julianDay = Math.floor(julianDay) + 0.5;
            /** @type {?} */
            var days = julianDay - ISLAMIC_EPOCH;
            /** @type {?} */
            var hYear = Math.floor((30 * days + 10646) / 10631.0);
            /** @type {?} */
            var hMonth = Math.ceil((days - 29 - getIslamicYearStart(hYear)) / 29.5);
            hMonth = Math.min(hMonth, 11);
            /** @type {?} */
            var hDay = Math.ceil(days - getIslamicMonthStart(hYear, hMonth)) + 1;
            return new NgbDate(hYear, hMonth + 1, hDay);
        };
        /**
         * Returns the equivalent JS date value for a give input islamic(civil) date.
         * `hDate` is an islamic(civil) date to be converted to Gregorian.
         */
        /**
         * Returns the equivalent JS date value for a give input islamic(civil) date.
         * `hDate` is an islamic(civil) date to be converted to Gregorian.
         * @param {?} hDate
         * @return {?}
         */
        NgbCalendarIslamicCivil.prototype.toGregorian = /**
         * Returns the equivalent JS date value for a give input islamic(civil) date.
         * `hDate` is an islamic(civil) date to be converted to Gregorian.
         * @param {?} hDate
         * @return {?}
         */
        function (hDate) {
            /** @type {?} */
            var hYear = hDate.year;
            /** @type {?} */
            var hMonth = hDate.month - 1;
            /** @type {?} */
            var hDay = hDate.day;
            /** @type {?} */
            var julianDay = hDay + Math.ceil(29.5 * hMonth) + (hYear - 1) * 354 + Math.floor((3 + 11 * hYear) / 30) + ISLAMIC_EPOCH - 1;
            /** @type {?} */
            var wjd = Math.floor(julianDay - 0.5) + 0.5;
            /** @type {?} */
            var depoch = wjd - GREGORIAN_EPOCH;
            /** @type {?} */
            var quadricent = Math.floor(depoch / 146097);
            /** @type {?} */
            var dqc = mod(depoch, 146097);
            /** @type {?} */
            var cent = Math.floor(dqc / 36524);
            /** @type {?} */
            var dcent = mod(dqc, 36524);
            /** @type {?} */
            var quad = Math.floor(dcent / 1461);
            /** @type {?} */
            var dquad = mod(dcent, 1461);
            /** @type {?} */
            var yindex = Math.floor(dquad / 365);
            /** @type {?} */
            var year = quadricent * 400 + cent * 100 + quad * 4 + yindex;
            if (!(cent === 4 || yindex === 4)) {
                year++;
            }
            /** @type {?} */
            var gYearStart = GREGORIAN_EPOCH + 365 * (year - 1) + Math.floor((year - 1) / 4) - Math.floor((year - 1) / 100) +
                Math.floor((year - 1) / 400);
            /** @type {?} */
            var yearday = wjd - gYearStart;
            /** @type {?} */
            var tjd = GREGORIAN_EPOCH - 1 + 365 * (year - 1) + Math.floor((year - 1) / 4) - Math.floor((year - 1) / 100) +
                Math.floor((year - 1) / 400) + Math.floor(739 / 12 + (isGregorianLeapYear(new Date(year, 3, 1)) ? -1 : -2) + 1);
            /** @type {?} */
            var leapadj = wjd < tjd ? 0 : isGregorianLeapYear(new Date(year, 3, 1)) ? 1 : 2;
            /** @type {?} */
            var month = Math.floor(((yearday + leapadj) * 12 + 373) / 367);
            /** @type {?} */
            var tjd2 = GREGORIAN_EPOCH - 1 + 365 * (year - 1) + Math.floor((year - 1) / 4) - Math.floor((year - 1) / 100) +
                Math.floor((year - 1) / 400) +
                Math.floor((367 * month - 362) / 12 + (month <= 2 ? 0 : isGregorianLeapYear(new Date(year, month - 1, 1)) ? -1 : -2) +
                    1);
            /** @type {?} */
            var day = wjd - tjd2 + 1;
            return new Date(year, month - 1, day);
        };
        /**
         * Returns the number of days in a specific Hijri month.
         * `month` is 1 for Muharram, 2 for Safar, etc.
         * `year` is any Hijri year.
         */
        /**
         * Returns the number of days in a specific Hijri month.
         * `month` is 1 for Muharram, 2 for Safar, etc.
         * `year` is any Hijri year.
         * @param {?} month
         * @param {?} year
         * @return {?}
         */
        NgbCalendarIslamicCivil.prototype.getDaysPerMonth = /**
         * Returns the number of days in a specific Hijri month.
         * `month` is 1 for Muharram, 2 for Safar, etc.
         * `year` is any Hijri year.
         * @param {?} month
         * @param {?} year
         * @return {?}
         */
        function (month, year) {
            year = year + Math.floor(month / 13);
            month = ((month - 1) % 12) + 1;
            /** @type {?} */
            var length = 29 + month % 2;
            if (month === 12 && isIslamicLeapYear(year)) {
                length++;
            }
            return length;
        };
        NgbCalendarIslamicCivil.decorators = [
            { type: core.Injectable }
        ];
        return NgbCalendarIslamicCivil;
    }(NgbCalendarHijri));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * Umalqura calendar is one type of Hijri calendars used in islamic countries.
     * This Calendar is used by Saudi Arabia for administrative purpose.
     * Unlike tabular calendars, the algorithm involves astronomical calculation, but it's still deterministic.
     * http://cldr.unicode.org/development/development-process/design-proposals/islamic-calendar-types
     * @type {?}
     */
    var GREGORIAN_FIRST_DATE = new Date(1882, 10, 12);
    /** @type {?} */
    var GREGORIAN_LAST_DATE = new Date(2174, 10, 25);
    /** @type {?} */
    var HIJRI_BEGIN = 1300;
    /** @type {?} */
    var HIJRI_END = 1600;
    /** @type {?} */
    var ONE_DAY = 1000 * 60 * 60 * 24;
    /** @type {?} */
    var MONTH_LENGTH = [
        // 1300-1304
        '101010101010', '110101010100', '111011001001', '011011010100', '011011101010',
        // 1305-1309
        '001101101100', '101010101101', '010101010101', '011010101001', '011110010010',
        // 1310-1314
        '101110101001', '010111010100', '101011011010', '010101011100', '110100101101',
        // 1315-1319
        '011010010101', '011101001010', '101101010100', '101101101010', '010110101101',
        // 1320-1324
        '010010101110', '101001001111', '010100010111', '011010001011', '011010100101',
        // 1325-1329
        '101011010101', '001011010110', '100101011011', '010010011101', '101001001101',
        // 1330-1334
        '110100100110', '110110010101', '010110101100', '100110110110', '001010111010',
        // 1335-1339
        '101001011011', '010100101011', '101010010101', '011011001010', '101011101001',
        // 1340-1344
        '001011110100', '100101110110', '001010110110', '100101010110', '101011001010',
        // 1345-1349
        '101110100100', '101111010010', '010111011001', '001011011100', '100101101101',
        // 1350-1354
        '010101001101', '101010100101', '101101010010', '101110100101', '010110110100',
        // 1355-1359
        '100110110110', '010101010111', '001010010111', '010101001011', '011010100011',
        // 1360-1364
        '011101010010', '101101100101', '010101101010', '101010101011', '010100101011',
        // 1365-1369
        '110010010101', '110101001010', '110110100101', '010111001010', '101011010110',
        // 1370-1374
        '100101010111', '010010101011', '100101001011', '101010100101', '101101010010',
        // 1375-1379
        '101101101010', '010101110101', '001001110110', '100010110111', '010001011011',
        // 1380-1384
        '010101010101', '010110101001', '010110110100', '100111011010', '010011011101',
        // 1385-1389
        '001001101110', '100100110110', '101010101010', '110101010100', '110110110010',
        // 1390-1394
        '010111010101', '001011011010', '100101011011', '010010101011', '101001010101',
        // 1395-1399
        '101101001001', '101101100100', '101101110001', '010110110100', '101010110101',
        // 1400-1404
        '101001010101', '110100100101', '111010010010', '111011001001', '011011010100',
        // 1405-1409
        '101011101001', '100101101011', '010010101011', '101010010011', '110101001001',
        // 1410-1414
        '110110100100', '110110110010', '101010111001', '010010111010', '101001011011',
        // 1415-1419
        '010100101011', '101010010101', '101100101010', '101101010101', '010101011100',
        // 1420-1424
        '010010111101', '001000111101', '100100011101', '101010010101', '101101001010',
        // 1425-1429
        '101101011010', '010101101101', '001010110110', '100100111011', '010010011011',
        // 1430-1434
        '011001010101', '011010101001', '011101010100', '101101101010', '010101101100',
        // 1435-1439
        '101010101101', '010101010101', '101100101001', '101110010010', '101110101001',
        // 1440-1444
        '010111010100', '101011011010', '010101011010', '101010101011', '010110010101',
        // 1445-1449
        '011101001001', '011101100100', '101110101010', '010110110101', '001010110110',
        // 1450-1454
        '101001010110', '111001001101', '101100100101', '101101010010', '101101101010',
        // 1455-1459
        '010110101101', '001010101110', '100100101111', '010010010111', '011001001011',
        // 1460-1464
        '011010100101', '011010101100', '101011010110', '010101011101', '010010011101',
        // 1465-1469
        '101001001101', '110100010110', '110110010101', '010110101010', '010110110101',
        // 1470-1474
        '001011011010', '100101011011', '010010101101', '010110010101', '011011001010',
        // 1475-1479
        '011011100100', '101011101010', '010011110101', '001010110110', '100101010110',
        // 1480-1484
        '101010101010', '101101010100', '101111010010', '010111011001', '001011101010',
        // 1485-1489
        '100101101101', '010010101101', '101010010101', '101101001010', '101110100101',
        // 1490-1494
        '010110110010', '100110110101', '010011010110', '101010010111', '010101000111',
        // 1495-1499
        '011010010011', '011101001001', '101101010101', '010101101010', '101001101011',
        // 1500-1504
        '010100101011', '101010001011', '110101000110', '110110100011', '010111001010',
        // 1505-1509
        '101011010110', '010011011011', '001001101011', '100101001011', '101010100101',
        // 1510-1514
        '101101010010', '101101101001', '010101110101', '000101110110', '100010110111',
        // 1515-1519
        '001001011011', '010100101011', '010101100101', '010110110100', '100111011010',
        // 1520-1524
        '010011101101', '000101101101', '100010110110', '101010100110', '110101010010',
        // 1525-1529
        '110110101001', '010111010100', '101011011010', '100101011011', '010010101011',
        // 1530-1534
        '011001010011', '011100101001', '011101100010', '101110101001', '010110110010',
        // 1535-1539
        '101010110101', '010101010101', '101100100101', '110110010010', '111011001001',
        // 1540-1544
        '011011010010', '101011101001', '010101101011', '010010101011', '101001010101',
        // 1545-1549
        '110100101001', '110101010100', '110110101010', '100110110101', '010010111010',
        // 1550-1554
        '101000111011', '010010011011', '101001001101', '101010101010', '101011010101',
        // 1555-1559
        '001011011010', '100101011101', '010001011110', '101000101110', '110010011010',
        // 1560-1564
        '110101010101', '011010110010', '011010111001', '010010111010', '101001011101',
        // 1565-1569
        '010100101101', '101010010101', '101101010010', '101110101000', '101110110100',
        // 1570-1574
        '010110111001', '001011011010', '100101011010', '101101001010', '110110100100',
        // 1575-1579
        '111011010001', '011011101000', '101101101010', '010101101101', '010100110101',
        // 1580-1584
        '011010010101', '110101001010', '110110101000', '110111010100', '011011011010',
        // 1585-1589
        '010101011011', '001010011101', '011000101011', '101100010101', '101101001010',
        // 1590-1594
        '101110010101', '010110101010', '101010101110', '100100101110', '110010001111',
        // 1595-1599
        '010100100111', '011010010101', '011010101010', '101011010110', '010101011101',
        // 1600
        '001010011101'
    ];
    /**
     * @param {?} date1
     * @param {?} date2
     * @return {?}
     */
    function getDaysDiff(date1, date2) {
        // Ignores the time part in date1 and date2:
        /** @type {?} */
        var time1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());
        /** @type {?} */
        var time2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());
        /** @type {?} */
        var diff = Math.abs(time1 - time2);
        return Math.round(diff / ONE_DAY);
    }
    var NgbCalendarIslamicUmalqura = /** @class */ (function (_super) {
        __extends(NgbCalendarIslamicUmalqura, _super);
        function NgbCalendarIslamicUmalqura() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
        * Returns the equivalent islamic(Umalqura) date value for a give input Gregorian date.
        * `gdate` is s JS Date to be converted to Hijri.
        */
        /**
         * Returns the equivalent islamic(Umalqura) date value for a give input Gregorian date.
         * `gdate` is s JS Date to be converted to Hijri.
         * @param {?} gDate
         * @return {?}
         */
        NgbCalendarIslamicUmalqura.prototype.fromGregorian = /**
         * Returns the equivalent islamic(Umalqura) date value for a give input Gregorian date.
         * `gdate` is s JS Date to be converted to Hijri.
         * @param {?} gDate
         * @return {?}
         */
        function (gDate) {
            /** @type {?} */
            var hDay = 1;
            /** @type {?} */
            var hMonth = 0;
            /** @type {?} */
            var hYear = 1300;
            /** @type {?} */
            var daysDiff = getDaysDiff(gDate, GREGORIAN_FIRST_DATE);
            if (gDate.getTime() - GREGORIAN_FIRST_DATE.getTime() >= 0 && gDate.getTime() - GREGORIAN_LAST_DATE.getTime() <= 0) {
                /** @type {?} */
                var year = 1300;
                for (var i = 0; i < MONTH_LENGTH.length; i++, year++) {
                    for (var j = 0; j < 12; j++) {
                        /** @type {?} */
                        var numOfDays = +MONTH_LENGTH[i][j] + 29;
                        if (daysDiff <= numOfDays) {
                            hDay = daysDiff + 1;
                            if (hDay > numOfDays) {
                                hDay = 1;
                                j++;
                            }
                            if (j > 11) {
                                j = 0;
                                year++;
                            }
                            hMonth = j;
                            hYear = year;
                            return new NgbDate(hYear, hMonth + 1, hDay);
                        }
                        daysDiff = daysDiff - numOfDays;
                    }
                }
            }
            else {
                return _super.prototype.fromGregorian.call(this, gDate);
            }
        };
        /**
        * Converts the current Hijri date to Gregorian.
        */
        /**
         * Converts the current Hijri date to Gregorian.
         * @param {?} hDate
         * @return {?}
         */
        NgbCalendarIslamicUmalqura.prototype.toGregorian = /**
         * Converts the current Hijri date to Gregorian.
         * @param {?} hDate
         * @return {?}
         */
        function (hDate) {
            /** @type {?} */
            var hYear = hDate.year;
            /** @type {?} */
            var hMonth = hDate.month - 1;
            /** @type {?} */
            var hDay = hDate.day;
            /** @type {?} */
            var gDate = new Date(GREGORIAN_FIRST_DATE);
            /** @type {?} */
            var dayDiff = hDay - 1;
            if (hYear >= HIJRI_BEGIN && hYear <= HIJRI_END) {
                for (var y = 0; y < hYear - HIJRI_BEGIN; y++) {
                    for (var m = 0; m < 12; m++) {
                        dayDiff += +MONTH_LENGTH[y][m] + 29;
                    }
                }
                for (var m = 0; m < hMonth; m++) {
                    dayDiff += +MONTH_LENGTH[hYear - HIJRI_BEGIN][m] + 29;
                }
                gDate.setDate(GREGORIAN_FIRST_DATE.getDate() + dayDiff);
            }
            else {
                gDate = _super.prototype.toGregorian.call(this, hDate);
            }
            return gDate;
        };
        /**
        * Returns the number of days in a specific Hijri hMonth.
        * `hMonth` is 1 for Muharram, 2 for Safar, etc.
        * `hYear` is any Hijri hYear.
        */
        /**
         * Returns the number of days in a specific Hijri hMonth.
         * `hMonth` is 1 for Muharram, 2 for Safar, etc.
         * `hYear` is any Hijri hYear.
         * @param {?} hMonth
         * @param {?} hYear
         * @return {?}
         */
        NgbCalendarIslamicUmalqura.prototype.getDaysPerMonth = /**
         * Returns the number of days in a specific Hijri hMonth.
         * `hMonth` is 1 for Muharram, 2 for Safar, etc.
         * `hYear` is any Hijri hYear.
         * @param {?} hMonth
         * @param {?} hYear
         * @return {?}
         */
        function (hMonth, hYear) {
            if (hYear >= HIJRI_BEGIN && hYear <= HIJRI_END) {
                /** @type {?} */
                var pos = hYear - HIJRI_BEGIN;
                return +MONTH_LENGTH[pos][hMonth - 1] + 29;
            }
            return _super.prototype.getDaysPerMonth.call(this, hMonth, hYear);
        };
        NgbCalendarIslamicUmalqura.decorators = [
            { type: core.Injectable }
        ];
        return NgbCalendarIslamicUmalqura;
    }(NgbCalendarIslamicCivil));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * Returns the equivalent JS date value for a give input Jalali date.
     * `jalaliDate` is an Jalali date to be converted to Gregorian.
     * @param {?} jalaliDate
     * @return {?}
     */
    function toGregorian(jalaliDate) {
        /** @type {?} */
        var jdn = jalaliToJulian(jalaliDate.year, jalaliDate.month, jalaliDate.day);
        /** @type {?} */
        var date = julianToGregorian(jdn);
        date.setHours(6, 30, 3, 200);
        return date;
    }
    /**
     * Returns the equivalent jalali date value for a give input Gregorian date.
     * `gdate` is a JS Date to be converted to jalali.
     * utc to local
     * @param {?} gdate
     * @return {?}
     */
    function fromGregorian(gdate) {
        /** @type {?} */
        var g2d = gregorianToJulian(gdate.getFullYear(), gdate.getMonth() + 1, gdate.getDate());
        return julianToJalali(g2d);
    }
    /**
     * @param {?} date
     * @param {?} yearValue
     * @return {?}
     */
    function setJalaliYear(date, yearValue) {
        date.year = +yearValue;
        return date;
    }
    /**
     * @param {?} date
     * @param {?} month
     * @return {?}
     */
    function setJalaliMonth(date, month) {
        month = +month;
        date.year = date.year + Math.floor((month - 1) / 12);
        date.month = Math.floor(((month - 1) % 12 + 12) % 12) + 1;
        return date;
    }
    /**
     * @param {?} date
     * @param {?} day
     * @return {?}
     */
    function setJalaliDay(date, day) {
        /** @type {?} */
        var mDays = getDaysPerMonth(date.month, date.year);
        if (day <= 0) {
            while (day <= 0) {
                date = setJalaliMonth(date, date.month - 1);
                mDays = getDaysPerMonth(date.month, date.year);
                day += mDays;
            }
        }
        else if (day > mDays) {
            while (day > mDays) {
                day -= mDays;
                date = setJalaliMonth(date, date.month + 1);
                mDays = getDaysPerMonth(date.month, date.year);
            }
        }
        date.day = day;
        return date;
    }
    /**
     * @param {?} a
     * @param {?} b
     * @return {?}
     */
    function mod$1(a, b) {
        return a - b * Math.floor(a / b);
    }
    /**
     * @param {?} a
     * @param {?} b
     * @return {?}
     */
    function div(a, b) {
        return Math.trunc(a / b);
    }
    /*
     This function determines if the Jalali (Persian) year is
     leap (366-day long) or is the common year (365 days), and
     finds the day in March (Gregorian calendar) of the first
     day of the Jalali year (jalaliYear).
     @param jalaliYear Jalali calendar year (-61 to 3177)
     @return
     leap: number of years since the last leap year (0 to 4)
     gYear: Gregorian year of the beginning of Jalali year
     march: the March day of Farvardin the 1st (1st day of jalaliYear)
     @see: http://www.astro.uni.torun.pl/~kb/Papers/EMP/PersianC-EMP.htm
     @see: http://www.fourmilab.ch/documents/calendar/
     */
    /**
     * @param {?} jalaliYear
     * @return {?}
     */
    function jalCal(jalaliYear) {
        // Jalali years starting the 33-year rule.
        /** @type {?} */
        var breaks = [-61, 9, 38, 199, 426, 686, 756, 818, 1111, 1181, 1210, 1635, 2060, 2097, 2192, 2262, 2324, 2394, 2456, 3178];
        /** @type {?} */
        var breaksLength = breaks.length;
        /** @type {?} */
        var gYear = jalaliYear + 621;
        /** @type {?} */
        var leapJ = -14;
        /** @type {?} */
        var jp = breaks[0];
        if (jalaliYear < jp || jalaliYear >= breaks[breaksLength - 1]) {
            throw new Error('Invalid Jalali year ' + jalaliYear);
        }
        // Find the limiting years for the Jalali year jalaliYear.
        /** @type {?} */
        var jump;
        for (var i = 1; i < breaksLength; i += 1) {
            /** @type {?} */
            var jm = breaks[i];
            jump = jm - jp;
            if (jalaliYear < jm) {
                break;
            }
            leapJ = leapJ + div(jump, 33) * 8 + div(mod$1(jump, 33), 4);
            jp = jm;
        }
        /** @type {?} */
        var n = jalaliYear - jp;
        // Find the number of leap years from AD 621 to the beginning
        // of the current Jalali year in the Persian calendar.
        leapJ = leapJ + div(n, 33) * 8 + div(mod$1(n, 33) + 3, 4);
        if (mod$1(jump, 33) === 4 && jump - n === 4) {
            leapJ += 1;
        }
        // And the same in the Gregorian calendar (until the year gYear).
        /** @type {?} */
        var leapG = div(gYear, 4) - div((div(gYear, 100) + 1) * 3, 4) - 150;
        // Determine the Gregorian date of Farvardin the 1st.
        /** @type {?} */
        var march = 20 + leapJ - leapG;
        // Find how many years have passed since the last leap year.
        if (jump - n < 6) {
            n = n - jump + div(jump + 4, 33) * 33;
        }
        /** @type {?} */
        var leap = mod$1(mod$1(n + 1, 33) - 1, 4);
        if (leap === -1) {
            leap = 4;
        }
        return { leap: leap, gy: gYear, march: march };
    }
    /*
     Calculates Gregorian and Julian calendar dates from the Julian Day number
     (jdn) for the period since jdn=-34839655 (i.e. the year -100100 of both
     calendars) to some millions years ahead of the present.
     @param jdn Julian Day number
     @return
     gYear: Calendar year (years BC numbered 0, -1, -2, ...)
     gMonth: Calendar month (1 to 12)
     gDay: Calendar day of the month M (1 to 28/29/30/31)
     */
    /**
     * @param {?} julianDayNumber
     * @return {?}
     */
    function julianToGregorian(julianDayNumber) {
        /** @type {?} */
        var j = 4 * julianDayNumber + 139361631;
        j = j + div(div(4 * julianDayNumber + 183187720, 146097) * 3, 4) * 4 - 3908;
        /** @type {?} */
        var i = div(mod$1(j, 1461), 4) * 5 + 308;
        /** @type {?} */
        var gDay = div(mod$1(i, 153), 5) + 1;
        /** @type {?} */
        var gMonth = mod$1(div(i, 153), 12) + 1;
        /** @type {?} */
        var gYear = div(j, 1461) - 100100 + div(8 - gMonth, 6);
        return new Date(gYear, gMonth - 1, gDay);
    }
    /*
     Converts a date of the Jalali calendar to the Julian Day number.
     @param jy Jalali year (1 to 3100)
     @param jm Jalali month (1 to 12)
     @param jd Jalali day (1 to 29/31)
     @return Julian Day number
     */
    /**
     * @param {?} gy
     * @param {?} gm
     * @param {?} gd
     * @return {?}
     */
    function gregorianToJulian(gy, gm, gd) {
        /** @type {?} */
        var d = div((gy + div(gm - 8, 6) + 100100) * 1461, 4) + div(153 * mod$1(gm + 9, 12) + 2, 5) + gd - 34840408;
        d = d - div(div(gy + 100100 + div(gm - 8, 6), 100) * 3, 4) + 752;
        return d;
    }
    /*
     Converts the Julian Day number to a date in the Jalali calendar.
     @param julianDayNumber Julian Day number
     @return
     jalaliYear: Jalali year (1 to 3100)
     jalaliMonth: Jalali month (1 to 12)
     jalaliDay: Jalali day (1 to 29/31)
     */
    /**
     * @param {?} julianDayNumber
     * @return {?}
     */
    function julianToJalali(julianDayNumber) {
        /** @type {?} */
        var gy = julianToGregorian(julianDayNumber).getFullYear() // Calculate Gregorian year (gy).
        ;
        /** @type {?} */
        var jalaliYear = gy - 621;
        /** @type {?} */
        var r = jalCal(jalaliYear);
        /** @type {?} */
        var gregorianDay = gregorianToJulian(gy, 3, r.march);
        /** @type {?} */
        var jalaliDay;
        /** @type {?} */
        var jalaliMonth;
        /** @type {?} */
        var numberOfDays;
        // Find number of days that passed since 1 Farvardin.
        numberOfDays = julianDayNumber - gregorianDay;
        if (numberOfDays >= 0) {
            if (numberOfDays <= 185) {
                // The first 6 months.
                jalaliMonth = 1 + div(numberOfDays, 31);
                jalaliDay = mod$1(numberOfDays, 31) + 1;
                return new NgbDate(jalaliYear, jalaliMonth, jalaliDay);
            }
            else {
                // The remaining months.
                numberOfDays -= 186;
            }
        }
        else {
            // Previous Jalali year.
            jalaliYear -= 1;
            numberOfDays += 179;
            if (r.leap === 1) {
                numberOfDays += 1;
            }
        }
        jalaliMonth = 7 + div(numberOfDays, 30);
        jalaliDay = mod$1(numberOfDays, 30) + 1;
        return new NgbDate(jalaliYear, jalaliMonth, jalaliDay);
    }
    /*
     Converts a date of the Jalali calendar to the Julian Day number.
     @param jYear Jalali year (1 to 3100)
     @param jMonth Jalali month (1 to 12)
     @param jDay Jalali day (1 to 29/31)
     @return Julian Day number
     */
    /**
     * @param {?} jYear
     * @param {?} jMonth
     * @param {?} jDay
     * @return {?}
     */
    function jalaliToJulian(jYear, jMonth, jDay) {
        /** @type {?} */
        var r = jalCal(jYear);
        return gregorianToJulian(r.gy, 3, r.march) + (jMonth - 1) * 31 - div(jMonth, 7) * (jMonth - 7) + jDay - 1;
    }
    /**
     * Returns the number of days in a specific jalali month.
     * @param {?} month
     * @param {?} year
     * @return {?}
     */
    function getDaysPerMonth(month, year) {
        if (month <= 6) {
            return 31;
        }
        if (month <= 11) {
            return 30;
        }
        if (jalCal(year).leap === 0) {
            return 30;
        }
        return 29;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbCalendarPersian = /** @class */ (function (_super) {
        __extends(NgbCalendarPersian, _super);
        function NgbCalendarPersian() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * @return {?}
         */
        NgbCalendarPersian.prototype.getDaysPerWeek = /**
         * @return {?}
         */
        function () { return 7; };
        /**
         * @return {?}
         */
        NgbCalendarPersian.prototype.getMonths = /**
         * @return {?}
         */
        function () { return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]; };
        /**
         * @return {?}
         */
        NgbCalendarPersian.prototype.getWeeksPerMonth = /**
         * @return {?}
         */
        function () { return 6; };
        /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        NgbCalendarPersian.prototype.getNext = /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        function (date, period, number) {
            if (period === void 0) { period = 'd'; }
            if (number === void 0) { number = 1; }
            date = new NgbDate(date.year, date.month, date.day);
            switch (period) {
                case 'y':
                    date = setJalaliYear(date, date.year + number);
                    date.month = 1;
                    date.day = 1;
                    return date;
                case 'm':
                    date = setJalaliMonth(date, date.month + number);
                    date.day = 1;
                    return date;
                case 'd':
                    return setJalaliDay(date, date.day + number);
                default:
                    return date;
            }
        };
        /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        NgbCalendarPersian.prototype.getPrev = /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        function (date, period, number) {
            if (period === void 0) { period = 'd'; }
            if (number === void 0) { number = 1; }
            return this.getNext(date, period, -number);
        };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbCalendarPersian.prototype.getWeekday = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            /** @type {?} */
            var day = toGregorian(date).getDay();
            // in JS Date Sun=0, in ISO 8601 Sun=7
            return day === 0 ? 7 : day;
        };
        /**
         * @param {?} week
         * @param {?} firstDayOfWeek
         * @return {?}
         */
        NgbCalendarPersian.prototype.getWeekNumber = /**
         * @param {?} week
         * @param {?} firstDayOfWeek
         * @return {?}
         */
        function (week, firstDayOfWeek) {
            // in JS Date Sun=0, in ISO 8601 Sun=7
            if (firstDayOfWeek === 7) {
                firstDayOfWeek = 0;
            }
            /** @type {?} */
            var thursdayIndex = (4 + 7 - firstDayOfWeek) % 7;
            /** @type {?} */
            var date = week[thursdayIndex];
            /** @type {?} */
            var jsDate = toGregorian(date);
            jsDate.setDate(jsDate.getDate() + 4 - (jsDate.getDay() || 7)); // Thursday
            // Thursday
            /** @type {?} */
            var time = jsDate.getTime();
            /** @type {?} */
            var startDate = toGregorian(new NgbDate(date.year, 1, 1));
            return Math.floor(Math.round((time - startDate.getTime()) / 86400000) / 7) + 1;
        };
        /**
         * @return {?}
         */
        NgbCalendarPersian.prototype.getToday = /**
         * @return {?}
         */
        function () { return fromGregorian(new Date()); };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbCalendarPersian.prototype.isValid = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return date && isInteger(date.year) && isInteger(date.month) && isInteger(date.day) &&
                !isNaN(toGregorian(date).getTime());
        };
        NgbCalendarPersian.decorators = [
            { type: core.Injectable }
        ];
        return NgbCalendarPersian;
    }(NgbCalendar));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var PARTS_PER_HOUR = 1080;
    /** @type {?} */
    var PARTS_PER_DAY = 24 * PARTS_PER_HOUR;
    /** @type {?} */
    var PARTS_FRACTIONAL_MONTH = 12 * PARTS_PER_HOUR + 793;
    /** @type {?} */
    var PARTS_PER_MONTH = 29 * PARTS_PER_DAY + PARTS_FRACTIONAL_MONTH;
    /** @type {?} */
    var BAHARAD = 11 * PARTS_PER_HOUR + 204;
    /** @type {?} */
    var HEBREW_DAY_ON_JAN_1_1970 = 2092591;
    /** @type {?} */
    var GREGORIAN_EPOCH$1 = 1721425.5;
    /**
     * @param {?} year
     * @return {?}
     */
    function isGregorianLeapYear$1(year) {
        return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0;
    }
    /**
     * @param {?} year
     * @return {?}
     */
    function numberOfFirstDayInYear(year) {
        /** @type {?} */
        var monthsBeforeYear = Math.floor((235 * year - 234) / 19);
        /** @type {?} */
        var fractionalMonthsBeforeYear = monthsBeforeYear * PARTS_FRACTIONAL_MONTH + BAHARAD;
        /** @type {?} */
        var dayNumber = monthsBeforeYear * 29 + Math.floor(fractionalMonthsBeforeYear / PARTS_PER_DAY);
        /** @type {?} */
        var timeOfDay = fractionalMonthsBeforeYear % PARTS_PER_DAY;
        /** @type {?} */
        var dayOfWeek = dayNumber % 7;
        if (dayOfWeek === 2 || dayOfWeek === 4 || dayOfWeek === 6) {
            dayNumber++;
            dayOfWeek = dayNumber % 7;
        }
        if (dayOfWeek === 1 && timeOfDay > 15 * PARTS_PER_HOUR + 204 && !isHebrewLeapYear(year)) {
            dayNumber += 2;
        }
        else if (dayOfWeek === 0 && timeOfDay > 21 * PARTS_PER_HOUR + 589 && isHebrewLeapYear(year - 1)) {
            dayNumber++;
        }
        return dayNumber;
    }
    /**
     * @param {?} month
     * @param {?} year
     * @return {?}
     */
    function getDaysInGregorianMonth(month, year) {
        /** @type {?} */
        var days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        if (isGregorianLeapYear$1(year)) {
            days[1]++;
        }
        return days[month - 1];
    }
    /**
     * @param {?} year
     * @return {?}
     */
    function getHebrewMonths(year) {
        return isHebrewLeapYear(year) ? 13 : 12;
    }
    /**
     * Returns the number of days in a specific Hebrew year.
     * `year` is any Hebrew year.
     * @param {?} year
     * @return {?}
     */
    function getDaysInHebrewYear(year) {
        return numberOfFirstDayInYear(year + 1) - numberOfFirstDayInYear(year);
    }
    /**
     * @param {?} year
     * @return {?}
     */
    function isHebrewLeapYear(year) {
        /** @type {?} */
        var b = (year * 12 + 17) % 19;
        return b >= ((b < 0) ? -7 : 12);
    }
    /**
     * Returns the number of days in a specific Hebrew month.
     * `month` is 1 for Nisan, 2 for Iyar etc. Note: Hebrew leap year contains 13 months.
     * `year` is any Hebrew year.
     * @param {?} month
     * @param {?} year
     * @return {?}
     */
    function getDaysInHebrewMonth(month, year) {
        /** @type {?} */
        var yearLength = numberOfFirstDayInYear(year + 1) - numberOfFirstDayInYear(year);
        /** @type {?} */
        var yearType = (yearLength <= 380 ? yearLength : (yearLength - 30)) - 353;
        /** @type {?} */
        var leapYear = isHebrewLeapYear(year);
        /** @type {?} */
        var daysInMonth = leapYear ? [30, 29, 29, 29, 30, 30, 29, 30, 29, 30, 29, 30, 29] :
            [30, 29, 29, 29, 30, 29, 30, 29, 30, 29, 30, 29];
        if (yearType > 0) {
            daysInMonth[2]++; // Kislev gets an extra day in normal or complete years.
        }
        if (yearType > 1) {
            daysInMonth[1]++; // Heshvan gets an extra day in complete years only.
        }
        return daysInMonth[month - 1];
    }
    /**
     * @param {?} date
     * @return {?}
     */
    function getDayNumberInHebrewYear(date) {
        /** @type {?} */
        var numberOfDay = 0;
        for (var i = 1; i < date.month; i++) {
            numberOfDay += getDaysInHebrewMonth(i, date.year);
        }
        return numberOfDay + date.day;
    }
    /**
     * @param {?} date
     * @param {?} val
     * @return {?}
     */
    function setHebrewMonth(date, val) {
        /** @type {?} */
        var after = val >= 0;
        if (!after) {
            val = -val;
        }
        while (val > 0) {
            if (after) {
                if (val > getHebrewMonths(date.year) - date.month) {
                    val -= getHebrewMonths(date.year) - date.month + 1;
                    date.year++;
                    date.month = 1;
                }
                else {
                    date.month += val;
                    val = 0;
                }
            }
            else {
                if (val >= date.month) {
                    date.year--;
                    val -= date.month;
                    date.month = getHebrewMonths(date.year);
                }
                else {
                    date.month -= val;
                    val = 0;
                }
            }
        }
        return date;
    }
    /**
     * @param {?} date
     * @param {?} val
     * @return {?}
     */
    function setHebrewDay(date, val) {
        /** @type {?} */
        var after = val >= 0;
        if (!after) {
            val = -val;
        }
        while (val > 0) {
            if (after) {
                if (val > getDaysInHebrewYear(date.year) - getDayNumberInHebrewYear(date)) {
                    val -= getDaysInHebrewYear(date.year) - getDayNumberInHebrewYear(date) + 1;
                    date.year++;
                    date.month = 1;
                    date.day = 1;
                }
                else if (val > getDaysInHebrewMonth(date.month, date.year) - date.day) {
                    val -= getDaysInHebrewMonth(date.month, date.year) - date.day + 1;
                    date.month++;
                    date.day = 1;
                }
                else {
                    date.day += val;
                    val = 0;
                }
            }
            else {
                if (val >= date.day) {
                    val -= date.day;
                    date.month--;
                    if (date.month === 0) {
                        date.year--;
                        date.month = getHebrewMonths(date.year);
                    }
                    date.day = getDaysInHebrewMonth(date.month, date.year);
                }
                else {
                    date.day -= val;
                    val = 0;
                }
            }
        }
        return date;
    }
    /**
     * Returns the equivalent Hebrew date value for a give input Gregorian date.
     * `gdate` is a JS Date to be converted to Hebrew date.
     * @param {?} gdate
     * @return {?}
     */
    function fromGregorian$1(gdate) {
        /** @type {?} */
        var date = new Date(gdate);
        /** @type {?} */
        var gYear = date.getFullYear();
        /** @type {?} */
        var gMonth = date.getMonth();
        /** @type {?} */
        var gDay = date.getDate();
        /** @type {?} */
        var julianDay = GREGORIAN_EPOCH$1 - 1 + 365 * (gYear - 1) + Math.floor((gYear - 1) / 4) -
            Math.floor((gYear - 1) / 100) + Math.floor((gYear - 1) / 400) +
            Math.floor((367 * (gMonth + 1) - 362) / 12 + (gMonth + 1 <= 2 ? 0 : isGregorianLeapYear$1(gYear) ? -1 : -2) + gDay);
        julianDay = Math.floor(julianDay + 0.5);
        /** @type {?} */
        var daysSinceHebEpoch = julianDay - 347997;
        /** @type {?} */
        var monthsSinceHebEpoch = Math.floor(daysSinceHebEpoch * PARTS_PER_DAY / PARTS_PER_MONTH);
        /** @type {?} */
        var hYear = Math.floor((monthsSinceHebEpoch * 19 + 234) / 235) + 1;
        /** @type {?} */
        var firstDayOfThisYear = numberOfFirstDayInYear(hYear);
        /** @type {?} */
        var dayOfYear = daysSinceHebEpoch - firstDayOfThisYear;
        while (dayOfYear < 1) {
            hYear--;
            firstDayOfThisYear = numberOfFirstDayInYear(hYear);
            dayOfYear = daysSinceHebEpoch - firstDayOfThisYear;
        }
        /** @type {?} */
        var hMonth = 1;
        /** @type {?} */
        var hDay = dayOfYear;
        while (hDay > getDaysInHebrewMonth(hMonth, hYear)) {
            hDay -= getDaysInHebrewMonth(hMonth, hYear);
            hMonth++;
        }
        return new NgbDate(hYear, hMonth, hDay);
    }
    /**
     * Returns the equivalent JS date value for a given Hebrew date.
     * `hebrewDate` is an Hebrew date to be converted to Gregorian.
     * @param {?} hebrewDate
     * @return {?}
     */
    function toGregorian$1(hebrewDate) {
        /** @type {?} */
        var hYear = hebrewDate.year;
        /** @type {?} */
        var hMonth = hebrewDate.month;
        /** @type {?} */
        var hDay = hebrewDate.day;
        /** @type {?} */
        var days = numberOfFirstDayInYear(hYear);
        for (var i = 1; i < hMonth; i++) {
            days += getDaysInHebrewMonth(i, hYear);
        }
        days += hDay;
        /** @type {?} */
        var diffDays = days - HEBREW_DAY_ON_JAN_1_1970;
        /** @type {?} */
        var after = diffDays >= 0;
        if (!after) {
            diffDays = -diffDays;
        }
        /** @type {?} */
        var gYear = 1970;
        /** @type {?} */
        var gMonth = 1;
        /** @type {?} */
        var gDay = 1;
        while (diffDays > 0) {
            if (after) {
                if (diffDays >= (isGregorianLeapYear$1(gYear) ? 366 : 365)) {
                    diffDays -= isGregorianLeapYear$1(gYear) ? 366 : 365;
                    gYear++;
                }
                else if (diffDays >= getDaysInGregorianMonth(gMonth, gYear)) {
                    diffDays -= getDaysInGregorianMonth(gMonth, gYear);
                    gMonth++;
                }
                else {
                    gDay += diffDays;
                    diffDays = 0;
                }
            }
            else {
                if (diffDays >= (isGregorianLeapYear$1(gYear - 1) ? 366 : 365)) {
                    diffDays -= isGregorianLeapYear$1(gYear - 1) ? 366 : 365;
                    gYear--;
                }
                else {
                    if (gMonth > 1) {
                        gMonth--;
                    }
                    else {
                        gMonth = 12;
                        gYear--;
                    }
                    if (diffDays >= getDaysInGregorianMonth(gMonth, gYear)) {
                        diffDays -= getDaysInGregorianMonth(gMonth, gYear);
                    }
                    else {
                        gDay = getDaysInGregorianMonth(gMonth, gYear) - diffDays + 1;
                        diffDays = 0;
                    }
                }
            }
        }
        return new Date(gYear, gMonth - 1, gDay);
    }
    /**
     * @param {?} numerals
     * @return {?}
     */
    function hebrewNumerals(numerals) {
        if (!numerals) {
            return '';
        }
        /** @type {?} */
        var hArray0_9 = ['', '\u05d0', '\u05d1', '\u05d2', '\u05d3', '\u05d4', '\u05d5', '\u05d6', '\u05d7', '\u05d8'];
        /** @type {?} */
        var hArray10_19 = [
            '\u05d9', '\u05d9\u05d0', '\u05d9\u05d1', '\u05d9\u05d2', '\u05d9\u05d3', '\u05d8\u05d5', '\u05d8\u05d6',
            '\u05d9\u05d6', '\u05d9\u05d7', '\u05d9\u05d8'
        ];
        /** @type {?} */
        var hArray20_90 = ['', '', '\u05db', '\u05dc', '\u05de', '\u05e0', '\u05e1', '\u05e2', '\u05e4', '\u05e6'];
        /** @type {?} */
        var hArray100_900 = [
            '', '\u05e7', '\u05e8', '\u05e9', '\u05ea', '\u05ea\u05e7', '\u05ea\u05e8', '\u05ea\u05e9', '\u05ea\u05ea',
            '\u05ea\u05ea\u05e7'
        ];
        /** @type {?} */
        var hArray1000_9000 = [
            '', '\u05d0', '\u05d1', '\u05d1\u05d0', '\u05d1\u05d1', '\u05d4', '\u05d4\u05d0', '\u05d4\u05d1',
            '\u05d4\u05d1\u05d0', '\u05d4\u05d1\u05d1'
        ];
        /** @type {?} */
        var geresh = '\u05f3';
        /** @type {?} */
        var gershaim = '\u05f4';
        /** @type {?} */
        var mem = 0;
        /** @type {?} */
        var result = [];
        /** @type {?} */
        var step = 0;
        while (numerals > 0) {
            /** @type {?} */
            var m = numerals % 10;
            if (step === 0) {
                mem = m;
            }
            else if (step === 1) {
                if (m !== 1) {
                    result.unshift(hArray20_90[m], hArray0_9[mem]);
                }
                else {
                    result.unshift(hArray10_19[mem]);
                }
            }
            else if (step === 2) {
                result.unshift(hArray100_900[m]);
            }
            else {
                if (m !== 5) {
                    result.unshift(hArray1000_9000[m], geresh, ' ');
                }
                break;
            }
            numerals = Math.floor(numerals / 10);
            if (step === 0 && numerals === 0) {
                result.unshift(hArray0_9[m]);
            }
            step++;
        }
        result = result.join('').split('');
        if (result.length === 1) {
            result.push(geresh);
        }
        else if (result.length > 1) {
            result.splice(result.length - 1, 0, gershaim);
        }
        return result.join('');
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * \@since 3.2.0
     */
    var NgbCalendarHebrew = /** @class */ (function (_super) {
        __extends(NgbCalendarHebrew, _super);
        function NgbCalendarHebrew() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * @return {?}
         */
        NgbCalendarHebrew.prototype.getDaysPerWeek = /**
         * @return {?}
         */
        function () { return 7; };
        /**
         * @param {?=} year
         * @return {?}
         */
        NgbCalendarHebrew.prototype.getMonths = /**
         * @param {?=} year
         * @return {?}
         */
        function (year) {
            if (year && isHebrewLeapYear(year)) {
                return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
            }
            else {
                return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
            }
        };
        /**
         * @return {?}
         */
        NgbCalendarHebrew.prototype.getWeeksPerMonth = /**
         * @return {?}
         */
        function () { return 6; };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbCalendarHebrew.prototype.isValid = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            /** @type {?} */
            var b = date && isNumber(date.year) && isNumber(date.month) && isNumber(date.day);
            b = b && date.month > 0 && date.month <= (isHebrewLeapYear(date.year) ? 13 : 12);
            b = b && date.day > 0 && date.day <= getDaysInHebrewMonth(date.month, date.year);
            return b && !isNaN(toGregorian$1(date).getTime());
        };
        /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        NgbCalendarHebrew.prototype.getNext = /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        function (date, period, number) {
            if (period === void 0) { period = 'd'; }
            if (number === void 0) { number = 1; }
            date = new NgbDate(date.year, date.month, date.day);
            switch (period) {
                case 'y':
                    date.year += number;
                    date.month = 1;
                    date.day = 1;
                    return date;
                case 'm':
                    date = setHebrewMonth(date, number);
                    date.day = 1;
                    return date;
                case 'd':
                    return setHebrewDay(date, number);
                default:
                    return date;
            }
        };
        /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        NgbCalendarHebrew.prototype.getPrev = /**
         * @param {?} date
         * @param {?=} period
         * @param {?=} number
         * @return {?}
         */
        function (date, period, number) {
            if (period === void 0) { period = 'd'; }
            if (number === void 0) { number = 1; }
            return this.getNext(date, period, -number);
        };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbCalendarHebrew.prototype.getWeekday = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            /** @type {?} */
            var day = toGregorian$1(date).getDay();
            // in JS Date Sun=0, in ISO 8601 Sun=7
            return day === 0 ? 7 : day;
        };
        /**
         * @param {?} week
         * @param {?} firstDayOfWeek
         * @return {?}
         */
        NgbCalendarHebrew.prototype.getWeekNumber = /**
         * @param {?} week
         * @param {?} firstDayOfWeek
         * @return {?}
         */
        function (week, firstDayOfWeek) {
            /** @type {?} */
            var date = week[week.length - 1];
            return Math.ceil(getDayNumberInHebrewYear(date) / 7);
        };
        /**
         * @return {?}
         */
        NgbCalendarHebrew.prototype.getToday = /**
         * @return {?}
         */
        function () { return fromGregorian$1(new Date()); };
        /**
         * @since 3.4.0
         */
        /**
         * \@since 3.4.0
         * @param {?} date
         * @return {?}
         */
        NgbCalendarHebrew.prototype.toGregorian = /**
         * \@since 3.4.0
         * @param {?} date
         * @return {?}
         */
        function (date) { return fromJSDate(toGregorian$1(date)); };
        /**
         * @since 3.4.0
         */
        /**
         * \@since 3.4.0
         * @param {?} date
         * @return {?}
         */
        NgbCalendarHebrew.prototype.fromGregorian = /**
         * \@since 3.4.0
         * @param {?} date
         * @return {?}
         */
        function (date) { return fromGregorian$1(toJSDate(date)); };
        NgbCalendarHebrew.decorators = [
            { type: core.Injectable }
        ];
        return NgbCalendarHebrew;
    }(NgbCalendar));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var WEEKDAYS = ['שני', 'שלישי', 'רביעי', 'חמישי', 'שישי', 'שבת', 'ראשון'];
    /** @type {?} */
    var MONTHS = ['תשרי', 'חשון', 'כסלו', 'טבת', 'שבט', 'אדר', 'ניסן', 'אייר', 'סיון', 'תמוז', 'אב', 'אלול'];
    /** @type {?} */
    var MONTHS_LEAP = ['תשרי', 'חשון', 'כסלו', 'טבת', 'שבט', 'אדר א׳', 'אדר ב׳', 'ניסן', 'אייר', 'סיון', 'תמוז', 'אב', 'אלול'];
    /**
     * \@since 3.2.0
     */
    var NgbDatepickerI18nHebrew = /** @class */ (function (_super) {
        __extends(NgbDatepickerI18nHebrew, _super);
        function NgbDatepickerI18nHebrew() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * @param {?} month
         * @param {?=} year
         * @return {?}
         */
        NgbDatepickerI18nHebrew.prototype.getMonthShortName = /**
         * @param {?} month
         * @param {?=} year
         * @return {?}
         */
        function (month, year) { return this.getMonthFullName(month, year); };
        /**
         * @param {?} month
         * @param {?=} year
         * @return {?}
         */
        NgbDatepickerI18nHebrew.prototype.getMonthFullName = /**
         * @param {?} month
         * @param {?=} year
         * @return {?}
         */
        function (month, year) {
            return isHebrewLeapYear(year) ? MONTHS_LEAP[month - 1] : MONTHS[month - 1];
        };
        /**
         * @param {?} weekday
         * @return {?}
         */
        NgbDatepickerI18nHebrew.prototype.getWeekdayShortName = /**
         * @param {?} weekday
         * @return {?}
         */
        function (weekday) { return WEEKDAYS[weekday - 1]; };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbDatepickerI18nHebrew.prototype.getDayAriaLabel = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return hebrewNumerals(date.day) + " " + this.getMonthFullName(date.month, date.year) + " " + hebrewNumerals(date.year);
        };
        /**
         * @param {?} date
         * @return {?}
         */
        NgbDatepickerI18nHebrew.prototype.getDayNumerals = /**
         * @param {?} date
         * @return {?}
         */
        function (date) { return hebrewNumerals(date.day); };
        /**
         * @param {?} weekNumber
         * @return {?}
         */
        NgbDatepickerI18nHebrew.prototype.getWeekNumerals = /**
         * @param {?} weekNumber
         * @return {?}
         */
        function (weekNumber) { return hebrewNumerals(weekNumber); };
        /**
         * @param {?} year
         * @return {?}
         */
        NgbDatepickerI18nHebrew.prototype.getYearNumerals = /**
         * @param {?} year
         * @return {?}
         */
        function (year) { return hebrewNumerals(year); };
        NgbDatepickerI18nHebrew.decorators = [
            { type: core.Injectable }
        ];
        return NgbDatepickerI18nHebrew;
    }(NgbDatepickerI18n));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * [`NgbDateAdapter`](#/components/datepicker/api#NgbDateAdapter) implementation that uses
     * native javascript dates as a user date model.
     */
    var NgbDateNativeAdapter = /** @class */ (function (_super) {
        __extends(NgbDateNativeAdapter, _super);
        function NgbDateNativeAdapter() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * Converts a native `Date` to a `NgbDateStruct`.
         */
        /**
         * Converts a native `Date` to a `NgbDateStruct`.
         * @param {?} date
         * @return {?}
         */
        NgbDateNativeAdapter.prototype.fromModel = /**
         * Converts a native `Date` to a `NgbDateStruct`.
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return (date instanceof Date && !isNaN(date.getTime())) ? this._fromNativeDate(date) : null;
        };
        /**
         * Converts a `NgbDateStruct` to a native `Date`.
         */
        /**
         * Converts a `NgbDateStruct` to a native `Date`.
         * @param {?} date
         * @return {?}
         */
        NgbDateNativeAdapter.prototype.toModel = /**
         * Converts a `NgbDateStruct` to a native `Date`.
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return date && isInteger(date.year) && isInteger(date.month) && isInteger(date.day) ? this._toNativeDate(date) :
                null;
        };
        /**
         * @protected
         * @param {?} date
         * @return {?}
         */
        NgbDateNativeAdapter.prototype._fromNativeDate = /**
         * @protected
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() };
        };
        /**
         * @protected
         * @param {?} date
         * @return {?}
         */
        NgbDateNativeAdapter.prototype._toNativeDate = /**
         * @protected
         * @param {?} date
         * @return {?}
         */
        function (date) {
            /** @type {?} */
            var jsDate = new Date(date.year, date.month - 1, date.day, 12);
            // avoid 30 -> 1930 conversion
            jsDate.setFullYear(date.year);
            return jsDate;
        };
        NgbDateNativeAdapter.decorators = [
            { type: core.Injectable }
        ];
        return NgbDateNativeAdapter;
    }(NgbDateAdapter));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * Same as [`NgbDateNativeAdapter`](#/components/datepicker/api#NgbDateNativeAdapter), but with UTC dates.
     *
     * \@since 3.2.0
     */
    var NgbDateNativeUTCAdapter = /** @class */ (function (_super) {
        __extends(NgbDateNativeUTCAdapter, _super);
        function NgbDateNativeUTCAdapter() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * @protected
         * @param {?} date
         * @return {?}
         */
        NgbDateNativeUTCAdapter.prototype._fromNativeDate = /**
         * @protected
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return { year: date.getUTCFullYear(), month: date.getUTCMonth() + 1, day: date.getUTCDate() };
        };
        /**
         * @protected
         * @param {?} date
         * @return {?}
         */
        NgbDateNativeUTCAdapter.prototype._toNativeDate = /**
         * @protected
         * @param {?} date
         * @return {?}
         */
        function (date) {
            /** @type {?} */
            var jsDate = new Date(Date.UTC(date.year, date.month - 1, date.day));
            // avoid 30 -> 1930 conversion
            jsDate.setUTCFullYear(date.year);
            return jsDate;
        };
        NgbDateNativeUTCAdapter.decorators = [
            { type: core.Injectable }
        ];
        return NgbDateNativeUTCAdapter;
    }(NgbDateNativeAdapter));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbDatepickerModule = /** @class */ (function () {
        function NgbDatepickerModule() {
        }
        NgbDatepickerModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [
                            NgbDatepicker, NgbDatepickerMonthView, NgbDatepickerNavigation, NgbDatepickerNavigationSelect, NgbDatepickerDayView,
                            NgbInputDatepicker
                        ],
                        exports: [NgbDatepicker, NgbInputDatepicker],
                        imports: [common.CommonModule, forms.FormsModule],
                        entryComponents: [NgbDatepicker]
                    },] }
        ];
        return NgbDatepickerModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A configuration service for the [`NgbDropdown`](#/components/dropdown/api#NgbDropdown) component.
     *
     * You can inject this service, typically in your root component, and customize the values of its properties in
     * order to provide default values for all the dropdowns used in the application.
     */
    var NgbDropdownConfig = /** @class */ (function () {
        function NgbDropdownConfig() {
            this.autoClose = true;
            this.placement = ['bottom-left', 'bottom-right', 'top-left', 'top-right'];
        }
        NgbDropdownConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbDropdownConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbDropdownConfig_Factory() { return new NgbDropdownConfig(); }, token: NgbDropdownConfig, providedIn: "root" });
        return NgbDropdownConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbNavbar = /** @class */ (function () {
        function NgbNavbar() {
        }
        NgbNavbar.decorators = [
            { type: core.Directive, args: [{ selector: '.navbar' },] }
        ];
        return NgbNavbar;
    }());
    /**
     * A directive you should put put on a dropdown item to enable keyboard navigation.
     * Arrow keys will move focus between items marked with this directive.
     *
     * \@since 4.1.0
     */
    var NgbDropdownItem = /** @class */ (function () {
        function NgbDropdownItem(elementRef) {
            this.elementRef = elementRef;
            this._disabled = false;
        }
        Object.defineProperty(NgbDropdownItem.prototype, "disabled", {
            get: /**
             * @return {?}
             */
            function () { return this._disabled; },
            set: /**
             * @param {?} value
             * @return {?}
             */
            function (value) {
                this._disabled = (/** @type {?} */ (value)) === '' || value === true; // accept an empty attribute as true
            },
            enumerable: true,
            configurable: true
        });
        NgbDropdownItem.decorators = [
            { type: core.Directive, args: [{ selector: '[ngbDropdownItem]', host: { 'class': 'dropdown-item', '[class.disabled]': 'disabled' } },] }
        ];
        /** @nocollapse */
        NgbDropdownItem.ctorParameters = function () { return [
            { type: core.ElementRef }
        ]; };
        NgbDropdownItem.propDecorators = {
            disabled: [{ type: core.Input }]
        };
        return NgbDropdownItem;
    }());
    /**
     * A directive that wraps dropdown menu content and dropdown items.
     */
    var NgbDropdownMenu = /** @class */ (function () {
        function NgbDropdownMenu(dropdown) {
            this.dropdown = dropdown;
            this.placement = 'bottom';
            this.isOpen = false;
        }
        NgbDropdownMenu.decorators = [
            { type: core.Directive, args: [{
                        selector: '[ngbDropdownMenu]',
                        host: {
                            '[class.dropdown-menu]': 'true',
                            '[class.show]': 'dropdown.isOpen()',
                            '[attr.x-placement]': 'placement',
                            '(keydown.ArrowUp)': 'dropdown.onKeyDown($event)',
                            '(keydown.ArrowDown)': 'dropdown.onKeyDown($event)',
                            '(keydown.Home)': 'dropdown.onKeyDown($event)',
                            '(keydown.End)': 'dropdown.onKeyDown($event)',
                            '(keydown.Enter)': 'dropdown.onKeyDown($event)',
                            '(keydown.Space)': 'dropdown.onKeyDown($event)'
                        }
                    },] }
        ];
        /** @nocollapse */
        NgbDropdownMenu.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [core.forwardRef((/**
                             * @return {?}
                             */
                            function () { return NgbDropdown; })),] }] }
        ]; };
        NgbDropdownMenu.propDecorators = {
            menuItems: [{ type: core.ContentChildren, args: [NgbDropdownItem,] }]
        };
        return NgbDropdownMenu;
    }());
    /**
     * A directive to mark an element to which dropdown menu will be anchored.
     *
     * This is a simple version of the `NgbDropdownToggle` directive.
     * It plays the same role, but doesn't listen to click events to toggle dropdown menu thus enabling support
     * for events other than click.
     *
     * \@since 1.1.0
     */
    var NgbDropdownAnchor = /** @class */ (function () {
        function NgbDropdownAnchor(dropdown, _elementRef) {
            this.dropdown = dropdown;
            this._elementRef = _elementRef;
            this.anchorEl = _elementRef.nativeElement;
        }
        /**
         * @return {?}
         */
        NgbDropdownAnchor.prototype.getNativeElement = /**
         * @return {?}
         */
        function () { return this._elementRef.nativeElement; };
        NgbDropdownAnchor.decorators = [
            { type: core.Directive, args: [{
                        selector: '[ngbDropdownAnchor]',
                        host: { 'class': 'dropdown-toggle', 'aria-haspopup': 'true', '[attr.aria-expanded]': 'dropdown.isOpen()' }
                    },] }
        ];
        /** @nocollapse */
        NgbDropdownAnchor.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [core.forwardRef((/**
                             * @return {?}
                             */
                            function () { return NgbDropdown; })),] }] },
            { type: core.ElementRef }
        ]; };
        return NgbDropdownAnchor;
    }());
    /**
     * A directive to mark an element that will toggle dropdown via the `click` event.
     *
     * You can also use `NgbDropdownAnchor` as an alternative.
     */
    var NgbDropdownToggle = /** @class */ (function (_super) {
        __extends(NgbDropdownToggle, _super);
        function NgbDropdownToggle(dropdown, elementRef) {
            return _super.call(this, dropdown, elementRef) || this;
        }
        NgbDropdownToggle.decorators = [
            { type: core.Directive, args: [{
                        selector: '[ngbDropdownToggle]',
                        host: {
                            'class': 'dropdown-toggle',
                            'aria-haspopup': 'true',
                            '[attr.aria-expanded]': 'dropdown.isOpen()',
                            '(click)': 'dropdown.toggle()',
                            '(keydown.ArrowUp)': 'dropdown.onKeyDown($event)',
                            '(keydown.ArrowDown)': 'dropdown.onKeyDown($event)',
                            '(keydown.Home)': 'dropdown.onKeyDown($event)',
                            '(keydown.End)': 'dropdown.onKeyDown($event)'
                        },
                        providers: [{ provide: NgbDropdownAnchor, useExisting: core.forwardRef((/**
                                 * @return {?}
                                 */
                                function () { return NgbDropdownToggle; })) }]
                    },] }
        ];
        /** @nocollapse */
        NgbDropdownToggle.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [core.forwardRef((/**
                             * @return {?}
                             */
                            function () { return NgbDropdown; })),] }] },
            { type: core.ElementRef }
        ]; };
        return NgbDropdownToggle;
    }(NgbDropdownAnchor));
    /**
     * A directive that provides contextual overlays for displaying lists of links and more.
     */
    var NgbDropdown = /** @class */ (function () {
        function NgbDropdown(_changeDetector, config, _document, _ngZone, _elementRef, _renderer, ngbNavbar) {
            var _this = this;
            this._changeDetector = _changeDetector;
            this._document = _document;
            this._ngZone = _ngZone;
            this._elementRef = _elementRef;
            this._renderer = _renderer;
            this._closed$ = new rxjs.Subject();
            /**
             * Defines whether or not the dropdown menu is opened initially.
             */
            this._open = false;
            /**
             * An event fired when the dropdown is opened or closed.
             *
             * The event payload is a `boolean`:
             * * `true` - the dropdown was opened
             * * `false` - the dropdown was closed
             */
            this.openChange = new core.EventEmitter();
            this.placement = config.placement;
            this.container = config.container;
            this.autoClose = config.autoClose;
            this.display = ngbNavbar ? 'static' : 'dynamic';
            this._zoneSubscription = _ngZone.onStable.subscribe((/**
             * @return {?}
             */
            function () { _this._positionMenu(); }));
        }
        /**
         * @return {?}
         */
        NgbDropdown.prototype.ngAfterContentInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            this._ngZone.onStable.pipe(operators.take(1)).subscribe((/**
             * @return {?}
             */
            function () {
                _this._applyPlacementClasses();
                if (_this._open) {
                    _this._setCloseHandlers();
                }
            }));
        };
        /**
         * @param {?} changes
         * @return {?}
         */
        NgbDropdown.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            if (changes.container && this._open) {
                this._applyContainer(this.container);
            }
            if (changes.placement && !changes.placement.isFirstChange) {
                this._applyPlacementClasses();
            }
        };
        /**
         * Checks if the dropdown menu is open.
         */
        /**
         * Checks if the dropdown menu is open.
         * @return {?}
         */
        NgbDropdown.prototype.isOpen = /**
         * Checks if the dropdown menu is open.
         * @return {?}
         */
        function () { return this._open; };
        /**
         * Opens the dropdown menu.
         */
        /**
         * Opens the dropdown menu.
         * @return {?}
         */
        NgbDropdown.prototype.open = /**
         * Opens the dropdown menu.
         * @return {?}
         */
        function () {
            if (!this._open) {
                this._open = true;
                this._applyContainer(this.container);
                this.openChange.emit(true);
                this._setCloseHandlers();
            }
        };
        /**
         * @private
         * @return {?}
         */
        NgbDropdown.prototype._setCloseHandlers = /**
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            /** @type {?} */
            var anchor = this._anchor;
            ngbAutoClose(this._ngZone, this._document, this.autoClose, (/**
             * @return {?}
             */
            function () { return _this.close(); }), this._closed$, this._menu ? [this._menuElement.nativeElement] : [], anchor ? [anchor.getNativeElement()] : [], '.dropdown-item,.dropdown-divider');
        };
        /**
         * Closes the dropdown menu.
         */
        /**
         * Closes the dropdown menu.
         * @return {?}
         */
        NgbDropdown.prototype.close = /**
         * Closes the dropdown menu.
         * @return {?}
         */
        function () {
            if (this._open) {
                this._open = false;
                this._resetContainer();
                this._closed$.next();
                this.openChange.emit(false);
                this._changeDetector.markForCheck();
            }
        };
        /**
         * Toggles the dropdown menu.
         */
        /**
         * Toggles the dropdown menu.
         * @return {?}
         */
        NgbDropdown.prototype.toggle = /**
         * Toggles the dropdown menu.
         * @return {?}
         */
        function () {
            if (this.isOpen()) {
                this.close();
            }
            else {
                this.open();
            }
        };
        /**
         * @return {?}
         */
        NgbDropdown.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            this._resetContainer();
            this._closed$.next();
            this._zoneSubscription.unsubscribe();
        };
        /**
         * @param {?} event
         * @return {?}
         */
        NgbDropdown.prototype.onKeyDown = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            var _this = this;
            // tslint:disable-next-line:deprecation
            /** @type {?} */
            var key = event.which;
            /** @type {?} */
            var itemElements = this._getMenuElements();
            /** @type {?} */
            var position = -1;
            /** @type {?} */
            var isEventFromItems = false;
            /** @type {?} */
            var isEventFromToggle = this._isEventFromToggle(event);
            if (!isEventFromToggle && itemElements.length) {
                itemElements.forEach((/**
                 * @param {?} itemElement
                 * @param {?} index
                 * @return {?}
                 */
                function (itemElement, index) {
                    if (itemElement.contains((/** @type {?} */ (event.target)))) {
                        isEventFromItems = true;
                    }
                    if (itemElement === _this._document.activeElement) {
                        position = index;
                    }
                }));
            }
            // closing on Enter / Space
            if (key === Key.Space || key === Key.Enter) {
                if (isEventFromItems && (this.autoClose === true || this.autoClose === 'inside')) {
                    this.close();
                }
                return;
            }
            // opening / navigating
            if (isEventFromToggle || isEventFromItems) {
                this.open();
                if (itemElements.length) {
                    switch (key) {
                        case Key.ArrowDown:
                            position = Math.min(position + 1, itemElements.length - 1);
                            break;
                        case Key.ArrowUp:
                            if (this._isDropup() && position === -1) {
                                position = itemElements.length - 1;
                                break;
                            }
                            position = Math.max(position - 1, 0);
                            break;
                        case Key.Home:
                            position = 0;
                            break;
                        case Key.End:
                            position = itemElements.length - 1;
                            break;
                    }
                    itemElements[position].focus();
                }
                event.preventDefault();
            }
        };
        /**
         * @private
         * @return {?}
         */
        NgbDropdown.prototype._isDropup = /**
         * @private
         * @return {?}
         */
        function () { return this._elementRef.nativeElement.classList.contains('dropup'); };
        /**
         * @private
         * @param {?} event
         * @return {?}
         */
        NgbDropdown.prototype._isEventFromToggle = /**
         * @private
         * @param {?} event
         * @return {?}
         */
        function (event) {
            return this._anchor.getNativeElement().contains((/** @type {?} */ (event.target)));
        };
        /**
         * @private
         * @return {?}
         */
        NgbDropdown.prototype._getMenuElements = /**
         * @private
         * @return {?}
         */
        function () {
            /** @type {?} */
            var menu = this._menu;
            if (menu == null) {
                return [];
            }
            return menu.menuItems.filter((/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return !item.disabled; })).map((/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return item.elementRef.nativeElement; }));
        };
        /**
         * @private
         * @return {?}
         */
        NgbDropdown.prototype._positionMenu = /**
         * @private
         * @return {?}
         */
        function () {
            /** @type {?} */
            var menu = this._menu;
            if (this.isOpen() && menu) {
                this._applyPlacementClasses(this.display === 'dynamic' ?
                    positionElements(this._anchor.anchorEl, this._bodyContainer || this._menuElement.nativeElement, this.placement, this.container === 'body') :
                    this._getFirstPlacement(this.placement));
            }
        };
        /**
         * @private
         * @param {?} placement
         * @return {?}
         */
        NgbDropdown.prototype._getFirstPlacement = /**
         * @private
         * @param {?} placement
         * @return {?}
         */
        function (placement) {
            return Array.isArray(placement) ? placement[0] : (/** @type {?} */ (placement.split(' ')[0]));
        };
        /**
         * @private
         * @return {?}
         */
        NgbDropdown.prototype._resetContainer = /**
         * @private
         * @return {?}
         */
        function () {
            /** @type {?} */
            var renderer = this._renderer;
            /** @type {?} */
            var menuElement = this._menuElement;
            if (menuElement) {
                /** @type {?} */
                var dropdownElement = this._elementRef.nativeElement;
                /** @type {?} */
                var dropdownMenuElement = menuElement.nativeElement;
                renderer.appendChild(dropdownElement, dropdownMenuElement);
                renderer.removeStyle(dropdownMenuElement, 'position');
                renderer.removeStyle(dropdownMenuElement, 'transform');
            }
            if (this._bodyContainer) {
                renderer.removeChild(this._document.body, this._bodyContainer);
                this._bodyContainer = null;
            }
        };
        /**
         * @private
         * @param {?=} container
         * @return {?}
         */
        NgbDropdown.prototype._applyContainer = /**
         * @private
         * @param {?=} container
         * @return {?}
         */
        function (container) {
            if (container === void 0) { container = null; }
            this._resetContainer();
            if (container === 'body') {
                /** @type {?} */
                var renderer = this._renderer;
                /** @type {?} */
                var dropdownMenuElement = this._menuElement.nativeElement;
                /** @type {?} */
                var bodyContainer = this._bodyContainer = this._bodyContainer || renderer.createElement('div');
                // Override some styles to have the positionning working
                renderer.setStyle(bodyContainer, 'position', 'absolute');
                renderer.setStyle(dropdownMenuElement, 'position', 'static');
                renderer.setStyle(bodyContainer, 'z-index', '1050');
                renderer.appendChild(bodyContainer, dropdownMenuElement);
                renderer.appendChild(this._document.body, bodyContainer);
            }
        };
        /**
         * @private
         * @param {?=} placement
         * @return {?}
         */
        NgbDropdown.prototype._applyPlacementClasses = /**
         * @private
         * @param {?=} placement
         * @return {?}
         */
        function (placement) {
            /** @type {?} */
            var menu = this._menu;
            if (menu) {
                if (!placement) {
                    placement = this._getFirstPlacement(this.placement);
                }
                /** @type {?} */
                var renderer = this._renderer;
                /** @type {?} */
                var dropdownElement = this._elementRef.nativeElement;
                // remove the current placement classes
                renderer.removeClass(dropdownElement, 'dropup');
                renderer.removeClass(dropdownElement, 'dropdown');
                menu.placement = this.display === 'static' ? null : placement;
                /*
                      * apply the new placement
                      * in case of top use up-arrow or down-arrow otherwise
                      */
                /** @type {?} */
                var dropdownClass = placement.search('^top') !== -1 ? 'dropup' : 'dropdown';
                renderer.addClass(dropdownElement, dropdownClass);
                /** @type {?} */
                var bodyContainer = this._bodyContainer;
                if (bodyContainer) {
                    renderer.removeClass(bodyContainer, 'dropup');
                    renderer.removeClass(bodyContainer, 'dropdown');
                    renderer.addClass(bodyContainer, dropdownClass);
                }
            }
        };
        NgbDropdown.decorators = [
            { type: core.Directive, args: [{ selector: '[ngbDropdown]', exportAs: 'ngbDropdown', host: { '[class.show]': 'isOpen()' } },] }
        ];
        /** @nocollapse */
        NgbDropdown.ctorParameters = function () { return [
            { type: core.ChangeDetectorRef },
            { type: NgbDropdownConfig },
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
            { type: core.NgZone },
            { type: core.ElementRef },
            { type: core.Renderer2 },
            { type: NgbNavbar, decorators: [{ type: core.Optional }] }
        ]; };
        NgbDropdown.propDecorators = {
            _menu: [{ type: core.ContentChild, args: [NgbDropdownMenu, { static: false },] }],
            _menuElement: [{ type: core.ContentChild, args: [NgbDropdownMenu, { read: core.ElementRef, static: false },] }],
            _anchor: [{ type: core.ContentChild, args: [NgbDropdownAnchor, { static: false },] }],
            autoClose: [{ type: core.Input }],
            _open: [{ type: core.Input, args: ['open',] }],
            placement: [{ type: core.Input }],
            container: [{ type: core.Input }],
            display: [{ type: core.Input }],
            openChange: [{ type: core.Output }]
        };
        return NgbDropdown;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NGB_DROPDOWN_DIRECTIVES = [NgbDropdown, NgbDropdownAnchor, NgbDropdownToggle, NgbDropdownMenu, NgbDropdownItem, NgbNavbar];
    var NgbDropdownModule = /** @class */ (function () {
        function NgbDropdownModule() {
        }
        NgbDropdownModule.decorators = [
            { type: core.NgModule, args: [{ declarations: NGB_DROPDOWN_DIRECTIVES, exports: NGB_DROPDOWN_DIRECTIVES },] }
        ];
        return NgbDropdownModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A configuration service for the [`NgbModal`](#/components/modal/api#NgbModal) service.
     *
     * You can inject this service, typically in your root component, and customize the values of its properties in
     * order to provide default values for all modals used in the application.
     *
     * \@since 3.1.0
     */
    var NgbModalConfig = /** @class */ (function () {
        function NgbModalConfig() {
            this.backdrop = true;
            this.keyboard = true;
        }
        NgbModalConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbModalConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbModalConfig_Factory() { return new NgbModalConfig(); }, token: NgbModalConfig, providedIn: "root" });
        return NgbModalConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ContentRef = /** @class */ (function () {
        function ContentRef(nodes, viewRef, componentRef) {
            this.nodes = nodes;
            this.viewRef = viewRef;
            this.componentRef = componentRef;
        }
        return ContentRef;
    }());
    /**
     * @template T
     */
    var /**
     * @template T
     */
    PopupService = /** @class */ (function () {
        function PopupService(_type, _injector, _viewContainerRef, _renderer, _componentFactoryResolver, _applicationRef) {
            this._type = _type;
            this._injector = _injector;
            this._viewContainerRef = _viewContainerRef;
            this._renderer = _renderer;
            this._componentFactoryResolver = _componentFactoryResolver;
            this._applicationRef = _applicationRef;
        }
        /**
         * @param {?=} content
         * @param {?=} context
         * @return {?}
         */
        PopupService.prototype.open = /**
         * @param {?=} content
         * @param {?=} context
         * @return {?}
         */
        function (content, context) {
            if (!this._windowRef) {
                this._contentRef = this._getContentRef(content, context);
                this._windowRef = this._viewContainerRef.createComponent(this._componentFactoryResolver.resolveComponentFactory(this._type), 0, this._injector, this._contentRef.nodes);
            }
            return this._windowRef;
        };
        /**
         * @return {?}
         */
        PopupService.prototype.close = /**
         * @return {?}
         */
        function () {
            if (this._windowRef) {
                this._viewContainerRef.remove(this._viewContainerRef.indexOf(this._windowRef.hostView));
                this._windowRef = null;
                if (this._contentRef.viewRef) {
                    this._applicationRef.detachView(this._contentRef.viewRef);
                    this._contentRef.viewRef.destroy();
                    this._contentRef = null;
                }
            }
        };
        /**
         * @private
         * @param {?} content
         * @param {?=} context
         * @return {?}
         */
        PopupService.prototype._getContentRef = /**
         * @private
         * @param {?} content
         * @param {?=} context
         * @return {?}
         */
        function (content, context) {
            if (!content) {
                return new ContentRef([]);
            }
            else if (content instanceof core.TemplateRef) {
                /** @type {?} */
                var viewRef = content.createEmbeddedView(context);
                this._applicationRef.attachView(viewRef);
                return new ContentRef([viewRef.rootNodes], viewRef);
            }
            else {
                return new ContentRef([[this._renderer.createText("" + content)]]);
            }
        };
        return PopupService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var noop = (/**
     * @return {?}
     */
    function () { });
    /**
     * Utility to handle the scrollbar.
     *
     * It allows to compensate the lack of a vertical scrollbar by adding an
     * equivalent padding on the right of the body, and to remove this compensation.
     */
    var ScrollBar = /** @class */ (function () {
        function ScrollBar(_document) {
            this._document = _document;
        }
        /**
         * Detects if a scrollbar is present and if yes, already compensates for its
         * removal by adding an equivalent padding on the right of the body.
         *
         * @return a callback used to revert the compensation (noop if there was none,
         * otherwise a function removing the padding)
         */
        /**
         * Detects if a scrollbar is present and if yes, already compensates for its
         * removal by adding an equivalent padding on the right of the body.
         *
         * @return {?} a callback used to revert the compensation (noop if there was none,
         * otherwise a function removing the padding)
         */
        ScrollBar.prototype.compensate = /**
         * Detects if a scrollbar is present and if yes, already compensates for its
         * removal by adding an equivalent padding on the right of the body.
         *
         * @return {?} a callback used to revert the compensation (noop if there was none,
         * otherwise a function removing the padding)
         */
        function () { return !this._isPresent() ? noop : this._adjustBody(this._getWidth()); };
        /**
         * Adds a padding of the given width on the right of the body.
         *
         * @return a callback used to revert the padding to its previous value
         */
        /**
         * Adds a padding of the given width on the right of the body.
         *
         * @private
         * @param {?} width
         * @return {?} a callback used to revert the padding to its previous value
         */
        ScrollBar.prototype._adjustBody = /**
         * Adds a padding of the given width on the right of the body.
         *
         * @private
         * @param {?} width
         * @return {?} a callback used to revert the padding to its previous value
         */
        function (width) {
            /** @type {?} */
            var body = this._document.body;
            /** @type {?} */
            var userSetPadding = body.style.paddingRight;
            /** @type {?} */
            var paddingAmount = parseFloat(window.getComputedStyle(body)['padding-right']);
            body.style['padding-right'] = paddingAmount + width + "px";
            return (/**
             * @return {?}
             */
            function () { return body.style['padding-right'] = userSetPadding; });
        };
        /**
         * Tells whether a scrollbar is currently present on the body.
         *
         * @return true if scrollbar is present, false otherwise
         */
        /**
         * Tells whether a scrollbar is currently present on the body.
         *
         * @private
         * @return {?} true if scrollbar is present, false otherwise
         */
        ScrollBar.prototype._isPresent = /**
         * Tells whether a scrollbar is currently present on the body.
         *
         * @private
         * @return {?} true if scrollbar is present, false otherwise
         */
        function () {
            /** @type {?} */
            var rect = this._document.body.getBoundingClientRect();
            return rect.left + rect.right < window.innerWidth;
        };
        /**
         * Calculates and returns the width of a scrollbar.
         *
         * @return the width of a scrollbar on this page
         */
        /**
         * Calculates and returns the width of a scrollbar.
         *
         * @private
         * @return {?} the width of a scrollbar on this page
         */
        ScrollBar.prototype._getWidth = /**
         * Calculates and returns the width of a scrollbar.
         *
         * @private
         * @return {?} the width of a scrollbar on this page
         */
        function () {
            /** @type {?} */
            var measurer = this._document.createElement('div');
            measurer.className = 'modal-scrollbar-measure';
            /** @type {?} */
            var body = this._document.body;
            body.appendChild(measurer);
            /** @type {?} */
            var width = measurer.getBoundingClientRect().width - measurer.clientWidth;
            body.removeChild(measurer);
            return width;
        };
        ScrollBar.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */
        ScrollBar.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] }
        ]; };
        /** @nocollapse */ ScrollBar.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function ScrollBar_Factory() { return new ScrollBar(core.ɵɵinject(common.DOCUMENT)); }, token: ScrollBar, providedIn: "root" });
        return ScrollBar;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbModalBackdrop = /** @class */ (function () {
        function NgbModalBackdrop() {
        }
        NgbModalBackdrop.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-modal-backdrop',
                        template: '',
                        host: { '[class]': '"modal-backdrop fade show" + (backdropClass ? " " + backdropClass : "")', 'style': 'z-index: 1050' }
                    }] }
        ];
        NgbModalBackdrop.propDecorators = {
            backdropClass: [{ type: core.Input }]
        };
        return NgbModalBackdrop;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A reference to the currently opened (active) modal.
     *
     * Instances of this class can be injected into your component passed as modal content.
     * So you can `.close()` or `.dismiss()` the modal window from your component.
     */
    var   /**
     * A reference to the currently opened (active) modal.
     *
     * Instances of this class can be injected into your component passed as modal content.
     * So you can `.close()` or `.dismiss()` the modal window from your component.
     */
    NgbActiveModal = /** @class */ (function () {
        function NgbActiveModal() {
        }
        /**
         * Closes the modal with an optional `result` value.
         *
         * The `NgbMobalRef.result` promise will be resolved with the provided value.
         */
        /**
         * Closes the modal with an optional `result` value.
         *
         * The `NgbMobalRef.result` promise will be resolved with the provided value.
         * @param {?=} result
         * @return {?}
         */
        NgbActiveModal.prototype.close = /**
         * Closes the modal with an optional `result` value.
         *
         * The `NgbMobalRef.result` promise will be resolved with the provided value.
         * @param {?=} result
         * @return {?}
         */
        function (result) { };
        /**
         * Dismisses the modal with an optional `reason` value.
         *
         * The `NgbModalRef.result` promise will be rejected with the provided value.
         */
        /**
         * Dismisses the modal with an optional `reason` value.
         *
         * The `NgbModalRef.result` promise will be rejected with the provided value.
         * @param {?=} reason
         * @return {?}
         */
        NgbActiveModal.prototype.dismiss = /**
         * Dismisses the modal with an optional `reason` value.
         *
         * The `NgbModalRef.result` promise will be rejected with the provided value.
         * @param {?=} reason
         * @return {?}
         */
        function (reason) { };
        return NgbActiveModal;
    }());
    /**
     * A reference to the newly opened modal returned by the `NgbModal.open()` method.
     */
    var   /**
     * A reference to the newly opened modal returned by the `NgbModal.open()` method.
     */
    NgbModalRef = /** @class */ (function () {
        function NgbModalRef(_windowCmptRef, _contentRef, _backdropCmptRef, _beforeDismiss) {
            var _this = this;
            this._windowCmptRef = _windowCmptRef;
            this._contentRef = _contentRef;
            this._backdropCmptRef = _backdropCmptRef;
            this._beforeDismiss = _beforeDismiss;
            _windowCmptRef.instance.dismissEvent.subscribe((/**
             * @param {?} reason
             * @return {?}
             */
            function (reason) { _this.dismiss(reason); }));
            this.result = new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                _this._resolve = resolve;
                _this._reject = reject;
            }));
            this.result.then(null, (/**
             * @return {?}
             */
            function () { }));
        }
        Object.defineProperty(NgbModalRef.prototype, "componentInstance", {
            /**
             * The instance of a component used for the modal content.
             *
             * When a `TemplateRef` is used as the content, will return `undefined`.
             */
            get: /**
             * The instance of a component used for the modal content.
             *
             * When a `TemplateRef` is used as the content, will return `undefined`.
             * @return {?}
             */
            function () {
                if (this._contentRef.componentRef) {
                    return this._contentRef.componentRef.instance;
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * Closes the modal with an optional `result` value.
         *
         * The `NgbMobalRef.result` promise will be resolved with the provided value.
         */
        /**
         * Closes the modal with an optional `result` value.
         *
         * The `NgbMobalRef.result` promise will be resolved with the provided value.
         * @param {?=} result
         * @return {?}
         */
        NgbModalRef.prototype.close = /**
         * Closes the modal with an optional `result` value.
         *
         * The `NgbMobalRef.result` promise will be resolved with the provided value.
         * @param {?=} result
         * @return {?}
         */
        function (result) {
            if (this._windowCmptRef) {
                this._resolve(result);
                this._removeModalElements();
            }
        };
        /**
         * @private
         * @param {?=} reason
         * @return {?}
         */
        NgbModalRef.prototype._dismiss = /**
         * @private
         * @param {?=} reason
         * @return {?}
         */
        function (reason) {
            this._reject(reason);
            this._removeModalElements();
        };
        /**
         * Dismisses the modal with an optional `reason` value.
         *
         * The `NgbModalRef.result` promise will be rejected with the provided value.
         */
        /**
         * Dismisses the modal with an optional `reason` value.
         *
         * The `NgbModalRef.result` promise will be rejected with the provided value.
         * @param {?=} reason
         * @return {?}
         */
        NgbModalRef.prototype.dismiss = /**
         * Dismisses the modal with an optional `reason` value.
         *
         * The `NgbModalRef.result` promise will be rejected with the provided value.
         * @param {?=} reason
         * @return {?}
         */
        function (reason) {
            var _this = this;
            if (this._windowCmptRef) {
                if (!this._beforeDismiss) {
                    this._dismiss(reason);
                }
                else {
                    /** @type {?} */
                    var dismiss = this._beforeDismiss();
                    if (dismiss && dismiss.then) {
                        dismiss.then((/**
                         * @param {?} result
                         * @return {?}
                         */
                        function (result) {
                            if (result !== false) {
                                _this._dismiss(reason);
                            }
                        }), (/**
                         * @return {?}
                         */
                        function () { }));
                    }
                    else if (dismiss !== false) {
                        this._dismiss(reason);
                    }
                }
            }
        };
        /**
         * @private
         * @return {?}
         */
        NgbModalRef.prototype._removeModalElements = /**
         * @private
         * @return {?}
         */
        function () {
            /** @type {?} */
            var windowNativeEl = this._windowCmptRef.location.nativeElement;
            windowNativeEl.parentNode.removeChild(windowNativeEl);
            this._windowCmptRef.destroy();
            if (this._backdropCmptRef) {
                /** @type {?} */
                var backdropNativeEl = this._backdropCmptRef.location.nativeElement;
                backdropNativeEl.parentNode.removeChild(backdropNativeEl);
                this._backdropCmptRef.destroy();
            }
            if (this._contentRef && this._contentRef.viewRef) {
                this._contentRef.viewRef.destroy();
            }
            this._windowCmptRef = null;
            this._backdropCmptRef = null;
            this._contentRef = null;
        };
        return NgbModalRef;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {number} */
    var ModalDismissReasons = {
        BACKDROP_CLICK: 0,
        ESC: 1,
    };
    ModalDismissReasons[ModalDismissReasons.BACKDROP_CLICK] = 'BACKDROP_CLICK';
    ModalDismissReasons[ModalDismissReasons.ESC] = 'ESC';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbModalWindow = /** @class */ (function () {
        function NgbModalWindow(_document, _elRef) {
            this._document = _document;
            this._elRef = _elRef;
            this.backdrop = true;
            this.keyboard = true;
            this.dismissEvent = new core.EventEmitter();
        }
        /**
         * @param {?} $event
         * @return {?}
         */
        NgbModalWindow.prototype.backdropClick = /**
         * @param {?} $event
         * @return {?}
         */
        function ($event) {
            if (this.backdrop === true && this._elRef.nativeElement === $event.target) {
                this.dismiss(ModalDismissReasons.BACKDROP_CLICK);
            }
        };
        /**
         * @param {?} $event
         * @return {?}
         */
        NgbModalWindow.prototype.escKey = /**
         * @param {?} $event
         * @return {?}
         */
        function ($event) {
            if (this.keyboard && !$event.defaultPrevented) {
                this.dismiss(ModalDismissReasons.ESC);
            }
        };
        /**
         * @param {?} reason
         * @return {?}
         */
        NgbModalWindow.prototype.dismiss = /**
         * @param {?} reason
         * @return {?}
         */
        function (reason) { this.dismissEvent.emit(reason); };
        /**
         * @return {?}
         */
        NgbModalWindow.prototype.ngOnInit = /**
         * @return {?}
         */
        function () { this._elWithFocus = this._document.activeElement; };
        /**
         * @return {?}
         */
        NgbModalWindow.prototype.ngAfterViewInit = /**
         * @return {?}
         */
        function () {
            if (!this._elRef.nativeElement.contains(document.activeElement)) {
                /** @type {?} */
                var autoFocusable = (/** @type {?} */ (this._elRef.nativeElement.querySelector("[ngbAutofocus]")));
                /** @type {?} */
                var firstFocusable = getFocusableBoundaryElements(this._elRef.nativeElement)[0];
                /** @type {?} */
                var elementToFocus = autoFocusable || firstFocusable || this._elRef.nativeElement;
                elementToFocus.focus();
            }
        };
        /**
         * @return {?}
         */
        NgbModalWindow.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var body = this._document.body;
            /** @type {?} */
            var elWithFocus = this._elWithFocus;
            /** @type {?} */
            var elementToFocus;
            if (elWithFocus && elWithFocus['focus'] && body.contains(elWithFocus)) {
                elementToFocus = elWithFocus;
            }
            else {
                elementToFocus = body;
            }
            elementToFocus.focus();
            this._elWithFocus = null;
        };
        NgbModalWindow.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-modal-window',
                        host: {
                            '[class]': '"modal fade show d-block" + (windowClass ? " " + windowClass : "")',
                            'role': 'dialog',
                            'tabindex': '-1',
                            '(keyup.esc)': 'escKey($event)',
                            '(click)': 'backdropClick($event)',
                            '[attr.aria-modal]': 'true',
                            '[attr.aria-labelledby]': 'ariaLabelledBy',
                        },
                        template: "\n    <div [class]=\"'modal-dialog' + (size ? ' modal-' + size : '') + (centered ? ' modal-dialog-centered' : '') +\n     (scrollable ? ' modal-dialog-scrollable' : '')\" role=\"document\">\n        <div class=\"modal-content\"><ng-content></ng-content></div>\n    </div>\n    ",
                        encapsulation: core.ViewEncapsulation.None,
                        styles: ["ngb-modal-window .component-host-scrollable{display:-ms-flexbox;display:flex;-ms-flex-direction:column;flex-direction:column;overflow:hidden}"]
                    }] }
        ];
        /** @nocollapse */
        NgbModalWindow.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
            { type: core.ElementRef }
        ]; };
        NgbModalWindow.propDecorators = {
            ariaLabelledBy: [{ type: core.Input }],
            backdrop: [{ type: core.Input }],
            centered: [{ type: core.Input }],
            keyboard: [{ type: core.Input }],
            scrollable: [{ type: core.Input }],
            size: [{ type: core.Input }],
            windowClass: [{ type: core.Input }],
            dismissEvent: [{ type: core.Output, args: ['dismiss',] }]
        };
        return NgbModalWindow;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbModalStack = /** @class */ (function () {
        function NgbModalStack(_applicationRef, _injector, _document, _scrollBar, _rendererFactory) {
            var _this = this;
            this._applicationRef = _applicationRef;
            this._injector = _injector;
            this._document = _document;
            this._scrollBar = _scrollBar;
            this._rendererFactory = _rendererFactory;
            this._activeWindowCmptHasChanged = new rxjs.Subject();
            this._ariaHiddenValues = new Map();
            this._backdropAttributes = ['backdropClass'];
            this._modalRefs = [];
            this._windowAttributes = ['ariaLabelledBy', 'backdrop', 'centered', 'keyboard', 'scrollable', 'size', 'windowClass'];
            this._windowCmpts = [];
            // Trap focus on active WindowCmpt
            this._activeWindowCmptHasChanged.subscribe((/**
             * @return {?}
             */
            function () {
                if (_this._windowCmpts.length) {
                    /** @type {?} */
                    var activeWindowCmpt = _this._windowCmpts[_this._windowCmpts.length - 1];
                    ngbFocusTrap(activeWindowCmpt.location.nativeElement, _this._activeWindowCmptHasChanged);
                    _this._revertAriaHidden();
                    _this._setAriaHidden(activeWindowCmpt.location.nativeElement);
                }
            }));
        }
        /**
         * @param {?} moduleCFR
         * @param {?} contentInjector
         * @param {?} content
         * @param {?} options
         * @return {?}
         */
        NgbModalStack.prototype.open = /**
         * @param {?} moduleCFR
         * @param {?} contentInjector
         * @param {?} content
         * @param {?} options
         * @return {?}
         */
        function (moduleCFR, contentInjector, content, options) {
            var _this = this;
            /** @type {?} */
            var containerEl = isDefined(options.container) ? this._document.querySelector(options.container) : this._document.body;
            /** @type {?} */
            var renderer = this._rendererFactory.createRenderer(null, null);
            /** @type {?} */
            var revertPaddingForScrollBar = this._scrollBar.compensate();
            /** @type {?} */
            var removeBodyClass = (/**
             * @return {?}
             */
            function () {
                if (!_this._modalRefs.length) {
                    renderer.removeClass(_this._document.body, 'modal-open');
                    _this._revertAriaHidden();
                }
            });
            if (!containerEl) {
                throw new Error("The specified modal container \"" + (options.container || 'body') + "\" was not found in the DOM.");
            }
            /** @type {?} */
            var activeModal = new NgbActiveModal();
            /** @type {?} */
            var contentRef = this._getContentRef(moduleCFR, options.injector || contentInjector, content, activeModal, options);
            /** @type {?} */
            var backdropCmptRef = options.backdrop !== false ? this._attachBackdrop(moduleCFR, containerEl) : null;
            /** @type {?} */
            var windowCmptRef = this._attachWindowComponent(moduleCFR, containerEl, contentRef);
            /** @type {?} */
            var ngbModalRef = new NgbModalRef(windowCmptRef, contentRef, backdropCmptRef, options.beforeDismiss);
            this._registerModalRef(ngbModalRef);
            this._registerWindowCmpt(windowCmptRef);
            ngbModalRef.result.then(revertPaddingForScrollBar, revertPaddingForScrollBar);
            ngbModalRef.result.then(removeBodyClass, removeBodyClass);
            activeModal.close = (/**
             * @param {?} result
             * @return {?}
             */
            function (result) { ngbModalRef.close(result); });
            activeModal.dismiss = (/**
             * @param {?} reason
             * @return {?}
             */
            function (reason) { ngbModalRef.dismiss(reason); });
            this._applyWindowOptions(windowCmptRef.instance, options);
            if (this._modalRefs.length === 1) {
                renderer.addClass(this._document.body, 'modal-open');
            }
            if (backdropCmptRef && backdropCmptRef.instance) {
                this._applyBackdropOptions(backdropCmptRef.instance, options);
            }
            return ngbModalRef;
        };
        /**
         * @param {?=} reason
         * @return {?}
         */
        NgbModalStack.prototype.dismissAll = /**
         * @param {?=} reason
         * @return {?}
         */
        function (reason) { this._modalRefs.forEach((/**
         * @param {?} ngbModalRef
         * @return {?}
         */
        function (ngbModalRef) { return ngbModalRef.dismiss(reason); })); };
        /**
         * @return {?}
         */
        NgbModalStack.prototype.hasOpenModals = /**
         * @return {?}
         */
        function () { return this._modalRefs.length > 0; };
        /**
         * @private
         * @param {?} moduleCFR
         * @param {?} containerEl
         * @return {?}
         */
        NgbModalStack.prototype._attachBackdrop = /**
         * @private
         * @param {?} moduleCFR
         * @param {?} containerEl
         * @return {?}
         */
        function (moduleCFR, containerEl) {
            /** @type {?} */
            var backdropFactory = moduleCFR.resolveComponentFactory(NgbModalBackdrop);
            /** @type {?} */
            var backdropCmptRef = backdropFactory.create(this._injector);
            this._applicationRef.attachView(backdropCmptRef.hostView);
            containerEl.appendChild(backdropCmptRef.location.nativeElement);
            return backdropCmptRef;
        };
        /**
         * @private
         * @param {?} moduleCFR
         * @param {?} containerEl
         * @param {?} contentRef
         * @return {?}
         */
        NgbModalStack.prototype._attachWindowComponent = /**
         * @private
         * @param {?} moduleCFR
         * @param {?} containerEl
         * @param {?} contentRef
         * @return {?}
         */
        function (moduleCFR, containerEl, contentRef) {
            /** @type {?} */
            var windowFactory = moduleCFR.resolveComponentFactory(NgbModalWindow);
            /** @type {?} */
            var windowCmptRef = windowFactory.create(this._injector, contentRef.nodes);
            this._applicationRef.attachView(windowCmptRef.hostView);
            containerEl.appendChild(windowCmptRef.location.nativeElement);
            return windowCmptRef;
        };
        /**
         * @private
         * @param {?} windowInstance
         * @param {?} options
         * @return {?}
         */
        NgbModalStack.prototype._applyWindowOptions = /**
         * @private
         * @param {?} windowInstance
         * @param {?} options
         * @return {?}
         */
        function (windowInstance, options) {
            this._windowAttributes.forEach((/**
             * @param {?} optionName
             * @return {?}
             */
            function (optionName) {
                if (isDefined(options[optionName])) {
                    windowInstance[optionName] = options[optionName];
                }
            }));
        };
        /**
         * @private
         * @param {?} backdropInstance
         * @param {?} options
         * @return {?}
         */
        NgbModalStack.prototype._applyBackdropOptions = /**
         * @private
         * @param {?} backdropInstance
         * @param {?} options
         * @return {?}
         */
        function (backdropInstance, options) {
            this._backdropAttributes.forEach((/**
             * @param {?} optionName
             * @return {?}
             */
            function (optionName) {
                if (isDefined(options[optionName])) {
                    backdropInstance[optionName] = options[optionName];
                }
            }));
        };
        /**
         * @private
         * @param {?} moduleCFR
         * @param {?} contentInjector
         * @param {?} content
         * @param {?} activeModal
         * @param {?} options
         * @return {?}
         */
        NgbModalStack.prototype._getContentRef = /**
         * @private
         * @param {?} moduleCFR
         * @param {?} contentInjector
         * @param {?} content
         * @param {?} activeModal
         * @param {?} options
         * @return {?}
         */
        function (moduleCFR, contentInjector, content, activeModal, options) {
            if (!content) {
                return new ContentRef([]);
            }
            else if (content instanceof core.TemplateRef) {
                return this._createFromTemplateRef(content, activeModal);
            }
            else if (isString(content)) {
                return this._createFromString(content);
            }
            else {
                return this._createFromComponent(moduleCFR, contentInjector, content, activeModal, options);
            }
        };
        /**
         * @private
         * @param {?} content
         * @param {?} activeModal
         * @return {?}
         */
        NgbModalStack.prototype._createFromTemplateRef = /**
         * @private
         * @param {?} content
         * @param {?} activeModal
         * @return {?}
         */
        function (content, activeModal) {
            /** @type {?} */
            var context = {
                $implicit: activeModal,
                close: /**
                 * @param {?} result
                 * @return {?}
                 */
                function (result) { activeModal.close(result); },
                dismiss: /**
                 * @param {?} reason
                 * @return {?}
                 */
                function (reason) { activeModal.dismiss(reason); }
            };
            /** @type {?} */
            var viewRef = content.createEmbeddedView(context);
            this._applicationRef.attachView(viewRef);
            return new ContentRef([viewRef.rootNodes], viewRef);
        };
        /**
         * @private
         * @param {?} content
         * @return {?}
         */
        NgbModalStack.prototype._createFromString = /**
         * @private
         * @param {?} content
         * @return {?}
         */
        function (content) {
            /** @type {?} */
            var component = this._document.createTextNode("" + content);
            return new ContentRef([[component]]);
        };
        /**
         * @private
         * @param {?} moduleCFR
         * @param {?} contentInjector
         * @param {?} content
         * @param {?} context
         * @param {?} options
         * @return {?}
         */
        NgbModalStack.prototype._createFromComponent = /**
         * @private
         * @param {?} moduleCFR
         * @param {?} contentInjector
         * @param {?} content
         * @param {?} context
         * @param {?} options
         * @return {?}
         */
        function (moduleCFR, contentInjector, content, context, options) {
            /** @type {?} */
            var contentCmptFactory = moduleCFR.resolveComponentFactory(content);
            /** @type {?} */
            var modalContentInjector = core.Injector.create({ providers: [{ provide: NgbActiveModal, useValue: context }], parent: contentInjector });
            /** @type {?} */
            var componentRef = contentCmptFactory.create(modalContentInjector);
            /** @type {?} */
            var componentNativeEl = componentRef.location.nativeElement;
            if (options.scrollable) {
                ((/** @type {?} */ (componentNativeEl))).classList.add('component-host-scrollable');
            }
            this._applicationRef.attachView(componentRef.hostView);
            // FIXME: we should here get rid of the component nativeElement
            // and use `[Array.from(componentNativeEl.childNodes)]` instead and remove the above CSS class.
            return new ContentRef([[componentNativeEl]], componentRef.hostView, componentRef);
        };
        /**
         * @private
         * @param {?} element
         * @return {?}
         */
        NgbModalStack.prototype._setAriaHidden = /**
         * @private
         * @param {?} element
         * @return {?}
         */
        function (element) {
            var _this = this;
            /** @type {?} */
            var parent = element.parentElement;
            if (parent && element !== this._document.body) {
                Array.from(parent.children).forEach((/**
                 * @param {?} sibling
                 * @return {?}
                 */
                function (sibling) {
                    if (sibling !== element && sibling.nodeName !== 'SCRIPT') {
                        _this._ariaHiddenValues.set(sibling, sibling.getAttribute('aria-hidden'));
                        sibling.setAttribute('aria-hidden', 'true');
                    }
                }));
                this._setAriaHidden(parent);
            }
        };
        /**
         * @private
         * @return {?}
         */
        NgbModalStack.prototype._revertAriaHidden = /**
         * @private
         * @return {?}
         */
        function () {
            this._ariaHiddenValues.forEach((/**
             * @param {?} value
             * @param {?} element
             * @return {?}
             */
            function (value, element) {
                if (value) {
                    element.setAttribute('aria-hidden', value);
                }
                else {
                    element.removeAttribute('aria-hidden');
                }
            }));
            this._ariaHiddenValues.clear();
        };
        /**
         * @private
         * @param {?} ngbModalRef
         * @return {?}
         */
        NgbModalStack.prototype._registerModalRef = /**
         * @private
         * @param {?} ngbModalRef
         * @return {?}
         */
        function (ngbModalRef) {
            var _this = this;
            /** @type {?} */
            var unregisterModalRef = (/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var index = _this._modalRefs.indexOf(ngbModalRef);
                if (index > -1) {
                    _this._modalRefs.splice(index, 1);
                }
            });
            this._modalRefs.push(ngbModalRef);
            ngbModalRef.result.then(unregisterModalRef, unregisterModalRef);
        };
        /**
         * @private
         * @param {?} ngbWindowCmpt
         * @return {?}
         */
        NgbModalStack.prototype._registerWindowCmpt = /**
         * @private
         * @param {?} ngbWindowCmpt
         * @return {?}
         */
        function (ngbWindowCmpt) {
            var _this = this;
            this._windowCmpts.push(ngbWindowCmpt);
            this._activeWindowCmptHasChanged.next();
            ngbWindowCmpt.onDestroy((/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var index = _this._windowCmpts.indexOf(ngbWindowCmpt);
                if (index > -1) {
                    _this._windowCmpts.splice(index, 1);
                    _this._activeWindowCmptHasChanged.next();
                }
            }));
        };
        NgbModalStack.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */
        NgbModalStack.ctorParameters = function () { return [
            { type: core.ApplicationRef },
            { type: core.Injector },
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
            { type: ScrollBar },
            { type: core.RendererFactory2 }
        ]; };
        /** @nocollapse */ NgbModalStack.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbModalStack_Factory() { return new NgbModalStack(core.ɵɵinject(core.ApplicationRef), core.ɵɵinject(core.INJECTOR), core.ɵɵinject(common.DOCUMENT), core.ɵɵinject(ScrollBar), core.ɵɵinject(core.RendererFactory2)); }, token: NgbModalStack, providedIn: "root" });
        return NgbModalStack;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A service for opening modal windows.
     *
     * Creating a modal is straightforward: create a component or a template and pass it as an argument to
     * the `.open()` method.
     */
    var NgbModal = /** @class */ (function () {
        function NgbModal(_moduleCFR, _injector, _modalStack, _config) {
            this._moduleCFR = _moduleCFR;
            this._injector = _injector;
            this._modalStack = _modalStack;
            this._config = _config;
        }
        /**
         * Opens a new modal window with the specified content and supplied options.
         *
         * Content can be provided as a `TemplateRef` or a component type. If you pass a component type as content,
         * then instances of those components can be injected with an instance of the `NgbActiveModal` class. You can then
         * use `NgbActiveModal` methods to close / dismiss modals from "inside" of your component.
         *
         * Also see the [`NgbModalOptions`](#/components/modal/api#NgbModalOptions) for the list of supported options.
         */
        /**
         * Opens a new modal window with the specified content and supplied options.
         *
         * Content can be provided as a `TemplateRef` or a component type. If you pass a component type as content,
         * then instances of those components can be injected with an instance of the `NgbActiveModal` class. You can then
         * use `NgbActiveModal` methods to close / dismiss modals from "inside" of your component.
         *
         * Also see the [`NgbModalOptions`](#/components/modal/api#NgbModalOptions) for the list of supported options.
         * @param {?} content
         * @param {?=} options
         * @return {?}
         */
        NgbModal.prototype.open = /**
         * Opens a new modal window with the specified content and supplied options.
         *
         * Content can be provided as a `TemplateRef` or a component type. If you pass a component type as content,
         * then instances of those components can be injected with an instance of the `NgbActiveModal` class. You can then
         * use `NgbActiveModal` methods to close / dismiss modals from "inside" of your component.
         *
         * Also see the [`NgbModalOptions`](#/components/modal/api#NgbModalOptions) for the list of supported options.
         * @param {?} content
         * @param {?=} options
         * @return {?}
         */
        function (content, options) {
            if (options === void 0) { options = {}; }
            /** @type {?} */
            var combinedOptions = Object.assign({}, this._config, options);
            return this._modalStack.open(this._moduleCFR, this._injector, content, combinedOptions);
        };
        /**
         * Dismisses all currently displayed modal windows with the supplied reason.
         *
         * @since 3.1.0
         */
        /**
         * Dismisses all currently displayed modal windows with the supplied reason.
         *
         * \@since 3.1.0
         * @param {?=} reason
         * @return {?}
         */
        NgbModal.prototype.dismissAll = /**
         * Dismisses all currently displayed modal windows with the supplied reason.
         *
         * \@since 3.1.0
         * @param {?=} reason
         * @return {?}
         */
        function (reason) { this._modalStack.dismissAll(reason); };
        /**
         * Indicates if there are currently any open modal windows in the application.
         *
         * @since 3.3.0
         */
        /**
         * Indicates if there are currently any open modal windows in the application.
         *
         * \@since 3.3.0
         * @return {?}
         */
        NgbModal.prototype.hasOpenModals = /**
         * Indicates if there are currently any open modal windows in the application.
         *
         * \@since 3.3.0
         * @return {?}
         */
        function () { return this._modalStack.hasOpenModals(); };
        NgbModal.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */
        NgbModal.ctorParameters = function () { return [
            { type: core.ComponentFactoryResolver },
            { type: core.Injector },
            { type: NgbModalStack },
            { type: NgbModalConfig }
        ]; };
        /** @nocollapse */ NgbModal.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbModal_Factory() { return new NgbModal(core.ɵɵinject(core.ComponentFactoryResolver), core.ɵɵinject(core.INJECTOR), core.ɵɵinject(NgbModalStack), core.ɵɵinject(NgbModalConfig)); }, token: NgbModal, providedIn: "root" });
        return NgbModal;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbModalModule = /** @class */ (function () {
        function NgbModalModule() {
        }
        NgbModalModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [NgbModalBackdrop, NgbModalWindow],
                        entryComponents: [NgbModalBackdrop, NgbModalWindow],
                        providers: [NgbModal]
                    },] }
        ];
        return NgbModalModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A configuration service for the [`NgbPagination`](#/components/pagination/api#NgbPagination) component.
     *
     * You can inject this service, typically in your root component, and customize the values of its properties in
     * order to provide default values for all the paginations used in the application.
     */
    var NgbPaginationConfig = /** @class */ (function () {
        function NgbPaginationConfig() {
            this.disabled = false;
            this.boundaryLinks = false;
            this.directionLinks = true;
            this.ellipses = true;
            this.maxSize = 0;
            this.pageSize = 10;
            this.rotate = false;
        }
        NgbPaginationConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbPaginationConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbPaginationConfig_Factory() { return new NgbPaginationConfig(); }, token: NgbPaginationConfig, providedIn: "root" });
        return NgbPaginationConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A directive to match the 'ellipsis' link template
     *
     * \@since 4.1.0
     */
    var NgbPaginationEllipsis = /** @class */ (function () {
        function NgbPaginationEllipsis(templateRef) {
            this.templateRef = templateRef;
        }
        NgbPaginationEllipsis.decorators = [
            { type: core.Directive, args: [{ selector: 'ng-template[ngbPaginationEllipsis]' },] }
        ];
        /** @nocollapse */
        NgbPaginationEllipsis.ctorParameters = function () { return [
            { type: core.TemplateRef }
        ]; };
        return NgbPaginationEllipsis;
    }());
    /**
     * A directive to match the 'first' link template
     *
     * \@since 4.1.0
     */
    var NgbPaginationFirst = /** @class */ (function () {
        function NgbPaginationFirst(templateRef) {
            this.templateRef = templateRef;
        }
        NgbPaginationFirst.decorators = [
            { type: core.Directive, args: [{ selector: 'ng-template[ngbPaginationFirst]' },] }
        ];
        /** @nocollapse */
        NgbPaginationFirst.ctorParameters = function () { return [
            { type: core.TemplateRef }
        ]; };
        return NgbPaginationFirst;
    }());
    /**
     * A directive to match the 'last' link template
     *
     * \@since 4.1.0
     */
    var NgbPaginationLast = /** @class */ (function () {
        function NgbPaginationLast(templateRef) {
            this.templateRef = templateRef;
        }
        NgbPaginationLast.decorators = [
            { type: core.Directive, args: [{ selector: 'ng-template[ngbPaginationLast]' },] }
        ];
        /** @nocollapse */
        NgbPaginationLast.ctorParameters = function () { return [
            { type: core.TemplateRef }
        ]; };
        return NgbPaginationLast;
    }());
    /**
     * A directive to match the 'next' link template
     *
     * \@since 4.1.0
     */
    var NgbPaginationNext = /** @class */ (function () {
        function NgbPaginationNext(templateRef) {
            this.templateRef = templateRef;
        }
        NgbPaginationNext.decorators = [
            { type: core.Directive, args: [{ selector: 'ng-template[ngbPaginationNext]' },] }
        ];
        /** @nocollapse */
        NgbPaginationNext.ctorParameters = function () { return [
            { type: core.TemplateRef }
        ]; };
        return NgbPaginationNext;
    }());
    /**
     * A directive to match the page 'number' link template
     *
     * \@since 4.1.0
     */
    var NgbPaginationNumber = /** @class */ (function () {
        function NgbPaginationNumber(templateRef) {
            this.templateRef = templateRef;
        }
        NgbPaginationNumber.decorators = [
            { type: core.Directive, args: [{ selector: 'ng-template[ngbPaginationNumber]' },] }
        ];
        /** @nocollapse */
        NgbPaginationNumber.ctorParameters = function () { return [
            { type: core.TemplateRef }
        ]; };
        return NgbPaginationNumber;
    }());
    /**
     * A directive to match the 'previous' link template
     *
     * \@since 4.1.0
     */
    var NgbPaginationPrevious = /** @class */ (function () {
        function NgbPaginationPrevious(templateRef) {
            this.templateRef = templateRef;
        }
        NgbPaginationPrevious.decorators = [
            { type: core.Directive, args: [{ selector: 'ng-template[ngbPaginationPrevious]' },] }
        ];
        /** @nocollapse */
        NgbPaginationPrevious.ctorParameters = function () { return [
            { type: core.TemplateRef }
        ]; };
        return NgbPaginationPrevious;
    }());
    /**
     * A component that displays page numbers and allows to customize them in several ways.
     */
    var NgbPagination = /** @class */ (function () {
        function NgbPagination(config) {
            this.pageCount = 0;
            this.pages = [];
            /**
             *  The current page.
             *
             *  Page numbers start with `1`.
             */
            this.page = 1;
            /**
             *  An event fired when the page is changed. Will fire only if collection size is set and all values are valid.
             *
             *  Event payload is the number of the newly selected page.
             *
             *  Page numbers start with `1`.
             */
            this.pageChange = new core.EventEmitter(true);
            this.disabled = config.disabled;
            this.boundaryLinks = config.boundaryLinks;
            this.directionLinks = config.directionLinks;
            this.ellipses = config.ellipses;
            this.maxSize = config.maxSize;
            this.pageSize = config.pageSize;
            this.rotate = config.rotate;
            this.size = config.size;
        }
        /**
         * @return {?}
         */
        NgbPagination.prototype.hasPrevious = /**
         * @return {?}
         */
        function () { return this.page > 1; };
        /**
         * @return {?}
         */
        NgbPagination.prototype.hasNext = /**
         * @return {?}
         */
        function () { return this.page < this.pageCount; };
        /**
         * @return {?}
         */
        NgbPagination.prototype.nextDisabled = /**
         * @return {?}
         */
        function () { return !this.hasNext() || this.disabled; };
        /**
         * @return {?}
         */
        NgbPagination.prototype.previousDisabled = /**
         * @return {?}
         */
        function () { return !this.hasPrevious() || this.disabled; };
        /**
         * @param {?} pageNumber
         * @return {?}
         */
        NgbPagination.prototype.selectPage = /**
         * @param {?} pageNumber
         * @return {?}
         */
        function (pageNumber) { this._updatePages(pageNumber); };
        /**
         * @param {?} changes
         * @return {?}
         */
        NgbPagination.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) { this._updatePages(this.page); };
        /**
         * @param {?} pageNumber
         * @return {?}
         */
        NgbPagination.prototype.isEllipsis = /**
         * @param {?} pageNumber
         * @return {?}
         */
        function (pageNumber) { return pageNumber === -1; };
        /**
         * Appends ellipses and first/last page number to the displayed pages
         */
        /**
         * Appends ellipses and first/last page number to the displayed pages
         * @private
         * @param {?} start
         * @param {?} end
         * @return {?}
         */
        NgbPagination.prototype._applyEllipses = /**
         * Appends ellipses and first/last page number to the displayed pages
         * @private
         * @param {?} start
         * @param {?} end
         * @return {?}
         */
        function (start, end) {
            if (this.ellipses) {
                if (start > 0) {
                    if (start > 1) {
                        this.pages.unshift(-1);
                    }
                    this.pages.unshift(1);
                }
                if (end < this.pageCount) {
                    if (end < (this.pageCount - 1)) {
                        this.pages.push(-1);
                    }
                    this.pages.push(this.pageCount);
                }
            }
        };
        /**
         * Rotates page numbers based on maxSize items visible.
         * Currently selected page stays in the middle:
         *
         * Ex. for selected page = 6:
         * [5,*6*,7] for maxSize = 3
         * [4,5,*6*,7] for maxSize = 4
         */
        /**
         * Rotates page numbers based on maxSize items visible.
         * Currently selected page stays in the middle:
         *
         * Ex. for selected page = 6:
         * [5,*6*,7] for maxSize = 3
         * [4,5,*6*,7] for maxSize = 4
         * @private
         * @return {?}
         */
        NgbPagination.prototype._applyRotation = /**
         * Rotates page numbers based on maxSize items visible.
         * Currently selected page stays in the middle:
         *
         * Ex. for selected page = 6:
         * [5,*6*,7] for maxSize = 3
         * [4,5,*6*,7] for maxSize = 4
         * @private
         * @return {?}
         */
        function () {
            /** @type {?} */
            var start = 0;
            /** @type {?} */
            var end = this.pageCount;
            /** @type {?} */
            var leftOffset = Math.floor(this.maxSize / 2);
            /** @type {?} */
            var rightOffset = this.maxSize % 2 === 0 ? leftOffset - 1 : leftOffset;
            if (this.page <= leftOffset) {
                // very beginning, no rotation -> [0..maxSize]
                end = this.maxSize;
            }
            else if (this.pageCount - this.page < leftOffset) {
                // very end, no rotation -> [len-maxSize..len]
                start = this.pageCount - this.maxSize;
            }
            else {
                // rotate
                start = this.page - leftOffset - 1;
                end = this.page + rightOffset;
            }
            return [start, end];
        };
        /**
         * Paginates page numbers based on maxSize items per page.
         */
        /**
         * Paginates page numbers based on maxSize items per page.
         * @private
         * @return {?}
         */
        NgbPagination.prototype._applyPagination = /**
         * Paginates page numbers based on maxSize items per page.
         * @private
         * @return {?}
         */
        function () {
            /** @type {?} */
            var page = Math.ceil(this.page / this.maxSize) - 1;
            /** @type {?} */
            var start = page * this.maxSize;
            /** @type {?} */
            var end = start + this.maxSize;
            return [start, end];
        };
        /**
         * @private
         * @param {?} newPageNo
         * @return {?}
         */
        NgbPagination.prototype._setPageInRange = /**
         * @private
         * @param {?} newPageNo
         * @return {?}
         */
        function (newPageNo) {
            /** @type {?} */
            var prevPageNo = this.page;
            this.page = getValueInRange(newPageNo, this.pageCount, 1);
            if (this.page !== prevPageNo && isNumber(this.collectionSize)) {
                this.pageChange.emit(this.page);
            }
        };
        /**
         * @private
         * @param {?} newPage
         * @return {?}
         */
        NgbPagination.prototype._updatePages = /**
         * @private
         * @param {?} newPage
         * @return {?}
         */
        function (newPage) {
            var _a, _b;
            this.pageCount = Math.ceil(this.collectionSize / this.pageSize);
            if (!isNumber(this.pageCount)) {
                this.pageCount = 0;
            }
            // fill-in model needed to render pages
            this.pages.length = 0;
            for (var i = 1; i <= this.pageCount; i++) {
                this.pages.push(i);
            }
            // set page within 1..max range
            this._setPageInRange(newPage);
            // apply maxSize if necessary
            if (this.maxSize > 0 && this.pageCount > this.maxSize) {
                /** @type {?} */
                var start = 0;
                /** @type {?} */
                var end = this.pageCount;
                // either paginating or rotating page numbers
                if (this.rotate) {
                    _a = __read(this._applyRotation(), 2), start = _a[0], end = _a[1];
                }
                else {
                    _b = __read(this._applyPagination(), 2), start = _b[0], end = _b[1];
                }
                this.pages = this.pages.slice(start, end);
                // adding ellipses
                this._applyEllipses(start, end);
            }
        };
        NgbPagination.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-pagination',
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        host: { 'role': 'navigation' },
                        template: "\n    <ng-template #first><span aria-hidden=\"true\" i18n=\"@@ngb.pagination.first\">&laquo;&laquo;</span></ng-template>\n    <ng-template #previous><span aria-hidden=\"true\" i18n=\"@@ngb.pagination.previous\">&laquo;</span></ng-template>\n    <ng-template #next><span aria-hidden=\"true\" i18n=\"@@ngb.pagination.next\">&raquo;</span></ng-template>\n    <ng-template #last><span aria-hidden=\"true\" i18n=\"@@ngb.pagination.last\">&raquo;&raquo;</span></ng-template>\n    <ng-template #ellipsis>...</ng-template>\n    <ng-template #defaultNumber let-page let-currentPage=\"currentPage\">\n      {{ page }}\n      <span *ngIf=\"page === currentPage\" class=\"sr-only\">(current)</span>\n    </ng-template>\n    <ul [class]=\"'pagination' + (size ? ' pagination-' + size : '')\">\n      <li *ngIf=\"boundaryLinks\" class=\"page-item\"\n        [class.disabled]=\"previousDisabled()\">\n        <a aria-label=\"First\" i18n-aria-label=\"@@ngb.pagination.first-aria\" class=\"page-link\" href\n          (click)=\"selectPage(1); $event.preventDefault()\" [attr.tabindex]=\"(hasPrevious() ? null : '-1')\">\n          <ng-template [ngTemplateOutlet]=\"tplFirst?.templateRef || first\"\n                       [ngTemplateOutletContext]=\"{disabled: previousDisabled(), currentPage: page}\"></ng-template>\n        </a>\n      </li>\n\n      <li *ngIf=\"directionLinks\" class=\"page-item\"\n        [class.disabled]=\"previousDisabled()\">\n        <a aria-label=\"Previous\" i18n-aria-label=\"@@ngb.pagination.previous-aria\" class=\"page-link\" href\n          (click)=\"selectPage(page-1); $event.preventDefault()\" [attr.tabindex]=\"(hasPrevious() ? null : '-1')\">\n          <ng-template [ngTemplateOutlet]=\"tplPrevious?.templateRef || previous\"\n                       [ngTemplateOutletContext]=\"{disabled: previousDisabled()}\"></ng-template>\n        </a>\n      </li>\n      <li *ngFor=\"let pageNumber of pages\" class=\"page-item\" [class.active]=\"pageNumber === page\"\n        [class.disabled]=\"isEllipsis(pageNumber) || disabled\">\n        <a *ngIf=\"isEllipsis(pageNumber)\" class=\"page-link\">\n          <ng-template [ngTemplateOutlet]=\"tplEllipsis?.templateRef || ellipsis\"\n                       [ngTemplateOutletContext]=\"{disabled: true, currentPage: page}\"></ng-template>\n        </a>\n        <a *ngIf=\"!isEllipsis(pageNumber)\" class=\"page-link\" href (click)=\"selectPage(pageNumber); $event.preventDefault()\">\n          <ng-template [ngTemplateOutlet]=\"tplNumber?.templateRef || defaultNumber\"\n                       [ngTemplateOutletContext]=\"{disabled: disabled, $implicit: pageNumber, currentPage: page}\"></ng-template>\n        </a>\n      </li>\n      <li *ngIf=\"directionLinks\" class=\"page-item\" [class.disabled]=\"nextDisabled()\">\n        <a aria-label=\"Next\" i18n-aria-label=\"@@ngb.pagination.next-aria\" class=\"page-link\" href\n          (click)=\"selectPage(page+1); $event.preventDefault()\" [attr.tabindex]=\"(hasNext() ? null : '-1')\">\n          <ng-template [ngTemplateOutlet]=\"tplNext?.templateRef || next\"\n                       [ngTemplateOutletContext]=\"{disabled: nextDisabled(), currentPage: page}\"></ng-template>\n        </a>\n      </li>\n\n      <li *ngIf=\"boundaryLinks\" class=\"page-item\" [class.disabled]=\"nextDisabled()\">\n        <a aria-label=\"Last\" i18n-aria-label=\"@@ngb.pagination.last-aria\" class=\"page-link\" href\n          (click)=\"selectPage(pageCount); $event.preventDefault()\" [attr.tabindex]=\"(hasNext() ? null : '-1')\">\n          <ng-template [ngTemplateOutlet]=\"tplLast?.templateRef || last\"\n                       [ngTemplateOutletContext]=\"{disabled: nextDisabled(), currentPage: page}\"></ng-template>\n        </a>\n      </li>\n    </ul>\n  "
                    }] }
        ];
        /** @nocollapse */
        NgbPagination.ctorParameters = function () { return [
            { type: NgbPaginationConfig }
        ]; };
        NgbPagination.propDecorators = {
            tplEllipsis: [{ type: core.ContentChild, args: [NgbPaginationEllipsis, { static: false },] }],
            tplFirst: [{ type: core.ContentChild, args: [NgbPaginationFirst, { static: false },] }],
            tplLast: [{ type: core.ContentChild, args: [NgbPaginationLast, { static: false },] }],
            tplNext: [{ type: core.ContentChild, args: [NgbPaginationNext, { static: false },] }],
            tplNumber: [{ type: core.ContentChild, args: [NgbPaginationNumber, { static: false },] }],
            tplPrevious: [{ type: core.ContentChild, args: [NgbPaginationPrevious, { static: false },] }],
            disabled: [{ type: core.Input }],
            boundaryLinks: [{ type: core.Input }],
            directionLinks: [{ type: core.Input }],
            ellipses: [{ type: core.Input }],
            rotate: [{ type: core.Input }],
            collectionSize: [{ type: core.Input }],
            maxSize: [{ type: core.Input }],
            page: [{ type: core.Input }],
            pageSize: [{ type: core.Input }],
            pageChange: [{ type: core.Output }],
            size: [{ type: core.Input }]
        };
        return NgbPagination;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var DIRECTIVES = [
        NgbPagination, NgbPaginationEllipsis, NgbPaginationFirst, NgbPaginationLast, NgbPaginationNext, NgbPaginationNumber,
        NgbPaginationPrevious
    ];
    var NgbPaginationModule = /** @class */ (function () {
        function NgbPaginationModule() {
        }
        NgbPaginationModule.decorators = [
            { type: core.NgModule, args: [{ declarations: DIRECTIVES, exports: DIRECTIVES, imports: [common.CommonModule] },] }
        ];
        return NgbPaginationModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var Trigger = /** @class */ (function () {
        function Trigger(open, close) {
            this.open = open;
            this.close = close;
            if (!close) {
                this.close = open;
            }
        }
        /**
         * @return {?}
         */
        Trigger.prototype.isManual = /**
         * @return {?}
         */
        function () { return this.open === 'manual' || this.close === 'manual'; };
        return Trigger;
    }());
    /** @type {?} */
    var DEFAULT_ALIASES = {
        'hover': ['mouseenter', 'mouseleave'],
        'focus': ['focusin', 'focusout'],
    };
    /**
     * @param {?} triggers
     * @param {?=} aliases
     * @return {?}
     */
    function parseTriggers(triggers, aliases) {
        if (aliases === void 0) { aliases = DEFAULT_ALIASES; }
        /** @type {?} */
        var trimmedTriggers = (triggers || '').trim();
        if (trimmedTriggers.length === 0) {
            return [];
        }
        /** @type {?} */
        var parsedTriggers = trimmedTriggers.split(/\s+/).map((/**
         * @param {?} trigger
         * @return {?}
         */
        function (trigger) { return trigger.split(':'); })).map((/**
         * @param {?} triggerPair
         * @return {?}
         */
        function (triggerPair) {
            /** @type {?} */
            var alias = aliases[triggerPair[0]] || triggerPair;
            return new Trigger(alias[0], alias[1]);
        }));
        /** @type {?} */
        var manualTriggers = parsedTriggers.filter((/**
         * @param {?} triggerPair
         * @return {?}
         */
        function (triggerPair) { return triggerPair.isManual(); }));
        if (manualTriggers.length > 1) {
            throw 'Triggers parse error: only one manual trigger is allowed';
        }
        if (manualTriggers.length === 1 && parsedTriggers.length > 1) {
            throw 'Triggers parse error: manual trigger can\'t be mixed with other triggers';
        }
        return parsedTriggers;
    }
    /**
     * @param {?} renderer
     * @param {?} nativeElement
     * @param {?} triggers
     * @param {?} isOpenedFn
     * @return {?}
     */
    function observeTriggers(renderer, nativeElement, triggers, isOpenedFn) {
        return new rxjs.Observable((/**
         * @param {?} subscriber
         * @return {?}
         */
        function (subscriber) {
            /** @type {?} */
            var listeners = [];
            /** @type {?} */
            var openFn = (/**
             * @return {?}
             */
            function () { return subscriber.next(true); });
            /** @type {?} */
            var closeFn = (/**
             * @return {?}
             */
            function () { return subscriber.next(false); });
            /** @type {?} */
            var toggleFn = (/**
             * @return {?}
             */
            function () { return subscriber.next(!isOpenedFn()); });
            triggers.forEach((/**
             * @param {?} trigger
             * @return {?}
             */
            function (trigger) {
                if (trigger.open === trigger.close) {
                    listeners.push(renderer.listen(nativeElement, trigger.open, toggleFn));
                }
                else {
                    listeners.push(renderer.listen(nativeElement, trigger.open, openFn), renderer.listen(nativeElement, trigger.close, closeFn));
                }
            }));
            return (/**
             * @return {?}
             */
            function () { listeners.forEach((/**
             * @param {?} unsubscribeFn
             * @return {?}
             */
            function (unsubscribeFn) { return unsubscribeFn(); })); });
        }));
    }
    /** @type {?} */
    var delayOrNoop = (/**
     * @template T
     * @param {?} time
     * @return {?}
     */
    function (time) { return time > 0 ? operators.delay(time) : (/**
     * @param {?} a
     * @return {?}
     */
    function (a) { return a; }); });
    /**
     * @param {?} openDelay
     * @param {?} closeDelay
     * @param {?} isOpenedFn
     * @return {?}
     */
    function triggerDelay(openDelay, closeDelay, isOpenedFn) {
        return (/**
         * @param {?} input$
         * @return {?}
         */
        function (input$) {
            /** @type {?} */
            var pending = null;
            /** @type {?} */
            var filteredInput$ = input$.pipe(operators.map((/**
             * @param {?} open
             * @return {?}
             */
            function (open) { return ({ open: open }); })), operators.filter((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                /** @type {?} */
                var currentlyOpen = isOpenedFn();
                if (currentlyOpen !== event.open && (!pending || pending.open === currentlyOpen)) {
                    pending = event;
                    return true;
                }
                if (pending && pending.open !== event.open) {
                    pending = null;
                }
                return false;
            })), operators.share());
            /** @type {?} */
            var delayedOpen$ = filteredInput$.pipe(operators.filter((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return event.open; })), delayOrNoop(openDelay));
            /** @type {?} */
            var delayedClose$ = filteredInput$.pipe(operators.filter((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return !event.open; })), delayOrNoop(closeDelay));
            return rxjs.merge(delayedOpen$, delayedClose$)
                .pipe(operators.filter((/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                if (event === pending) {
                    pending = null;
                    return event.open !== isOpenedFn();
                }
                return false;
            })), operators.map((/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return event.open; })));
        });
    }
    /**
     * @param {?} renderer
     * @param {?} nativeElement
     * @param {?} triggers
     * @param {?} isOpenedFn
     * @param {?} openFn
     * @param {?} closeFn
     * @param {?=} openDelay
     * @param {?=} closeDelay
     * @return {?}
     */
    function listenToTriggers(renderer, nativeElement, triggers, isOpenedFn, openFn, closeFn, openDelay, closeDelay) {
        if (openDelay === void 0) { openDelay = 0; }
        if (closeDelay === void 0) { closeDelay = 0; }
        /** @type {?} */
        var parsedTriggers = parseTriggers(triggers);
        if (parsedTriggers.length === 1 && parsedTriggers[0].isManual()) {
            return (/**
             * @return {?}
             */
            function () { });
        }
        /** @type {?} */
        var subscription = observeTriggers(renderer, nativeElement, parsedTriggers, isOpenedFn)
            .pipe(triggerDelay(openDelay, closeDelay, isOpenedFn))
            .subscribe((/**
         * @param {?} open
         * @return {?}
         */
        function (open) { return (open ? openFn() : closeFn()); }));
        return (/**
         * @return {?}
         */
        function () { return subscription.unsubscribe(); });
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A configuration service for the [`NgbPopover`](#/components/popover/api#NgbPopover) component.
     *
     * You can inject this service, typically in your root component, and customize the values of its properties in
     * order to provide default values for all the popovers used in the application.
     */
    var NgbPopoverConfig = /** @class */ (function () {
        function NgbPopoverConfig() {
            this.autoClose = true;
            this.placement = 'auto';
            this.triggers = 'click';
            this.disablePopover = false;
            this.openDelay = 0;
            this.closeDelay = 0;
        }
        NgbPopoverConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbPopoverConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbPopoverConfig_Factory() { return new NgbPopoverConfig(); }, token: NgbPopoverConfig, providedIn: "root" });
        return NgbPopoverConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var nextId$3 = 0;
    var NgbPopoverWindow = /** @class */ (function () {
        function NgbPopoverWindow() {
        }
        /**
         * @return {?}
         */
        NgbPopoverWindow.prototype.isTitleTemplate = /**
         * @return {?}
         */
        function () { return this.title instanceof core.TemplateRef; };
        NgbPopoverWindow.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-popover-window',
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        encapsulation: core.ViewEncapsulation.None,
                        host: { '[class]': '"popover" + (popoverClass ? " " + popoverClass : "")', 'role': 'tooltip', '[id]': 'id' },
                        template: "\n    <div class=\"arrow\"></div>\n    <h3 class=\"popover-header\" *ngIf=\"title != null\">\n      <ng-template #simpleTitle>{{title}}</ng-template>\n      <ng-template [ngTemplateOutlet]=\"isTitleTemplate() ? title : simpleTitle\" [ngTemplateOutletContext]=\"context\"></ng-template>\n    </h3>\n    <div class=\"popover-body\"><ng-content></ng-content></div>",
                        styles: ["ngb-popover-window.bs-popover-bottom>.arrow,ngb-popover-window.bs-popover-top>.arrow{left:50%;margin-left:-.5rem}ngb-popover-window.bs-popover-bottom-left>.arrow,ngb-popover-window.bs-popover-top-left>.arrow{left:2em}ngb-popover-window.bs-popover-bottom-right>.arrow,ngb-popover-window.bs-popover-top-right>.arrow{left:auto;right:2em}ngb-popover-window.bs-popover-left>.arrow,ngb-popover-window.bs-popover-right>.arrow{top:50%;margin-top:-.5rem}ngb-popover-window.bs-popover-left-top>.arrow,ngb-popover-window.bs-popover-right-top>.arrow{top:.7em}ngb-popover-window.bs-popover-left-bottom>.arrow,ngb-popover-window.bs-popover-right-bottom>.arrow{top:auto;bottom:.7em}"]
                    }] }
        ];
        NgbPopoverWindow.propDecorators = {
            title: [{ type: core.Input }],
            id: [{ type: core.Input }],
            popoverClass: [{ type: core.Input }],
            context: [{ type: core.Input }]
        };
        return NgbPopoverWindow;
    }());
    /**
     * A lightweight and extensible directive for fancy popover creation.
     */
    var NgbPopover = /** @class */ (function () {
        function NgbPopover(_elementRef, _renderer, injector, componentFactoryResolver, viewContainerRef, config, _ngZone, _document, _changeDetector, _applicationRef) {
            var _this = this;
            this._elementRef = _elementRef;
            this._renderer = _renderer;
            this._ngZone = _ngZone;
            this._document = _document;
            this._changeDetector = _changeDetector;
            this._applicationRef = _applicationRef;
            /**
             * An event emitted when the popover is shown. Contains no payload.
             */
            this.shown = new core.EventEmitter();
            /**
             * An event emitted when the popover is hidden. Contains no payload.
             */
            this.hidden = new core.EventEmitter();
            this._ngbPopoverWindowId = "ngb-popover-" + nextId$3++;
            this.autoClose = config.autoClose;
            this.placement = config.placement;
            this.triggers = config.triggers;
            this.container = config.container;
            this.disablePopover = config.disablePopover;
            this.popoverClass = config.popoverClass;
            this.openDelay = config.openDelay;
            this.closeDelay = config.closeDelay;
            this._popupService = new PopupService(NgbPopoverWindow, injector, viewContainerRef, _renderer, componentFactoryResolver, _applicationRef);
            this._zoneSubscription = _ngZone.onStable.subscribe((/**
             * @return {?}
             */
            function () {
                if (_this._windowRef) {
                    positionElements(_this._elementRef.nativeElement, _this._windowRef.location.nativeElement, _this.placement, _this.container === 'body', 'bs-popover');
                }
            }));
        }
        /**
         * @private
         * @return {?}
         */
        NgbPopover.prototype._isDisabled = /**
         * @private
         * @return {?}
         */
        function () {
            if (this.disablePopover) {
                return true;
            }
            if (!this.ngbPopover && !this.popoverTitle) {
                return true;
            }
            return false;
        };
        /**
         * Opens the popover.
         *
         * This is considered to be a "manual" triggering.
         * The `context` is an optional value to be injected into the popover template when it is created.
         */
        /**
         * Opens the popover.
         *
         * This is considered to be a "manual" triggering.
         * The `context` is an optional value to be injected into the popover template when it is created.
         * @param {?=} context
         * @return {?}
         */
        NgbPopover.prototype.open = /**
         * Opens the popover.
         *
         * This is considered to be a "manual" triggering.
         * The `context` is an optional value to be injected into the popover template when it is created.
         * @param {?=} context
         * @return {?}
         */
        function (context) {
            var _this = this;
            if (!this._windowRef && !this._isDisabled()) {
                this._windowRef = this._popupService.open(this.ngbPopover, context);
                this._windowRef.instance.title = this.popoverTitle;
                this._windowRef.instance.context = context;
                this._windowRef.instance.popoverClass = this.popoverClass;
                this._windowRef.instance.id = this._ngbPopoverWindowId;
                this._renderer.setAttribute(this._elementRef.nativeElement, 'aria-describedby', this._ngbPopoverWindowId);
                if (this.container === 'body') {
                    this._document.querySelector(this.container).appendChild(this._windowRef.location.nativeElement);
                }
                // We need to detect changes, because we don't know where .open() might be called from.
                // Ex. opening popover from one of lifecycle hooks that run after the CD
                // (say from ngAfterViewInit) will result in 'ExpressionHasChanged' exception
                this._windowRef.changeDetectorRef.detectChanges();
                // We need to mark for check, because popover won't work inside the OnPush component.
                // Ex. when we use expression like `{{ popover.isOpen() : 'opened' : 'closed' }}`
                // inside the template of an OnPush component and we change the popover from
                // open -> closed, the expression in question won't be updated unless we explicitly
                // mark the parent component to be checked.
                this._windowRef.changeDetectorRef.markForCheck();
                ngbAutoClose(this._ngZone, this._document, this.autoClose, (/**
                 * @return {?}
                 */
                function () { return _this.close(); }), this.hidden, [this._windowRef.location.nativeElement]);
                this.shown.emit();
            }
        };
        /**
         * Closes the popover.
         *
         * This is considered to be a "manual" triggering of the popover.
         */
        /**
         * Closes the popover.
         *
         * This is considered to be a "manual" triggering of the popover.
         * @return {?}
         */
        NgbPopover.prototype.close = /**
         * Closes the popover.
         *
         * This is considered to be a "manual" triggering of the popover.
         * @return {?}
         */
        function () {
            if (this._windowRef) {
                this._renderer.removeAttribute(this._elementRef.nativeElement, 'aria-describedby');
                this._popupService.close();
                this._windowRef = null;
                this.hidden.emit();
                this._changeDetector.markForCheck();
            }
        };
        /**
         * Toggles the popover.
         *
         * This is considered to be a "manual" triggering of the popover.
         */
        /**
         * Toggles the popover.
         *
         * This is considered to be a "manual" triggering of the popover.
         * @return {?}
         */
        NgbPopover.prototype.toggle = /**
         * Toggles the popover.
         *
         * This is considered to be a "manual" triggering of the popover.
         * @return {?}
         */
        function () {
            if (this._windowRef) {
                this.close();
            }
            else {
                this.open();
            }
        };
        /**
         * Returns `true`, if the popover is currently shown.
         */
        /**
         * Returns `true`, if the popover is currently shown.
         * @return {?}
         */
        NgbPopover.prototype.isOpen = /**
         * Returns `true`, if the popover is currently shown.
         * @return {?}
         */
        function () { return this._windowRef != null; };
        /**
         * @return {?}
         */
        NgbPopover.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            this._unregisterListenersFn = listenToTriggers(this._renderer, this._elementRef.nativeElement, this.triggers, this.isOpen.bind(this), this.open.bind(this), this.close.bind(this), +this.openDelay, +this.closeDelay);
        };
        /**
         * @param {?} changes
         * @return {?}
         */
        NgbPopover.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            // close popover if title and content become empty, or disablePopover set to true
            if ((changes['ngbPopover'] || changes['popoverTitle'] || changes['disablePopover']) && this._isDisabled()) {
                this.close();
            }
        };
        /**
         * @return {?}
         */
        NgbPopover.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            this.close();
            // This check is needed as it might happen that ngOnDestroy is called before ngOnInit
            // under certain conditions, see: https://github.com/ng-bootstrap/ng-bootstrap/issues/2199
            if (this._unregisterListenersFn) {
                this._unregisterListenersFn();
            }
            this._zoneSubscription.unsubscribe();
        };
        NgbPopover.decorators = [
            { type: core.Directive, args: [{ selector: '[ngbPopover]', exportAs: 'ngbPopover' },] }
        ];
        /** @nocollapse */
        NgbPopover.ctorParameters = function () { return [
            { type: core.ElementRef },
            { type: core.Renderer2 },
            { type: core.Injector },
            { type: core.ComponentFactoryResolver },
            { type: core.ViewContainerRef },
            { type: NgbPopoverConfig },
            { type: core.NgZone },
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
            { type: core.ChangeDetectorRef },
            { type: core.ApplicationRef }
        ]; };
        NgbPopover.propDecorators = {
            autoClose: [{ type: core.Input }],
            ngbPopover: [{ type: core.Input }],
            popoverTitle: [{ type: core.Input }],
            placement: [{ type: core.Input }],
            triggers: [{ type: core.Input }],
            container: [{ type: core.Input }],
            disablePopover: [{ type: core.Input }],
            popoverClass: [{ type: core.Input }],
            openDelay: [{ type: core.Input }],
            closeDelay: [{ type: core.Input }],
            shown: [{ type: core.Output }],
            hidden: [{ type: core.Output }]
        };
        return NgbPopover;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbPopoverModule = /** @class */ (function () {
        function NgbPopoverModule() {
        }
        NgbPopoverModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [NgbPopover, NgbPopoverWindow],
                        exports: [NgbPopover],
                        imports: [common.CommonModule],
                        entryComponents: [NgbPopoverWindow]
                    },] }
        ];
        return NgbPopoverModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A configuration service for the [`NgbProgressbar`](#/components/progressbar/api#NgbProgressbar) component.
     *
     * You can inject this service, typically in your root component, and customize the values of its properties in
     * order to provide default values for all the progress bars used in the application.
     */
    var NgbProgressbarConfig = /** @class */ (function () {
        function NgbProgressbarConfig() {
            this.max = 100;
            this.animated = false;
            this.striped = false;
            this.showValue = false;
        }
        NgbProgressbarConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbProgressbarConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbProgressbarConfig_Factory() { return new NgbProgressbarConfig(); }, token: NgbProgressbarConfig, providedIn: "root" });
        return NgbProgressbarConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A directive that provides feedback on the progress of a workflow or an action.
     */
    var NgbProgressbar = /** @class */ (function () {
        function NgbProgressbar(config) {
            /**
             * The current value for the progress bar.
             *
             * Should be in the `[0, max]` range.
             */
            this.value = 0;
            this.max = config.max;
            this.animated = config.animated;
            this.striped = config.striped;
            this.type = config.type;
            this.showValue = config.showValue;
            this.height = config.height;
        }
        /**
         * @return {?}
         */
        NgbProgressbar.prototype.getValue = /**
         * @return {?}
         */
        function () { return getValueInRange(this.value, this.max); };
        /**
         * @return {?}
         */
        NgbProgressbar.prototype.getPercentValue = /**
         * @return {?}
         */
        function () { return 100 * this.getValue() / this.max; };
        NgbProgressbar.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-progressbar',
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        template: "\n    <div class=\"progress\" [style.height]=\"height\">\n      <div class=\"progress-bar{{type ? ' bg-' + type : ''}}{{animated ? ' progress-bar-animated' : ''}}{{striped ?\n    ' progress-bar-striped' : ''}}\" role=\"progressbar\" [style.width.%]=\"getPercentValue()\"\n    [attr.aria-valuenow]=\"getValue()\" aria-valuemin=\"0\" [attr.aria-valuemax]=\"max\">\n        <span *ngIf=\"showValue\" i18n=\"@@ngb.progressbar.value\">{{getPercentValue()}}%</span><ng-content></ng-content>\n      </div>\n    </div>\n  "
                    }] }
        ];
        /** @nocollapse */
        NgbProgressbar.ctorParameters = function () { return [
            { type: NgbProgressbarConfig }
        ]; };
        NgbProgressbar.propDecorators = {
            max: [{ type: core.Input }],
            animated: [{ type: core.Input }],
            striped: [{ type: core.Input }],
            showValue: [{ type: core.Input }],
            type: [{ type: core.Input }],
            value: [{ type: core.Input }],
            height: [{ type: core.Input }]
        };
        return NgbProgressbar;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbProgressbarModule = /** @class */ (function () {
        function NgbProgressbarModule() {
        }
        NgbProgressbarModule.decorators = [
            { type: core.NgModule, args: [{ declarations: [NgbProgressbar], exports: [NgbProgressbar], imports: [common.CommonModule] },] }
        ];
        return NgbProgressbarModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A configuration service for the [`NgbRating`](#/components/rating/api#NgbRating) component.
     *
     * You can inject this service, typically in your root component, and customize the values of its properties in
     * order to provide default values for all the ratings used in the application.
     */
    var NgbRatingConfig = /** @class */ (function () {
        function NgbRatingConfig() {
            this.max = 10;
            this.readonly = false;
            this.resettable = false;
        }
        NgbRatingConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbRatingConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbRatingConfig_Factory() { return new NgbRatingConfig(); }, token: NgbRatingConfig, providedIn: "root" });
        return NgbRatingConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NGB_RATING_VALUE_ACCESSOR = {
        provide: forms.NG_VALUE_ACCESSOR,
        useExisting: core.forwardRef((/**
         * @return {?}
         */
        function () { return NgbRating; })),
        multi: true
    };
    /**
     * A directive that helps visualising and interacting with a star rating bar.
     */
    var NgbRating = /** @class */ (function () {
        function NgbRating(config, _changeDetectorRef) {
            this._changeDetectorRef = _changeDetectorRef;
            this.contexts = [];
            this.disabled = false;
            /**
             * An event emitted when the user is hovering over a given rating.
             *
             * Event payload equals to the rating being hovered over.
             */
            this.hover = new core.EventEmitter();
            /**
             * An event emitted when the user stops hovering over a given rating.
             *
             * Event payload equals to the rating of the last item being hovered over.
             */
            this.leave = new core.EventEmitter();
            /**
             * An event emitted when the user selects a new rating.
             *
             * Event payload equals to the newly selected rating.
             */
            this.rateChange = new core.EventEmitter(true);
            this.onChange = (/**
             * @param {?} _
             * @return {?}
             */
            function (_) { });
            this.onTouched = (/**
             * @return {?}
             */
            function () { });
            this.max = config.max;
            this.readonly = config.readonly;
        }
        /**
         * @return {?}
         */
        NgbRating.prototype.ariaValueText = /**
         * @return {?}
         */
        function () { return this.nextRate + " out of " + this.max; };
        /**
         * @param {?} value
         * @return {?}
         */
        NgbRating.prototype.enter = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            if (!this.readonly && !this.disabled) {
                this._updateState(value);
            }
            this.hover.emit(value);
        };
        /**
         * @return {?}
         */
        NgbRating.prototype.handleBlur = /**
         * @return {?}
         */
        function () { this.onTouched(); };
        /**
         * @param {?} value
         * @return {?}
         */
        NgbRating.prototype.handleClick = /**
         * @param {?} value
         * @return {?}
         */
        function (value) { this.update(this.resettable && this.rate === value ? 0 : value); };
        /**
         * @param {?} event
         * @return {?}
         */
        NgbRating.prototype.handleKeyDown = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            // tslint:disable-next-line:deprecation
            switch (event.which) {
                case Key.ArrowDown:
                case Key.ArrowLeft:
                    this.update(this.rate - 1);
                    break;
                case Key.ArrowUp:
                case Key.ArrowRight:
                    this.update(this.rate + 1);
                    break;
                case Key.Home:
                    this.update(0);
                    break;
                case Key.End:
                    this.update(this.max);
                    break;
                default:
                    return;
            }
            // note 'return' in default case
            event.preventDefault();
        };
        /**
         * @param {?} changes
         * @return {?}
         */
        NgbRating.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            if (changes['rate']) {
                this.update(this.rate);
            }
        };
        /**
         * @return {?}
         */
        NgbRating.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            this.contexts = Array.from({ length: this.max }, (/**
             * @param {?} v
             * @param {?} k
             * @return {?}
             */
            function (v, k) { return ({ fill: 0, index: k }); }));
            this._updateState(this.rate);
        };
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbRating.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this.onChange = fn; };
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbRating.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this.onTouched = fn; };
        /**
         * @return {?}
         */
        NgbRating.prototype.reset = /**
         * @return {?}
         */
        function () {
            this.leave.emit(this.nextRate);
            this._updateState(this.rate);
        };
        /**
         * @param {?} isDisabled
         * @return {?}
         */
        NgbRating.prototype.setDisabledState = /**
         * @param {?} isDisabled
         * @return {?}
         */
        function (isDisabled) { this.disabled = isDisabled; };
        /**
         * @param {?} value
         * @param {?=} internalChange
         * @return {?}
         */
        NgbRating.prototype.update = /**
         * @param {?} value
         * @param {?=} internalChange
         * @return {?}
         */
        function (value, internalChange) {
            if (internalChange === void 0) { internalChange = true; }
            /** @type {?} */
            var newRate = getValueInRange(value, this.max, 0);
            if (!this.readonly && !this.disabled && this.rate !== newRate) {
                this.rate = newRate;
                this.rateChange.emit(this.rate);
            }
            if (internalChange) {
                this.onChange(this.rate);
                this.onTouched();
            }
            this._updateState(this.rate);
        };
        /**
         * @param {?} value
         * @return {?}
         */
        NgbRating.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.update(value, false);
            this._changeDetectorRef.markForCheck();
        };
        /**
         * @private
         * @param {?} index
         * @return {?}
         */
        NgbRating.prototype._getFillValue = /**
         * @private
         * @param {?} index
         * @return {?}
         */
        function (index) {
            /** @type {?} */
            var diff = this.nextRate - index;
            if (diff >= 1) {
                return 100;
            }
            if (diff < 1 && diff > 0) {
                return parseInt((diff * 100).toFixed(2), 10);
            }
            return 0;
        };
        /**
         * @private
         * @param {?} nextValue
         * @return {?}
         */
        NgbRating.prototype._updateState = /**
         * @private
         * @param {?} nextValue
         * @return {?}
         */
        function (nextValue) {
            var _this = this;
            this.nextRate = nextValue;
            this.contexts.forEach((/**
             * @param {?} context
             * @param {?} index
             * @return {?}
             */
            function (context, index) { return context.fill = _this._getFillValue(index); }));
        };
        NgbRating.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-rating',
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        host: {
                            'class': 'd-inline-flex',
                            'tabindex': '0',
                            'role': 'slider',
                            'aria-valuemin': '0',
                            '[attr.aria-valuemax]': 'max',
                            '[attr.aria-valuenow]': 'nextRate',
                            '[attr.aria-valuetext]': 'ariaValueText()',
                            '[attr.aria-disabled]': 'readonly ? true : null',
                            '(blur)': 'handleBlur()',
                            '(keydown)': 'handleKeyDown($event)',
                            '(mouseleave)': 'reset()'
                        },
                        template: "\n    <ng-template #t let-fill=\"fill\">{{ fill === 100 ? '&#9733;' : '&#9734;' }}</ng-template>\n    <ng-template ngFor [ngForOf]=\"contexts\" let-index=\"index\">\n      <span class=\"sr-only\">({{ index < nextRate ? '*' : ' ' }})</span>\n      <span (mouseenter)=\"enter(index + 1)\" (click)=\"handleClick(index + 1)\" [style.cursor]=\"readonly || disabled ? 'default' : 'pointer'\">\n        <ng-template [ngTemplateOutlet]=\"starTemplate || starTemplateFromContent || t\" [ngTemplateOutletContext]=\"contexts[index]\">\n        </ng-template>\n      </span>\n    </ng-template>\n  ",
                        providers: [NGB_RATING_VALUE_ACCESSOR]
                    }] }
        ];
        /** @nocollapse */
        NgbRating.ctorParameters = function () { return [
            { type: NgbRatingConfig },
            { type: core.ChangeDetectorRef }
        ]; };
        NgbRating.propDecorators = {
            max: [{ type: core.Input }],
            rate: [{ type: core.Input }],
            readonly: [{ type: core.Input }],
            resettable: [{ type: core.Input }],
            starTemplate: [{ type: core.Input }],
            starTemplateFromContent: [{ type: core.ContentChild, args: [core.TemplateRef, { static: false },] }],
            hover: [{ type: core.Output }],
            leave: [{ type: core.Output }],
            rateChange: [{ type: core.Output }]
        };
        return NgbRating;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbRatingModule = /** @class */ (function () {
        function NgbRatingModule() {
        }
        NgbRatingModule.decorators = [
            { type: core.NgModule, args: [{ declarations: [NgbRating], exports: [NgbRating], imports: [common.CommonModule] },] }
        ];
        return NgbRatingModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A configuration service for the [`NgbTabset`](#/components/tabset/api#NgbTabset) component.
     *
     * You can inject this service, typically in your root component, and customize the values of its properties in
     * order to provide default values for all the tabsets used in the application.
     */
    var NgbTabsetConfig = /** @class */ (function () {
        function NgbTabsetConfig() {
            this.justify = 'start';
            this.orientation = 'horizontal';
            this.type = 'tabs';
        }
        NgbTabsetConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbTabsetConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbTabsetConfig_Factory() { return new NgbTabsetConfig(); }, token: NgbTabsetConfig, providedIn: "root" });
        return NgbTabsetConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var nextId$4 = 0;
    /**
     * A directive to wrap tab titles that need to contain HTML markup or other directives.
     *
     * Alternatively you could use the `NgbTab.title` input for string titles.
     */
    var NgbTabTitle = /** @class */ (function () {
        function NgbTabTitle(templateRef) {
            this.templateRef = templateRef;
        }
        NgbTabTitle.decorators = [
            { type: core.Directive, args: [{ selector: 'ng-template[ngbTabTitle]' },] }
        ];
        /** @nocollapse */
        NgbTabTitle.ctorParameters = function () { return [
            { type: core.TemplateRef }
        ]; };
        return NgbTabTitle;
    }());
    /**
     * A directive to wrap content to be displayed in a tab.
     */
    var NgbTabContent = /** @class */ (function () {
        function NgbTabContent(templateRef) {
            this.templateRef = templateRef;
        }
        NgbTabContent.decorators = [
            { type: core.Directive, args: [{ selector: 'ng-template[ngbTabContent]' },] }
        ];
        /** @nocollapse */
        NgbTabContent.ctorParameters = function () { return [
            { type: core.TemplateRef }
        ]; };
        return NgbTabContent;
    }());
    /**
     * A directive representing an individual tab.
     */
    var NgbTab = /** @class */ (function () {
        function NgbTab() {
            /**
             * The tab identifier.
             *
             * Must be unique for the entire document for proper accessibility support.
             */
            this.id = "ngb-tab-" + nextId$4++;
            /**
             * If `true`, the current tab is disabled and can't be toggled.
             */
            this.disabled = false;
        }
        /**
         * @return {?}
         */
        NgbTab.prototype.ngAfterContentChecked = /**
         * @return {?}
         */
        function () {
            // We are using @ContentChildren instead of @ContentChild as in the Angular version being used
            // only @ContentChildren allows us to specify the {descendants: false} option.
            // Without {descendants: false} we are hitting bugs described in:
            // https://github.com/ng-bootstrap/ng-bootstrap/issues/2240
            this.titleTpl = this.titleTpls.first;
            this.contentTpl = this.contentTpls.first;
        };
        NgbTab.decorators = [
            { type: core.Directive, args: [{ selector: 'ngb-tab' },] }
        ];
        NgbTab.propDecorators = {
            id: [{ type: core.Input }],
            title: [{ type: core.Input }],
            disabled: [{ type: core.Input }],
            titleTpls: [{ type: core.ContentChildren, args: [NgbTabTitle, { descendants: false },] }],
            contentTpls: [{ type: core.ContentChildren, args: [NgbTabContent, { descendants: false },] }]
        };
        return NgbTab;
    }());
    /**
     * A component that makes it easy to create tabbed interface.
     */
    var NgbTabset = /** @class */ (function () {
        function NgbTabset(config) {
            /**
             * If `true`, non-visible tabs content will be removed from DOM. Otherwise it will just be hidden.
             */
            this.destroyOnHide = true;
            /**
             * A tab change event emitted right before the tab change happens.
             *
             * See [`NgbTabChangeEvent`](#/components/tabset/api#NgbTabChangeEvent) for payload details.
             */
            this.tabChange = new core.EventEmitter();
            this.type = config.type;
            this.justify = config.justify;
            this.orientation = config.orientation;
        }
        Object.defineProperty(NgbTabset.prototype, "justify", {
            /**
             * The horizontal alignment of the tabs with flexbox utilities.
             */
            set: /**
             * The horizontal alignment of the tabs with flexbox utilities.
             * @param {?} className
             * @return {?}
             */
            function (className) {
                if (className === 'fill' || className === 'justified') {
                    this.justifyClass = "nav-" + className;
                }
                else {
                    this.justifyClass = "justify-content-" + className;
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * Selects the tab with the given id and shows its associated content panel.
         *
         * Any other tab that was previously selected becomes unselected and its associated pane is removed from DOM or
         * hidden depending on the `destroyOnHide` value.
         */
        /**
         * Selects the tab with the given id and shows its associated content panel.
         *
         * Any other tab that was previously selected becomes unselected and its associated pane is removed from DOM or
         * hidden depending on the `destroyOnHide` value.
         * @param {?} tabId
         * @return {?}
         */
        NgbTabset.prototype.select = /**
         * Selects the tab with the given id and shows its associated content panel.
         *
         * Any other tab that was previously selected becomes unselected and its associated pane is removed from DOM or
         * hidden depending on the `destroyOnHide` value.
         * @param {?} tabId
         * @return {?}
         */
        function (tabId) {
            /** @type {?} */
            var selectedTab = this._getTabById(tabId);
            if (selectedTab && !selectedTab.disabled && this.activeId !== selectedTab.id) {
                /** @type {?} */
                var defaultPrevented_1 = false;
                this.tabChange.emit({ activeId: this.activeId, nextId: selectedTab.id, preventDefault: (/**
                     * @return {?}
                     */
                    function () { defaultPrevented_1 = true; }) });
                if (!defaultPrevented_1) {
                    this.activeId = selectedTab.id;
                }
            }
        };
        /**
         * @return {?}
         */
        NgbTabset.prototype.ngAfterContentChecked = /**
         * @return {?}
         */
        function () {
            // auto-correct activeId that might have been set incorrectly as input
            /** @type {?} */
            var activeTab = this._getTabById(this.activeId);
            this.activeId = activeTab ? activeTab.id : (this.tabs.length ? this.tabs.first.id : null);
        };
        /**
         * @private
         * @param {?} id
         * @return {?}
         */
        NgbTabset.prototype._getTabById = /**
         * @private
         * @param {?} id
         * @return {?}
         */
        function (id) {
            /** @type {?} */
            var tabsWithId = this.tabs.filter((/**
             * @param {?} tab
             * @return {?}
             */
            function (tab) { return tab.id === id; }));
            return tabsWithId.length ? tabsWithId[0] : null;
        };
        NgbTabset.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-tabset',
                        exportAs: 'ngbTabset',
                        template: "\n    <ul [class]=\"'nav nav-' + type + (orientation == 'horizontal'?  ' ' + justifyClass : ' flex-column')\" role=\"tablist\">\n      <li class=\"nav-item\" *ngFor=\"let tab of tabs\">\n        <a [id]=\"tab.id\" class=\"nav-link\" [class.active]=\"tab.id === activeId\" [class.disabled]=\"tab.disabled\"\n          href (click)=\"select(tab.id); $event.preventDefault()\" role=\"tab\" [attr.tabindex]=\"(tab.disabled ? '-1': undefined)\"\n          [attr.aria-controls]=\"(!destroyOnHide || tab.id === activeId ? tab.id + '-panel' : null)\"\n          [attr.aria-selected]=\"tab.id === activeId\" [attr.aria-disabled]=\"tab.disabled\">\n          {{tab.title}}<ng-template [ngTemplateOutlet]=\"tab.titleTpl?.templateRef\"></ng-template>\n        </a>\n      </li>\n    </ul>\n    <div class=\"tab-content\">\n      <ng-template ngFor let-tab [ngForOf]=\"tabs\">\n        <div\n          class=\"tab-pane {{tab.id === activeId ? 'active' : null}}\"\n          *ngIf=\"!destroyOnHide || tab.id === activeId\"\n          role=\"tabpanel\"\n          [attr.aria-labelledby]=\"tab.id\" id=\"{{tab.id}}-panel\">\n          <ng-template [ngTemplateOutlet]=\"tab.contentTpl?.templateRef\"></ng-template>\n        </div>\n      </ng-template>\n    </div>\n  "
                    }] }
        ];
        /** @nocollapse */
        NgbTabset.ctorParameters = function () { return [
            { type: NgbTabsetConfig }
        ]; };
        NgbTabset.propDecorators = {
            tabs: [{ type: core.ContentChildren, args: [NgbTab,] }],
            activeId: [{ type: core.Input }],
            destroyOnHide: [{ type: core.Input }],
            justify: [{ type: core.Input }],
            orientation: [{ type: core.Input }],
            type: [{ type: core.Input }],
            tabChange: [{ type: core.Output }]
        };
        return NgbTabset;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NGB_TABSET_DIRECTIVES = [NgbTabset, NgbTab, NgbTabContent, NgbTabTitle];
    var NgbTabsetModule = /** @class */ (function () {
        function NgbTabsetModule() {
        }
        NgbTabsetModule.decorators = [
            { type: core.NgModule, args: [{ declarations: NGB_TABSET_DIRECTIVES, exports: NGB_TABSET_DIRECTIVES, imports: [common.CommonModule] },] }
        ];
        return NgbTabsetModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbTime = /** @class */ (function () {
        function NgbTime(hour, minute, second) {
            this.hour = toInteger(hour);
            this.minute = toInteger(minute);
            this.second = toInteger(second);
        }
        /**
         * @param {?=} step
         * @return {?}
         */
        NgbTime.prototype.changeHour = /**
         * @param {?=} step
         * @return {?}
         */
        function (step) {
            if (step === void 0) { step = 1; }
            this.updateHour((isNaN(this.hour) ? 0 : this.hour) + step);
        };
        /**
         * @param {?} hour
         * @return {?}
         */
        NgbTime.prototype.updateHour = /**
         * @param {?} hour
         * @return {?}
         */
        function (hour) {
            if (isNumber(hour)) {
                this.hour = (hour < 0 ? 24 + hour : hour) % 24;
            }
            else {
                this.hour = NaN;
            }
        };
        /**
         * @param {?=} step
         * @return {?}
         */
        NgbTime.prototype.changeMinute = /**
         * @param {?=} step
         * @return {?}
         */
        function (step) {
            if (step === void 0) { step = 1; }
            this.updateMinute((isNaN(this.minute) ? 0 : this.minute) + step);
        };
        /**
         * @param {?} minute
         * @return {?}
         */
        NgbTime.prototype.updateMinute = /**
         * @param {?} minute
         * @return {?}
         */
        function (minute) {
            if (isNumber(minute)) {
                this.minute = minute % 60 < 0 ? 60 + minute % 60 : minute % 60;
                this.changeHour(Math.floor(minute / 60));
            }
            else {
                this.minute = NaN;
            }
        };
        /**
         * @param {?=} step
         * @return {?}
         */
        NgbTime.prototype.changeSecond = /**
         * @param {?=} step
         * @return {?}
         */
        function (step) {
            if (step === void 0) { step = 1; }
            this.updateSecond((isNaN(this.second) ? 0 : this.second) + step);
        };
        /**
         * @param {?} second
         * @return {?}
         */
        NgbTime.prototype.updateSecond = /**
         * @param {?} second
         * @return {?}
         */
        function (second) {
            if (isNumber(second)) {
                this.second = second < 0 ? 60 + second % 60 : second % 60;
                this.changeMinute(Math.floor(second / 60));
            }
            else {
                this.second = NaN;
            }
        };
        /**
         * @param {?=} checkSecs
         * @return {?}
         */
        NgbTime.prototype.isValid = /**
         * @param {?=} checkSecs
         * @return {?}
         */
        function (checkSecs) {
            if (checkSecs === void 0) { checkSecs = true; }
            return isNumber(this.hour) && isNumber(this.minute) && (checkSecs ? isNumber(this.second) : true);
        };
        /**
         * @return {?}
         */
        NgbTime.prototype.toString = /**
         * @return {?}
         */
        function () { return (this.hour || 0) + ":" + (this.minute || 0) + ":" + (this.second || 0); };
        return NgbTime;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A configuration service for the [`NgbTimepicker`](#/components/timepicker/api#NgbTimepicker) component.
     *
     * You can inject this service, typically in your root component, and customize the values of its properties in
     * order to provide default values for all the timepickers used in the application.
     */
    var NgbTimepickerConfig = /** @class */ (function () {
        function NgbTimepickerConfig() {
            this.meridian = false;
            this.spinners = true;
            this.seconds = false;
            this.hourStep = 1;
            this.minuteStep = 1;
            this.secondStep = 1;
            this.disabled = false;
            this.readonlyInputs = false;
            this.size = 'medium';
        }
        NgbTimepickerConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbTimepickerConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbTimepickerConfig_Factory() { return new NgbTimepickerConfig(); }, token: NgbTimepickerConfig, providedIn: "root" });
        return NgbTimepickerConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @return {?}
     */
    function NGB_DATEPICKER_TIME_ADAPTER_FACTORY() {
        return new NgbTimeStructAdapter();
    }
    /**
     * An abstract service that does the conversion between the internal timepicker `NgbTimeStruct` model and
     * any provided user time model `T`, ex. a string, a native date, etc.
     *
     * The adapter is used **only** for conversion when binding timepicker to a form control,
     * ex. `[(ngModel)]="userTimeModel"`. Here `userTimeModel` can be of any type.
     *
     * The default timepicker implementation assumes we use `NgbTimeStruct` as a user model.
     *
     * See the [custom time adapter demo](#/components/timepicker/examples#adapter) for an example.
     *
     * \@since 2.2.0
     * @abstract
     * @template T
     */
    var NgbTimeAdapter = /** @class */ (function () {
        function NgbTimeAdapter() {
        }
        NgbTimeAdapter.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root', useFactory: NGB_DATEPICKER_TIME_ADAPTER_FACTORY },] }
        ];
        /** @nocollapse */ NgbTimeAdapter.ngInjectableDef = core.ɵɵdefineInjectable({ factory: NGB_DATEPICKER_TIME_ADAPTER_FACTORY, token: NgbTimeAdapter, providedIn: "root" });
        return NgbTimeAdapter;
    }());
    var NgbTimeStructAdapter = /** @class */ (function (_super) {
        __extends(NgbTimeStructAdapter, _super);
        function NgbTimeStructAdapter() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * Converts a NgbTimeStruct value into NgbTimeStruct value
         */
        /**
         * Converts a NgbTimeStruct value into NgbTimeStruct value
         * @param {?} time
         * @return {?}
         */
        NgbTimeStructAdapter.prototype.fromModel = /**
         * Converts a NgbTimeStruct value into NgbTimeStruct value
         * @param {?} time
         * @return {?}
         */
        function (time) {
            return (time && isInteger(time.hour) && isInteger(time.minute)) ?
                { hour: time.hour, minute: time.minute, second: isInteger(time.second) ? time.second : null } :
                null;
        };
        /**
         * Converts a NgbTimeStruct value into NgbTimeStruct value
         */
        /**
         * Converts a NgbTimeStruct value into NgbTimeStruct value
         * @param {?} time
         * @return {?}
         */
        NgbTimeStructAdapter.prototype.toModel = /**
         * Converts a NgbTimeStruct value into NgbTimeStruct value
         * @param {?} time
         * @return {?}
         */
        function (time) {
            return (time && isInteger(time.hour) && isInteger(time.minute)) ?
                { hour: time.hour, minute: time.minute, second: isInteger(time.second) ? time.second : null } :
                null;
        };
        NgbTimeStructAdapter.decorators = [
            { type: core.Injectable }
        ];
        return NgbTimeStructAdapter;
    }(NgbTimeAdapter));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @param {?} locale
     * @return {?}
     */
    function NGB_TIMEPICKER_I18N_FACTORY(locale) {
        return new NgbTimepickerI18nDefault(locale);
    }
    /**
     * Type of the service supplying day periods (for example, 'AM' and 'PM') to NgbTimepicker component.
     * The default implementation of this service honors the Angular locale, and uses the registered locale data,
     * as explained in the Angular i18n guide.
     * @abstract
     */
    var NgbTimepickerI18n = /** @class */ (function () {
        function NgbTimepickerI18n() {
        }
        NgbTimepickerI18n.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root', useFactory: NGB_TIMEPICKER_I18N_FACTORY, deps: [core.LOCALE_ID] },] }
        ];
        /** @nocollapse */ NgbTimepickerI18n.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbTimepickerI18n_Factory() { return NGB_TIMEPICKER_I18N_FACTORY(core.ɵɵinject(core.LOCALE_ID)); }, token: NgbTimepickerI18n, providedIn: "root" });
        return NgbTimepickerI18n;
    }());
    var NgbTimepickerI18nDefault = /** @class */ (function (_super) {
        __extends(NgbTimepickerI18nDefault, _super);
        function NgbTimepickerI18nDefault(locale) {
            var _this = _super.call(this) || this;
            _this._periods = common.getLocaleDayPeriods(locale, common.FormStyle.Standalone, common.TranslationWidth.Narrow);
            return _this;
        }
        /**
         * @return {?}
         */
        NgbTimepickerI18nDefault.prototype.getMorningPeriod = /**
         * @return {?}
         */
        function () { return this._periods[0]; };
        /**
         * @return {?}
         */
        NgbTimepickerI18nDefault.prototype.getAfternoonPeriod = /**
         * @return {?}
         */
        function () { return this._periods[1]; };
        NgbTimepickerI18nDefault.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        NgbTimepickerI18nDefault.ctorParameters = function () { return [
            { type: String, decorators: [{ type: core.Inject, args: [core.LOCALE_ID,] }] }
        ]; };
        return NgbTimepickerI18nDefault;
    }(NgbTimepickerI18n));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NGB_TIMEPICKER_VALUE_ACCESSOR = {
        provide: forms.NG_VALUE_ACCESSOR,
        useExisting: core.forwardRef((/**
         * @return {?}
         */
        function () { return NgbTimepicker; })),
        multi: true
    };
    /**
     * A directive that helps with wth picking hours, minutes and seconds.
     */
    var NgbTimepicker = /** @class */ (function () {
        function NgbTimepicker(_config, _ngbTimeAdapter, _cd, i18n) {
            this._config = _config;
            this._ngbTimeAdapter = _ngbTimeAdapter;
            this._cd = _cd;
            this.i18n = i18n;
            this.onChange = (/**
             * @param {?} _
             * @return {?}
             */
            function (_) { });
            this.onTouched = (/**
             * @return {?}
             */
            function () { });
            this.meridian = _config.meridian;
            this.spinners = _config.spinners;
            this.seconds = _config.seconds;
            this.hourStep = _config.hourStep;
            this.minuteStep = _config.minuteStep;
            this.secondStep = _config.secondStep;
            this.disabled = _config.disabled;
            this.readonlyInputs = _config.readonlyInputs;
            this.size = _config.size;
        }
        Object.defineProperty(NgbTimepicker.prototype, "hourStep", {
            get: /**
             * @return {?}
             */
            function () { return this._hourStep; },
            /**
             * The number of hours to add/subtract when clicking hour spinners.
             */
            set: /**
             * The number of hours to add/subtract when clicking hour spinners.
             * @param {?} step
             * @return {?}
             */
            function (step) {
                this._hourStep = isInteger(step) ? step : this._config.hourStep;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbTimepicker.prototype, "minuteStep", {
            get: /**
             * @return {?}
             */
            function () { return this._minuteStep; },
            /**
             * The number of minutes to add/subtract when clicking minute spinners.
             */
            set: /**
             * The number of minutes to add/subtract when clicking minute spinners.
             * @param {?} step
             * @return {?}
             */
            function (step) {
                this._minuteStep = isInteger(step) ? step : this._config.minuteStep;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbTimepicker.prototype, "secondStep", {
            get: /**
             * @return {?}
             */
            function () { return this._secondStep; },
            /**
             * The number of seconds to add/subtract when clicking second spinners.
             */
            set: /**
             * The number of seconds to add/subtract when clicking second spinners.
             * @param {?} step
             * @return {?}
             */
            function (step) {
                this._secondStep = isInteger(step) ? step : this._config.secondStep;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} value
         * @return {?}
         */
        NgbTimepicker.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            /** @type {?} */
            var structValue = this._ngbTimeAdapter.fromModel(value);
            this.model = structValue ? new NgbTime(structValue.hour, structValue.minute, structValue.second) : new NgbTime();
            if (!this.seconds && (!structValue || !isNumber(structValue.second))) {
                this.model.second = 0;
            }
            this._cd.markForCheck();
        };
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbTimepicker.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this.onChange = fn; };
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbTimepicker.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this.onTouched = fn; };
        /**
         * @param {?} isDisabled
         * @return {?}
         */
        NgbTimepicker.prototype.setDisabledState = /**
         * @param {?} isDisabled
         * @return {?}
         */
        function (isDisabled) { this.disabled = isDisabled; };
        /**
         * @param {?} step
         * @return {?}
         */
        NgbTimepicker.prototype.changeHour = /**
         * @param {?} step
         * @return {?}
         */
        function (step) {
            this.model.changeHour(step);
            this.propagateModelChange();
        };
        /**
         * @param {?} step
         * @return {?}
         */
        NgbTimepicker.prototype.changeMinute = /**
         * @param {?} step
         * @return {?}
         */
        function (step) {
            this.model.changeMinute(step);
            this.propagateModelChange();
        };
        /**
         * @param {?} step
         * @return {?}
         */
        NgbTimepicker.prototype.changeSecond = /**
         * @param {?} step
         * @return {?}
         */
        function (step) {
            this.model.changeSecond(step);
            this.propagateModelChange();
        };
        /**
         * @param {?} newVal
         * @return {?}
         */
        NgbTimepicker.prototype.updateHour = /**
         * @param {?} newVal
         * @return {?}
         */
        function (newVal) {
            /** @type {?} */
            var isPM = this.model.hour >= 12;
            /** @type {?} */
            var enteredHour = toInteger(newVal);
            if (this.meridian && (isPM && enteredHour < 12 || !isPM && enteredHour === 12)) {
                this.model.updateHour(enteredHour + 12);
            }
            else {
                this.model.updateHour(enteredHour);
            }
            this.propagateModelChange();
        };
        /**
         * @param {?} newVal
         * @return {?}
         */
        NgbTimepicker.prototype.updateMinute = /**
         * @param {?} newVal
         * @return {?}
         */
        function (newVal) {
            this.model.updateMinute(toInteger(newVal));
            this.propagateModelChange();
        };
        /**
         * @param {?} newVal
         * @return {?}
         */
        NgbTimepicker.prototype.updateSecond = /**
         * @param {?} newVal
         * @return {?}
         */
        function (newVal) {
            this.model.updateSecond(toInteger(newVal));
            this.propagateModelChange();
        };
        /**
         * @return {?}
         */
        NgbTimepicker.prototype.toggleMeridian = /**
         * @return {?}
         */
        function () {
            if (this.meridian) {
                this.changeHour(12);
            }
        };
        /**
         * @param {?} value
         * @return {?}
         */
        NgbTimepicker.prototype.formatHour = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            if (isNumber(value)) {
                if (this.meridian) {
                    return padNumber(value % 12 === 0 ? 12 : value % 12);
                }
                else {
                    return padNumber(value % 24);
                }
            }
            else {
                return padNumber(NaN);
            }
        };
        /**
         * @param {?} value
         * @return {?}
         */
        NgbTimepicker.prototype.formatMinSec = /**
         * @param {?} value
         * @return {?}
         */
        function (value) { return padNumber(value); };
        Object.defineProperty(NgbTimepicker.prototype, "isSmallSize", {
            get: /**
             * @return {?}
             */
            function () { return this.size === 'small'; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(NgbTimepicker.prototype, "isLargeSize", {
            get: /**
             * @return {?}
             */
            function () { return this.size === 'large'; },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} changes
         * @return {?}
         */
        NgbTimepicker.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            if (changes['seconds'] && !this.seconds && this.model && !isNumber(this.model.second)) {
                this.model.second = 0;
                this.propagateModelChange(false);
            }
        };
        /**
         * @private
         * @param {?=} touched
         * @return {?}
         */
        NgbTimepicker.prototype.propagateModelChange = /**
         * @private
         * @param {?=} touched
         * @return {?}
         */
        function (touched) {
            if (touched === void 0) { touched = true; }
            if (touched) {
                this.onTouched();
            }
            if (this.model.isValid(this.seconds)) {
                this.onChange(this._ngbTimeAdapter.toModel({ hour: this.model.hour, minute: this.model.minute, second: this.model.second }));
            }
            else {
                this.onChange(this._ngbTimeAdapter.toModel(null));
            }
        };
        NgbTimepicker.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-timepicker',
                        encapsulation: core.ViewEncapsulation.None,
                        template: "\n    <fieldset [disabled]=\"disabled\" [class.disabled]=\"disabled\">\n      <div class=\"ngb-tp\">\n        <div class=\"ngb-tp-input-container ngb-tp-hour\">\n          <button *ngIf=\"spinners\" tabindex=\"-1\" type=\"button\" (click)=\"changeHour(hourStep)\"\n            class=\"btn btn-link\" [class.btn-sm]=\"isSmallSize\" [class.btn-lg]=\"isLargeSize\" [class.disabled]=\"disabled\"\n            [disabled]=\"disabled\">\n            <span class=\"chevron ngb-tp-chevron\"></span>\n            <span class=\"sr-only\" i18n=\"@@ngb.timepicker.increment-hours\">Increment hours</span>\n          </button>\n          <input type=\"text\" class=\"ngb-tp-input form-control\" [class.form-control-sm]=\"isSmallSize\" [class.form-control-lg]=\"isLargeSize\"\n            maxlength=\"2\" placeholder=\"HH\" i18n-placeholder=\"@@ngb.timepicker.HH\"\n            [value]=\"formatHour(model?.hour)\" (change)=\"updateHour($event.target.value)\"\n            [readOnly]=\"readonlyInputs\" [disabled]=\"disabled\" aria-label=\"Hours\" i18n-aria-label=\"@@ngb.timepicker.hours\"\n            (keydown.ArrowUp)=\"changeHour(hourStep); $event.preventDefault()\"\n            (keydown.ArrowDown)=\"changeHour(-hourStep); $event.preventDefault()\">\n          <button *ngIf=\"spinners\" tabindex=\"-1\" type=\"button\" (click)=\"changeHour(-hourStep)\"\n            class=\"btn btn-link\" [class.btn-sm]=\"isSmallSize\" [class.btn-lg]=\"isLargeSize\" [class.disabled]=\"disabled\"\n            [disabled]=\"disabled\">\n            <span class=\"chevron ngb-tp-chevron bottom\"></span>\n            <span class=\"sr-only\" i18n=\"@@ngb.timepicker.decrement-hours\">Decrement hours</span>\n          </button>\n        </div>\n        <div class=\"ngb-tp-spacer\">:</div>\n        <div class=\"ngb-tp-input-container ngb-tp-minute\">\n          <button *ngIf=\"spinners\" tabindex=\"-1\" type=\"button\" (click)=\"changeMinute(minuteStep)\"\n            class=\"btn btn-link\" [class.btn-sm]=\"isSmallSize\" [class.btn-lg]=\"isLargeSize\" [class.disabled]=\"disabled\"\n            [disabled]=\"disabled\">\n            <span class=\"chevron ngb-tp-chevron\"></span>\n            <span class=\"sr-only\" i18n=\"@@ngb.timepicker.increment-minutes\">Increment minutes</span>\n          </button>\n          <input type=\"text\" class=\"ngb-tp-input form-control\" [class.form-control-sm]=\"isSmallSize\" [class.form-control-lg]=\"isLargeSize\"\n            maxlength=\"2\" placeholder=\"MM\" i18n-placeholder=\"@@ngb.timepicker.MM\"\n            [value]=\"formatMinSec(model?.minute)\" (change)=\"updateMinute($event.target.value)\"\n            [readOnly]=\"readonlyInputs\" [disabled]=\"disabled\" aria-label=\"Minutes\" i18n-aria-label=\"@@ngb.timepicker.minutes\"\n            (keydown.ArrowUp)=\"changeMinute(minuteStep); $event.preventDefault()\"\n            (keydown.ArrowDown)=\"changeMinute(-minuteStep); $event.preventDefault()\">\n          <button *ngIf=\"spinners\" tabindex=\"-1\" type=\"button\" (click)=\"changeMinute(-minuteStep)\"\n            class=\"btn btn-link\" [class.btn-sm]=\"isSmallSize\" [class.btn-lg]=\"isLargeSize\"  [class.disabled]=\"disabled\"\n            [disabled]=\"disabled\">\n            <span class=\"chevron ngb-tp-chevron bottom\"></span>\n            <span class=\"sr-only\"  i18n=\"@@ngb.timepicker.decrement-minutes\">Decrement minutes</span>\n          </button>\n        </div>\n        <div *ngIf=\"seconds\" class=\"ngb-tp-spacer\">:</div>\n        <div *ngIf=\"seconds\" class=\"ngb-tp-input-container ngb-tp-second\">\n          <button *ngIf=\"spinners\" tabindex=\"-1\" type=\"button\" (click)=\"changeSecond(secondStep)\"\n            class=\"btn btn-link\" [class.btn-sm]=\"isSmallSize\" [class.btn-lg]=\"isLargeSize\" [class.disabled]=\"disabled\"\n            [disabled]=\"disabled\">\n            <span class=\"chevron ngb-tp-chevron\"></span>\n            <span class=\"sr-only\" i18n=\"@@ngb.timepicker.increment-seconds\">Increment seconds</span>\n          </button>\n          <input type=\"text\" class=\"ngb-tp-input form-control\" [class.form-control-sm]=\"isSmallSize\" [class.form-control-lg]=\"isLargeSize\"\n            maxlength=\"2\" placeholder=\"SS\" i18n-placeholder=\"@@ngb.timepicker.SS\"\n            [value]=\"formatMinSec(model?.second)\" (change)=\"updateSecond($event.target.value)\"\n            [readOnly]=\"readonlyInputs\" [disabled]=\"disabled\" aria-label=\"Seconds\" i18n-aria-label=\"@@ngb.timepicker.seconds\"\n            (keydown.ArrowUp)=\"changeSecond(secondStep); $event.preventDefault()\"\n            (keydown.ArrowDown)=\"changeSecond(-secondStep); $event.preventDefault()\">\n          <button *ngIf=\"spinners\" tabindex=\"-1\" type=\"button\" (click)=\"changeSecond(-secondStep)\"\n            class=\"btn btn-link\" [class.btn-sm]=\"isSmallSize\" [class.btn-lg]=\"isLargeSize\"  [class.disabled]=\"disabled\"\n            [disabled]=\"disabled\">\n            <span class=\"chevron ngb-tp-chevron bottom\"></span>\n            <span class=\"sr-only\" i18n=\"@@ngb.timepicker.decrement-seconds\">Decrement seconds</span>\n          </button>\n        </div>\n        <div *ngIf=\"meridian\" class=\"ngb-tp-spacer\"></div>\n        <div *ngIf=\"meridian\" class=\"ngb-tp-meridian\">\n          <button type=\"button\" class=\"btn btn-outline-primary\" [class.btn-sm]=\"isSmallSize\" [class.btn-lg]=\"isLargeSize\"\n            [disabled]=\"disabled\" [class.disabled]=\"disabled\"\n                  (click)=\"toggleMeridian()\">\n            <ng-container *ngIf=\"model?.hour >= 12; else am\" i18n=\"@@ngb.timepicker.PM\">{{ i18n.getAfternoonPeriod() }}</ng-container>\n            <ng-template #am i18n=\"@@ngb.timepicker.AM\">{{ i18n.getMorningPeriod() }}</ng-template>\n          </button>\n        </div>\n      </div>\n    </fieldset>\n  ",
                        providers: [NGB_TIMEPICKER_VALUE_ACCESSOR],
                        styles: ["ngb-timepicker{font-size:1rem}.ngb-tp{display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center}.ngb-tp-input-container{width:4em}.ngb-tp-chevron::before{border-style:solid;border-width:.29em .29em 0 0;content:\"\";display:inline-block;height:.69em;left:.05em;position:relative;top:.15em;-webkit-transform:rotate(-45deg);transform:rotate(-45deg);vertical-align:middle;width:.69em}.ngb-tp-chevron.bottom:before{top:-.3em;-webkit-transform:rotate(135deg);transform:rotate(135deg)}.ngb-tp-input{text-align:center}.ngb-tp-hour,.ngb-tp-meridian,.ngb-tp-minute,.ngb-tp-second{display:-ms-flexbox;display:flex;-ms-flex-direction:column;flex-direction:column;-ms-flex-align:center;align-items:center;-ms-flex-pack:distribute;justify-content:space-around}.ngb-tp-spacer{width:1em;text-align:center}"]
                    }] }
        ];
        /** @nocollapse */
        NgbTimepicker.ctorParameters = function () { return [
            { type: NgbTimepickerConfig },
            { type: NgbTimeAdapter },
            { type: core.ChangeDetectorRef },
            { type: NgbTimepickerI18n }
        ]; };
        NgbTimepicker.propDecorators = {
            meridian: [{ type: core.Input }],
            spinners: [{ type: core.Input }],
            seconds: [{ type: core.Input }],
            hourStep: [{ type: core.Input }],
            minuteStep: [{ type: core.Input }],
            secondStep: [{ type: core.Input }],
            readonlyInputs: [{ type: core.Input }],
            size: [{ type: core.Input }]
        };
        return NgbTimepicker;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbTimepickerModule = /** @class */ (function () {
        function NgbTimepickerModule() {
        }
        NgbTimepickerModule.decorators = [
            { type: core.NgModule, args: [{ declarations: [NgbTimepicker], exports: [NgbTimepicker], imports: [common.CommonModule] },] }
        ];
        return NgbTimepickerModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * Configuration service for the NgbToast component. You can inject this service, typically in your root component,
     * and customize the values of its properties in order to provide default values for all the toasts used in the
     * application.
     *
     * \@since 5.0.0
     */
    var NgbToastConfig = /** @class */ (function () {
        function NgbToastConfig() {
            this.autohide = true;
            this.delay = 500;
            this.ariaLive = 'polite';
        }
        NgbToastConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbToastConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbToastConfig_Factory() { return new NgbToastConfig(); }, token: NgbToastConfig, providedIn: "root" });
        return NgbToastConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * This directive allows the usage of HTML markup or other directives
     * inside of the toast's header.
     *
     * \@since 5.0.0
     */
    var NgbToastHeader = /** @class */ (function () {
        function NgbToastHeader() {
        }
        NgbToastHeader.decorators = [
            { type: core.Directive, args: [{ selector: '[ngbToastHeader]' },] }
        ];
        return NgbToastHeader;
    }());
    /**
     * Toasts provide feedback messages as notifications to the user.
     * Goal is to mimic the push notifications available both on mobile and desktop operating systems.
     *
     * \@since 5.0.0
     */
    var NgbToast = /** @class */ (function () {
        function NgbToast(ariaLive, config) {
            this.ariaLive = ariaLive;
            /**
             * A template like `<ng-template ngbToastHeader></ng-template>` can be
             * used in the projected content to allow markup usage.
             */
            this.contentHeaderTpl = null;
            /**
             * An event fired immediately when toast's `hide()` method has been called.
             * It can only occur in 2 different scenarios:
             * - `autohide` timeout fires
             * - user clicks on a closing cross (&times)
             *
             * Additionally this output is purely informative. The toast won't disappear. It's up to the user to take care of
             * that.
             */
            this.hideOutput = new core.EventEmitter();
            if (this.ariaLive == null) {
                this.ariaLive = config.ariaLive;
            }
            this.delay = config.delay;
            this.autohide = config.autohide;
        }
        /**
         * @return {?}
         */
        NgbToast.prototype.ngAfterContentInit = /**
         * @return {?}
         */
        function () { this._init(); };
        /**
         * @param {?} changes
         * @return {?}
         */
        NgbToast.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            if ('autohide' in changes) {
                this._clearTimeout();
                this._init();
            }
        };
        /**
         * @return {?}
         */
        NgbToast.prototype.hide = /**
         * @return {?}
         */
        function () {
            this._clearTimeout();
            this.hideOutput.emit();
        };
        /**
         * @private
         * @return {?}
         */
        NgbToast.prototype._init = /**
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            if (this.autohide && !this._timeoutID) {
                this._timeoutID = setTimeout((/**
                 * @return {?}
                 */
                function () { return _this.hide(); }), this.delay);
            }
        };
        /**
         * @private
         * @return {?}
         */
        NgbToast.prototype._clearTimeout = /**
         * @private
         * @return {?}
         */
        function () {
            if (this._timeoutID) {
                clearTimeout(this._timeoutID);
                this._timeoutID = null;
            }
        };
        NgbToast.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-toast',
                        exportAs: 'ngbToast',
                        encapsulation: core.ViewEncapsulation.None,
                        host: {
                            'role': 'alert',
                            '[attr.aria-live]': 'ariaLive',
                            'aria-atomic': 'true',
                            '[class.toast]': 'true',
                            '[class.show]': 'true',
                            '[class.autohide]': 'autohide',
                        },
                        template: "\n    <ng-template #headerTpl>\n      <strong class=\"mr-auto\">{{header}}</strong>\n    </ng-template>\n    <ng-template [ngIf]=\"contentHeaderTpl || header\">\n      <div class=\"toast-header\">\n        <ng-template [ngTemplateOutlet]=\"contentHeaderTpl || headerTpl\"></ng-template>\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" i18n-aria-label=\"@@ngb.toast.close-aria\" (click)=\"hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n    </ng-template>\n    <div class=\"toast-body\">\n      <ng-content></ng-content>\n    </div>\n  ",
                        styles: [".ngb-toasts{position:fixed;top:0;right:0;margin:.5em;z-index:1200}ngb-toast .toast-header .close{margin-left:auto;margin-bottom:.25rem}"]
                    }] }
        ];
        /** @nocollapse */
        NgbToast.ctorParameters = function () { return [
            { type: String, decorators: [{ type: core.Attribute, args: ['aria-live',] }] },
            { type: NgbToastConfig }
        ]; };
        NgbToast.propDecorators = {
            delay: [{ type: core.Input }],
            autohide: [{ type: core.Input }],
            header: [{ type: core.Input }],
            contentHeaderTpl: [{ type: core.ContentChild, args: [NgbToastHeader, { read: core.TemplateRef, static: true },] }],
            hideOutput: [{ type: core.Output, args: ['hide',] }]
        };
        return NgbToast;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbToastModule = /** @class */ (function () {
        function NgbToastModule() {
        }
        NgbToastModule.decorators = [
            { type: core.NgModule, args: [{ declarations: [NgbToast, NgbToastHeader], imports: [common.CommonModule], exports: [NgbToast, NgbToastHeader] },] }
        ];
        return NgbToastModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A configuration service for the [`NgbTooltip`](#/components/tooltip/api#NgbTooltip) component.
     *
     * You can inject this service, typically in your root component, and customize the values of its properties in
     * order to provide default values for all the tooltips used in the application.
     */
    var NgbTooltipConfig = /** @class */ (function () {
        function NgbTooltipConfig() {
            this.autoClose = true;
            this.placement = 'auto';
            this.triggers = 'hover focus';
            this.disableTooltip = false;
            this.openDelay = 0;
            this.closeDelay = 0;
        }
        NgbTooltipConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbTooltipConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbTooltipConfig_Factory() { return new NgbTooltipConfig(); }, token: NgbTooltipConfig, providedIn: "root" });
        return NgbTooltipConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var nextId$5 = 0;
    var NgbTooltipWindow = /** @class */ (function () {
        function NgbTooltipWindow() {
        }
        NgbTooltipWindow.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-tooltip-window',
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        encapsulation: core.ViewEncapsulation.None,
                        host: { '[class]': '"tooltip show" + (tooltipClass ? " " + tooltipClass : "")', 'role': 'tooltip', '[id]': 'id' },
                        template: "<div class=\"arrow\"></div><div class=\"tooltip-inner\"><ng-content></ng-content></div>",
                        styles: ["ngb-tooltip-window.bs-tooltip-bottom .arrow,ngb-tooltip-window.bs-tooltip-top .arrow{left:calc(50% - .4rem)}ngb-tooltip-window.bs-tooltip-bottom-left .arrow,ngb-tooltip-window.bs-tooltip-top-left .arrow{left:1em}ngb-tooltip-window.bs-tooltip-bottom-right .arrow,ngb-tooltip-window.bs-tooltip-top-right .arrow{left:auto;right:.8rem}ngb-tooltip-window.bs-tooltip-left .arrow,ngb-tooltip-window.bs-tooltip-right .arrow{top:calc(50% - .4rem)}ngb-tooltip-window.bs-tooltip-left-top .arrow,ngb-tooltip-window.bs-tooltip-right-top .arrow{top:.4rem}ngb-tooltip-window.bs-tooltip-left-bottom .arrow,ngb-tooltip-window.bs-tooltip-right-bottom .arrow{top:auto;bottom:.4rem}"]
                    }] }
        ];
        NgbTooltipWindow.propDecorators = {
            id: [{ type: core.Input }],
            tooltipClass: [{ type: core.Input }]
        };
        return NgbTooltipWindow;
    }());
    /**
     * A lightweight and extensible directive for fancy tooltip creation.
     */
    var NgbTooltip = /** @class */ (function () {
        function NgbTooltip(_elementRef, _renderer, injector, componentFactoryResolver, viewContainerRef, config, _ngZone, _document, _changeDetector, _applicationRef) {
            var _this = this;
            this._elementRef = _elementRef;
            this._renderer = _renderer;
            this._ngZone = _ngZone;
            this._document = _document;
            this._changeDetector = _changeDetector;
            this._applicationRef = _applicationRef;
            /**
             * An event emitted when the tooltip is shown. Contains no payload.
             */
            this.shown = new core.EventEmitter();
            /**
             * An event emitted when the popover is hidden. Contains no payload.
             */
            this.hidden = new core.EventEmitter();
            this._ngbTooltipWindowId = "ngb-tooltip-" + nextId$5++;
            this.autoClose = config.autoClose;
            this.placement = config.placement;
            this.triggers = config.triggers;
            this.container = config.container;
            this.disableTooltip = config.disableTooltip;
            this.tooltipClass = config.tooltipClass;
            this.openDelay = config.openDelay;
            this.closeDelay = config.closeDelay;
            this._popupService = new PopupService(NgbTooltipWindow, injector, viewContainerRef, _renderer, componentFactoryResolver, _applicationRef);
            this._zoneSubscription = _ngZone.onStable.subscribe((/**
             * @return {?}
             */
            function () {
                if (_this._windowRef) {
                    positionElements(_this._elementRef.nativeElement, _this._windowRef.location.nativeElement, _this.placement, _this.container === 'body', 'bs-tooltip');
                }
            }));
        }
        Object.defineProperty(NgbTooltip.prototype, "ngbTooltip", {
            get: /**
             * @return {?}
             */
            function () { return this._ngbTooltip; },
            /**
             * The string content or a `TemplateRef` for the content to be displayed in the tooltip.
             *
             * If the content if falsy, the tooltip won't open.
             */
            set: /**
             * The string content or a `TemplateRef` for the content to be displayed in the tooltip.
             *
             * If the content if falsy, the tooltip won't open.
             * @param {?} value
             * @return {?}
             */
            function (value) {
                this._ngbTooltip = value;
                if (!value && this._windowRef) {
                    this.close();
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * Opens the tooltip.
         *
         * This is considered to be a "manual" triggering.
         * The `context` is an optional value to be injected into the tooltip template when it is created.
         */
        /**
         * Opens the tooltip.
         *
         * This is considered to be a "manual" triggering.
         * The `context` is an optional value to be injected into the tooltip template when it is created.
         * @param {?=} context
         * @return {?}
         */
        NgbTooltip.prototype.open = /**
         * Opens the tooltip.
         *
         * This is considered to be a "manual" triggering.
         * The `context` is an optional value to be injected into the tooltip template when it is created.
         * @param {?=} context
         * @return {?}
         */
        function (context) {
            var _this = this;
            if (!this._windowRef && this._ngbTooltip && !this.disableTooltip) {
                this._windowRef = this._popupService.open(this._ngbTooltip, context);
                this._windowRef.instance.tooltipClass = this.tooltipClass;
                this._windowRef.instance.id = this._ngbTooltipWindowId;
                this._renderer.setAttribute(this._elementRef.nativeElement, 'aria-describedby', this._ngbTooltipWindowId);
                if (this.container === 'body') {
                    this._document.querySelector(this.container).appendChild(this._windowRef.location.nativeElement);
                }
                // We need to detect changes, because we don't know where .open() might be called from.
                // Ex. opening tooltip from one of lifecycle hooks that run after the CD
                // (say from ngAfterViewInit) will result in 'ExpressionHasChanged' exception
                this._windowRef.changeDetectorRef.detectChanges();
                // We need to mark for check, because tooltip won't work inside the OnPush component.
                // Ex. when we use expression like `{{ tooltip.isOpen() : 'opened' : 'closed' }}`
                // inside the template of an OnPush component and we change the tooltip from
                // open -> closed, the expression in question won't be updated unless we explicitly
                // mark the parent component to be checked.
                this._windowRef.changeDetectorRef.markForCheck();
                ngbAutoClose(this._ngZone, this._document, this.autoClose, (/**
                 * @return {?}
                 */
                function () { return _this.close(); }), this.hidden, [this._windowRef.location.nativeElement]);
                this.shown.emit();
            }
        };
        /**
         * Closes the tooltip.
         *
         * This is considered to be a "manual" triggering of the tooltip.
         */
        /**
         * Closes the tooltip.
         *
         * This is considered to be a "manual" triggering of the tooltip.
         * @return {?}
         */
        NgbTooltip.prototype.close = /**
         * Closes the tooltip.
         *
         * This is considered to be a "manual" triggering of the tooltip.
         * @return {?}
         */
        function () {
            if (this._windowRef != null) {
                this._renderer.removeAttribute(this._elementRef.nativeElement, 'aria-describedby');
                this._popupService.close();
                this._windowRef = null;
                this.hidden.emit();
                this._changeDetector.markForCheck();
            }
        };
        /**
         * Toggles the tooltip.
         *
         * This is considered to be a "manual" triggering of the tooltip.
         */
        /**
         * Toggles the tooltip.
         *
         * This is considered to be a "manual" triggering of the tooltip.
         * @return {?}
         */
        NgbTooltip.prototype.toggle = /**
         * Toggles the tooltip.
         *
         * This is considered to be a "manual" triggering of the tooltip.
         * @return {?}
         */
        function () {
            if (this._windowRef) {
                this.close();
            }
            else {
                this.open();
            }
        };
        /**
         * Returns `true`, if the popover is currently shown.
         */
        /**
         * Returns `true`, if the popover is currently shown.
         * @return {?}
         */
        NgbTooltip.prototype.isOpen = /**
         * Returns `true`, if the popover is currently shown.
         * @return {?}
         */
        function () { return this._windowRef != null; };
        /**
         * @return {?}
         */
        NgbTooltip.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            this._unregisterListenersFn = listenToTriggers(this._renderer, this._elementRef.nativeElement, this.triggers, this.isOpen.bind(this), this.open.bind(this), this.close.bind(this), +this.openDelay, +this.closeDelay);
        };
        /**
         * @return {?}
         */
        NgbTooltip.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            this.close();
            // This check is needed as it might happen that ngOnDestroy is called before ngOnInit
            // under certain conditions, see: https://github.com/ng-bootstrap/ng-bootstrap/issues/2199
            if (this._unregisterListenersFn) {
                this._unregisterListenersFn();
            }
            this._zoneSubscription.unsubscribe();
        };
        NgbTooltip.decorators = [
            { type: core.Directive, args: [{ selector: '[ngbTooltip]', exportAs: 'ngbTooltip' },] }
        ];
        /** @nocollapse */
        NgbTooltip.ctorParameters = function () { return [
            { type: core.ElementRef },
            { type: core.Renderer2 },
            { type: core.Injector },
            { type: core.ComponentFactoryResolver },
            { type: core.ViewContainerRef },
            { type: NgbTooltipConfig },
            { type: core.NgZone },
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
            { type: core.ChangeDetectorRef },
            { type: core.ApplicationRef }
        ]; };
        NgbTooltip.propDecorators = {
            autoClose: [{ type: core.Input }],
            placement: [{ type: core.Input }],
            triggers: [{ type: core.Input }],
            container: [{ type: core.Input }],
            disableTooltip: [{ type: core.Input }],
            tooltipClass: [{ type: core.Input }],
            openDelay: [{ type: core.Input }],
            closeDelay: [{ type: core.Input }],
            shown: [{ type: core.Output }],
            hidden: [{ type: core.Output }],
            ngbTooltip: [{ type: core.Input }]
        };
        return NgbTooltip;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbTooltipModule = /** @class */ (function () {
        function NgbTooltipModule() {
        }
        NgbTooltipModule.decorators = [
            { type: core.NgModule, args: [{ declarations: [NgbTooltip, NgbTooltipWindow], exports: [NgbTooltip], entryComponents: [NgbTooltipWindow] },] }
        ];
        return NgbTooltipModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A component that helps with text highlighting.
     *
     * If splits the `result` text into parts that contain the searched `term` and generates the HTML markup to simplify
     * highlighting:
     *
     * Ex. `result="Alaska"` and `term="as"` will produce `Al<span class="ngb-highlight">as</span>ka`.
     */
    var NgbHighlight = /** @class */ (function () {
        function NgbHighlight() {
            /**
             * The CSS class for `<span>` elements wrapping the `term` inside the `result`.
             */
            this.highlightClass = 'ngb-highlight';
        }
        /**
         * @param {?} changes
         * @return {?}
         */
        NgbHighlight.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            /** @type {?} */
            var result = toString(this.result);
            /** @type {?} */
            var terms = Array.isArray(this.term) ? this.term : [this.term];
            /** @type {?} */
            var escapedTerms = terms.map((/**
             * @param {?} term
             * @return {?}
             */
            function (term) { return regExpEscape(toString(term)); })).filter((/**
             * @param {?} term
             * @return {?}
             */
            function (term) { return term; }));
            this.parts = escapedTerms.length ? result.split(new RegExp("(" + escapedTerms.join('|') + ")", 'gmi')) : [result];
        };
        NgbHighlight.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-highlight',
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        encapsulation: core.ViewEncapsulation.None,
                        template: "<ng-template ngFor [ngForOf]=\"parts\" let-part let-isOdd=\"odd\">" +
                            "<span *ngIf=\"isOdd; else even\" [class]=\"highlightClass\">{{part}}</span><ng-template #even>{{part}}</ng-template>" +
                            "</ng-template>",
                        styles: [".ngb-highlight{font-weight:700}"]
                    }] }
        ];
        NgbHighlight.propDecorators = {
            highlightClass: [{ type: core.Input }],
            result: [{ type: core.Input }],
            term: [{ type: core.Input }]
        };
        return NgbHighlight;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbTypeaheadWindow = /** @class */ (function () {
        function NgbTypeaheadWindow() {
            this.activeIdx = 0;
            /**
             * Flag indicating if the first row should be active initially
             */
            this.focusFirst = true;
            /**
             * A function used to format a given result before display. This function should return a formatted string without any
             * HTML markup
             */
            this.formatter = toString;
            /**
             * Event raised when user selects a particular result row
             */
            this.selectEvent = new core.EventEmitter();
            this.activeChangeEvent = new core.EventEmitter();
        }
        /**
         * @return {?}
         */
        NgbTypeaheadWindow.prototype.hasActive = /**
         * @return {?}
         */
        function () { return this.activeIdx > -1 && this.activeIdx < this.results.length; };
        /**
         * @return {?}
         */
        NgbTypeaheadWindow.prototype.getActive = /**
         * @return {?}
         */
        function () { return this.results[this.activeIdx]; };
        /**
         * @param {?} activeIdx
         * @return {?}
         */
        NgbTypeaheadWindow.prototype.markActive = /**
         * @param {?} activeIdx
         * @return {?}
         */
        function (activeIdx) {
            this.activeIdx = activeIdx;
            this._activeChanged();
        };
        /**
         * @return {?}
         */
        NgbTypeaheadWindow.prototype.next = /**
         * @return {?}
         */
        function () {
            if (this.activeIdx === this.results.length - 1) {
                this.activeIdx = this.focusFirst ? (this.activeIdx + 1) % this.results.length : -1;
            }
            else {
                this.activeIdx++;
            }
            this._activeChanged();
        };
        /**
         * @return {?}
         */
        NgbTypeaheadWindow.prototype.prev = /**
         * @return {?}
         */
        function () {
            if (this.activeIdx < 0) {
                this.activeIdx = this.results.length - 1;
            }
            else if (this.activeIdx === 0) {
                this.activeIdx = this.focusFirst ? this.results.length - 1 : -1;
            }
            else {
                this.activeIdx--;
            }
            this._activeChanged();
        };
        /**
         * @return {?}
         */
        NgbTypeaheadWindow.prototype.resetActive = /**
         * @return {?}
         */
        function () {
            this.activeIdx = this.focusFirst ? 0 : -1;
            this._activeChanged();
        };
        /**
         * @param {?} item
         * @return {?}
         */
        NgbTypeaheadWindow.prototype.select = /**
         * @param {?} item
         * @return {?}
         */
        function (item) { this.selectEvent.emit(item); };
        /**
         * @return {?}
         */
        NgbTypeaheadWindow.prototype.ngOnInit = /**
         * @return {?}
         */
        function () { this.resetActive(); };
        /**
         * @private
         * @return {?}
         */
        NgbTypeaheadWindow.prototype._activeChanged = /**
         * @private
         * @return {?}
         */
        function () {
            this.activeChangeEvent.emit(this.activeIdx >= 0 ? this.id + '-' + this.activeIdx : undefined);
        };
        NgbTypeaheadWindow.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngb-typeahead-window',
                        exportAs: 'ngbTypeaheadWindow',
                        host: { '(mousedown)': '$event.preventDefault()', 'class': 'dropdown-menu show', 'role': 'listbox', '[id]': 'id' },
                        template: "\n    <ng-template #rt let-result=\"result\" let-term=\"term\" let-formatter=\"formatter\">\n      <ngb-highlight [result]=\"formatter(result)\" [term]=\"term\"></ngb-highlight>\n    </ng-template>\n    <ng-template ngFor [ngForOf]=\"results\" let-result let-idx=\"index\">\n      <button type=\"button\" class=\"dropdown-item\" role=\"option\"\n        [id]=\"id + '-' + idx\"\n        [class.active]=\"idx === activeIdx\"\n        (mouseenter)=\"markActive(idx)\"\n        (click)=\"select(result)\">\n          <ng-template [ngTemplateOutlet]=\"resultTemplate || rt\"\n          [ngTemplateOutletContext]=\"{result: result, term: term, formatter: formatter}\"></ng-template>\n      </button>\n    </ng-template>\n  "
                    }] }
        ];
        NgbTypeaheadWindow.propDecorators = {
            id: [{ type: core.Input }],
            focusFirst: [{ type: core.Input }],
            results: [{ type: core.Input }],
            term: [{ type: core.Input }],
            formatter: [{ type: core.Input }],
            resultTemplate: [{ type: core.Input }],
            selectEvent: [{ type: core.Output, args: ['select',] }],
            activeChangeEvent: [{ type: core.Output, args: ['activeChange',] }]
        };
        return NgbTypeaheadWindow;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var ARIA_LIVE_DELAY = new core.InjectionToken('live announcer delay', { providedIn: 'root', factory: ARIA_LIVE_DELAY_FACTORY });
    /**
     * @return {?}
     */
    function ARIA_LIVE_DELAY_FACTORY() {
        return 100;
    }
    /**
     * @param {?} document
     * @param {?=} lazyCreate
     * @return {?}
     */
    function getLiveElement(document, lazyCreate) {
        if (lazyCreate === void 0) { lazyCreate = false; }
        /** @type {?} */
        var element = (/** @type {?} */ (document.body.querySelector('#ngb-live')));
        if (element == null && lazyCreate) {
            element = document.createElement('div');
            element.setAttribute('id', 'ngb-live');
            element.setAttribute('aria-live', 'polite');
            element.setAttribute('aria-atomic', 'true');
            element.classList.add('sr-only');
            document.body.appendChild(element);
        }
        return element;
    }
    var Live = /** @class */ (function () {
        function Live(_document, _delay) {
            this._document = _document;
            this._delay = _delay;
        }
        /**
         * @return {?}
         */
        Live.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var element = getLiveElement(this._document);
            if (element) {
                element.parentElement.removeChild(element);
            }
        };
        /**
         * @param {?} message
         * @return {?}
         */
        Live.prototype.say = /**
         * @param {?} message
         * @return {?}
         */
        function (message) {
            /** @type {?} */
            var element = getLiveElement(this._document, true);
            /** @type {?} */
            var delay = this._delay;
            element.textContent = '';
            /** @type {?} */
            var setText = (/**
             * @return {?}
             */
            function () { return element.textContent = message; });
            if (delay === null) {
                setText();
            }
            else {
                setTimeout(setText, delay);
            }
        };
        Live.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */
        Live.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
            { type: undefined, decorators: [{ type: core.Inject, args: [ARIA_LIVE_DELAY,] }] }
        ]; };
        /** @nocollapse */ Live.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function Live_Factory() { return new Live(core.ɵɵinject(common.DOCUMENT), core.ɵɵinject(ARIA_LIVE_DELAY)); }, token: Live, providedIn: "root" });
        return Live;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * A configuration service for the [`NgbTypeahead`](#/components/typeahead/api#NgbTypeahead) component.
     *
     * You can inject this service, typically in your root component, and customize the values of its properties in
     * order to provide default values for all the typeaheads used in the application.
     */
    var NgbTypeaheadConfig = /** @class */ (function () {
        function NgbTypeaheadConfig() {
            this.editable = true;
            this.focusFirst = true;
            this.showHint = false;
            this.placement = ['bottom-left', 'bottom-right', 'top-left', 'top-right'];
        }
        NgbTypeaheadConfig.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] }
        ];
        /** @nocollapse */ NgbTypeaheadConfig.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NgbTypeaheadConfig_Factory() { return new NgbTypeaheadConfig(); }, token: NgbTypeaheadConfig, providedIn: "root" });
        return NgbTypeaheadConfig;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NGB_TYPEAHEAD_VALUE_ACCESSOR = {
        provide: forms.NG_VALUE_ACCESSOR,
        useExisting: core.forwardRef((/**
         * @return {?}
         */
        function () { return NgbTypeahead; })),
        multi: true
    };
    /** @type {?} */
    var nextWindowId = 0;
    /**
     * A directive providing a simple way of creating powerful typeaheads from any text input.
     */
    var NgbTypeahead = /** @class */ (function () {
        function NgbTypeahead(_elementRef, _viewContainerRef, _renderer, _injector, componentFactoryResolver, config, ngZone, _live, _document, _ngZone, _changeDetector, _applicationRef) {
            var _this = this;
            this._elementRef = _elementRef;
            this._viewContainerRef = _viewContainerRef;
            this._renderer = _renderer;
            this._injector = _injector;
            this._live = _live;
            this._document = _document;
            this._ngZone = _ngZone;
            this._changeDetector = _changeDetector;
            this._applicationRef = _applicationRef;
            this._closed$ = new rxjs.Subject();
            /**
             * The value for the `autocomplete` attribute for the `<input>` element.
             *
             * Defaults to `"off"` to disable the native browser autocomplete, but you can override it if necessary.
             *
             * \@since 2.1.0
             */
            this.autocomplete = 'off';
            /**
             * The preferred placement of the typeahead.
             *
             * Possible values are `"top"`, `"top-left"`, `"top-right"`, `"bottom"`, `"bottom-left"`,
             * `"bottom-right"`, `"left"`, `"left-top"`, `"left-bottom"`, `"right"`, `"right-top"`,
             * `"right-bottom"`
             *
             * Accepts an array of strings or a string with space separated possible values.
             *
             * The default order of preference is `"bottom-left bottom-right top-left top-right"`
             *
             * Please see the [positioning overview](#/positioning) for more details.
             */
            this.placement = 'bottom-left';
            /**
             * An event emitted right before an item is selected from the result list.
             *
             * Event payload is of type [`NgbTypeaheadSelectItemEvent`](#/components/typeahead/api#NgbTypeaheadSelectItemEvent).
             */
            this.selectItem = new core.EventEmitter();
            this.popupId = "ngb-typeahead-" + nextWindowId++;
            this._onTouched = (/**
             * @return {?}
             */
            function () { });
            this._onChange = (/**
             * @param {?} _
             * @return {?}
             */
            function (_) { });
            this.container = config.container;
            this.editable = config.editable;
            this.focusFirst = config.focusFirst;
            this.showHint = config.showHint;
            this.placement = config.placement;
            this._valueChanges = rxjs.fromEvent(_elementRef.nativeElement, 'input')
                .pipe(operators.map((/**
             * @param {?} $event
             * @return {?}
             */
            function ($event) { return ((/** @type {?} */ ($event.target))).value; })));
            this._resubscribeTypeahead = new rxjs.BehaviorSubject(null);
            this._popupService = new PopupService(NgbTypeaheadWindow, _injector, _viewContainerRef, _renderer, componentFactoryResolver, _applicationRef);
            this._zoneSubscription = ngZone.onStable.subscribe((/**
             * @return {?}
             */
            function () {
                if (_this.isPopupOpen()) {
                    positionElements(_this._elementRef.nativeElement, _this._windowRef.location.nativeElement, _this.placement, _this.container === 'body');
                }
            }));
        }
        /**
         * @return {?}
         */
        NgbTypeahead.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            /** @type {?} */
            var inputValues$ = this._valueChanges.pipe(operators.tap((/**
             * @param {?} value
             * @return {?}
             */
            function (value) {
                _this._inputValueBackup = _this.showHint ? value : null;
                if (_this.editable) {
                    _this._onChange(value);
                }
            })));
            /** @type {?} */
            var results$ = inputValues$.pipe(this.ngbTypeahead);
            /** @type {?} */
            var processedResults$ = results$.pipe(operators.tap((/**
             * @return {?}
             */
            function () {
                if (!_this.editable) {
                    _this._onChange(undefined);
                }
            })));
            /** @type {?} */
            var userInput$ = this._resubscribeTypeahead.pipe(operators.switchMap((/**
             * @return {?}
             */
            function () { return processedResults$; })));
            this._subscription = this._subscribeToUserInput(userInput$);
        };
        /**
         * @return {?}
         */
        NgbTypeahead.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            this._closePopup();
            this._unsubscribeFromUserInput();
            this._zoneSubscription.unsubscribe();
        };
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbTypeahead.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this._onChange = fn; };
        /**
         * @param {?} fn
         * @return {?}
         */
        NgbTypeahead.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) { this._onTouched = fn; };
        /**
         * @param {?} value
         * @return {?}
         */
        NgbTypeahead.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._writeInputValue(this._formatItemForInput(value));
            if (this.showHint) {
                this._inputValueBackup = value;
            }
        };
        /**
         * @param {?} isDisabled
         * @return {?}
         */
        NgbTypeahead.prototype.setDisabledState = /**
         * @param {?} isDisabled
         * @return {?}
         */
        function (isDisabled) {
            this._renderer.setProperty(this._elementRef.nativeElement, 'disabled', isDisabled);
        };
        /**
         * Dismisses typeahead popup window
         */
        /**
         * Dismisses typeahead popup window
         * @return {?}
         */
        NgbTypeahead.prototype.dismissPopup = /**
         * Dismisses typeahead popup window
         * @return {?}
         */
        function () {
            if (this.isPopupOpen()) {
                this._resubscribeTypeahead.next(null);
                this._closePopup();
                if (this.showHint && this._inputValueBackup !== null) {
                    this._writeInputValue(this._inputValueBackup);
                }
                this._changeDetector.markForCheck();
            }
        };
        /**
         * Returns true if the typeahead popup window is displayed
         */
        /**
         * Returns true if the typeahead popup window is displayed
         * @return {?}
         */
        NgbTypeahead.prototype.isPopupOpen = /**
         * Returns true if the typeahead popup window is displayed
         * @return {?}
         */
        function () { return this._windowRef != null; };
        /**
         * @return {?}
         */
        NgbTypeahead.prototype.handleBlur = /**
         * @return {?}
         */
        function () {
            this._resubscribeTypeahead.next(null);
            this._onTouched();
        };
        /**
         * @param {?} event
         * @return {?}
         */
        NgbTypeahead.prototype.handleKeyDown = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            if (!this.isPopupOpen()) {
                return;
            }
            // tslint:disable-next-line:deprecation
            switch (event.which) {
                case Key.ArrowDown:
                    event.preventDefault();
                    this._windowRef.instance.next();
                    this._showHint();
                    break;
                case Key.ArrowUp:
                    event.preventDefault();
                    this._windowRef.instance.prev();
                    this._showHint();
                    break;
                case Key.Enter:
                case Key.Tab:
                    /** @type {?} */
                    var result = this._windowRef.instance.getActive();
                    if (isDefined(result)) {
                        event.preventDefault();
                        event.stopPropagation();
                        this._selectResult(result);
                    }
                    this._closePopup();
                    break;
            }
        };
        /**
         * @private
         * @return {?}
         */
        NgbTypeahead.prototype._openPopup = /**
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            if (!this.isPopupOpen()) {
                this._inputValueBackup = this._elementRef.nativeElement.value;
                this._windowRef = this._popupService.open();
                this._windowRef.instance.id = this.popupId;
                this._windowRef.instance.selectEvent.subscribe((/**
                 * @param {?} result
                 * @return {?}
                 */
                function (result) { return _this._selectResultClosePopup(result); }));
                this._windowRef.instance.activeChangeEvent.subscribe((/**
                 * @param {?} activeId
                 * @return {?}
                 */
                function (activeId) { return _this.activeDescendant = activeId; }));
                if (this.container === 'body') {
                    window.document.querySelector(this.container).appendChild(this._windowRef.location.nativeElement);
                }
                this._changeDetector.markForCheck();
                ngbAutoClose(this._ngZone, this._document, 'outside', (/**
                 * @return {?}
                 */
                function () { return _this.dismissPopup(); }), this._closed$, [this._elementRef.nativeElement, this._windowRef.location.nativeElement]);
            }
        };
        /**
         * @private
         * @return {?}
         */
        NgbTypeahead.prototype._closePopup = /**
         * @private
         * @return {?}
         */
        function () {
            this._closed$.next();
            this._popupService.close();
            this._windowRef = null;
            this.activeDescendant = undefined;
        };
        /**
         * @private
         * @param {?} result
         * @return {?}
         */
        NgbTypeahead.prototype._selectResult = /**
         * @private
         * @param {?} result
         * @return {?}
         */
        function (result) {
            /** @type {?} */
            var defaultPrevented = false;
            this.selectItem.emit({ item: result, preventDefault: (/**
                 * @return {?}
                 */
                function () { defaultPrevented = true; }) });
            this._resubscribeTypeahead.next(null);
            if (!defaultPrevented) {
                this.writeValue(result);
                this._onChange(result);
            }
        };
        /**
         * @private
         * @param {?} result
         * @return {?}
         */
        NgbTypeahead.prototype._selectResultClosePopup = /**
         * @private
         * @param {?} result
         * @return {?}
         */
        function (result) {
            this._selectResult(result);
            this._closePopup();
        };
        /**
         * @private
         * @return {?}
         */
        NgbTypeahead.prototype._showHint = /**
         * @private
         * @return {?}
         */
        function () {
            if (this.showHint && this._windowRef.instance.hasActive() && this._inputValueBackup != null) {
                /** @type {?} */
                var userInputLowerCase = this._inputValueBackup.toLowerCase();
                /** @type {?} */
                var formattedVal = this._formatItemForInput(this._windowRef.instance.getActive());
                if (userInputLowerCase === formattedVal.substr(0, this._inputValueBackup.length).toLowerCase()) {
                    this._writeInputValue(this._inputValueBackup + formattedVal.substr(this._inputValueBackup.length));
                    this._elementRef.nativeElement['setSelectionRange'].apply(this._elementRef.nativeElement, [this._inputValueBackup.length, formattedVal.length]);
                }
                else {
                    this._writeInputValue(formattedVal);
                }
            }
        };
        /**
         * @private
         * @param {?} item
         * @return {?}
         */
        NgbTypeahead.prototype._formatItemForInput = /**
         * @private
         * @param {?} item
         * @return {?}
         */
        function (item) {
            return item != null && this.inputFormatter ? this.inputFormatter(item) : toString(item);
        };
        /**
         * @private
         * @param {?} value
         * @return {?}
         */
        NgbTypeahead.prototype._writeInputValue = /**
         * @private
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._renderer.setProperty(this._elementRef.nativeElement, 'value', toString(value));
        };
        /**
         * @private
         * @param {?} userInput$
         * @return {?}
         */
        NgbTypeahead.prototype._subscribeToUserInput = /**
         * @private
         * @param {?} userInput$
         * @return {?}
         */
        function (userInput$) {
            var _this = this;
            return userInput$.subscribe((/**
             * @param {?} results
             * @return {?}
             */
            function (results) {
                if (!results || results.length === 0) {
                    _this._closePopup();
                }
                else {
                    _this._openPopup();
                    _this._windowRef.instance.focusFirst = _this.focusFirst;
                    _this._windowRef.instance.results = results;
                    _this._windowRef.instance.term = _this._elementRef.nativeElement.value;
                    if (_this.resultFormatter) {
                        _this._windowRef.instance.formatter = _this.resultFormatter;
                    }
                    if (_this.resultTemplate) {
                        _this._windowRef.instance.resultTemplate = _this.resultTemplate;
                    }
                    _this._windowRef.instance.resetActive();
                    // The observable stream we are subscribing to might have async steps
                    // and if a component containing typeahead is using the OnPush strategy
                    // the change detection turn wouldn't be invoked automatically.
                    _this._windowRef.changeDetectorRef.detectChanges();
                    _this._showHint();
                }
                // live announcer
                /** @type {?} */
                var count = results ? results.length : 0;
                _this._live.say(count === 0 ? 'No results available' : count + " result" + (count === 1 ? '' : 's') + " available");
            }));
        };
        /**
         * @private
         * @return {?}
         */
        NgbTypeahead.prototype._unsubscribeFromUserInput = /**
         * @private
         * @return {?}
         */
        function () {
            if (this._subscription) {
                this._subscription.unsubscribe();
            }
            this._subscription = null;
        };
        NgbTypeahead.decorators = [
            { type: core.Directive, args: [{
                        selector: 'input[ngbTypeahead]',
                        exportAs: 'ngbTypeahead',
                        host: {
                            '(blur)': 'handleBlur()',
                            '[class.open]': 'isPopupOpen()',
                            '(keydown)': 'handleKeyDown($event)',
                            '[autocomplete]': 'autocomplete',
                            'autocapitalize': 'off',
                            'autocorrect': 'off',
                            'role': 'combobox',
                            'aria-multiline': 'false',
                            '[attr.aria-autocomplete]': 'showHint ? "both" : "list"',
                            '[attr.aria-activedescendant]': 'activeDescendant',
                            '[attr.aria-owns]': 'isPopupOpen() ? popupId : null',
                            '[attr.aria-expanded]': 'isPopupOpen()'
                        },
                        providers: [NGB_TYPEAHEAD_VALUE_ACCESSOR]
                    },] }
        ];
        /** @nocollapse */
        NgbTypeahead.ctorParameters = function () { return [
            { type: core.ElementRef },
            { type: core.ViewContainerRef },
            { type: core.Renderer2 },
            { type: core.Injector },
            { type: core.ComponentFactoryResolver },
            { type: NgbTypeaheadConfig },
            { type: core.NgZone },
            { type: Live },
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
            { type: core.NgZone },
            { type: core.ChangeDetectorRef },
            { type: core.ApplicationRef }
        ]; };
        NgbTypeahead.propDecorators = {
            autocomplete: [{ type: core.Input }],
            container: [{ type: core.Input }],
            editable: [{ type: core.Input }],
            focusFirst: [{ type: core.Input }],
            inputFormatter: [{ type: core.Input }],
            ngbTypeahead: [{ type: core.Input }],
            resultFormatter: [{ type: core.Input }],
            resultTemplate: [{ type: core.Input }],
            showHint: [{ type: core.Input }],
            placement: [{ type: core.Input }],
            selectItem: [{ type: core.Output }]
        };
        return NgbTypeahead;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgbTypeaheadModule = /** @class */ (function () {
        function NgbTypeaheadModule() {
        }
        NgbTypeaheadModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [NgbTypeahead, NgbHighlight, NgbTypeaheadWindow],
                        exports: [NgbTypeahead, NgbHighlight],
                        imports: [common.CommonModule],
                        entryComponents: [NgbTypeaheadWindow]
                    },] }
        ];
        return NgbTypeaheadModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NGB_MODULES = [
        NgbAccordionModule, NgbAlertModule, NgbButtonsModule, NgbCarouselModule, NgbCollapseModule, NgbDatepickerModule,
        NgbDropdownModule, NgbModalModule, NgbPaginationModule, NgbPopoverModule, NgbProgressbarModule, NgbRatingModule,
        NgbTabsetModule, NgbTimepickerModule, NgbToastModule, NgbTooltipModule, NgbTypeaheadModule
    ];
    var NgbModule = /** @class */ (function () {
        function NgbModule() {
        }
        NgbModule.decorators = [
            { type: core.NgModule, args: [{ imports: NGB_MODULES, exports: NGB_MODULES },] }
        ];
        return NgbModule;
    }());

    exports.ModalDismissReasons = ModalDismissReasons;
    exports.NgbAccordion = NgbAccordion;
    exports.NgbAccordionConfig = NgbAccordionConfig;
    exports.NgbAccordionModule = NgbAccordionModule;
    exports.NgbActiveModal = NgbActiveModal;
    exports.NgbAlert = NgbAlert;
    exports.NgbAlertConfig = NgbAlertConfig;
    exports.NgbAlertModule = NgbAlertModule;
    exports.NgbButtonLabel = NgbButtonLabel;
    exports.NgbButtonsModule = NgbButtonsModule;
    exports.NgbCalendar = NgbCalendar;
    exports.NgbCalendarGregorian = NgbCalendarGregorian;
    exports.NgbCalendarHebrew = NgbCalendarHebrew;
    exports.NgbCalendarIslamicCivil = NgbCalendarIslamicCivil;
    exports.NgbCalendarIslamicUmalqura = NgbCalendarIslamicUmalqura;
    exports.NgbCalendarPersian = NgbCalendarPersian;
    exports.NgbCarousel = NgbCarousel;
    exports.NgbCarouselConfig = NgbCarouselConfig;
    exports.NgbCarouselModule = NgbCarouselModule;
    exports.NgbCheckBox = NgbCheckBox;
    exports.NgbCollapse = NgbCollapse;
    exports.NgbCollapseModule = NgbCollapseModule;
    exports.NgbDate = NgbDate;
    exports.NgbDateAdapter = NgbDateAdapter;
    exports.NgbDateNativeAdapter = NgbDateNativeAdapter;
    exports.NgbDateNativeUTCAdapter = NgbDateNativeUTCAdapter;
    exports.NgbDateParserFormatter = NgbDateParserFormatter;
    exports.NgbDatepicker = NgbDatepicker;
    exports.NgbDatepickerConfig = NgbDatepickerConfig;
    exports.NgbDatepickerI18n = NgbDatepickerI18n;
    exports.NgbDatepickerI18nHebrew = NgbDatepickerI18nHebrew;
    exports.NgbDatepickerModule = NgbDatepickerModule;
    exports.NgbDropdown = NgbDropdown;
    exports.NgbDropdownAnchor = NgbDropdownAnchor;
    exports.NgbDropdownConfig = NgbDropdownConfig;
    exports.NgbDropdownItem = NgbDropdownItem;
    exports.NgbDropdownMenu = NgbDropdownMenu;
    exports.NgbDropdownModule = NgbDropdownModule;
    exports.NgbDropdownToggle = NgbDropdownToggle;
    exports.NgbHighlight = NgbHighlight;
    exports.NgbInputDatepicker = NgbInputDatepicker;
    exports.NgbModal = NgbModal;
    exports.NgbModalConfig = NgbModalConfig;
    exports.NgbModalModule = NgbModalModule;
    exports.NgbModalRef = NgbModalRef;
    exports.NgbModule = NgbModule;
    exports.NgbPagination = NgbPagination;
    exports.NgbPaginationConfig = NgbPaginationConfig;
    exports.NgbPaginationEllipsis = NgbPaginationEllipsis;
    exports.NgbPaginationFirst = NgbPaginationFirst;
    exports.NgbPaginationLast = NgbPaginationLast;
    exports.NgbPaginationModule = NgbPaginationModule;
    exports.NgbPaginationNext = NgbPaginationNext;
    exports.NgbPaginationNumber = NgbPaginationNumber;
    exports.NgbPaginationPrevious = NgbPaginationPrevious;
    exports.NgbPanel = NgbPanel;
    exports.NgbPanelContent = NgbPanelContent;
    exports.NgbPanelHeader = NgbPanelHeader;
    exports.NgbPanelTitle = NgbPanelTitle;
    exports.NgbPanelToggle = NgbPanelToggle;
    exports.NgbPopover = NgbPopover;
    exports.NgbPopoverConfig = NgbPopoverConfig;
    exports.NgbPopoverModule = NgbPopoverModule;
    exports.NgbProgressbar = NgbProgressbar;
    exports.NgbProgressbarConfig = NgbProgressbarConfig;
    exports.NgbProgressbarModule = NgbProgressbarModule;
    exports.NgbRadio = NgbRadio;
    exports.NgbRadioGroup = NgbRadioGroup;
    exports.NgbRating = NgbRating;
    exports.NgbRatingConfig = NgbRatingConfig;
    exports.NgbRatingModule = NgbRatingModule;
    exports.NgbSlide = NgbSlide;
    exports.NgbSlideEventDirection = NgbSlideEventDirection;
    exports.NgbSlideEventSource = NgbSlideEventSource;
    exports.NgbTab = NgbTab;
    exports.NgbTabContent = NgbTabContent;
    exports.NgbTabTitle = NgbTabTitle;
    exports.NgbTabset = NgbTabset;
    exports.NgbTabsetConfig = NgbTabsetConfig;
    exports.NgbTabsetModule = NgbTabsetModule;
    exports.NgbTimeAdapter = NgbTimeAdapter;
    exports.NgbTimepicker = NgbTimepicker;
    exports.NgbTimepickerConfig = NgbTimepickerConfig;
    exports.NgbTimepickerI18n = NgbTimepickerI18n;
    exports.NgbTimepickerModule = NgbTimepickerModule;
    exports.NgbToast = NgbToast;
    exports.NgbToastConfig = NgbToastConfig;
    exports.NgbToastHeader = NgbToastHeader;
    exports.NgbToastModule = NgbToastModule;
    exports.NgbTooltip = NgbTooltip;
    exports.NgbTooltipConfig = NgbTooltipConfig;
    exports.NgbTooltipModule = NgbTooltipModule;
    exports.NgbTypeahead = NgbTypeahead;
    exports.NgbTypeaheadConfig = NgbTypeaheadConfig;
    exports.NgbTypeaheadModule = NgbTypeaheadModule;
    exports.ɵa = NGB_CAROUSEL_DIRECTIVES;
    exports.ɵb = NGB_DATEPICKER_CALENDAR_FACTORY;
    exports.ɵba = ARIA_LIVE_DELAY;
    exports.ɵbb = ARIA_LIVE_DELAY_FACTORY;
    exports.ɵbc = Live;
    exports.ɵbd = NgbCalendarHijri;
    exports.ɵbe = ContentRef;
    exports.ɵc = NgbDatepickerMonthView;
    exports.ɵd = NgbDatepickerDayView;
    exports.ɵe = NgbDatepickerNavigation;
    exports.ɵf = NgbDatepickerNavigationSelect;
    exports.ɵg = NGB_DATEPICKER_18N_FACTORY;
    exports.ɵh = NgbDatepickerI18nDefault;
    exports.ɵi = NGB_DATEPICKER_DATE_ADAPTER_FACTORY;
    exports.ɵj = NgbDateStructAdapter;
    exports.ɵk = NGB_DATEPICKER_PARSER_FORMATTER_FACTORY;
    exports.ɵl = NgbDateISOParserFormatter;
    exports.ɵm = NgbNavbar;
    exports.ɵn = NgbPopoverWindow;
    exports.ɵo = NGB_DATEPICKER_TIME_ADAPTER_FACTORY;
    exports.ɵp = NgbTimeStructAdapter;
    exports.ɵq = NGB_TIMEPICKER_I18N_FACTORY;
    exports.ɵr = NgbTimepickerI18nDefault;
    exports.ɵs = NgbTooltipWindow;
    exports.ɵt = NgbTypeaheadWindow;
    exports.ɵu = NgbDatepickerService;
    exports.ɵv = NgbDatepickerKeyMapService;
    exports.ɵw = NgbModalBackdrop;
    exports.ɵx = NgbModalWindow;
    exports.ɵy = NgbModalStack;
    exports.ɵz = ScrollBar;

    Object.defineProperty(exports, '__esModule', { value: true });

}));
//# sourceMappingURL=ng-bootstrap.umd.js.map

;/*!
  * Bootstrap v4.3.1 (https://getbootstrap.com/)
  * Copyright 2011-2019 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
  */
!function(t,e){"object"==typeof exports&&"undefined"!=typeof module?e(exports,require("jquery"),require("popper.js")):"function"==typeof define&&define.amd?define(["exports","jquery","popper.js"],e):e((t=t||self).bootstrap={},t.jQuery,t.Popper)}(this,function(t,g,u){"use strict";function i(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function s(t,e,n){return e&&i(t.prototype,e),n&&i(t,n),t}function l(o){for(var t=1;t<arguments.length;t++){var r=null!=arguments[t]?arguments[t]:{},e=Object.keys(r);"function"==typeof Object.getOwnPropertySymbols&&(e=e.concat(Object.getOwnPropertySymbols(r).filter(function(t){return Object.getOwnPropertyDescriptor(r,t).enumerable}))),e.forEach(function(t){var e,n,i;e=o,i=r[n=t],n in e?Object.defineProperty(e,n,{value:i,enumerable:!0,configurable:!0,writable:!0}):e[n]=i})}return o}g=g&&g.hasOwnProperty("default")?g.default:g,u=u&&u.hasOwnProperty("default")?u.default:u;var e="transitionend";function n(t){var e=this,n=!1;return g(this).one(_.TRANSITION_END,function(){n=!0}),setTimeout(function(){n||_.triggerTransitionEnd(e)},t),this}var _={TRANSITION_END:"bsTransitionEnd",getUID:function(t){for(;t+=~~(1e6*Math.random()),document.getElementById(t););return t},getSelectorFromElement:function(t){var e=t.getAttribute("data-target");if(!e||"#"===e){var n=t.getAttribute("href");e=n&&"#"!==n?n.trim():""}try{return document.querySelector(e)?e:null}catch(t){return null}},getTransitionDurationFromElement:function(t){if(!t)return 0;var e=g(t).css("transition-duration"),n=g(t).css("transition-delay"),i=parseFloat(e),o=parseFloat(n);return i||o?(e=e.split(",")[0],n=n.split(",")[0],1e3*(parseFloat(e)+parseFloat(n))):0},reflow:function(t){return t.offsetHeight},triggerTransitionEnd:function(t){g(t).trigger(e)},supportsTransitionEnd:function(){return Boolean(e)},isElement:function(t){return(t[0]||t).nodeType},typeCheckConfig:function(t,e,n){for(var i in n)if(Object.prototype.hasOwnProperty.call(n,i)){var o=n[i],r=e[i],s=r&&_.isElement(r)?"element":(a=r,{}.toString.call(a).match(/\s([a-z]+)/i)[1].toLowerCase());if(!new RegExp(o).test(s))throw new Error(t.toUpperCase()+': Option "'+i+'" provided type "'+s+'" but expected type "'+o+'".')}var a},findShadowRoot:function(t){if(!document.documentElement.attachShadow)return null;if("function"!=typeof t.getRootNode)return t instanceof ShadowRoot?t:t.parentNode?_.findShadowRoot(t.parentNode):null;var e=t.getRootNode();return e instanceof ShadowRoot?e:null}};g.fn.emulateTransitionEnd=n,g.event.special[_.TRANSITION_END]={bindType:e,delegateType:e,handle:function(t){if(g(t.target).is(this))return t.handleObj.handler.apply(this,arguments)}};var o="alert",r="bs.alert",a="."+r,c=g.fn[o],h={CLOSE:"close"+a,CLOSED:"closed"+a,CLICK_DATA_API:"click"+a+".data-api"},f="alert",d="fade",m="show",p=function(){function i(t){this._element=t}var t=i.prototype;return t.close=function(t){var e=this._element;t&&(e=this._getRootElement(t)),this._triggerCloseEvent(e).isDefaultPrevented()||this._removeElement(e)},t.dispose=function(){g.removeData(this._element,r),this._element=null},t._getRootElement=function(t){var e=_.getSelectorFromElement(t),n=!1;return e&&(n=document.querySelector(e)),n||(n=g(t).closest("."+f)[0]),n},t._triggerCloseEvent=function(t){var e=g.Event(h.CLOSE);return g(t).trigger(e),e},t._removeElement=function(e){var n=this;if(g(e).removeClass(m),g(e).hasClass(d)){var t=_.getTransitionDurationFromElement(e);g(e).one(_.TRANSITION_END,function(t){return n._destroyElement(e,t)}).emulateTransitionEnd(t)}else this._destroyElement(e)},t._destroyElement=function(t){g(t).detach().trigger(h.CLOSED).remove()},i._jQueryInterface=function(n){return this.each(function(){var t=g(this),e=t.data(r);e||(e=new i(this),t.data(r,e)),"close"===n&&e[n](this)})},i._handleDismiss=function(e){return function(t){t&&t.preventDefault(),e.close(this)}},s(i,null,[{key:"VERSION",get:function(){return"4.3.1"}}]),i}();g(document).on(h.CLICK_DATA_API,'[data-dismiss="alert"]',p._handleDismiss(new p)),g.fn[o]=p._jQueryInterface,g.fn[o].Constructor=p,g.fn[o].noConflict=function(){return g.fn[o]=c,p._jQueryInterface};var v="button",y="bs.button",E="."+y,C=".data-api",T=g.fn[v],S="active",b="btn",I="focus",D='[data-toggle^="button"]',w='[data-toggle="buttons"]',A='input:not([type="hidden"])',N=".active",O=".btn",k={CLICK_DATA_API:"click"+E+C,FOCUS_BLUR_DATA_API:"focus"+E+C+" blur"+E+C},P=function(){function n(t){this._element=t}var t=n.prototype;return t.toggle=function(){var t=!0,e=!0,n=g(this._element).closest(w)[0];if(n){var i=this._element.querySelector(A);if(i){if("radio"===i.type)if(i.checked&&this._element.classList.contains(S))t=!1;else{var o=n.querySelector(N);o&&g(o).removeClass(S)}if(t){if(i.hasAttribute("disabled")||n.hasAttribute("disabled")||i.classList.contains("disabled")||n.classList.contains("disabled"))return;i.checked=!this._element.classList.contains(S),g(i).trigger("change")}i.focus(),e=!1}}e&&this._element.setAttribute("aria-pressed",!this._element.classList.contains(S)),t&&g(this._element).toggleClass(S)},t.dispose=function(){g.removeData(this._element,y),this._element=null},n._jQueryInterface=function(e){return this.each(function(){var t=g(this).data(y);t||(t=new n(this),g(this).data(y,t)),"toggle"===e&&t[e]()})},s(n,null,[{key:"VERSION",get:function(){return"4.3.1"}}]),n}();g(document).on(k.CLICK_DATA_API,D,function(t){t.preventDefault();var e=t.target;g(e).hasClass(b)||(e=g(e).closest(O)),P._jQueryInterface.call(g(e),"toggle")}).on(k.FOCUS_BLUR_DATA_API,D,function(t){var e=g(t.target).closest(O)[0];g(e).toggleClass(I,/^focus(in)?$/.test(t.type))}),g.fn[v]=P._jQueryInterface,g.fn[v].Constructor=P,g.fn[v].noConflict=function(){return g.fn[v]=T,P._jQueryInterface};var L="carousel",j="bs.carousel",H="."+j,R=".data-api",x=g.fn[L],F={interval:5e3,keyboard:!0,slide:!1,pause:"hover",wrap:!0,touch:!0},U={interval:"(number|boolean)",keyboard:"boolean",slide:"(boolean|string)",pause:"(string|boolean)",wrap:"boolean",touch:"boolean"},W="next",q="prev",M="left",K="right",Q={SLIDE:"slide"+H,SLID:"slid"+H,KEYDOWN:"keydown"+H,MOUSEENTER:"mouseenter"+H,MOUSELEAVE:"mouseleave"+H,TOUCHSTART:"touchstart"+H,TOUCHMOVE:"touchmove"+H,TOUCHEND:"touchend"+H,POINTERDOWN:"pointerdown"+H,POINTERUP:"pointerup"+H,DRAG_START:"dragstart"+H,LOAD_DATA_API:"load"+H+R,CLICK_DATA_API:"click"+H+R},B="carousel",V="active",Y="slide",z="carousel-item-right",X="carousel-item-left",$="carousel-item-next",G="carousel-item-prev",J="pointer-event",Z=".active",tt=".active.carousel-item",et=".carousel-item",nt=".carousel-item img",it=".carousel-item-next, .carousel-item-prev",ot=".carousel-indicators",rt="[data-slide], [data-slide-to]",st='[data-ride="carousel"]',at={TOUCH:"touch",PEN:"pen"},lt=function(){function r(t,e){this._items=null,this._interval=null,this._activeElement=null,this._isPaused=!1,this._isSliding=!1,this.touchTimeout=null,this.touchStartX=0,this.touchDeltaX=0,this._config=this._getConfig(e),this._element=t,this._indicatorsElement=this._element.querySelector(ot),this._touchSupported="ontouchstart"in document.documentElement||0<navigator.maxTouchPoints,this._pointerEvent=Boolean(window.PointerEvent||window.MSPointerEvent),this._addEventListeners()}var t=r.prototype;return t.next=function(){this._isSliding||this._slide(W)},t.nextWhenVisible=function(){!document.hidden&&g(this._element).is(":visible")&&"hidden"!==g(this._element).css("visibility")&&this.next()},t.prev=function(){this._isSliding||this._slide(q)},t.pause=function(t){t||(this._isPaused=!0),this._element.querySelector(it)&&(_.triggerTransitionEnd(this._element),this.cycle(!0)),clearInterval(this._interval),this._interval=null},t.cycle=function(t){t||(this._isPaused=!1),this._interval&&(clearInterval(this._interval),this._interval=null),this._config.interval&&!this._isPaused&&(this._interval=setInterval((document.visibilityState?this.nextWhenVisible:this.next).bind(this),this._config.interval))},t.to=function(t){var e=this;this._activeElement=this._element.querySelector(tt);var n=this._getItemIndex(this._activeElement);if(!(t>this._items.length-1||t<0))if(this._isSliding)g(this._element).one(Q.SLID,function(){return e.to(t)});else{if(n===t)return this.pause(),void this.cycle();var i=n<t?W:q;this._slide(i,this._items[t])}},t.dispose=function(){g(this._element).off(H),g.removeData(this._element,j),this._items=null,this._config=null,this._element=null,this._interval=null,this._isPaused=null,this._isSliding=null,this._activeElement=null,this._indicatorsElement=null},t._getConfig=function(t){return t=l({},F,t),_.typeCheckConfig(L,t,U),t},t._handleSwipe=function(){var t=Math.abs(this.touchDeltaX);if(!(t<=40)){var e=t/this.touchDeltaX;0<e&&this.prev(),e<0&&this.next()}},t._addEventListeners=function(){var e=this;this._config.keyboard&&g(this._element).on(Q.KEYDOWN,function(t){return e._keydown(t)}),"hover"===this._config.pause&&g(this._element).on(Q.MOUSEENTER,function(t){return e.pause(t)}).on(Q.MOUSELEAVE,function(t){return e.cycle(t)}),this._config.touch&&this._addTouchEventListeners()},t._addTouchEventListeners=function(){var n=this;if(this._touchSupported){var e=function(t){n._pointerEvent&&at[t.originalEvent.pointerType.toUpperCase()]?n.touchStartX=t.originalEvent.clientX:n._pointerEvent||(n.touchStartX=t.originalEvent.touches[0].clientX)},i=function(t){n._pointerEvent&&at[t.originalEvent.pointerType.toUpperCase()]&&(n.touchDeltaX=t.originalEvent.clientX-n.touchStartX),n._handleSwipe(),"hover"===n._config.pause&&(n.pause(),n.touchTimeout&&clearTimeout(n.touchTimeout),n.touchTimeout=setTimeout(function(t){return n.cycle(t)},500+n._config.interval))};g(this._element.querySelectorAll(nt)).on(Q.DRAG_START,function(t){return t.preventDefault()}),this._pointerEvent?(g(this._element).on(Q.POINTERDOWN,function(t){return e(t)}),g(this._element).on(Q.POINTERUP,function(t){return i(t)}),this._element.classList.add(J)):(g(this._element).on(Q.TOUCHSTART,function(t){return e(t)}),g(this._element).on(Q.TOUCHMOVE,function(t){var e;(e=t).originalEvent.touches&&1<e.originalEvent.touches.length?n.touchDeltaX=0:n.touchDeltaX=e.originalEvent.touches[0].clientX-n.touchStartX}),g(this._element).on(Q.TOUCHEND,function(t){return i(t)}))}},t._keydown=function(t){if(!/input|textarea/i.test(t.target.tagName))switch(t.which){case 37:t.preventDefault(),this.prev();break;case 39:t.preventDefault(),this.next()}},t._getItemIndex=function(t){return this._items=t&&t.parentNode?[].slice.call(t.parentNode.querySelectorAll(et)):[],this._items.indexOf(t)},t._getItemByDirection=function(t,e){var n=t===W,i=t===q,o=this._getItemIndex(e),r=this._items.length-1;if((i&&0===o||n&&o===r)&&!this._config.wrap)return e;var s=(o+(t===q?-1:1))%this._items.length;return-1===s?this._items[this._items.length-1]:this._items[s]},t._triggerSlideEvent=function(t,e){var n=this._getItemIndex(t),i=this._getItemIndex(this._element.querySelector(tt)),o=g.Event(Q.SLIDE,{relatedTarget:t,direction:e,from:i,to:n});return g(this._element).trigger(o),o},t._setActiveIndicatorElement=function(t){if(this._indicatorsElement){var e=[].slice.call(this._indicatorsElement.querySelectorAll(Z));g(e).removeClass(V);var n=this._indicatorsElement.children[this._getItemIndex(t)];n&&g(n).addClass(V)}},t._slide=function(t,e){var n,i,o,r=this,s=this._element.querySelector(tt),a=this._getItemIndex(s),l=e||s&&this._getItemByDirection(t,s),c=this._getItemIndex(l),h=Boolean(this._interval);if(o=t===W?(n=X,i=$,M):(n=z,i=G,K),l&&g(l).hasClass(V))this._isSliding=!1;else if(!this._triggerSlideEvent(l,o).isDefaultPrevented()&&s&&l){this._isSliding=!0,h&&this.pause(),this._setActiveIndicatorElement(l);var u=g.Event(Q.SLID,{relatedTarget:l,direction:o,from:a,to:c});if(g(this._element).hasClass(Y)){g(l).addClass(i),_.reflow(l),g(s).addClass(n),g(l).addClass(n);var f=parseInt(l.getAttribute("data-interval"),10);this._config.interval=f?(this._config.defaultInterval=this._config.defaultInterval||this._config.interval,f):this._config.defaultInterval||this._config.interval;var d=_.getTransitionDurationFromElement(s);g(s).one(_.TRANSITION_END,function(){g(l).removeClass(n+" "+i).addClass(V),g(s).removeClass(V+" "+i+" "+n),r._isSliding=!1,setTimeout(function(){return g(r._element).trigger(u)},0)}).emulateTransitionEnd(d)}else g(s).removeClass(V),g(l).addClass(V),this._isSliding=!1,g(this._element).trigger(u);h&&this.cycle()}},r._jQueryInterface=function(i){return this.each(function(){var t=g(this).data(j),e=l({},F,g(this).data());"object"==typeof i&&(e=l({},e,i));var n="string"==typeof i?i:e.slide;if(t||(t=new r(this,e),g(this).data(j,t)),"number"==typeof i)t.to(i);else if("string"==typeof n){if("undefined"==typeof t[n])throw new TypeError('No method named "'+n+'"');t[n]()}else e.interval&&e.ride&&(t.pause(),t.cycle())})},r._dataApiClickHandler=function(t){var e=_.getSelectorFromElement(this);if(e){var n=g(e)[0];if(n&&g(n).hasClass(B)){var i=l({},g(n).data(),g(this).data()),o=this.getAttribute("data-slide-to");o&&(i.interval=!1),r._jQueryInterface.call(g(n),i),o&&g(n).data(j).to(o),t.preventDefault()}}},s(r,null,[{key:"VERSION",get:function(){return"4.3.1"}},{key:"Default",get:function(){return F}}]),r}();g(document).on(Q.CLICK_DATA_API,rt,lt._dataApiClickHandler),g(window).on(Q.LOAD_DATA_API,function(){for(var t=[].slice.call(document.querySelectorAll(st)),e=0,n=t.length;e<n;e++){var i=g(t[e]);lt._jQueryInterface.call(i,i.data())}}),g.fn[L]=lt._jQueryInterface,g.fn[L].Constructor=lt,g.fn[L].noConflict=function(){return g.fn[L]=x,lt._jQueryInterface};var ct="collapse",ht="bs.collapse",ut="."+ht,ft=g.fn[ct],dt={toggle:!0,parent:""},gt={toggle:"boolean",parent:"(string|element)"},_t={SHOW:"show"+ut,SHOWN:"shown"+ut,HIDE:"hide"+ut,HIDDEN:"hidden"+ut,CLICK_DATA_API:"click"+ut+".data-api"},mt="show",pt="collapse",vt="collapsing",yt="collapsed",Et="width",Ct="height",Tt=".show, .collapsing",St='[data-toggle="collapse"]',bt=function(){function a(e,t){this._isTransitioning=!1,this._element=e,this._config=this._getConfig(t),this._triggerArray=[].slice.call(document.querySelectorAll('[data-toggle="collapse"][href="#'+e.id+'"],[data-toggle="collapse"][data-target="#'+e.id+'"]'));for(var n=[].slice.call(document.querySelectorAll(St)),i=0,o=n.length;i<o;i++){var r=n[i],s=_.getSelectorFromElement(r),a=[].slice.call(document.querySelectorAll(s)).filter(function(t){return t===e});null!==s&&0<a.length&&(this._selector=s,this._triggerArray.push(r))}this._parent=this._config.parent?this._getParent():null,this._config.parent||this._addAriaAndCollapsedClass(this._element,this._triggerArray),this._config.toggle&&this.toggle()}var t=a.prototype;return t.toggle=function(){g(this._element).hasClass(mt)?this.hide():this.show()},t.show=function(){var t,e,n=this;if(!this._isTransitioning&&!g(this._element).hasClass(mt)&&(this._parent&&0===(t=[].slice.call(this._parent.querySelectorAll(Tt)).filter(function(t){return"string"==typeof n._config.parent?t.getAttribute("data-parent")===n._config.parent:t.classList.contains(pt)})).length&&(t=null),!(t&&(e=g(t).not(this._selector).data(ht))&&e._isTransitioning))){var i=g.Event(_t.SHOW);if(g(this._element).trigger(i),!i.isDefaultPrevented()){t&&(a._jQueryInterface.call(g(t).not(this._selector),"hide"),e||g(t).data(ht,null));var o=this._getDimension();g(this._element).removeClass(pt).addClass(vt),this._element.style[o]=0,this._triggerArray.length&&g(this._triggerArray).removeClass(yt).attr("aria-expanded",!0),this.setTransitioning(!0);var r="scroll"+(o[0].toUpperCase()+o.slice(1)),s=_.getTransitionDurationFromElement(this._element);g(this._element).one(_.TRANSITION_END,function(){g(n._element).removeClass(vt).addClass(pt).addClass(mt),n._element.style[o]="",n.setTransitioning(!1),g(n._element).trigger(_t.SHOWN)}).emulateTransitionEnd(s),this._element.style[o]=this._element[r]+"px"}}},t.hide=function(){var t=this;if(!this._isTransitioning&&g(this._element).hasClass(mt)){var e=g.Event(_t.HIDE);if(g(this._element).trigger(e),!e.isDefaultPrevented()){var n=this._getDimension();this._element.style[n]=this._element.getBoundingClientRect()[n]+"px",_.reflow(this._element),g(this._element).addClass(vt).removeClass(pt).removeClass(mt);var i=this._triggerArray.length;if(0<i)for(var o=0;o<i;o++){var r=this._triggerArray[o],s=_.getSelectorFromElement(r);if(null!==s)g([].slice.call(document.querySelectorAll(s))).hasClass(mt)||g(r).addClass(yt).attr("aria-expanded",!1)}this.setTransitioning(!0);this._element.style[n]="";var a=_.getTransitionDurationFromElement(this._element);g(this._element).one(_.TRANSITION_END,function(){t.setTransitioning(!1),g(t._element).removeClass(vt).addClass(pt).trigger(_t.HIDDEN)}).emulateTransitionEnd(a)}}},t.setTransitioning=function(t){this._isTransitioning=t},t.dispose=function(){g.removeData(this._element,ht),this._config=null,this._parent=null,this._element=null,this._triggerArray=null,this._isTransitioning=null},t._getConfig=function(t){return(t=l({},dt,t)).toggle=Boolean(t.toggle),_.typeCheckConfig(ct,t,gt),t},t._getDimension=function(){return g(this._element).hasClass(Et)?Et:Ct},t._getParent=function(){var t,n=this;_.isElement(this._config.parent)?(t=this._config.parent,"undefined"!=typeof this._config.parent.jquery&&(t=this._config.parent[0])):t=document.querySelector(this._config.parent);var e='[data-toggle="collapse"][data-parent="'+this._config.parent+'"]',i=[].slice.call(t.querySelectorAll(e));return g(i).each(function(t,e){n._addAriaAndCollapsedClass(a._getTargetFromElement(e),[e])}),t},t._addAriaAndCollapsedClass=function(t,e){var n=g(t).hasClass(mt);e.length&&g(e).toggleClass(yt,!n).attr("aria-expanded",n)},a._getTargetFromElement=function(t){var e=_.getSelectorFromElement(t);return e?document.querySelector(e):null},a._jQueryInterface=function(i){return this.each(function(){var t=g(this),e=t.data(ht),n=l({},dt,t.data(),"object"==typeof i&&i?i:{});if(!e&&n.toggle&&/show|hide/.test(i)&&(n.toggle=!1),e||(e=new a(this,n),t.data(ht,e)),"string"==typeof i){if("undefined"==typeof e[i])throw new TypeError('No method named "'+i+'"');e[i]()}})},s(a,null,[{key:"VERSION",get:function(){return"4.3.1"}},{key:"Default",get:function(){return dt}}]),a}();g(document).on(_t.CLICK_DATA_API,St,function(t){"A"===t.currentTarget.tagName&&t.preventDefault();var n=g(this),e=_.getSelectorFromElement(this),i=[].slice.call(document.querySelectorAll(e));g(i).each(function(){var t=g(this),e=t.data(ht)?"toggle":n.data();bt._jQueryInterface.call(t,e)})}),g.fn[ct]=bt._jQueryInterface,g.fn[ct].Constructor=bt,g.fn[ct].noConflict=function(){return g.fn[ct]=ft,bt._jQueryInterface};var It="dropdown",Dt="bs.dropdown",wt="."+Dt,At=".data-api",Nt=g.fn[It],Ot=new RegExp("38|40|27"),kt={HIDE:"hide"+wt,HIDDEN:"hidden"+wt,SHOW:"show"+wt,SHOWN:"shown"+wt,CLICK:"click"+wt,CLICK_DATA_API:"click"+wt+At,KEYDOWN_DATA_API:"keydown"+wt+At,KEYUP_DATA_API:"keyup"+wt+At},Pt="disabled",Lt="show",jt="dropup",Ht="dropright",Rt="dropleft",xt="dropdown-menu-right",Ft="position-static",Ut='[data-toggle="dropdown"]',Wt=".dropdown form",qt=".dropdown-menu",Mt=".navbar-nav",Kt=".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)",Qt="top-start",Bt="top-end",Vt="bottom-start",Yt="bottom-end",zt="right-start",Xt="left-start",$t={offset:0,flip:!0,boundary:"scrollParent",reference:"toggle",display:"dynamic"},Gt={offset:"(number|string|function)",flip:"boolean",boundary:"(string|element)",reference:"(string|element)",display:"string"},Jt=function(){function c(t,e){this._element=t,this._popper=null,this._config=this._getConfig(e),this._menu=this._getMenuElement(),this._inNavbar=this._detectNavbar(),this._addEventListeners()}var t=c.prototype;return t.toggle=function(){if(!this._element.disabled&&!g(this._element).hasClass(Pt)){var t=c._getParentFromElement(this._element),e=g(this._menu).hasClass(Lt);if(c._clearMenus(),!e){var n={relatedTarget:this._element},i=g.Event(kt.SHOW,n);if(g(t).trigger(i),!i.isDefaultPrevented()){if(!this._inNavbar){if("undefined"==typeof u)throw new TypeError("Bootstrap's dropdowns require Popper.js (https://popper.js.org/)");var o=this._element;"parent"===this._config.reference?o=t:_.isElement(this._config.reference)&&(o=this._config.reference,"undefined"!=typeof this._config.reference.jquery&&(o=this._config.reference[0])),"scrollParent"!==this._config.boundary&&g(t).addClass(Ft),this._popper=new u(o,this._menu,this._getPopperConfig())}"ontouchstart"in document.documentElement&&0===g(t).closest(Mt).length&&g(document.body).children().on("mouseover",null,g.noop),this._element.focus(),this._element.setAttribute("aria-expanded",!0),g(this._menu).toggleClass(Lt),g(t).toggleClass(Lt).trigger(g.Event(kt.SHOWN,n))}}}},t.show=function(){if(!(this._element.disabled||g(this._element).hasClass(Pt)||g(this._menu).hasClass(Lt))){var t={relatedTarget:this._element},e=g.Event(kt.SHOW,t),n=c._getParentFromElement(this._element);g(n).trigger(e),e.isDefaultPrevented()||(g(this._menu).toggleClass(Lt),g(n).toggleClass(Lt).trigger(g.Event(kt.SHOWN,t)))}},t.hide=function(){if(!this._element.disabled&&!g(this._element).hasClass(Pt)&&g(this._menu).hasClass(Lt)){var t={relatedTarget:this._element},e=g.Event(kt.HIDE,t),n=c._getParentFromElement(this._element);g(n).trigger(e),e.isDefaultPrevented()||(g(this._menu).toggleClass(Lt),g(n).toggleClass(Lt).trigger(g.Event(kt.HIDDEN,t)))}},t.dispose=function(){g.removeData(this._element,Dt),g(this._element).off(wt),this._element=null,(this._menu=null)!==this._popper&&(this._popper.destroy(),this._popper=null)},t.update=function(){this._inNavbar=this._detectNavbar(),null!==this._popper&&this._popper.scheduleUpdate()},t._addEventListeners=function(){var e=this;g(this._element).on(kt.CLICK,function(t){t.preventDefault(),t.stopPropagation(),e.toggle()})},t._getConfig=function(t){return t=l({},this.constructor.Default,g(this._element).data(),t),_.typeCheckConfig(It,t,this.constructor.DefaultType),t},t._getMenuElement=function(){if(!this._menu){var t=c._getParentFromElement(this._element);t&&(this._menu=t.querySelector(qt))}return this._menu},t._getPlacement=function(){var t=g(this._element.parentNode),e=Vt;return t.hasClass(jt)?(e=Qt,g(this._menu).hasClass(xt)&&(e=Bt)):t.hasClass(Ht)?e=zt:t.hasClass(Rt)?e=Xt:g(this._menu).hasClass(xt)&&(e=Yt),e},t._detectNavbar=function(){return 0<g(this._element).closest(".navbar").length},t._getOffset=function(){var e=this,t={};return"function"==typeof this._config.offset?t.fn=function(t){return t.offsets=l({},t.offsets,e._config.offset(t.offsets,e._element)||{}),t}:t.offset=this._config.offset,t},t._getPopperConfig=function(){var t={placement:this._getPlacement(),modifiers:{offset:this._getOffset(),flip:{enabled:this._config.flip},preventOverflow:{boundariesElement:this._config.boundary}}};return"static"===this._config.display&&(t.modifiers.applyStyle={enabled:!1}),t},c._jQueryInterface=function(e){return this.each(function(){var t=g(this).data(Dt);if(t||(t=new c(this,"object"==typeof e?e:null),g(this).data(Dt,t)),"string"==typeof e){if("undefined"==typeof t[e])throw new TypeError('No method named "'+e+'"');t[e]()}})},c._clearMenus=function(t){if(!t||3!==t.which&&("keyup"!==t.type||9===t.which))for(var e=[].slice.call(document.querySelectorAll(Ut)),n=0,i=e.length;n<i;n++){var o=c._getParentFromElement(e[n]),r=g(e[n]).data(Dt),s={relatedTarget:e[n]};if(t&&"click"===t.type&&(s.clickEvent=t),r){var a=r._menu;if(g(o).hasClass(Lt)&&!(t&&("click"===t.type&&/input|textarea/i.test(t.target.tagName)||"keyup"===t.type&&9===t.which)&&g.contains(o,t.target))){var l=g.Event(kt.HIDE,s);g(o).trigger(l),l.isDefaultPrevented()||("ontouchstart"in document.documentElement&&g(document.body).children().off("mouseover",null,g.noop),e[n].setAttribute("aria-expanded","false"),g(a).removeClass(Lt),g(o).removeClass(Lt).trigger(g.Event(kt.HIDDEN,s)))}}}},c._getParentFromElement=function(t){var e,n=_.getSelectorFromElement(t);return n&&(e=document.querySelector(n)),e||t.parentNode},c._dataApiKeydownHandler=function(t){if((/input|textarea/i.test(t.target.tagName)?!(32===t.which||27!==t.which&&(40!==t.which&&38!==t.which||g(t.target).closest(qt).length)):Ot.test(t.which))&&(t.preventDefault(),t.stopPropagation(),!this.disabled&&!g(this).hasClass(Pt))){var e=c._getParentFromElement(this),n=g(e).hasClass(Lt);if(n&&(!n||27!==t.which&&32!==t.which)){var i=[].slice.call(e.querySelectorAll(Kt));if(0!==i.length){var o=i.indexOf(t.target);38===t.which&&0<o&&o--,40===t.which&&o<i.length-1&&o++,o<0&&(o=0),i[o].focus()}}else{if(27===t.which){var r=e.querySelector(Ut);g(r).trigger("focus")}g(this).trigger("click")}}},s(c,null,[{key:"VERSION",get:function(){return"4.3.1"}},{key:"Default",get:function(){return $t}},{key:"DefaultType",get:function(){return Gt}}]),c}();g(document).on(kt.KEYDOWN_DATA_API,Ut,Jt._dataApiKeydownHandler).on(kt.KEYDOWN_DATA_API,qt,Jt._dataApiKeydownHandler).on(kt.CLICK_DATA_API+" "+kt.KEYUP_DATA_API,Jt._clearMenus).on(kt.CLICK_DATA_API,Ut,function(t){t.preventDefault(),t.stopPropagation(),Jt._jQueryInterface.call(g(this),"toggle")}).on(kt.CLICK_DATA_API,Wt,function(t){t.stopPropagation()}),g.fn[It]=Jt._jQueryInterface,g.fn[It].Constructor=Jt,g.fn[It].noConflict=function(){return g.fn[It]=Nt,Jt._jQueryInterface};var Zt="modal",te="bs.modal",ee="."+te,ne=g.fn[Zt],ie={backdrop:!0,keyboard:!0,focus:!0,show:!0},oe={backdrop:"(boolean|string)",keyboard:"boolean",focus:"boolean",show:"boolean"},re={HIDE:"hide"+ee,HIDDEN:"hidden"+ee,SHOW:"show"+ee,SHOWN:"shown"+ee,FOCUSIN:"focusin"+ee,RESIZE:"resize"+ee,CLICK_DISMISS:"click.dismiss"+ee,KEYDOWN_DISMISS:"keydown.dismiss"+ee,MOUSEUP_DISMISS:"mouseup.dismiss"+ee,MOUSEDOWN_DISMISS:"mousedown.dismiss"+ee,CLICK_DATA_API:"click"+ee+".data-api"},se="modal-dialog-scrollable",ae="modal-scrollbar-measure",le="modal-backdrop",ce="modal-open",he="fade",ue="show",fe=".modal-dialog",de=".modal-body",ge='[data-toggle="modal"]',_e='[data-dismiss="modal"]',me=".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",pe=".sticky-top",ve=function(){function o(t,e){this._config=this._getConfig(e),this._element=t,this._dialog=t.querySelector(fe),this._backdrop=null,this._isShown=!1,this._isBodyOverflowing=!1,this._ignoreBackdropClick=!1,this._isTransitioning=!1,this._scrollbarWidth=0}var t=o.prototype;return t.toggle=function(t){return this._isShown?this.hide():this.show(t)},t.show=function(t){var e=this;if(!this._isShown&&!this._isTransitioning){g(this._element).hasClass(he)&&(this._isTransitioning=!0);var n=g.Event(re.SHOW,{relatedTarget:t});g(this._element).trigger(n),this._isShown||n.isDefaultPrevented()||(this._isShown=!0,this._checkScrollbar(),this._setScrollbar(),this._adjustDialog(),this._setEscapeEvent(),this._setResizeEvent(),g(this._element).on(re.CLICK_DISMISS,_e,function(t){return e.hide(t)}),g(this._dialog).on(re.MOUSEDOWN_DISMISS,function(){g(e._element).one(re.MOUSEUP_DISMISS,function(t){g(t.target).is(e._element)&&(e._ignoreBackdropClick=!0)})}),this._showBackdrop(function(){return e._showElement(t)}))}},t.hide=function(t){var e=this;if(t&&t.preventDefault(),this._isShown&&!this._isTransitioning){var n=g.Event(re.HIDE);if(g(this._element).trigger(n),this._isShown&&!n.isDefaultPrevented()){this._isShown=!1;var i=g(this._element).hasClass(he);if(i&&(this._isTransitioning=!0),this._setEscapeEvent(),this._setResizeEvent(),g(document).off(re.FOCUSIN),g(this._element).removeClass(ue),g(this._element).off(re.CLICK_DISMISS),g(this._dialog).off(re.MOUSEDOWN_DISMISS),i){var o=_.getTransitionDurationFromElement(this._element);g(this._element).one(_.TRANSITION_END,function(t){return e._hideModal(t)}).emulateTransitionEnd(o)}else this._hideModal()}}},t.dispose=function(){[window,this._element,this._dialog].forEach(function(t){return g(t).off(ee)}),g(document).off(re.FOCUSIN),g.removeData(this._element,te),this._config=null,this._element=null,this._dialog=null,this._backdrop=null,this._isShown=null,this._isBodyOverflowing=null,this._ignoreBackdropClick=null,this._isTransitioning=null,this._scrollbarWidth=null},t.handleUpdate=function(){this._adjustDialog()},t._getConfig=function(t){return t=l({},ie,t),_.typeCheckConfig(Zt,t,oe),t},t._showElement=function(t){var e=this,n=g(this._element).hasClass(he);this._element.parentNode&&this._element.parentNode.nodeType===Node.ELEMENT_NODE||document.body.appendChild(this._element),this._element.style.display="block",this._element.removeAttribute("aria-hidden"),this._element.setAttribute("aria-modal",!0),g(this._dialog).hasClass(se)?this._dialog.querySelector(de).scrollTop=0:this._element.scrollTop=0,n&&_.reflow(this._element),g(this._element).addClass(ue),this._config.focus&&this._enforceFocus();var i=g.Event(re.SHOWN,{relatedTarget:t}),o=function(){e._config.focus&&e._element.focus(),e._isTransitioning=!1,g(e._element).trigger(i)};if(n){var r=_.getTransitionDurationFromElement(this._dialog);g(this._dialog).one(_.TRANSITION_END,o).emulateTransitionEnd(r)}else o()},t._enforceFocus=function(){var e=this;g(document).off(re.FOCUSIN).on(re.FOCUSIN,function(t){document!==t.target&&e._element!==t.target&&0===g(e._element).has(t.target).length&&e._element.focus()})},t._setEscapeEvent=function(){var e=this;this._isShown&&this._config.keyboard?g(this._element).on(re.KEYDOWN_DISMISS,function(t){27===t.which&&(t.preventDefault(),e.hide())}):this._isShown||g(this._element).off(re.KEYDOWN_DISMISS)},t._setResizeEvent=function(){var e=this;this._isShown?g(window).on(re.RESIZE,function(t){return e.handleUpdate(t)}):g(window).off(re.RESIZE)},t._hideModal=function(){var t=this;this._element.style.display="none",this._element.setAttribute("aria-hidden",!0),this._element.removeAttribute("aria-modal"),this._isTransitioning=!1,this._showBackdrop(function(){g(document.body).removeClass(ce),t._resetAdjustments(),t._resetScrollbar(),g(t._element).trigger(re.HIDDEN)})},t._removeBackdrop=function(){this._backdrop&&(g(this._backdrop).remove(),this._backdrop=null)},t._showBackdrop=function(t){var e=this,n=g(this._element).hasClass(he)?he:"";if(this._isShown&&this._config.backdrop){if(this._backdrop=document.createElement("div"),this._backdrop.className=le,n&&this._backdrop.classList.add(n),g(this._backdrop).appendTo(document.body),g(this._element).on(re.CLICK_DISMISS,function(t){e._ignoreBackdropClick?e._ignoreBackdropClick=!1:t.target===t.currentTarget&&("static"===e._config.backdrop?e._element.focus():e.hide())}),n&&_.reflow(this._backdrop),g(this._backdrop).addClass(ue),!t)return;if(!n)return void t();var i=_.getTransitionDurationFromElement(this._backdrop);g(this._backdrop).one(_.TRANSITION_END,t).emulateTransitionEnd(i)}else if(!this._isShown&&this._backdrop){g(this._backdrop).removeClass(ue);var o=function(){e._removeBackdrop(),t&&t()};if(g(this._element).hasClass(he)){var r=_.getTransitionDurationFromElement(this._backdrop);g(this._backdrop).one(_.TRANSITION_END,o).emulateTransitionEnd(r)}else o()}else t&&t()},t._adjustDialog=function(){var t=this._element.scrollHeight>document.documentElement.clientHeight;!this._isBodyOverflowing&&t&&(this._element.style.paddingLeft=this._scrollbarWidth+"px"),this._isBodyOverflowing&&!t&&(this._element.style.paddingRight=this._scrollbarWidth+"px")},t._resetAdjustments=function(){this._element.style.paddingLeft="",this._element.style.paddingRight=""},t._checkScrollbar=function(){var t=document.body.getBoundingClientRect();this._isBodyOverflowing=t.left+t.right<window.innerWidth,this._scrollbarWidth=this._getScrollbarWidth()},t._setScrollbar=function(){var o=this;if(this._isBodyOverflowing){var t=[].slice.call(document.querySelectorAll(me)),e=[].slice.call(document.querySelectorAll(pe));g(t).each(function(t,e){var n=e.style.paddingRight,i=g(e).css("padding-right");g(e).data("padding-right",n).css("padding-right",parseFloat(i)+o._scrollbarWidth+"px")}),g(e).each(function(t,e){var n=e.style.marginRight,i=g(e).css("margin-right");g(e).data("margin-right",n).css("margin-right",parseFloat(i)-o._scrollbarWidth+"px")});var n=document.body.style.paddingRight,i=g(document.body).css("padding-right");g(document.body).data("padding-right",n).css("padding-right",parseFloat(i)+this._scrollbarWidth+"px")}g(document.body).addClass(ce)},t._resetScrollbar=function(){var t=[].slice.call(document.querySelectorAll(me));g(t).each(function(t,e){var n=g(e).data("padding-right");g(e).removeData("padding-right"),e.style.paddingRight=n||""});var e=[].slice.call(document.querySelectorAll(""+pe));g(e).each(function(t,e){var n=g(e).data("margin-right");"undefined"!=typeof n&&g(e).css("margin-right",n).removeData("margin-right")});var n=g(document.body).data("padding-right");g(document.body).removeData("padding-right"),document.body.style.paddingRight=n||""},t._getScrollbarWidth=function(){var t=document.createElement("div");t.className=ae,document.body.appendChild(t);var e=t.getBoundingClientRect().width-t.clientWidth;return document.body.removeChild(t),e},o._jQueryInterface=function(n,i){return this.each(function(){var t=g(this).data(te),e=l({},ie,g(this).data(),"object"==typeof n&&n?n:{});if(t||(t=new o(this,e),g(this).data(te,t)),"string"==typeof n){if("undefined"==typeof t[n])throw new TypeError('No method named "'+n+'"');t[n](i)}else e.show&&t.show(i)})},s(o,null,[{key:"VERSION",get:function(){return"4.3.1"}},{key:"Default",get:function(){return ie}}]),o}();g(document).on(re.CLICK_DATA_API,ge,function(t){var e,n=this,i=_.getSelectorFromElement(this);i&&(e=document.querySelector(i));var o=g(e).data(te)?"toggle":l({},g(e).data(),g(this).data());"A"!==this.tagName&&"AREA"!==this.tagName||t.preventDefault();var r=g(e).one(re.SHOW,function(t){t.isDefaultPrevented()||r.one(re.HIDDEN,function(){g(n).is(":visible")&&n.focus()})});ve._jQueryInterface.call(g(e),o,this)}),g.fn[Zt]=ve._jQueryInterface,g.fn[Zt].Constructor=ve,g.fn[Zt].noConflict=function(){return g.fn[Zt]=ne,ve._jQueryInterface};var ye=["background","cite","href","itemtype","longdesc","poster","src","xlink:href"],Ee={"*":["class","dir","id","lang","role",/^aria-[\w-]*$/i],a:["target","href","title","rel"],area:[],b:[],br:[],col:[],code:[],div:[],em:[],hr:[],h1:[],h2:[],h3:[],h4:[],h5:[],h6:[],i:[],img:["src","alt","title","width","height"],li:[],ol:[],p:[],pre:[],s:[],small:[],span:[],sub:[],sup:[],strong:[],u:[],ul:[]},Ce=/^(?:(?:https?|mailto|ftp|tel|file):|[^&:/?#]*(?:[/?#]|$))/gi,Te=/^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+/]+=*$/i;function Se(t,s,e){if(0===t.length)return t;if(e&&"function"==typeof e)return e(t);for(var n=(new window.DOMParser).parseFromString(t,"text/html"),a=Object.keys(s),l=[].slice.call(n.body.querySelectorAll("*")),i=function(t,e){var n=l[t],i=n.nodeName.toLowerCase();if(-1===a.indexOf(n.nodeName.toLowerCase()))return n.parentNode.removeChild(n),"continue";var o=[].slice.call(n.attributes),r=[].concat(s["*"]||[],s[i]||[]);o.forEach(function(t){(function(t,e){var n=t.nodeName.toLowerCase();if(-1!==e.indexOf(n))return-1===ye.indexOf(n)||Boolean(t.nodeValue.match(Ce)||t.nodeValue.match(Te));for(var i=e.filter(function(t){return t instanceof RegExp}),o=0,r=i.length;o<r;o++)if(n.match(i[o]))return!0;return!1})(t,r)||n.removeAttribute(t.nodeName)})},o=0,r=l.length;o<r;o++)i(o);return n.body.innerHTML}var be="tooltip",Ie="bs.tooltip",De="."+Ie,we=g.fn[be],Ae="bs-tooltip",Ne=new RegExp("(^|\\s)"+Ae+"\\S+","g"),Oe=["sanitize","whiteList","sanitizeFn"],ke={animation:"boolean",template:"string",title:"(string|element|function)",trigger:"string",delay:"(number|object)",html:"boolean",selector:"(string|boolean)",placement:"(string|function)",offset:"(number|string|function)",container:"(string|element|boolean)",fallbackPlacement:"(string|array)",boundary:"(string|element)",sanitize:"boolean",sanitizeFn:"(null|function)",whiteList:"object"},Pe={AUTO:"auto",TOP:"top",RIGHT:"right",BOTTOM:"bottom",LEFT:"left"},Le={animation:!0,template:'<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,selector:!1,placement:"top",offset:0,container:!1,fallbackPlacement:"flip",boundary:"scrollParent",sanitize:!0,sanitizeFn:null,whiteList:Ee},je="show",He="out",Re={HIDE:"hide"+De,HIDDEN:"hidden"+De,SHOW:"show"+De,SHOWN:"shown"+De,INSERTED:"inserted"+De,CLICK:"click"+De,FOCUSIN:"focusin"+De,FOCUSOUT:"focusout"+De,MOUSEENTER:"mouseenter"+De,MOUSELEAVE:"mouseleave"+De},xe="fade",Fe="show",Ue=".tooltip-inner",We=".arrow",qe="hover",Me="focus",Ke="click",Qe="manual",Be=function(){function i(t,e){if("undefined"==typeof u)throw new TypeError("Bootstrap's tooltips require Popper.js (https://popper.js.org/)");this._isEnabled=!0,this._timeout=0,this._hoverState="",this._activeTrigger={},this._popper=null,this.element=t,this.config=this._getConfig(e),this.tip=null,this._setListeners()}var t=i.prototype;return t.enable=function(){this._isEnabled=!0},t.disable=function(){this._isEnabled=!1},t.toggleEnabled=function(){this._isEnabled=!this._isEnabled},t.toggle=function(t){if(this._isEnabled)if(t){var e=this.constructor.DATA_KEY,n=g(t.currentTarget).data(e);n||(n=new this.constructor(t.currentTarget,this._getDelegateConfig()),g(t.currentTarget).data(e,n)),n._activeTrigger.click=!n._activeTrigger.click,n._isWithActiveTrigger()?n._enter(null,n):n._leave(null,n)}else{if(g(this.getTipElement()).hasClass(Fe))return void this._leave(null,this);this._enter(null,this)}},t.dispose=function(){clearTimeout(this._timeout),g.removeData(this.element,this.constructor.DATA_KEY),g(this.element).off(this.constructor.EVENT_KEY),g(this.element).closest(".modal").off("hide.bs.modal"),this.tip&&g(this.tip).remove(),this._isEnabled=null,this._timeout=null,this._hoverState=null,(this._activeTrigger=null)!==this._popper&&this._popper.destroy(),this._popper=null,this.element=null,this.config=null,this.tip=null},t.show=function(){var e=this;if("none"===g(this.element).css("display"))throw new Error("Please use show on visible elements");var t=g.Event(this.constructor.Event.SHOW);if(this.isWithContent()&&this._isEnabled){g(this.element).trigger(t);var n=_.findShadowRoot(this.element),i=g.contains(null!==n?n:this.element.ownerDocument.documentElement,this.element);if(t.isDefaultPrevented()||!i)return;var o=this.getTipElement(),r=_.getUID(this.constructor.NAME);o.setAttribute("id",r),this.element.setAttribute("aria-describedby",r),this.setContent(),this.config.animation&&g(o).addClass(xe);var s="function"==typeof this.config.placement?this.config.placement.call(this,o,this.element):this.config.placement,a=this._getAttachment(s);this.addAttachmentClass(a);var l=this._getContainer();g(o).data(this.constructor.DATA_KEY,this),g.contains(this.element.ownerDocument.documentElement,this.tip)||g(o).appendTo(l),g(this.element).trigger(this.constructor.Event.INSERTED),this._popper=new u(this.element,o,{placement:a,modifiers:{offset:this._getOffset(),flip:{behavior:this.config.fallbackPlacement},arrow:{element:We},preventOverflow:{boundariesElement:this.config.boundary}},onCreate:function(t){t.originalPlacement!==t.placement&&e._handlePopperPlacementChange(t)},onUpdate:function(t){return e._handlePopperPlacementChange(t)}}),g(o).addClass(Fe),"ontouchstart"in document.documentElement&&g(document.body).children().on("mouseover",null,g.noop);var c=function(){e.config.animation&&e._fixTransition();var t=e._hoverState;e._hoverState=null,g(e.element).trigger(e.constructor.Event.SHOWN),t===He&&e._leave(null,e)};if(g(this.tip).hasClass(xe)){var h=_.getTransitionDurationFromElement(this.tip);g(this.tip).one(_.TRANSITION_END,c).emulateTransitionEnd(h)}else c()}},t.hide=function(t){var e=this,n=this.getTipElement(),i=g.Event(this.constructor.Event.HIDE),o=function(){e._hoverState!==je&&n.parentNode&&n.parentNode.removeChild(n),e._cleanTipClass(),e.element.removeAttribute("aria-describedby"),g(e.element).trigger(e.constructor.Event.HIDDEN),null!==e._popper&&e._popper.destroy(),t&&t()};if(g(this.element).trigger(i),!i.isDefaultPrevented()){if(g(n).removeClass(Fe),"ontouchstart"in document.documentElement&&g(document.body).children().off("mouseover",null,g.noop),this._activeTrigger[Ke]=!1,this._activeTrigger[Me]=!1,this._activeTrigger[qe]=!1,g(this.tip).hasClass(xe)){var r=_.getTransitionDurationFromElement(n);g(n).one(_.TRANSITION_END,o).emulateTransitionEnd(r)}else o();this._hoverState=""}},t.update=function(){null!==this._popper&&this._popper.scheduleUpdate()},t.isWithContent=function(){return Boolean(this.getTitle())},t.addAttachmentClass=function(t){g(this.getTipElement()).addClass(Ae+"-"+t)},t.getTipElement=function(){return this.tip=this.tip||g(this.config.template)[0],this.tip},t.setContent=function(){var t=this.getTipElement();this.setElementContent(g(t.querySelectorAll(Ue)),this.getTitle()),g(t).removeClass(xe+" "+Fe)},t.setElementContent=function(t,e){"object"!=typeof e||!e.nodeType&&!e.jquery?this.config.html?(this.config.sanitize&&(e=Se(e,this.config.whiteList,this.config.sanitizeFn)),t.html(e)):t.text(e):this.config.html?g(e).parent().is(t)||t.empty().append(e):t.text(g(e).text())},t.getTitle=function(){var t=this.element.getAttribute("data-original-title");return t||(t="function"==typeof this.config.title?this.config.title.call(this.element):this.config.title),t},t._getOffset=function(){var e=this,t={};return"function"==typeof this.config.offset?t.fn=function(t){return t.offsets=l({},t.offsets,e.config.offset(t.offsets,e.element)||{}),t}:t.offset=this.config.offset,t},t._getContainer=function(){return!1===this.config.container?document.body:_.isElement(this.config.container)?g(this.config.container):g(document).find(this.config.container)},t._getAttachment=function(t){return Pe[t.toUpperCase()]},t._setListeners=function(){var i=this;this.config.trigger.split(" ").forEach(function(t){if("click"===t)g(i.element).on(i.constructor.Event.CLICK,i.config.selector,function(t){return i.toggle(t)});else if(t!==Qe){var e=t===qe?i.constructor.Event.MOUSEENTER:i.constructor.Event.FOCUSIN,n=t===qe?i.constructor.Event.MOUSELEAVE:i.constructor.Event.FOCUSOUT;g(i.element).on(e,i.config.selector,function(t){return i._enter(t)}).on(n,i.config.selector,function(t){return i._leave(t)})}}),g(this.element).closest(".modal").on("hide.bs.modal",function(){i.element&&i.hide()}),this.config.selector?this.config=l({},this.config,{trigger:"manual",selector:""}):this._fixTitle()},t._fixTitle=function(){var t=typeof this.element.getAttribute("data-original-title");(this.element.getAttribute("title")||"string"!==t)&&(this.element.setAttribute("data-original-title",this.element.getAttribute("title")||""),this.element.setAttribute("title",""))},t._enter=function(t,e){var n=this.constructor.DATA_KEY;(e=e||g(t.currentTarget).data(n))||(e=new this.constructor(t.currentTarget,this._getDelegateConfig()),g(t.currentTarget).data(n,e)),t&&(e._activeTrigger["focusin"===t.type?Me:qe]=!0),g(e.getTipElement()).hasClass(Fe)||e._hoverState===je?e._hoverState=je:(clearTimeout(e._timeout),e._hoverState=je,e.config.delay&&e.config.delay.show?e._timeout=setTimeout(function(){e._hoverState===je&&e.show()},e.config.delay.show):e.show())},t._leave=function(t,e){var n=this.constructor.DATA_KEY;(e=e||g(t.currentTarget).data(n))||(e=new this.constructor(t.currentTarget,this._getDelegateConfig()),g(t.currentTarget).data(n,e)),t&&(e._activeTrigger["focusout"===t.type?Me:qe]=!1),e._isWithActiveTrigger()||(clearTimeout(e._timeout),e._hoverState=He,e.config.delay&&e.config.delay.hide?e._timeout=setTimeout(function(){e._hoverState===He&&e.hide()},e.config.delay.hide):e.hide())},t._isWithActiveTrigger=function(){for(var t in this._activeTrigger)if(this._activeTrigger[t])return!0;return!1},t._getConfig=function(t){var e=g(this.element).data();return Object.keys(e).forEach(function(t){-1!==Oe.indexOf(t)&&delete e[t]}),"number"==typeof(t=l({},this.constructor.Default,e,"object"==typeof t&&t?t:{})).delay&&(t.delay={show:t.delay,hide:t.delay}),"number"==typeof t.title&&(t.title=t.title.toString()),"number"==typeof t.content&&(t.content=t.content.toString()),_.typeCheckConfig(be,t,this.constructor.DefaultType),t.sanitize&&(t.template=Se(t.template,t.whiteList,t.sanitizeFn)),t},t._getDelegateConfig=function(){var t={};if(this.config)for(var e in this.config)this.constructor.Default[e]!==this.config[e]&&(t[e]=this.config[e]);return t},t._cleanTipClass=function(){var t=g(this.getTipElement()),e=t.attr("class").match(Ne);null!==e&&e.length&&t.removeClass(e.join(""))},t._handlePopperPlacementChange=function(t){var e=t.instance;this.tip=e.popper,this._cleanTipClass(),this.addAttachmentClass(this._getAttachment(t.placement))},t._fixTransition=function(){var t=this.getTipElement(),e=this.config.animation;null===t.getAttribute("x-placement")&&(g(t).removeClass(xe),this.config.animation=!1,this.hide(),this.show(),this.config.animation=e)},i._jQueryInterface=function(n){return this.each(function(){var t=g(this).data(Ie),e="object"==typeof n&&n;if((t||!/dispose|hide/.test(n))&&(t||(t=new i(this,e),g(this).data(Ie,t)),"string"==typeof n)){if("undefined"==typeof t[n])throw new TypeError('No method named "'+n+'"');t[n]()}})},s(i,null,[{key:"VERSION",get:function(){return"4.3.1"}},{key:"Default",get:function(){return Le}},{key:"NAME",get:function(){return be}},{key:"DATA_KEY",get:function(){return Ie}},{key:"Event",get:function(){return Re}},{key:"EVENT_KEY",get:function(){return De}},{key:"DefaultType",get:function(){return ke}}]),i}();g.fn[be]=Be._jQueryInterface,g.fn[be].Constructor=Be,g.fn[be].noConflict=function(){return g.fn[be]=we,Be._jQueryInterface};var Ve="popover",Ye="bs.popover",ze="."+Ye,Xe=g.fn[Ve],$e="bs-popover",Ge=new RegExp("(^|\\s)"+$e+"\\S+","g"),Je=l({},Be.Default,{placement:"right",trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'}),Ze=l({},Be.DefaultType,{content:"(string|element|function)"}),tn="fade",en="show",nn=".popover-header",on=".popover-body",rn={HIDE:"hide"+ze,HIDDEN:"hidden"+ze,SHOW:"show"+ze,SHOWN:"shown"+ze,INSERTED:"inserted"+ze,CLICK:"click"+ze,FOCUSIN:"focusin"+ze,FOCUSOUT:"focusout"+ze,MOUSEENTER:"mouseenter"+ze,MOUSELEAVE:"mouseleave"+ze},sn=function(t){var e,n;function i(){return t.apply(this,arguments)||this}n=t,(e=i).prototype=Object.create(n.prototype),(e.prototype.constructor=e).__proto__=n;var o=i.prototype;return o.isWithContent=function(){return this.getTitle()||this._getContent()},o.addAttachmentClass=function(t){g(this.getTipElement()).addClass($e+"-"+t)},o.getTipElement=function(){return this.tip=this.tip||g(this.config.template)[0],this.tip},o.setContent=function(){var t=g(this.getTipElement());this.setElementContent(t.find(nn),this.getTitle());var e=this._getContent();"function"==typeof e&&(e=e.call(this.element)),this.setElementContent(t.find(on),e),t.removeClass(tn+" "+en)},o._getContent=function(){return this.element.getAttribute("data-content")||this.config.content},o._cleanTipClass=function(){var t=g(this.getTipElement()),e=t.attr("class").match(Ge);null!==e&&0<e.length&&t.removeClass(e.join(""))},i._jQueryInterface=function(n){return this.each(function(){var t=g(this).data(Ye),e="object"==typeof n?n:null;if((t||!/dispose|hide/.test(n))&&(t||(t=new i(this,e),g(this).data(Ye,t)),"string"==typeof n)){if("undefined"==typeof t[n])throw new TypeError('No method named "'+n+'"');t[n]()}})},s(i,null,[{key:"VERSION",get:function(){return"4.3.1"}},{key:"Default",get:function(){return Je}},{key:"NAME",get:function(){return Ve}},{key:"DATA_KEY",get:function(){return Ye}},{key:"Event",get:function(){return rn}},{key:"EVENT_KEY",get:function(){return ze}},{key:"DefaultType",get:function(){return Ze}}]),i}(Be);g.fn[Ve]=sn._jQueryInterface,g.fn[Ve].Constructor=sn,g.fn[Ve].noConflict=function(){return g.fn[Ve]=Xe,sn._jQueryInterface};var an="scrollspy",ln="bs.scrollspy",cn="."+ln,hn=g.fn[an],un={offset:10,method:"auto",target:""},fn={offset:"number",method:"string",target:"(string|element)"},dn={ACTIVATE:"activate"+cn,SCROLL:"scroll"+cn,LOAD_DATA_API:"load"+cn+".data-api"},gn="dropdown-item",_n="active",mn='[data-spy="scroll"]',pn=".nav, .list-group",vn=".nav-link",yn=".nav-item",En=".list-group-item",Cn=".dropdown",Tn=".dropdown-item",Sn=".dropdown-toggle",bn="offset",In="position",Dn=function(){function n(t,e){var n=this;this._element=t,this._scrollElement="BODY"===t.tagName?window:t,this._config=this._getConfig(e),this._selector=this._config.target+" "+vn+","+this._config.target+" "+En+","+this._config.target+" "+Tn,this._offsets=[],this._targets=[],this._activeTarget=null,this._scrollHeight=0,g(this._scrollElement).on(dn.SCROLL,function(t){return n._process(t)}),this.refresh(),this._process()}var t=n.prototype;return t.refresh=function(){var e=this,t=this._scrollElement===this._scrollElement.window?bn:In,o="auto"===this._config.method?t:this._config.method,r=o===In?this._getScrollTop():0;this._offsets=[],this._targets=[],this._scrollHeight=this._getScrollHeight(),[].slice.call(document.querySelectorAll(this._selector)).map(function(t){var e,n=_.getSelectorFromElement(t);if(n&&(e=document.querySelector(n)),e){var i=e.getBoundingClientRect();if(i.width||i.height)return[g(e)[o]().top+r,n]}return null}).filter(function(t){return t}).sort(function(t,e){return t[0]-e[0]}).forEach(function(t){e._offsets.push(t[0]),e._targets.push(t[1])})},t.dispose=function(){g.removeData(this._element,ln),g(this._scrollElement).off(cn),this._element=null,this._scrollElement=null,this._config=null,this._selector=null,this._offsets=null,this._targets=null,this._activeTarget=null,this._scrollHeight=null},t._getConfig=function(t){if("string"!=typeof(t=l({},un,"object"==typeof t&&t?t:{})).target){var e=g(t.target).attr("id");e||(e=_.getUID(an),g(t.target).attr("id",e)),t.target="#"+e}return _.typeCheckConfig(an,t,fn),t},t._getScrollTop=function(){return this._scrollElement===window?this._scrollElement.pageYOffset:this._scrollElement.scrollTop},t._getScrollHeight=function(){return this._scrollElement.scrollHeight||Math.max(document.body.scrollHeight,document.documentElement.scrollHeight)},t._getOffsetHeight=function(){return this._scrollElement===window?window.innerHeight:this._scrollElement.getBoundingClientRect().height},t._process=function(){var t=this._getScrollTop()+this._config.offset,e=this._getScrollHeight(),n=this._config.offset+e-this._getOffsetHeight();if(this._scrollHeight!==e&&this.refresh(),n<=t){var i=this._targets[this._targets.length-1];this._activeTarget!==i&&this._activate(i)}else{if(this._activeTarget&&t<this._offsets[0]&&0<this._offsets[0])return this._activeTarget=null,void this._clear();for(var o=this._offsets.length;o--;){this._activeTarget!==this._targets[o]&&t>=this._offsets[o]&&("undefined"==typeof this._offsets[o+1]||t<this._offsets[o+1])&&this._activate(this._targets[o])}}},t._activate=function(e){this._activeTarget=e,this._clear();var t=this._selector.split(",").map(function(t){return t+'[data-target="'+e+'"],'+t+'[href="'+e+'"]'}),n=g([].slice.call(document.querySelectorAll(t.join(","))));n.hasClass(gn)?(n.closest(Cn).find(Sn).addClass(_n),n.addClass(_n)):(n.addClass(_n),n.parents(pn).prev(vn+", "+En).addClass(_n),n.parents(pn).prev(yn).children(vn).addClass(_n)),g(this._scrollElement).trigger(dn.ACTIVATE,{relatedTarget:e})},t._clear=function(){[].slice.call(document.querySelectorAll(this._selector)).filter(function(t){return t.classList.contains(_n)}).forEach(function(t){return t.classList.remove(_n)})},n._jQueryInterface=function(e){return this.each(function(){var t=g(this).data(ln);if(t||(t=new n(this,"object"==typeof e&&e),g(this).data(ln,t)),"string"==typeof e){if("undefined"==typeof t[e])throw new TypeError('No method named "'+e+'"');t[e]()}})},s(n,null,[{key:"VERSION",get:function(){return"4.3.1"}},{key:"Default",get:function(){return un}}]),n}();g(window).on(dn.LOAD_DATA_API,function(){for(var t=[].slice.call(document.querySelectorAll(mn)),e=t.length;e--;){var n=g(t[e]);Dn._jQueryInterface.call(n,n.data())}}),g.fn[an]=Dn._jQueryInterface,g.fn[an].Constructor=Dn,g.fn[an].noConflict=function(){return g.fn[an]=hn,Dn._jQueryInterface};var wn="bs.tab",An="."+wn,Nn=g.fn.tab,On={HIDE:"hide"+An,HIDDEN:"hidden"+An,SHOW:"show"+An,SHOWN:"shown"+An,CLICK_DATA_API:"click"+An+".data-api"},kn="dropdown-menu",Pn="active",Ln="disabled",jn="fade",Hn="show",Rn=".dropdown",xn=".nav, .list-group",Fn=".active",Un="> li > .active",Wn='[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',qn=".dropdown-toggle",Mn="> .dropdown-menu .active",Kn=function(){function i(t){this._element=t}var t=i.prototype;return t.show=function(){var n=this;if(!(this._element.parentNode&&this._element.parentNode.nodeType===Node.ELEMENT_NODE&&g(this._element).hasClass(Pn)||g(this._element).hasClass(Ln))){var t,i,e=g(this._element).closest(xn)[0],o=_.getSelectorFromElement(this._element);if(e){var r="UL"===e.nodeName||"OL"===e.nodeName?Un:Fn;i=(i=g.makeArray(g(e).find(r)))[i.length-1]}var s=g.Event(On.HIDE,{relatedTarget:this._element}),a=g.Event(On.SHOW,{relatedTarget:i});if(i&&g(i).trigger(s),g(this._element).trigger(a),!a.isDefaultPrevented()&&!s.isDefaultPrevented()){o&&(t=document.querySelector(o)),this._activate(this._element,e);var l=function(){var t=g.Event(On.HIDDEN,{relatedTarget:n._element}),e=g.Event(On.SHOWN,{relatedTarget:i});g(i).trigger(t),g(n._element).trigger(e)};t?this._activate(t,t.parentNode,l):l()}}},t.dispose=function(){g.removeData(this._element,wn),this._element=null},t._activate=function(t,e,n){var i=this,o=(!e||"UL"!==e.nodeName&&"OL"!==e.nodeName?g(e).children(Fn):g(e).find(Un))[0],r=n&&o&&g(o).hasClass(jn),s=function(){return i._transitionComplete(t,o,n)};if(o&&r){var a=_.getTransitionDurationFromElement(o);g(o).removeClass(Hn).one(_.TRANSITION_END,s).emulateTransitionEnd(a)}else s()},t._transitionComplete=function(t,e,n){if(e){g(e).removeClass(Pn);var i=g(e.parentNode).find(Mn)[0];i&&g(i).removeClass(Pn),"tab"===e.getAttribute("role")&&e.setAttribute("aria-selected",!1)}if(g(t).addClass(Pn),"tab"===t.getAttribute("role")&&t.setAttribute("aria-selected",!0),_.reflow(t),t.classList.contains(jn)&&t.classList.add(Hn),t.parentNode&&g(t.parentNode).hasClass(kn)){var o=g(t).closest(Rn)[0];if(o){var r=[].slice.call(o.querySelectorAll(qn));g(r).addClass(Pn)}t.setAttribute("aria-expanded",!0)}n&&n()},i._jQueryInterface=function(n){return this.each(function(){var t=g(this),e=t.data(wn);if(e||(e=new i(this),t.data(wn,e)),"string"==typeof n){if("undefined"==typeof e[n])throw new TypeError('No method named "'+n+'"');e[n]()}})},s(i,null,[{key:"VERSION",get:function(){return"4.3.1"}}]),i}();g(document).on(On.CLICK_DATA_API,Wn,function(t){t.preventDefault(),Kn._jQueryInterface.call(g(this),"show")}),g.fn.tab=Kn._jQueryInterface,g.fn.tab.Constructor=Kn,g.fn.tab.noConflict=function(){return g.fn.tab=Nn,Kn._jQueryInterface};var Qn="toast",Bn="bs.toast",Vn="."+Bn,Yn=g.fn[Qn],zn={CLICK_DISMISS:"click.dismiss"+Vn,HIDE:"hide"+Vn,HIDDEN:"hidden"+Vn,SHOW:"show"+Vn,SHOWN:"shown"+Vn},Xn="fade",$n="hide",Gn="show",Jn="showing",Zn={animation:"boolean",autohide:"boolean",delay:"number"},ti={animation:!0,autohide:!0,delay:500},ei='[data-dismiss="toast"]',ni=function(){function i(t,e){this._element=t,this._config=this._getConfig(e),this._timeout=null,this._setListeners()}var t=i.prototype;return t.show=function(){var t=this;g(this._element).trigger(zn.SHOW),this._config.animation&&this._element.classList.add(Xn);var e=function(){t._element.classList.remove(Jn),t._element.classList.add(Gn),g(t._element).trigger(zn.SHOWN),t._config.autohide&&t.hide()};if(this._element.classList.remove($n),this._element.classList.add(Jn),this._config.animation){var n=_.getTransitionDurationFromElement(this._element);g(this._element).one(_.TRANSITION_END,e).emulateTransitionEnd(n)}else e()},t.hide=function(t){var e=this;this._element.classList.contains(Gn)&&(g(this._element).trigger(zn.HIDE),t?this._close():this._timeout=setTimeout(function(){e._close()},this._config.delay))},t.dispose=function(){clearTimeout(this._timeout),this._timeout=null,this._element.classList.contains(Gn)&&this._element.classList.remove(Gn),g(this._element).off(zn.CLICK_DISMISS),g.removeData(this._element,Bn),this._element=null,this._config=null},t._getConfig=function(t){return t=l({},ti,g(this._element).data(),"object"==typeof t&&t?t:{}),_.typeCheckConfig(Qn,t,this.constructor.DefaultType),t},t._setListeners=function(){var t=this;g(this._element).on(zn.CLICK_DISMISS,ei,function(){return t.hide(!0)})},t._close=function(){var t=this,e=function(){t._element.classList.add($n),g(t._element).trigger(zn.HIDDEN)};if(this._element.classList.remove(Gn),this._config.animation){var n=_.getTransitionDurationFromElement(this._element);g(this._element).one(_.TRANSITION_END,e).emulateTransitionEnd(n)}else e()},i._jQueryInterface=function(n){return this.each(function(){var t=g(this),e=t.data(Bn);if(e||(e=new i(this,"object"==typeof n&&n),t.data(Bn,e)),"string"==typeof n){if("undefined"==typeof e[n])throw new TypeError('No method named "'+n+'"');e[n](this)}})},s(i,null,[{key:"VERSION",get:function(){return"4.3.1"}},{key:"DefaultType",get:function(){return Zn}},{key:"Default",get:function(){return ti}}]),i}();g.fn[Qn]=ni._jQueryInterface,g.fn[Qn].Constructor=ni,g.fn[Qn].noConflict=function(){return g.fn[Qn]=Yn,ni._jQueryInterface},function(){if("undefined"==typeof g)throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");var t=g.fn.jquery.split(" ")[0].split(".");if(t[0]<2&&t[1]<9||1===t[0]&&9===t[1]&&t[2]<1||4<=t[0])throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0")}(),t.Util=_,t.Alert=p,t.Button=P,t.Carousel=lt,t.Collapse=bt,t.Dropdown=Jt,t.Modal=ve,t.Popover=sn,t.Scrollspy=Dn,t.Tab=Kn,t.Toast=ni,t.Tooltip=Be,Object.defineProperty(t,"__esModule",{value:!0})});
//# sourceMappingURL=bootstrap.min.js.map
;
//# sourceMappingURL=scripts.js.map